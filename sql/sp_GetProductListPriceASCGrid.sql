USE [mina]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListPriceASCGrid]    Script Date: 2/20/2015 6:55:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 20, 2015>
-- Description:	<All Products Price ASC>
-- exec sp_GetProductListPriceASCGrid
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListPriceASCGrid]
   @pcatid int,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 AND a.prodcatID = @prodcatID';


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause + ' ORDER BY a.productPrice ASC ';
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID;
     
	

    
END
