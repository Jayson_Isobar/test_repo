﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for TheScienceOfSwisseCls
/// </summary>
public class TheScienceOfSwisseCls
{
 
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;

    public string thescienceofswisse { get; set; }

    public static TheScienceOfSwisseCls GetTheScienceOfSwisseCls()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        //cmd = new SqlCommand("sp_GetScienceOfSwisseList", con);
        cmd = new SqlCommand("SELECT * FROM tbl_TheScienceOfSwisse WHERE NOT id = 6", con);
        //cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandType = CommandType.Text;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        TheScienceOfSwisseCls rc;
        rc = new TheScienceOfSwisseCls();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                rc.thescienceofswisse += "<li><a href='" + myRow[2].ToString() + "'>" + myRow[1].ToString() + "</a></li>";
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }


}


