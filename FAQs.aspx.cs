﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_FAQs : BaseForm
{

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadFAQs();
    }
    public void LoadFAQs()
    {
        FAQsCls faq = new FAQsCls();
        faq = FAQsCls.GetFAQsCls();
        string faqs = faq.faqs;
        faqs = faqs.Replace("<img src='", "<img src='" + base.CDNdomain);
        litContent.Text = faqs;
    }
}