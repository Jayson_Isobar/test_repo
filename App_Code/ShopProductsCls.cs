﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;


/// <summary>
/// Summary description for ShopProductsCls
/// </summary>
public class ShopProductsCls
{

    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;
    public string userid { get; set; }
    public string productID { get; set; }
    public string bannerName { get; set; }
    public string product { get; set; }
    public string prodcatName { get; set; }
    public string productPop { get; set; }
    public string prodIngredientName { get; set; }
    public string prodConcernName { get; set; }
    public string totalProduct { get; set; }


    public static ShopProductsCls GetProdCategories()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodCatName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodCatName += "<li class='cat_" + myRow[0].ToString() + "'><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Category) + "'>" + myRow[1].ToString() + "</a></li>"; //" (" + myRow[3].ToString() + ") " + "</a></li>";

            }
            rc.prodcatName += prodCatName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdIngredients()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdIngredientListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodIngredientName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodIngredientName += "<li><input name='check' type='checkbox' value='" + myRow[0].ToString() + "'>" + myRow[1].ToString() + "</li>";

            }
            rc.prodIngredientName += prodIngredientName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdIngredientsHome()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdIngredientListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodIngredientName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodIngredientName += "<li><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Ingredient) + "'>" + myRow[1].ToString() + "</a></li>";
            }
            rc.prodIngredientName += prodIngredientName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdIngredientsHover()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdIngredientListHover", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodIngredientName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodIngredientName += "<li><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Ingredient) + "'>" + myRow[1].ToString() + "</a></li>";
            }
            rc.prodIngredientName += prodIngredientName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdConcernsHome()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdConcernListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodConcernName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodConcernName += "<li><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Concern) + "'>" + myRow[1].ToString() + "</a></li>";

            }
            rc.prodConcernName += prodConcernName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdConcernsHover()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdConcernListHover", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodConcernName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodConcernName += "<li><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Concern) + "'>" + myRow[1].ToString() + "</a></li>";

            }
            rc.prodConcernName += prodConcernName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetBanners()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetBannerListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string bannerName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {

                bannerName += "<li>" +
                                    "<a href='" + myRow[3].ToString() + "'>" +
                                        "<span style='background:transparent url(images/" + myRow[2].ToString() + ") no-repeat scroll center 0'>" +
                    //"<img src='images/" + myRow[2].ToString() + "' width='1500' height='500' alt='" + myRow[1].ToString() + "'>" +
                                        "</span>" +
                                    "</a>" +
                              "</li>";
            }

            rc.bannerName += bannerName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductsRecentView()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductListRecentlyViewed", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {

                product += "<div class='recentlyviewed-box'>" +
                                "<a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Product) + "'>" +
                                    "<img src='images/" + myRow[3].ToString() + "' width='128' height='196' alt='" + myRow[1].ToString() + "' class='centerImage'>" +
                                "</a>" +
                                "<div class='product-details-wrap-recent'>" +
                                    "<a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Product) + "'><p class='product-name'>" + myRow[1].ToString() + "</p></a>" +
                                    "<div class='product-rate rating_01' data-sku='" + myRow[8].ToString() + "'>" +
                    //"<span class='rating'></span>" +
                    //"<div class='clearfloat'></div>" +
                                    "</div>" +
                                    "<div class='clearfloat'></div>" +
                                    "<div class='product-price'><p> SGD " + myRow[4].ToString() + " <span class='txt12'>MSRP</span></p></div>" +
                                        "<div class='clearfloat'></div>" +
                                "</div>" +
                            "</div>";

            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetTotalProdByCategory(string id)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductCountPerCategory", con);
        cmd.Parameters.AddWithValue("@prodcatID", id);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string totalProduct = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                totalProduct += myRow[0].ToString();

            }
            rc.totalProduct += totalProduct;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatByCategory(string id)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCategoryName", con);
        cmd.Parameters.AddWithValue("@prodcatID", id);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodcatName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodcatName += myRow[0].ToString();

            }
            rc.prodcatName += prodcatName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }



    public static ShopProductsCls GetProdIngByIngredient(string id)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdIngredientName", con);
        cmd.Parameters.AddWithValue("@prodingID", id);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodingName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodingName += myRow[0].ToString();

            }
            rc.prodIngredientName += prodingName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdConByConcern(string id)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdConcernName", con);
        cmd.Parameters.AddWithValue("@prodconID", id);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodconName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodconName += myRow[0].ToString();

            }
            rc.prodConcernName += prodconName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatFilter(string Ingredients, string Concerns, string Ages, string Genders)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;

        rc = new ShopProductsCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }

                rc.prodcatName += "<h3 class='mlft12'>" + myRow[1].ToString() + "</h3>";

                SqlCommand cmd2 = new SqlCommand("sp_GetProductListActiveSample", con);
                SqlDataAdapter da2 = new SqlDataAdapter();
                DataSet ds2 = new DataSet();
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@pcatid", myRow[0].ToString());
                cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
                cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
                cmd2.Parameters.AddWithValue("@prodAgeID", Ages);
                cmd2.Parameters.AddWithValue("@prodGenderID", Genders);
                SqlDataReader dr2 = cmd2.ExecuteReader();
                DataTable dt2 = new DataTable();
                dt2.Load(dr2);

                da2 = new SqlDataAdapter(cmd2);
                da2.Fill(ds2);

                string prodItems = "";

                string linkAllProd = "";

                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 3 == 0)
                        {
                            i = " last";
                            i2 = "<div class='clearfloat'></div>";
                        }


                        linkAllProd = "<h4 class='txt-rt mtop15'><a href='ProductCategory.aspx?id=" + myRow[0].ToString() + "&cat=" + myRow[1].ToString() + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                        prodItems += "<div class='products-box " + i + "'>" +

                                       "<a href='ProductDetails.aspx?id=" + myRow2[0].ToString() + "&name=" + myRow2[1].ToString() + "'>" +
                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                            "<p class='product-name'>" +
                                                "<a href='ProductDetails.aspx?id=" + myRow2[0].ToString() + "&name=" + myRow2[1].ToString() + "'>" + myRow2[1].ToString() + "</a>" +
                                            "</p>" +
                                            "<div class='product-rate rating_02' data-sku='" + myRow2[7].ToString() + "'>" +
                            //"<span class='rating'></span>" +
                            //"<div class='clearfloat'></div>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                            "<div class='product-price'>" +
                                                "<p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p>" +
                                            "</div>" +
                            //   "<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                            "<div class='clearfloat'></div>" +
                                        "</div>" +
                                    "</div>" + i2;
                    }

                    openDiv = "<div class='products-container border-gry'>";
                    closeDiv = "</div>";

                    product = rc.prodcatName + openDiv +
                              prodItems + linkAllProd + closeDiv + divClear;

                }
                rc.prodcatName = product;

                oldcounter = counter;
            }
            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatFilter2(string Cat, string Ingredients, string Concerns, string Ages, string Genders)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdPerCategory", con);
        cmd.Parameters.AddWithValue("@pcatid", Cat);
        cmd.Parameters.AddWithValue("@prodIngID", Ingredients);
        cmd.Parameters.AddWithValue("@prodConcernID", Concerns);
        cmd.Parameters.AddWithValue("@prodAgeID", Ages);
        cmd.Parameters.AddWithValue("@prodGenderID", Genders);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodcatName = "";
        string productPop = "";

        string pSize2 = "";
        foreach (DataRow myRow in tbRow.Rows)
        {
            if (myRow[11].ToString() != "")
            {
                pSize2 = " <option>" + myRow[11].ToString() + " " + myRow[9].ToString() + "</option>";
            }

            prodcatName += "<div class='product-list-view'>" +
                                "<div class='product-list-view-img fltlft'>" +
                                    "<a href='#' data-featherlight='#f" + myRow[0].ToString() + "'><img src='images/" + myRow[3].ToString() + "' alt='" + myRow[1].ToString() + "' class='centerImage'></a>" +
                                "</div>" +
                                "<div class='product-list-view-desc fltlft'>" +
                                    "<h3>" + myRow[1].ToString() + "</h3>" +
                                    "<p>" + myRow[10].ToString() + "</p>" +
                                    "<div class='product-rate rating_03 mtop20' data-sku='" + myRow[7].ToString() + "'>" +
                //"<span class='rating'></span>" +
                //"<div class='clearfloat'></div>" +
                                    "</div>" +
                                    "<div class='clearfloat'></div>" +
                                    "<div class='product-price'><p> SGD " + myRow[4].ToString() + " <span class='txt12'> MSRP</span></p></div>" +
                //"<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                    "<div class='clearfloat'></div>" +
                                "</div>" +
                                "<div class='clearfloat'></div>" +
                            "</div>";

            productPop += "<div class='product-pop' id='f" + myRow[0].ToString() + "'><div class='product-pop-imgbox'>" +
         " <img src='images/" + myRow[3].ToString() + "' alt='" + myRow[1].ToString() + "' class='centerImage'> </div>" +
          "<div class='product-pop-desc'>" +
          "<h3>" + myRow[1].ToString() + "</h3>" +
          "<p>" + myRow[10].ToString() + "</p>" +
      "<div class='product-rate rating_04' data-sku='" + myRow[7].ToString() + "'>" +
                 "<span class='rating'></span>" +
                //	"<p class='fltlft'>4.4 / 5 (75) <span class='recommended'>98% recommended</span></p>" +
        "<div class='clearfloat'></div>" +
        "</div>" +
        "<div class='product-price mtop10'><h3>SGD " + myRow[4].ToString() + " <span class='txt12'> MSRP</span></h3></div> " +
        "<div class='clearfloat'></div> " +
        "<form action='product size' method='get'>" +
      " <label class='txt-up'>size</label>" +
      " <select name='tablets'>" +
        " <option>" + myRow[8].ToString() + " " + myRow[9].ToString() + "</option>" +
        pSize2 +
      " </select>" +
                // " <label class='txt-up mlft12'>Quantity</label>" +
                //"  <select name='tablets'>" +
                //	" <option>1</option>" +
                //  " </select>" +
      " <div class='clearfloat'></div>" +
                // " <div class='btn color-rd mtop20'><h4><a href='#'>add to cart</a></h4></div>" +
     " </form>   " +
     " </div>" +
     " <div class='clearfloat'></div>" +
      "<div class='col_64 fltlft mtop15'><p>" + myRow[2].ToString() + "</p>" +
      "<p class='mtop10'>" +
   "<a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Product ) + "' class='txt-bl'>More Details &raquo;</a>" +
      "</p>" +
     " </div>" +
    "</div></div>";

        }
        rc.prodcatName += prodcatName;
        rc.productPop += productPop;

        con.Close();
        return rc;

    }

    public static ShopProductsCls GetTotalProd(string Ingredients, string Concerns, string Ages, string Genders)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductCount", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string totalProduct = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                totalProduct += myRow[0].ToString();

            }
            rc.totalProduct += totalProduct;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatSortPopular(string Ingredients, string Concerns, string Ages, string Genders)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }

                rc.prodcatName += "<h3 class='mlft12 header_" + myRow[0].ToString() + "'>" + myRow[1].ToString() + "</h3>";
                //SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopular", con);
                SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopular2", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@pcatid", myRow[0].ToString());
                cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
                cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
                cmd2.Parameters.AddWithValue("@prodAgeID", Ages);
                cmd2.Parameters.AddWithValue("@prodGenderID", Genders);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                DataTable dt2 = new DataTable();
                dt2.Load(dr2);
                string prodItems = "";

                string linkAllProd = "";
                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 3 == 0)
                        {
                            i = "last";
                            i2 = "<div class='clearfloat'></div>";
                        }


                        linkAllProd = "<h4 class='txt-rt mtop15'><a class='header_" + myRow[0].ToString() + "' href='"+  UrlHelper.FormatPageUrl (myRow[0].ToString(), PageType.Type.Category) + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                        prodItems += "<div class='products-box " + i + "'>" +
                                     "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product)  + "'>" +

                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                            "<div class='product-name'>" +
                                               //"<p><a href='ProductDetails.aspx?id=" + myRow2[0].ToString() + "&name=" + myRow2[1].ToString() + "'>" + myRow2[1].ToString() + "</a></p>" +
                                               "<p><a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" + myRow2[1].ToString() + "</a></p>" +
                                            "</div>" +
                                            "<div class='product-rate rating_05' data-sku='" + myRow2[7].ToString() + "'>" +
                            //"<span class='rating'></span>" +
                            // "<div class='clearfloat'></div>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                            "<div class='product-price'>" +
                                                "<p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p>" +
                                            "</div>" +
                            //  "<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                            "<div class='clearfloat'></div>" +
                                            "</div>" +
                                        "</div>" + i2;
                    }

                    openDiv = "<div class='products-container line_" + myRow[0].ToString() + "'>";
                    closeDiv = "</div>";

                    product = rc.prodcatName + openDiv +
                              prodItems + linkAllProd + closeDiv + divClear;

                }
                rc.prodcatName = product;
                rc.totalProduct = dt2.Rows.Count.ToString();

            }
            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatSortAlphabetically(string Ingredients, string Concerns)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }

                rc.prodcatName += "<h3 class='mlft12'>" + myRow[1].ToString() + "</h3>";

                SqlCommand cmd2 = new SqlCommand("sp_GetProductListAlphabetically", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@pcatid", myRow[0].ToString());
                cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
                cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                DataTable dt2 = new DataTable();
                dt2.Load(dr2);
                string prodItems = "";

                string linkAllProd = "";
                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 3 == 0)
                        {
                            i = "last";
                            i2 = "<div class='clearfloat'></div>";
                        }


                        linkAllProd = "<h4 class='txt-rt mtop15'><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Category) + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";



                        prodItems += "<div class='products-box " + i + "'>" +
                                        "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product)  + "'>" +
                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                              "<p class='product-name'><a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" + myRow2[1].ToString() + "</a>" + "</p>" +
                                            "<div class='product-rate rating_06' data-sku='" + myRow2[7].ToString() + "'>" +
                            //"<span class='rating'></span>" +
                            //"<div class='clearfloat'></div>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                            "<div class='product-price'><p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                            //  "<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                            "<div class='clearfloat'></div>" +
                                        "</div>" +
                                    "</div>" + i2;
                    }
                    openDiv = "<div class='products-container border-gry'>";
                    closeDiv = "</div>";

                    product = rc.prodcatName + openDiv +
                              prodItems + linkAllProd + closeDiv + divClear;

                }
                rc.prodcatName = product;

            }
            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatSortPriceASC(string Ingredients, string Concerns)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }


                rc.prodcatName += "<h3 class='mlft12'>" + myRow[1].ToString() + "</h3>";

                SqlCommand cmd2 = new SqlCommand("sp_GetProductListPriceASC", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@pcatid", myRow[0].ToString());
                cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
                cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                DataTable dt2 = new DataTable();
                dt2.Load(dr2);
                string prodItems = "";

                string linkAllProd = "";
                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 3 == 0)
                        {
                            i = "last";
                            i2 = "<div class='clearfloat'></div>";
                        }


                        linkAllProd = "<h4 class='txt-rt mtop15'><a href='ProductCategory.aspx?id=" + myRow[0].ToString() + "&cat=" + myRow[1].ToString() + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                        prodItems += "<div class='products-box " + i + "'>" +
                                         "<a href='ProductDetails.aspx?id=" + myRow2[0].ToString() + "&name=" + myRow2[1].ToString() + "'>" +
                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                          "<p class='product-name'><a href='ProductDetails.aspx?id=" + myRow2[0].ToString() + "&name=" + myRow2[1].ToString() + "'>" + myRow2[1].ToString() + "</a></p>" +
                                            "<div class='product-rate rating_07' data-sku='" + myRow2[7].ToString() + "'>" +
                            //"<span class='rating'></span>" +
                            //"<div class='clearfloat'></div>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                            "<div class='product-price'><p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                            // "<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                            "<div class='clearfloat'></div>" +
                                        "</div>" +
                                    "</div>" + i2;
                    }

                    openDiv = "<div class='products-container border-gry'>";
                    closeDiv = "</div>";

                    product = rc.prodcatName + openDiv +
                              prodItems + linkAllProd + closeDiv + divClear;

                }
                rc.prodcatName = product;

            }
            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductBenefit(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productID", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {

                product += myRow[5].ToString();

            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductSizes(string productID)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productID", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {

                product += myRow[10].ToString() + " " + myRow[14].ToString() + " " + myRow[11].ToString() + " " + myRow[14].ToString();

            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductSizesSKU(string productID)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productID", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                string prod2 = "";
                if (myRow[9].ToString() != "")
                {
                    // prod2 = "<option data-price='" + myRow[9].ToString() + "' data-sku='" + myRow[13].ToString() + "'>" + myRow[11].ToString() + " " + myRow[14].ToString() + "</option>";
                    prod2 = "<option value='2' data-price='" + myRow[9].ToString() + "' data-sku='" + myRow[13].ToString() + "'>" + myRow[11].ToString() + " " + myRow[14].ToString() + "</option>";
                }
                //product += " <select class='prodSizesSKU'>" +
                //             "<option data-price='" + myRow[8].ToString() + "' data-sku='" + myRow[12].ToString() + "'>" + myRow[10].ToString() + " " + myRow[14].ToString() + "</option>" +
                //             prod2 +
                //           "</select>";
                product += "<select id='selOption' class='InputField'>" +
                                "<option value='1' data-price='" + myRow[8].ToString() + "' data-sku='" + myRow[12].ToString() + "'>" + myRow[10].ToString() + " " + myRow[14].ToString() + "</option>" +
                prod2 + "</select>";


            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductIngredientDirection(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productID", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {

                product += "<div id='ingredients'>" +
                            myRow[6].ToString() +
                        "</div>" +

                        "<div id='directions'>" +
                            myRow[7].ToString() +
                        "</div>";
            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductRetail(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductRetailersByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                product += "<tr>" +
                        "<td>" +
                        "<img src='" + myRow[1].ToString() + "' width='107' height='61' alt='" + myRow[0].ToString() + "'></td>" +
                        "<td>" +
                            myRow[2].ToString() +
                        "</td>" +
                        "<td>" +
                            "<span class='btn color-rd fltlft'><h4><a href='" + myRow[3].ToString() + "'>Preview</a></h4></span>" +
                        "</td>" +
                    "</tr>";
            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductDescPrice(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        string prodPrice = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                product += "<p class='mtop15'>" + myRow[2].ToString() + "</p>";
                prodPrice = myRow[8].ToString();
            }
            rc.product += product;
            rc.productID = prodPrice;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductSKU(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {

                product += "<div class='product-rate rating_00' id='productID' data-sku='" + myRow[12].ToString() + "'></div>";
            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductDetail(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                product += "<h3>" + myRow[0].ToString() + "</h3>" +
                            "<h1>" + myRow[1].ToString() + "</h1>";
            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductDetailImage(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                product += "<img src='images/" + myRow[3].ToString() + "' alt='" + myRow[1].ToString() + "' height='410' class='centerImage'>";
            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProductSNS(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductDetailsByID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                product +=
                       "<a href=\"javascript:share('https://www.facebook.com/sharer/sharer.php?u=http://" + myRow[15].ToString() + "')\"> " +
                           "<img src=\"images/facebook-ico.jpg\" alt=\"Facebook\" /> " +
                       "</a> " +
                       "<a href=\"javascript:share('http://twitter.com/home?status=" + myRow[1].ToString() + " http://tinyurl.com/nt9nxnb')\"> " +
                           "<img src=\"images/twitter-ico.jpg\" alt=\"Twitter\" /> " +
                       "</a> " +
                       "<a href=\"javascript:share('https://plus.google.com/" + myRow[12].ToString() + "/posts')\"> " +
                           "<img src=\"images/gplus-ico.jpg\" alt=\"Google +\" /> " +
                       "</a> ";

            }
            rc.product += product;

            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }


    public bool InsertToRecentView()
    {
        bool result = false;

        _helper = new SqlHelper();
        try
        {

            if (_helper.CreateConnection())
            {
                _helper.BeginTransaction();
                _helper.CreateCommand("sp_InsertRecentlyViewed", true);
                _helper.AddParameter("@userid", this.userid, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@productid", this.productID, DbType.String, ParameterDirection.Input);

                result = _helper.ExecuteNonQuery();

                _helper.ClearCommandParameters();

                if (result)
                {
                    _helper.CommitTransaction();

                }


            }

        }
        catch (Exception ex)
        {

            _helper.RollbackTransaction();
            ErrorHandler.Handle(ex);


        }
        finally
        {
            _helper.CloseConnection();

        }


        return result;

    }

    public static ShopProductsCls GetProductRelatedProduct(string productID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetRelatedProductsList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productid", productID);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string product = "";
        if (tbRow.Rows.Count > 0)
        {
            int ctr = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                string i = "";
                string i2 = "";

                ctr = ctr + 1;

                if (ctr % 2 == 0)
                {
                    i = "last";
                    i2 = "<div class='clearfloat'></div>";
                }

                product += "<div class='alsolike-wrap-box " + i + "'>" +
                               "<a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Product) + "'>" +
                                    "<img src='images/" + myRow[2].ToString() + "' width='140' height='210' alt='" + myRow[1].ToString() + "' class='centerImage'>" +
                                "</a>" +
                                "<div class='alsolike-wrap-box-details'>" +
                                    "<div class='product-name also-like'>" +
                                       "<a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Product) + "'>" +
                                            "<p>" + myRow[1].ToString() + "</p>" +
                                        "</a>" +
                                    "</div>" +
                                    "<div class='product-rate rating_08' data-sku='" + myRow[4].ToString() + "'>" +
                    //"<span class='rating'></span>" +
                    //"<div class='clearfloat'></div>" +
                                    "</div>" +
                                    "<div class='clearfloat'></div>" +
                                    "<div class='product-price'><p>SGD " + myRow[3].ToString() + " <span class='txt12'> MSRP</span></p></div>" +
                                    "<div class='clearfloat'></div>" +
                                "</div>" +
                            "</div>" + i2;
            }
            rc.product += product;

            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetShopProductHover()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetShopProductMenuHover_Cat", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        string prodCatName = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                prodCatName += "<div class='nav-column'>" +
                          "<h3>" + myRow[1].ToString() + "</h3>" +
                          "<a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Category)  + "'><img src='images/" + myRow[2].ToString() + "' width='192' height='112' alt='" + myRow[1].ToString() + "'></a>" +
                        "</div>";

            }
            rc.prodcatName += prodCatName;


            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatSortPopularGridByCat(string Cat, string Ingredients, string Concerns, string Ages, string Genders)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatDetailsByID", con);
        cmd.Parameters.AddWithValue("@prodcatID", Cat);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        //if (tbRow.Rows.Count > 0)
        //{
        int counter = 0;
        int oldcounter = 0;
        foreach (DataRow myRow in tbRow.Rows)
        {
            counter = counter + 1;

            string divClear = "";
            if (oldcounter != counter)
            {
                divClear = "<div class='clearfloat'></div>";
            }



            //SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopularGrid", con);
            SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopularGrid2", con);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@pcatid", Cat);
            cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
            cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
            cmd2.Parameters.AddWithValue("@prodAgeID", Ages);
            cmd2.Parameters.AddWithValue("@prodGenderID", Genders);
            SqlDataReader dr2 = cmd2.ExecuteReader();

            DataTable dt2 = new DataTable();
            dt2.Load(dr2);
            string prodItems = "";

            string linkAllProd = "";
            if (dt2.Rows.Count > 0)
            {
                int ctr = 0;
                string openDiv = "";
                string closeDiv = "";

                foreach (DataRow myRow2 in dt2.Rows)
                {
                    string i = "";
                    string i2 = "";

                    ctr = ctr + 1;

                    if (ctr % 3 == 0)
                    {
                        i = "last";
                        i2 = "<div class='clearfloat'></div>";
                    }

                    var categoryId = myRow[1].ToString();
                    var productId = myRow2[0].ToString();


                    linkAllProd = "<h4 class='txt-rt mtop15'><a href='" + UrlHelper.FormatPageUrl(categoryId, PageType.Type.Category) + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                    prodItems += "<div class='products-box " + i + "'>" +
                                "<a href='" + UrlHelper.FormatPageUrl(productId, PageType.Type.Product) + "'>" +
                                        "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                    "</a>" +
                                    "<div class='product-details-wrap'>" +
                                        "<div class='product-name'>" +
                                            "<p>" +
                               "<p><a href='" + UrlHelper.FormatPageUrl(productId, PageType.Type.Product) + "'>" + myRow2[1].ToString() + "</a>" + "</p>" +
                                        "</div>" +
                                        "<div class='product-rate rating_09' data-sku='" + myRow2[7].ToString() + "'>" +
                        //"<span class='rating'></span>" +
                        // "<div class='clearfloat'></div>" +
                                        "</div>" +
                                        "<div class='clearfloat'></div>" +
                                        "<div class='product-price'><p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                        //  "<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                        "<div class='clearfloat'></div>" +
                                    "</div>" +
                                "</div>" + i2;
                }

                openDiv = "<div class='products-container line_" + Cat + "'>";
                closeDiv = "</div>";

                product = "<h3 class='mlft12 header_" + Cat + "'>" + myRow[0].ToString() + "</h3>" + openDiv + prodItems + closeDiv;

            }
            rc.prodcatName = product;
            rc.totalProduct = dt2.Rows.Count.ToString();

        }
        con.Close();
        return rc;
        // }
        //else
        //{
        //    return null;
        //}
    }

    public static ShopProductsCls GetProdCatSortAlphabeticallyGrid(string Cat, string Ingredients, string Concerns)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatDetailsByID", con);
        cmd.Parameters.AddWithValue("@prodcatID", Cat);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        //if (tbRow.Rows.Count > 0)
        // {
        int counter = 0;
        int oldcounter = 0;
        foreach (DataRow myRow in tbRow.Rows)
        {
            counter = counter + 1;

            string divClear = "";
            if (oldcounter != counter)
            {
                divClear = "<div class='clearfloat'></div>";
            }

            rc.prodcatName += "<h3 class='mlft12'>" + myRow[0].ToString() + "</h3>";

            SqlCommand cmd2 = new SqlCommand("sp_GetProductListAlphabeticallyGrid", con);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@pcatid", Cat);
            cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
            cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
            SqlDataReader dr2 = cmd2.ExecuteReader();

            DataTable dt2 = new DataTable();
            dt2.Load(dr2);
            string prodItems = "";

            string linkAllProd = "";
            if (dt2.Rows.Count > 0)
            {
                int ctr = 0;
                string openDiv = "";
                string closeDiv = "";

                foreach (DataRow myRow2 in dt2.Rows)
                {
                    string i = "";
                    string i2 = "";

                    ctr = ctr + 1;

                    if (ctr % 3 == 0)
                    {
                        i = "last";
                        i2 = "<div class='clearfloat'></div>";
                    }


                    linkAllProd = "<h4 class='txt-rt mtop15'><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Category)  + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                    prodItems += "<div class='products-box " + i + "'>" +
                                       "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product)  + "'>" +
                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                            "<p class='product-name'>" +
                               "<p class='product-name'><a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "&name=" + myRow2[1].ToString() + "'>" + myRow2[1].ToString() + "</a></p>" +
                                            "<div class='product-rate rating_10' data-sku='" + myRow2[7].ToString() + "'>" +
                        //"<span class='rating'></span>" +
                        //"<div class='clearfloat'></div>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                            "<div class='product-price'><p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                        //  "<div class='product-compare'><input name='compare' type='checkbox' value='' class='fltlft'><p class='fltlft'>Compare</p><div class='clearfloat'></div></div>" +
                                            "<div class='clearfloat'></div>" +
                                        "</div>" +
                                    "</div>" + i2;
                }

                openDiv = "<div class='products-container border-gry'>";
                closeDiv = "</div>";

                product = "<h3 class='mlft12'>" + myRow[0].ToString() + "</h3>" +
                          prodItems;

            }
            rc.prodcatName = product;
            rc.totalProduct = dt2.Rows.Count.ToString();

        }
        con.Close();
        return rc;
        //}
        //else
        //{
        //    return null;
        //}
    }

    public static ShopProductsCls GetProdCatSortPriceASCGrid(string Cat, string Ingredients, string Concerns)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatDetailsByID", con);
        cmd.Parameters.AddWithValue("@prodcatID", Cat);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }


                rc.prodcatName += "<h3 class='mlft12'>" + myRow[1].ToString() + "</h3>";

                SqlCommand cmd2 = new SqlCommand("sp_GetProductListPriceASCGrid", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@pcatid", Cat);
                cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
                cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                DataTable dt2 = new DataTable();
                dt2.Load(dr2);
                string prodItems = "";

                string linkAllProd = "";
                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 3 == 0)
                        {
                            i = "last";
                            i2 = "<div class='clearfloat'></div>";
                        }


                        linkAllProd = "<h4 class='txt-rt mtop15'><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Category) + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                        prodItems += "<div class='products-box " + i + "'>" +
                                        "<a href='" + UrlHelper.FormatPageUrl( myRow2[0].ToString(), PageType.Type.Product)  + "'>" +
                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                            "<p class='product-name'>" +
                                               "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "&name=" + myRow2[1].ToString() + "'>" + myRow2[1].ToString() + "</a>" +
                                            "</p>" +
                                            "<div class='product-rate rating_11' data-sku='" + myRow2[7].ToString() + "'>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                            "<div class='product-price'>" +
                                                "<p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p>" +
                                            "</div>" +
                                            "<div class='clearfloat'></div>" +
                                        "</div>" +
                                    "</div>" + i2;
                    }

                    openDiv = "<div class='products-container border-gry'>";
                    closeDiv = "</div>";

                    product = "<h3 class='mlft12'>" + myRow[0].ToString() + "</h3>" + prodItems;

                }
                rc.prodcatName = product;
                rc.totalProduct = dt2.Rows.Count.ToString();
            }
            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }

    public static ShopProductsCls GetProdCatSortPopularGridByCon(string Cat, string Ingredients, string Concerns)
    {
        int total = 0;
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatListActive", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;

        rc = new ShopProductsCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }

                // rc.prodcatName += "<h3 class='mlft12'>" + myRow[1].ToString() + "</h3>";


                SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopularGrid", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@pcatid", myRow[0].ToString());
                cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
                cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                DataTable dt2 = new DataTable();
                dt2.Load(dr2);
                string prodItems = "";

                string linkAllProd = "";
                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 3 == 0)
                        {
                            i = "last";
                            i2 = "<div class='clearfloat'></div>";
                        }


                        linkAllProd = "<h4 class='txt-rt mtop15'><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.Category) + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                        prodItems += "<div class='products-box " + i + "'>" +
                                         "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" +
                                            "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                        "</a>" +
                                        "<div class='product-details-wrap'>" +
                                            "<div class='product-name'>" +
                                                "<p>" +
                                       "<p><a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" + myRow2[1].ToString() + "</a></p></div>" +
                                                "<div class='product-rate rating_12' data-sku='" + myRow2[7].ToString() + "'>" +
                                                "</div>" +
                                                "<div class='clearfloat'></div>" +
                            //"<div class='product-price'>" +
                            //    "<p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p>" +
                            //"</div>" +
                                            "<div class='clearfloat'></div>" +
                                        "</div>" +
                                    "</div>" + i2;


                    }

                    openDiv = "<div class='products-container line_" + myRow[0].ToString() + "'>";
                    closeDiv = "</div>";

                    product += "<h3 class='mlft12 byConcern header_" + myRow[0].ToString() + "'>" + myRow[1].ToString() + "</h3>" + openDiv + prodItems + closeDiv + divClear;
                    total += dt2.Rows.Count;

                }
                rc.prodcatName = product;
                rc.totalProduct = total.ToString();

            }
            con.Close();
            return rc;
        }
        else
        {
            return null;
        }
    }




    #region Handler
    public static List<ProductModel> GetProductListWithCategory()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProductsWithCategory", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        var productList = new List<ProductModel>();
        SlugHelper slugHelper = new SlugHelper();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                var categoryName = slugHelper.GenerateSlug(myRow[2].ToString());
                var productName = slugHelper.GenerateSlug(myRow[3].ToString());
                var productModel = new ProductModel
                {
                    ProductId = myRow[0].ToString(),
                    CategoryId = myRow[1].ToString(),
                    CategoryName = categoryName
                    ,
                    ProductName = productName
                };
                productList.Add(productModel);
            }
        }
        con.Close();
        return productList;
    }
    public static List<CategoryModel> GetCategoryList()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        var categoryList = new List<CategoryModel>();
        SlugHelper slugHelper = new SlugHelper();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                var categoryName = slugHelper.GenerateSlug(myRow[1].ToString());

                var categoryModel = new CategoryModel
                {
                    CategoryId = myRow[0].ToString(),
                    CategoryName = categoryName
                };
                categoryList.Add(categoryModel);
            }
        }
        con.Close();
        return categoryList;
    }
    #endregion


    public static ShopProductsCls GetSearchResult(string Cat, string Ingredients, string Concerns, string Ages, string Genders)
    {
        string product = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetProdCatDetailsByID", con);
        cmd.Parameters.AddWithValue("@prodcatID", Cat);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        ShopProductsCls rc;
        rc = new ShopProductsCls();
        //if (tbRow.Rows.Count > 0)
        //{
        int counter = 0;
        int oldcounter = 0;
        foreach (DataRow myRow in tbRow.Rows)
        {
            counter = counter + 1;

            string divClear = "";
            if (oldcounter != counter)
            {
                divClear = "<div class='clearfloat'></div>";
            }



            //SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopularGrid", con);
            SqlCommand cmd2 = new SqlCommand("sp_GetProductListPopularGrid2", con);
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@pcatid", Cat);
            cmd2.Parameters.AddWithValue("@prodIngID", Ingredients);
            cmd2.Parameters.AddWithValue("@prodConcernID", Concerns);
            cmd2.Parameters.AddWithValue("@prodAgeID", Ages);
            cmd2.Parameters.AddWithValue("@prodGenderID", Genders);
            SqlDataReader dr2 = cmd2.ExecuteReader();

            DataTable dt2 = new DataTable();
            dt2.Load(dr2);
            string prodItems = "";

            string linkAllProd = "";
            if (dt2.Rows.Count > 0)
            {
                int ctr = 0;
                string openDiv = "";
                string closeDiv = "";

                foreach (DataRow myRow2 in dt2.Rows)
                {
                    string i = "";
                    string i2 = "";

                    ctr = ctr + 1;

                    if (ctr % 3 == 0)
                    {
                        i = "last";
                        i2 = "<div class='clearfloat'></div>";
                    }

                    var categoryId = myRow[1].ToString();
                    var productId = myRow2[0].ToString();


                    linkAllProd = "<h4 class='txt-rt mtop15'><a href='" + UrlHelper.FormatPageUrl(categoryId, PageType.Type.Category) + "'>SEE ALL " + myRow[1].ToString() + " ...</a></h4>";

                    prodItems += "<div class='products-box " + i + "'>" +
                                "<a href='" + UrlHelper.FormatPageUrl(productId, PageType.Type.Product) + "'>" +
                                        "<img src='images/" + myRow2[3].ToString() + "' width='140' height='210' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                    "</a>" +
                                    "<div class='product-details-wrap'>" +
                                        "<p class='product-name'><a href='" + UrlHelper.FormatPageUrl(productId, PageType.Type.Product) + "'>" + myRow2[1].ToString() + "</a>" + "</p>" +
                                        "<p class='productDetail display-none'>" + myRow2[2].ToString() + "</p>" +
                                        "<div class='product-rate rating_13' data-sku='" + myRow2[7].ToString() + "'>" +
                                        "</div>" +
                                        "<div class='clearfloat'></div>" +
                                        "<div class='product-price'><p>SGD " + myRow2[4].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                                        "<div class='clearfloat'></div>" +
                                    "</div>" +
                                "</div>" + i2;
                }

                openDiv = "<div class='products-container line_" + Cat + "'>";
                closeDiv = "</div>";

                product = openDiv + prodItems + closeDiv;

            }
            rc.prodcatName = product;
            rc.totalProduct = dt2.Rows.Count.ToString();

        }
        con.Close();
        return rc;
    }

  
}


