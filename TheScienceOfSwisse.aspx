﻿<%@ Page Title="The Science Of Swisse" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TheScienceOfSwisse.aspx.cs" Inherits="HeaderNavigationFlyout_TheScienceOfSwisse" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>The Science of Swisse | Swisse Vitamins</title>
<meta property="og:title" content="The Science of Swisse Vitamins" />
<meta name="keywords" content="vitamins, multivitamins, scientific research, integrative health, clinical trials, " />
<meta name="description" content="At Swisse we pride ourselves on the development of scientifically validated vitamin supplements that assist in maintaining optimal health and wellbeing." />
<link rel="canonical" href="http://www.swisse.com.sg/the-science-of-swisse" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                The Science of Swisse
            </p>
        </div>
    </div>

<section class="container">
<h2>the science of swisse</h2>
<div class="col_14 fltlft color-dgray mtop20">
<div class="sidebar-menu">
<h4>The science of Swisse</h4>
<ul>
 <asp:Literal ID="litContent" runat="server"></asp:Literal>
</ul>
</div>
</div>
<div class="col_72 fltrt mtop20">
    <a href="/the-science-of-swisse/our-research">
        <img src="<%=this.CDNdomain %>images/science-swisse-video.jpg" alt="Supported by science" />
    </a>

    <a href="/the-science-of-swisse/our-research">
        <img src="<%=this.CDNdomain %>images/our-research.jpg" alt="Our Research" />
    </a>

    <a href="/the-science-of-swisse/clinical-trials">
        <img src="<%=this.CDNdomain %>images/clinical-trials.jpg" alt="Clinical Trials" class="fltrt" />
    </a>
    <div class="clearfloat"></div>

    <a href="/the-science-of-swisse/ingredients-glossary">
        <img src="<%=this.CDNdomain %>images/our-ingredients.jpg" alt="Our Ingredients" class="fltlft" />
    </a>

    <a href="/the-science-of-swisse/swisse-scientific-advisory-panel>
        <img src="<%=this.CDNdomain %>images/scientific-advisory-panel.jpg" alt="Scientific Advisory Panel" class="fltrt" />
    </a>
  </div>
<div class="clearfloat"></div>
</section>

</asp:Content>
