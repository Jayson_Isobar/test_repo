﻿
<%@ Page Title="Our Scientific Partnership" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OurScientificPartnership.aspx.cs" Inherits="HeaderNavigationFlyout_OurScientificPartnership" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>Our Scientific Partnerships | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Vitamins - Our Scientific Partnerships" />
<meta name="keywords" content="vitamins, multivitamins, scientific research, integrative health, clinical trials, vitamin research, health research, research and development, vitamin science, university partnerships" />
<meta name="description" content="Swisse Vitamins is at the forefront of investing in science and research for the complementary medicines industry. Read about our scientific partnerships here." />
<link rel="canonical" href="http://www.swisse.com.sg/the-science-of-swisse/our-scientific-partnerships" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/the-science-of-swisse">The Science of Swisse</a>
                <span class="mlr">></span>
                Our Scientific Partnerships
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>
<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litRelatedProd" runat="server"></asp:Literal>
          <div class="clearfloat"></div>
          </div>    
    </div>
	
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>
<li><a href="/OurResearch.aspx">Our Research</a></li>
<li><a href="/ClinicalTrials.aspx">Clinical Trials</a></li>
<li><a href="/IngredientsGlossary.aspx">Ingredients Glossary</a></li>
<li><a href="/SwisseScientificAdvisoryPanel.aspx">Swisse Scientific Advisory Panel</a></li>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>


</asp:Content>
