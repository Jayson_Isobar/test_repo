﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BINctrl : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BIN obj_bin = new BIN();
        string SKU = Request.Form["sku"];
        obj_bin.ParseInfo(SKU);
        if (obj_bin.rowCtr > 0) { noStockMsg.Visible = false; }
        retailer_table.DataSource = obj_bin.tbl_BIN;
        retailer_table.DataBind();
    }
}