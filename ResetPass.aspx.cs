﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.IO;

using System.Net.Mail;
using System.Net;

public partial class ResetPass : BaseForm
{
    String gmailSMTP = ConfigurationManager.AppSettings["gmailSMTP"];
    String gmailSMTPpass = ConfigurationManager.AppSettings["gmailSMTPpass"];
    public string isSent { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    public void SendEmail()
    {
        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        client.Timeout = 10000;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(gmailSMTP, gmailSMTPpass);

        MailMessage mm = new MailMessage(gmailSMTP, txtEmail.Text, "New Password", "Here's the new password that has been generated: Swisse2015!");
        mm.BodyEncoding = UTF8Encoding.UTF8;
        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

        client.Send(mm);
    }
    
    public void SendEmail_localhost()
    {
        SmtpClient client = new SmtpClient("localhost", 25);

        MailAddress from = new MailAddress("resetpassword@swisse.com.sg", "resetpassword@swisse.com.sg", System.Text.Encoding.UTF8);
        MailAddress to = new MailAddress(txtEmail.Text);
        MailMessage message = new MailMessage(from, to);
        message.Body = "Here's the new password that has been generated: Swisse2015!";

        message.BodyEncoding = System.Text.Encoding.UTF8;
        message.Subject = "New Password";
        message.SubjectEncoding = System.Text.Encoding.UTF8;
        message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

        client.Send(message);

        message.Dispose();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();

            string spName2 = "";
            string Password = Encrypt("Swisse2015!");
            spName2 = "sp_UpdateUserPassword";
            SqlCommand cmd2 = new SqlCommand(spName2, con);
            con.Open();
            cmd2.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
            cmd2.Parameters.AddWithValue("@newPassword", Password);
            cmd2.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr2 = cmd2.ExecuteReader();
            DataTable dt2 = new DataTable();
            dt2.Load(dr2);

            //Resets Locked Account.
            string spName = "";
            spName = "sp_UpdateFailedAttemptBackZero";
            SqlCommand cmd = new SqlCommand(spName, con);
            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();

            con.Close();
            SendEmail_localhost();
            this.isSent = "true";
            //Session["forgotSuccess"] = "New Password Sent to Email! Please Login to continue."; 
            //Response.Redirect("Login.aspx");
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page),
              "Script", "setTimeout(function(){ ShowMessageNow(); }, 1500);", true);
    }
}
