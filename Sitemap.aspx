﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Sitemap.aspx.cs" Inherits="Sitemap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<section class="breadcrumbs">
    <div class="container">
        <p>
            <a href="/index.aspx">Home</a>
            <span class="mlr">></span>
            Sitemap
        </p>
    </div>
</section>   
<section id="sitemap" class="container">
    <h2>Sitemap</h2>
</section> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerContent" Runat="Server">
</asp:Content>

