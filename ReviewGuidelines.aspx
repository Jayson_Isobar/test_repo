﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReviewGuidelines.aspx.cs" Inherits="ReviewGuidelines" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link type="text/css" rel="stylesheet" href="<%=this.CDNdomain %>css/popup.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <h2>
            Swisse values your feedback!
        </h2>
        <p>When writing your review, please consider the following guidelines:</p>
        <ul>
            <li>Focus on the product and your individual experience using it</li>
			<li>Provide details about why you liked or disliked a product</li>
			<li>All submitted reviews are subject to the terms set forth in our Terms of Use</li>
			<li>We reserve the right not to post your review if it contains any of the following types of content or violates other guidelines:</li>
			<li>Obscenities, discriminatory language, or other language not suitable for a public forum</li>
			<li>Advertisements, “spam” content, or references to other products, offers, or websites</li>
			<li>Email addresses, URLs, phone numbers, physical addresses or other forms of contact information</li>
			<li>Critical or spiteful comments on other reviews posted on the page or their authors</li>
        </ul>
		<p>In addition, if you wish to share feedback with us about product selection, pricing, ordering, delivery or other customer service issues, please do not submit this feedback through a product review. Instead, contact us directly.</p>
		<p>Enjoy writing your review!</p>
		
    </div>
    </form>
</body>
</html>
