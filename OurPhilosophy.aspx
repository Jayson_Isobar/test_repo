﻿<%@ Page Title="Our Philosophy" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OurPhilosophy.aspx.cs" Inherits="HeaderNavigationFlyout_OurPhilosophy" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>Our Philosophy | Swisse Vitamins</title>
<meta property="og:title" content="Our Philosophy - Swisse Vitamins" />
<meta name="keywords" content="multivitmains, vitamins, Australian companies, about Swisse, Swisse philosophy, Swisse Australia, vitamins and supplements, health companies, health brands, health and wellbeing" />
<meta name="description" content="Health, happiness and wellbeing. These three things are at the core of everything we do here at Swisse." />
<link rel="canonical" href="http://www.swisse.com.sg/our-story/our-philosophy" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/our-story">Our Story</a>
                <span class="mlr">></span>
                Our Philosophy
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>
<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">Our Philosophy</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litAboutSwisseRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>

<li><a href="/our-story/about-swisse">About Swisse</a></li>
<li><a href="/our-story/our-philosophy">Our Philosophy</a></li>
<li><a href="/our-story/swisse-ambassadors">Swisse Ambassadors</a></li>
<li><a href="/our-story/corporate-social-responsibility">Swisse Social Responsibility</a></li>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>
</asp:Content>
