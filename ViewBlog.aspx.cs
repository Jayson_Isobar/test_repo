﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_ViewBlog : BaseForm
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    string uid = "";
    string blogID = "";

    protected Item Item
    {
        get
        {
            return HttpContext.Current.Items["Item"] as Item;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var id = Item.Id;
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetBlogContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@link", id);
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                litBreadCrumbs.Text = "<span>" + myRow[1].ToString() + "</span>";
                litContent.Text = "<h3><strong>" + myRow[1].ToString() + "</strong></h3><p></p>" + myRow[2].ToString().Replace("<img src=\"", "<img src=\"" + base.CDNdomain);
                blogID = myRow[0].ToString();

                string spName2 = "";
                spName2 = "sp_GetRelatedProductsBLogList";
                SqlCommand cmd2 = new SqlCommand(spName2, con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@blogid", myRow[0].ToString());
                SqlDataReader dr2 = cmd2.ExecuteReader();
                DataTable dt2 = new DataTable();
                dt2.Load(dr2);

                if (dt2.Rows.Count > 0)
                {
                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        lblRP.Visible = true;
                        litBlogRelatedProd.Text += "<div class='recentlyviewed-box'>" +
                            "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" +
                                "<img src='" + base.CDNdomain + "images/" + myRow2[2].ToString() + "' width='128' height='196' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                            "</a>" +
                            " <div class='product-details-wrap-recent'>" +
                            "  <p class='product-name'>" +
                            "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" +
                            myRow2[1].ToString() + 
                            "</a>" +
                            "</p>" +
                             " <div class='product-rate' data-sku='" + myRow2[4].ToString() + "'>" +
                           //"  <span class='rating'></span>" +
                               " <div class='clearfloat'></div>" +
                             " </div>" +
                              "<div class='clearfloat'></div>" +
                              "<div class='product-price'><p>SGD " + myRow2[3].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                              "<div class='clearfloat'></div>" +
                              "</div>" +
                        "</div>";

                    }
                }


                LoadCtrRecommend();


                string spName3 = "";
                spName3 = "sp_GetRelatedBlogs";
                SqlCommand cmd3 = new SqlCommand(spName3, con);
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Parameters.AddWithValue("@blogid", myRow[0].ToString());
                SqlDataReader dr3 = cmd3.ExecuteReader();
                DataTable dt3 = new DataTable();
                dt3.Load(dr3);

                if (dt3.Rows.Count > 0)
                {
                    foreach (DataRow myRow3 in dt3.Rows)
                    {
                        litRelatedBlog.Text += "<li><a href='" + UrlHelper.FormatPageUrl(myRow3[2].ToString(), PageType.Type.BlogItem) + "'>" + myRow3[1].ToString() + "</a></li>";

                    }
                }
            }

        }

        con.Close();
    }
    public void LoadCtrRecommend() {
        HealthAndHappinessBlogCls pc = new HealthAndHappinessBlogCls();

        pc = HealthAndHappinessBlogCls.GetTotalRecommended(blogID);
        string cat = pc.totalBlog;

        if (Int32.Parse(cat) > 1)
        {
            litCtrRecommend.Text = "Recommended " + "<strong>" + cat + "</strong>" + " times";
        }
        else if (Int32.Parse(cat) == 1)
        {
            litCtrRecommend.Text = "Recommended " + "<strong>" + cat + "</strong>" + " time";
        }
        else if (Int32.Parse(cat) == 0)
        {
            litCtrRecommend.Visible = false;
        }

    
    }
    protected void btnRecommend_Click(object sender, EventArgs e)
    {
        if (Session["uid"] != null)
        {
            uid = Session["uid"].ToString();
        }

        HealthAndHappinessBlogCls uAdd = new HealthAndHappinessBlogCls();
        uAdd.userid = uid;
        uAdd.blogID = blogID;

        uAdd.InsertToRecommended();

       //LoadCtrRecommend();
        Response.Redirect(UrlHelper.FormatPageUrl(Item.Id, PageType.Type.BlogItem));
    }
}