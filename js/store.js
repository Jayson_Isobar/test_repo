﻿function initBranchList() {

    // Load json data from json
    $.ajax({
        async: false,
        url: "/data/swisse-store.json",
        type: "get",
        dataType: "json"
    }).success(function (data) {

        window.SG = data["stores"];

        getBranchesByArea("storelist")

    }).fail(function () {

        alert('Unable to load the json file containing the branch locations.');

    });

}



function getBranchesByArea(areaID) {
    var myArea = window.SG;
    var count = 0;

    for (branch in myArea) {
        count++;

        var branchName = myArea[branch]['store-name'];
        var street1 = myArea[branch]['street1'];
        var street2 = myArea[branch]['street2'];
        var city = myArea[branch]['city'];
        var zip = myArea[branch]['zip'];
        var telno = myArea[branch]['phone'];
        var faxno = myArea[branch]['fax'];

        var distance = "0.0";

        renderBranchSelection(count, branch, branchName, street1, street2, city, zip, telno, faxno, distance);
    }
}

function renderBranchSelection(count, branch, branchName, street1, street2, city, zip, telno, faxno, distance) {
    $("#storelist").append('<div id="branch-' + branch + '" class="store">' +
                                '<span>' + count + '</span>' +
                                '<div>' +
                                    '<p class="storeName"><a href="javascript:void(0);" onclick="showTheShop(\'' + branch + '\')">' + branchName + '</a></p>' +
                                    '<p class="street1">' + street1 + '</p>' +
                                    '<p class="street2">' + street2 + '</p>' +
                                    '<p class="cityzip">' + city + ' '+ zip +'</p>' +
                                    '<p class="telno">Tel: ' + telno + '</p>' +
                                    '<p class="faxno">Fax: ' + faxno + '</p>' +
                                    '<p class="distance">' + distance + ' miles</p>' +
                                '</div>' +
                            '</div>');

    setBranchMarker(branch);
}

function setBranchMarker(currBranch) {
    

    var myArea = window.SG;

    for (branch in myArea) {
        
        if (branch == currBranch) {
            var lat = myArea[branch]['lat'];
            var lng = myArea[branch]['lng'];
            var zoom = 17;
            var branchName = myArea[branch]['store-name'];
            var street1 = myArea[branch]['street1'];
            var street2 = myArea[branch]['street2'];
            var city = myArea[branch]['city'];
            var zip = myArea[branch]['zip'];
            var telno = myArea[branch]['phone'];
            var faxno = myArea[branch]['fax'];

            updateLocation(branch, branchName, lat, lng, street1, street2, city, zip, telno, faxno);
        }
    }
}

function updateLocation(branch, branchName, lat, lng, street1, street2, city, zip, telno, faxno) {
    var center = new google.maps.LatLng(lat, lng);
    var tipoffsety = -330;
    var tipoffsetx = -250;
    var tipboxbg = "#2d2d2d";
    var tipboxPd = "20px";
    var infoBoxContent = "";

    infoBoxContent = '<div class="branchInfo">' +
                                '<p class="storeName">' + branchName + '</p>' +
                                '<span><img src="/images/logoGuardian.jpg" /></span>' +
                                '<div>' +
                                    '<p class="street1">' + street1 + '</p>' +
                                    '<p class="street2">' + street2 + '</p>' +
                                    '<p class="cityzip">' + city + ' '+ zip +'</p>' +
                                    '<p class="telno">Tel: ' + telno + '</p>' +
                                    '<p class="faxno">Fax: ' + faxno + '</p>' +
                                    '<a href="#" class="btn color-rd mtop20">Get Directions</a>' +
                                '</div>' +
                                '<img class="arrow" src="/images/arrowInfoBox.png" />' +
                        '</div>' 
    
    esMarker = new google.maps.Marker({
        position: center,
        map: map,
        icon: imgPath + "marker.png",
    });

    tipoffsety = -65;
    tipoffsetx = -182;

    currmarker = esMarker;
    markersArray.push(currmarker);

    google.maps.event.addListener(esMarker, "click", function (e) {
        var infoBox = new InfoBox({ center: esMarker.getPosition(), map: map });
    });

    var infoBoxOptions = {
        content: infoBoxContent
			, disableAutoPan: false
			, maxWidth: 0
			, pixelOffset: new google.maps.Size(tipoffsetx, tipoffsety)
			, zIndex: null
			, boxStyle: {
			    background: tipboxbg,
			    padding: tipboxPd
			, opacity: 1
			, width: "330px"
			}
		, closeBoxMargin: "-10px -10px 2px 2px"
		, closeBoxURL: imgPath + "closeInfoBox.png"
		, infoBoxClearance: new google.maps.Size(1, 1)
		, isHidden: false
		, pane: "floatPane"
		, alignBottom: true
		, enableEventPropagation: false
    };


    google.maps.event.addListener(currmarker, "click", function (e) {
        ib.open(map, this);
        showTheShop(branch);
    });

    var ib = new InfoBox(infoBoxOptions);
    
}

function showTheShop(currBranch) {

    var myArea = window.SG;

    $(".infoBox").hide();

    if ($("#blpanel" + currBranch).css('display') == 'block') {
        $("#blpanel" + currBranch).slideUp("fast", function () {

            $(this).siblings().find(".bl-toggle-icon").html("-");
            $(this).siblings().find(".bl-toggle-icon").stop().addClass("collapse").removeClass("expand");
            $(this).siblings().find(".bl-branch-text").stop().parent().removeClass('currentbranch');
            $(this).parent().stop().removeClass('currentbranch');

        });
    }
    else {
        if (currBranch != "") {
        } else {
            document.getElementById('branchInfo').innerHTML = '';
        }

        var currentArea = $("#bl-area").val();

        for (branch in myArea) {

            if (branch == currBranch) {

                var lat = myArea[branch]['lat'];
                var lng = myArea[branch]['lng'];
                var zoom = 17;
                zoomMoveToLocation(lat, lng, zoom);
            }
        }
    }
}