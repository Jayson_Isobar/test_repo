﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for UserCls
/// </summary>
public class UserCls
{

    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;
    public int userID { get; set; }
    public string userName { get; set; }
    public string userEmail { get; set; }
    public string userPassword { get; set; }
    public string userfName { get; set; }
    public string usermName { get; set; }
    public string userlName { get; set; }
    public int userStatus { get; set; }
    public string userBirthMonth { get; set; }
    public string userBirthYear { get; set; }
    public string userPostCode { get; set; }
    public int checkAgreed { get; set; }
    public int optInStatus { get; set; } // status wether user opt out or opt in
    public bool firstLogged { get; set; }



    public static UserCls GetForgotEmail(string Email)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetUserEmail", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Email", Email);
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        con.Close();

        DataTable tb = ds.Tables[0];


        UserCls GU;
        GU = new UserCls();
        if (tb.Rows.Count > 0)
        {
            System.Data.DataRow row;
            row = tb.Rows[0];

            GU.userEmail = System.Convert.ToString(row["userEmail"]);
            return GU;

        }
        else
        {
            return null;
        }
    }

    public static UserCls CheckEmail(string Email)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_CheckDuplicationEmail", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Email", Email);
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        con.Close();

        DataTable tb = ds.Tables[0];


        UserCls GU;
        GU = new UserCls();
        if (tb.Rows.Count > 0)
        {
            System.Data.DataRow row;
            row = tb.Rows[0];

            GU.userEmail = System.Convert.ToString(row["userEmail"]);
            return GU;

        }
        else
        {
            return null;
        }
    }

    public static UserCls GetUserByEmailPass(string Email, string Password)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetUserByEmailPassword", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Email", Email);
        cmd.Parameters.AddWithValue("@Password", Password);
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        con.Close();

        DataTable user = ds.Tables[0];


        UserCls GU;
        GU = new UserCls();
        if (user.Rows.Count > 0)
        {
            System.Data.DataRow rowUser;
            rowUser = user.Rows[0];
            GU.userID = System.Convert.ToInt32(rowUser["userID"]);
            GU.userName = System.Convert.ToString(rowUser["userName"]);
            GU.userEmail = System.Convert.ToString(rowUser["userEmail"]);
            GU.userPassword = System.Convert.ToString(rowUser["userPassword"]);
            GU.checkAgreed = System.Convert.ToInt16(rowUser["checkAgreed"]);
            GU.firstLogged = System.Convert.ToBoolean(rowUser["firstLogged"]);
            return GU;

        }
        else
        {
            return null;
        }
    }
    public bool AddUser()
    {
        bool result = false;

        _helper = new SqlHelper();
        try
        {

            if (_helper.CreateConnection())
            {
                _helper.BeginTransaction();
                _helper.CreateCommand("sp_InsertUser", true);
                _helper.AddParameter("@UserName", this.userName, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@Password", this.userPassword, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@Email", this.userEmail, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@FirstName", this.userfName, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@MiddleName", this.usermName, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@LastName", this.userlName, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@Status", this.userStatus, DbType.Int32, ParameterDirection.Input);
                _helper.AddParameter("@userBirthMonth", this.userBirthMonth, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@userBirthYear", this.userBirthYear, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@userPostCode", this.userPostCode, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@checkAgreed", this.checkAgreed, DbType.Int32, ParameterDirection.Input);

                result = _helper.ExecuteNonQuery();

                _helper.ClearCommandParameters();

                if (result)
                {
                    _helper.CommitTransaction();

                }


            }

        }
        catch (Exception ex)
        {

            _helper.RollbackTransaction();
            ErrorHandler.Handle(ex);


        }
        finally
        {
            _helper.CloseConnection();

        }


        return result;

    }

    public bool Update_Email_Status(UserCls StatusUpdate)
    {
        bool result = false;

        _helper = new SqlHelper();
        try
        {
            if (_helper.CreateConnection())
            {
                _helper.BeginTransaction();
                _helper.CreateCommand("sp_UpdateUserByEmailRegistration", true);
                _helper.AddParameter("@Email", this.userEmail, DbType.String, ParameterDirection.Input);

                result = _helper.ExecuteNonQuery();
                _helper.ClearCommandParameters();
                if (result)
                {
                    _helper.CommitTransaction();
                }
            }
        }
        catch (Exception ex)
        {
            _helper.RollbackTransaction();
            ErrorHandler.Handle(ex);
        }
        finally
        {
            _helper.CloseConnection();
        }
        return result;
    }

    public static UserCls GetUserProfile(string Email)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_ViewProfile", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Email", Email);
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        con.Close();

        DataTable tbUser = ds.Tables[0];

        UserCls GU;
        GU = new UserCls();
        if (tbUser.Rows.Count > 0)
        {
            System.Data.DataRow rowUser;
            rowUser = tbUser.Rows[0];

            GU.userfName = System.Convert.ToString(rowUser["userFN"]);
            GU.userlName = System.Convert.ToString(rowUser["userLN"]);
            GU.userBirthMonth = System.Convert.ToString(rowUser["userBirthMonth"]);
            GU.userBirthYear = System.Convert.ToString(rowUser["userBirthYear"]);
            GU.userPostCode = System.Convert.ToString(rowUser["userPostCode"]);
            GU.checkAgreed = System.Convert.ToInt32(rowUser["checkAgreed"]);
            return GU;
        }
        else
        {
            return null;
        }
    }

    public bool EditProfile(string Email)
    {
        bool result = false;

        _helper = new SqlHelper();
        try
        {

            if (_helper.CreateConnection())
            {
                _helper.BeginTransaction();
                _helper.CreateCommand("sp_UpdateProfile", true);
                _helper.AddParameter("@Email", this.userEmail, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@FirstName", this.userfName, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@LastName", this.userlName, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@UserBMonth", this.userBirthMonth, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@UserBYear", this.userBirthYear, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@UserPostCode", this.userPostCode, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@checkAgreed", this.checkAgreed, DbType.Int32, ParameterDirection.Input);
                _helper.AddParameter("@userStatus", this.userStatus, DbType.Int32, ParameterDirection.Input);
                _helper.AddParameter("@optInStatus", this.optInStatus, DbType.Int32, ParameterDirection.Input);

                result = _helper.ExecuteNonQuery();

                _helper.ClearCommandParameters();

                if (result)
                {
                    _helper.CommitTransaction();

                }

            }

        }
        catch (Exception ex)
        {

            _helper.RollbackTransaction();
            ErrorHandler.Handle(ex);


        }
        finally
        {
            _helper.CloseConnection();

        }


        return result;

    }

    public bool ResetPassword(string Email)
    {
        bool result = false;

        _helper = new SqlHelper();
        try
        {

            if (_helper.CreateConnection())
            {
                _helper.BeginTransaction();
                _helper.CreateCommand("sp_UpdateUserPasswordByEmail", true);
                _helper.AddParameter("@Email", this.userEmail, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@pass", this.userPassword, DbType.String, ParameterDirection.Input);

                result = _helper.ExecuteNonQuery();

                _helper.ClearCommandParameters();

                if (result)
                {
                    _helper.CommitTransaction();

                }

            }

        }
        catch (Exception ex)
        {

            _helper.RollbackTransaction();
            ErrorHandler.Handle(ex);


        }
        finally
        {
            _helper.CloseConnection();

        }


        return result;

    }
}


