
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsFAQList]    Script Date: 2/20/2015 4:08:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 20, 2015>
-- Description:	<Get FAQ Related Products By ID>
-- exec sp_GetRelatedProductsFAQList[1]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsFAQList]
   @faqid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_FAQ_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.faqid= @faqid
END
