﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ProductCategories : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["cid"] != null)
            {
                LoadViewpnlViewProdCat();
            }
            else
            {
                LoadData();
            }
        }
    }

    protected void btnViewDeleted_Click(object sender, EventArgs e)
    {
        gvProdCat.Columns[0].Visible = false;
        btnBackView.Visible = true;
        btnViewDeleted.Visible = false;
        LoadData2();
    }

    protected void btnBackView_Click(object sender, EventArgs e)
    {
        gvProdCat.Columns[0].Visible = true;
        btnBackView.Visible = false;
        btnViewDeleted.Visible = true;
        LoadData();
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdCatList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdCat.DataSource = ds;
        gvProdCat.DataBind();
        con.Close();

    }

    public void LoadData2()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdCatDeletedList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdCat.DataSource = ds;
        gvProdCat.DataBind();
        con.Close();

    }

    protected void gvProdCatRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            var someId = e.CommandArgument;
            Response.Redirect("ProductCategories.aspx?cid=" + someId.ToString(), false);
        }
        else if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProdCatByID";
            cmd.Parameters.AddWithValue("@prodcatID", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Redirect("ProductCategories.aspx");
        }
    }

    protected void gvProdCat_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProdCat.EditIndex = e.NewEditIndex;
        LoadData();
    }

    public void LoadViewpnlViewProdCat()
    {
        pnlGVProdCat.Visible = false;
        pnlViewProdCat.Visible = true;
        btnUpdate.Visible = true;

        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProdCatDetailsByID";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@prodcatID", Request.QueryString["cid"].ToString());
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            txtProdCatName.Text = dt.Rows[0]["prodcatName"].ToString().Trim();
            txtProdCatDesc.Text = dt.Rows[0]["prodcatDesc"].ToString().Trim();
            if (dt.Rows[0]["prodcatStatus"].ToString().Trim() == "1")
            {
                cbStatus.Checked = true;
            }
            else {
                cbStatus.Checked = false;
            }
        }
        con.Close();
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_UpdateProdCatByID";
        cmd.Parameters.AddWithValue("@prodcatID", Request.QueryString["cid"]);
        cmd.Parameters.AddWithValue("@prodcatName", txtProdCatName.Text.Trim());
        cmd.Parameters.AddWithValue("@prodcatDesc", txtProdCatDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else{
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@prodcatStatus", cbStat);

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ProductCategories.aspx"); 
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductCategories.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProdCat.Visible = false;
        pnlViewProdCat.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProdCat";
        cmd.Parameters.AddWithValue("@prodcatName", txtProdCatName.Text.Trim());
        cmd.Parameters.AddWithValue("@prodcatDesc", txtProdCatDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else
        {
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@prodcatStatus", cbStat);

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ProductCategories.aspx"); 
    }

    protected void gvProdCat_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProdCat.PageIndex = e.NewPageIndex;
        LoadData();
    }
}
