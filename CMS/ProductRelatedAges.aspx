﻿<%@ Page Title="Product Related Ages" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductRelatedAges.aspx.cs" Inherits="ProductRelatedAges" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT AGE" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Products List" 
        onclick="btnBackView_Click" />
        <p></p>
        
    <asp:Panel ID="pnlGVProdAge" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdAge" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdAgeRowCommand"
            OnRowEditing="gvProdAge_RowEditing"
            OnPageIndexChanging="gvProdAge_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
            OnRowDeleting="PendingRecordsGridview_RowDeleting">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("id") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Age" SortExpression="agerange">
                    <ItemTemplate>
                        <asp:Label ID="lblprodAgeName" runat="server" Text='<%# Bind("agerange") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

               
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdAge" runat="server" Visible="False">
            
             <asp:Label ID="Label1" runat="server" Text="Product Age"></asp:Label>
             <asp:DropDownList ID="ddlProdAge" runat="server" AutoPostBack="True">
             </asp:DropDownList>
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
