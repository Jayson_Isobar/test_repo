﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ClinicalTrialsRelatedProducts : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDDLTheme();
        }
    }
    public void LoadDDLTheme()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProductList";
        SqlCommand cmd1 = new SqlCommand(spName, con);
        cmd1.CommandType = CommandType.StoredProcedure;
        con.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
        DataTable dtPC = new DataTable();
        adapter.Fill(dtPC);

        ddlTheme.DataSource = dtPC;

        ddlTheme.DataTextField = "productName";
        ddlTheme.DataValueField = "productid";
        ddlTheme.DataBind();

        con.Close();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_InsertClinicalTrialsProducts";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@productid", ddlTheme.SelectedValue);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

    }

    protected void gvBlog_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvBlog.EditIndex = e.NewEditIndex;
    }

    protected void gvBlog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBlog.PageIndex = e.NewPageIndex;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ClinicalTrials.aspx");
    }
}
