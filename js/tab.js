﻿(function ($) {

    var tabContainer = $(".tabContainer");
    var tabs = $(".tabs");
    var tab = $(".tabs li");
    var tabsContent = $(".tabsContent");
    var tabContent = $(".tabsContent li");


    // Init
    tab.css({ "float": "left" })
    tabs.find(":first").addClass("active");
    tabsContent.find(":first").addClass("active");
    tabContent.not(":first").hide()

    tab.click(function () {
        var _this = $(this);
        var tabIndex = _this.index();

        if (!_this.hasClass("active")) {

            // Set active tab
            tab.removeAttr("class");
            _this.addClass("active");

            // Set active content
            $(tabContent).fadeOut("fast");
            $(tabContent[tabIndex]).fadeIn();
        };
    });
} (jQuery));