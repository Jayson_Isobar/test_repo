﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.IO;



public partial class ForgotPass : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();

            string spName2 = "";
            string Password = Encrypt(txtPassword.Text);
            spName2 = "sp_UpdateUserPassword";
            SqlCommand cmd2 = new SqlCommand(spName2, con);
            con.Open();
            cmd2.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
            cmd2.Parameters.AddWithValue("@newPassword", Password);
            cmd2.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr2 = cmd2.ExecuteReader();
            DataTable dt2 = new DataTable();
            dt2.Load(dr2);
            con.Close();
       
        //Session["forgotSuccess"] = "New Password Sent to Email! Please Login to continue."; 
        Response.Redirect("Login.aspx");
    }
}
