﻿<%@ Page Title="Product Category" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ProductCategory.aspx.cs" Inherits="ProductCategory" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

    <title><%= Item.MetaTitle %></title>
    <meta property="og:title" content="<%= Item.MetaOpenGraphTitle %>"  />
    <meta name="description" content="<%= Item.MetaDescription %>" />
    <meta name="keywords" content="<%= Item.MetaKeywords %>" />
    <link rel="canonical" href="<%= Item.CanonicalUrl %>" />

    <link href="<%=CDNdomain %>css/featherlight.css" rel="stylesheet" type="text/css">
    <script src="<%=CDNdomain %>js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="<%=CDNdomain %>js/featherlight.js" type="text/javascript"></script>
    <link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <input type="hidden" id="itemUrl" value="<%=Item.Url %>" />
    <input type="hidden" id="cdnValue" value="<%=this.CDNdomain %>" />
    <section class="banner-pr">
        <div class="breadcrumbs">
            <div class="container">
                <p>
                    <a href="/index.aspx">Home</a>
                    <span class="mlr">></span>
                    Product Category
                </p>
            </div>
        </div>
    </section>
    <section class="banner-pr-bottom color-blck">
    </section>
    <section id="product-wrap">
<div class="container">
    	<div id="sp-sidebar" class="color-dgray">
       	<div class="sidebar-lft list">
        <h3>Browse by Category</h3>
        <ul class="by-category">
            <asp:Literal ID="litProdCat" runat="server"></asp:Literal>
        </ul>
        </div>
       	<div class="sidebar-lft">
        <h3>Refine by Ingredient</h3>
        <ul class="by-category">
           <asp:CheckBoxList ID="cbIng" runat="server"  DataSourceID="sdsIngredients" DataTextField="prodingredientName"
          DataValueField="prodingredientid"  AutoPostBack="True"  OnSelectedIndexChanged="cbIng_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsIngredients" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetProdIngredientListActive" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtIngredients" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div>
       	<div class="sidebar-lft">
        <h3>Refine by Concern</h3>
        <ul class="by-category">
           <asp:CheckBoxList ID="cbConcern" runat="server"  DataSourceID="sdsConcerns" DataTextField="prodconcernName"
          DataValueField="prodconcernid"  AutoPostBack="True"  OnSelectedIndexChanged="cbConcern_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsConcerns" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetProdConcernListActive" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtConcerns" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div> 
        <div class="sidebar-lft">
        <h3>Refine by Age</h3>
        <ul class="by-category refineAge">
            <asp:CheckBoxList ID="cbAge" runat="server"  DataSourceID="sdsAge" DataTextField="agerange"
          DataValueField="id"  AutoPostBack="True"  OnSelectedIndexChanged="cbAge_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsAge" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetAgeList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtAges" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div>  
        	<div class="sidebar-lft">
        <h3>Refine by Gender</h3>
        <ul class="by-category refineGender">
            <asp:CheckBoxList ID="cbGender" runat="server"  DataSourceID="sdsGender" DataTextField="gender"
          DataValueField="id"  AutoPostBack="True"  OnSelectedIndexChanged="cbGender_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsGender" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetGenderList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtGenders" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div>              
      </div>
       
        <div id="sp-content">
            <div class="sp-content-banner">
                <img src="" alt="Discover the Swisse difference">
            </div>
            <div class="viewing-filter">
                <asp:Panel ID="viewingFilterPanel" runat="server" >
                    <h4>Viewing: <%--<asp:label ID="lblViewing" runat="server"></asp:label>--%>
                        <asp:Repeater ID="filterRepeater" runat="server">
                            <ItemTemplate>
                                <span>
                                    <%# Eval("Name").ToString() %>
                                    <asp:LinkButton CssClass="closeTheme" CommandName="FilterRemove" CommandArgument='<%#Container.ItemIndex + "," + Eval("Value")+","+ Eval("Type") %>' OnCommand="FilterRemove_Command" runat="server">
                                        <img src="<%=this.CDNdomain %>images/btnX.png" alt="" />
                                    </asp:LinkButton>
                                </span>
                            </ItemTemplate>
                        </asp:Repeater>
                        <span class="clearAll">
                            <asp:LinkButton Text="Clear All" CommandName="FilterRemoveAll" OnCommand="FilterRemove_Command" runat="server" />
                        </span>
                    </h4>
                </asp:Panel>
            </div>
            <div id="sorting" class=" color-blck">
                <section class="banner-pr-bottom">
                    <h4 class="fltlft mlft20">
                        <asp:Label ID="lblTotalItems" runat="server"></asp:Label>
                        <%--<img src="<%=this.CDNdomain %>images/sort.jpg" class="mlft12" alt="Sort">
                        <img src="<%=this.CDNdomain %>images/sort2.jpg" width="20" height="20" alt="Sort">--%>
                    </h4>

                    <div class="sortby fltrt">
                        <h4 class="fltlft">SORT</h4>
                        <div class="fltlft">
                            <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="True" onselectedindexchanged="ddlSort_SelectedIndexChanged" name="sortby">
                                <asp:ListItem>Most Popular</asp:ListItem>
                                <%--<asp:ListItem>Price</asp:ListItem>--%>
                                <asp:ListItem>Alphabetically</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>  
                </section>
            </div>
            <div class="clearfloat"></div>   

       <asp:Literal ID="litProdCatName" runat="server"></asp:Literal>
       <asp:Literal ID="litProduct" runat="server"></asp:Literal>
                                      
        <asp:Literal ID="litPop" runat="server"></asp:Literal>
		<%--<div class="product-pop" id="f1">
			<div class="product-pop-imgbox">
			  <img src="<%=this.CDNdomain %>images/ultivite men.jpg" alt="Ultivite" class="centerImage"> </div>
			  <div class="product-pop-desc">
			  <h3>Ultivite Men</h3>
			<div class="product-rate">
			  <ul class="rating threestar fltlft">
					<li class="one"><a href="#" title="1 Star">1</a></li>
					<li class="two"><a href="#" title="2 Stars">2</a></li>
					<li class="three"><a href="#" title="3 Stars">3</a></li>
					<li class="four"><a href="#" title="4 Stars">4</a></li>
					<li class="five"><a href="#" title="5 Stars">5</a></li>
				</ul>
				<p class="fltlft">4.4 / 5 (75) <span class="recommended">98% recommended</span></p>
			<div class="clearfloat"></div>
			</div>
			<div class="product-price mtop10"><h3>$39.00 <span class="txt12">MSRP</span></h3></div> 
			<div class="clearfloat"></div> 
			<form action="product size" method="get">
		   <label class="txt-up">size</label>
		   <select name="tablets">
			 <option>60 Tablets</option>
		   </select>
		   <label class="txt-up mlft12">Quantity</label>
		   <select name="tablets">
			 <option>1</option>
		   </select>
		   <div class="clearfloat"></div>
		   <div class="btn color-rd mtop20"><h4><a href="#">add to cart</a></h4></div>
		  </form>    
		  </div>
		  <div class="clearfloat"></div>
		  <div class="col_64 fltlft mtop15"><p>Swisse Women’s Ultivite is the only targeted multivitamin to include over 50 premium quality vitamins, minerals, antioxidants and herbal extracts to help maintain a woman’s nutritional health.</p>
		  <p class="mtop10"><a href="#" class="txt-bl">More Details &raquo;</a></p>
		  </div>
		</div>--%>
    
    </div><!--Product Wrap -->
        <div class="clearfloat"></div>
    </div>
</section>

    <section class="recentlyviewed">
<div class="container">
<h3>RECENTLY VIEWED PRODUCTS</h3>
<div class="recentlyviewed-wrap mtop15">
	<asp:Literal ID="litProdRecentView" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
</section>

</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            var cdn = $("#cdnValue").val();

            //TODO: original code
            //var GetQuery = window.location.href.split("cat=")[1];
            //var GetQuery = $("form").attr("action").split("cat=")[1];
            //var imgSrc = GetQuery.split("+&+").join("_").replace("%2c+", ",");

            //$(".banner-pr").css("background", "url(" + cdn + "images/bg-" + imgSrc + ".jpg) no-repeat top center");
            //$(".sp-content-banner img").attr("src", cdn + "images/banner-" + imgSrc + ".png");

            var GetQuery = $("#itemUrl").val();
            var imgSrc = GetQuery.split("-and-").join("_").replace("%2c+", ",");

            $(".banner-pr").css("background", "url(" + cdn + "images/bg-" + imgSrc + ".jpg) no-repeat top center");
            $(".sp-content-banner img").attr("src", cdn + "images/banner-" + imgSrc + ".png");

            var catList = $("#sp-sidebar div ul li");
            catList.each(function () {

                var cat = $(this).children("a").attr("href");
                var catSplit = cat.split("/")[2];

                if (catSplit == GetQuery) {
                    $(this).addClass("active");
                }
            });
        });
    </script>

</asp:Content>
