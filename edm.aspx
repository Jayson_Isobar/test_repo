﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edm.aspx.cs" Inherits="edm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Swisse Vitamins | Australia's #1 Multivitamin Brand</title>
    <link href="<%=this.CDNdomain %>css/edm.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container">
        <div class="header">
            <a href="http://www.swisse.com.sg">
                <img src="<%=this.CDNdomain %>images/eDM_head.jpg" alt="Discover the Swisse Difference" />
            </a>
            <div id="sns">
                <a href="https://www.facebook.com/swisse.singapore"></a>
                <a href="https://www.youtube.com/channel/UCznR9SSBtzRU3SCRfWz2pAA"></a>
                <a href="https://instagram.com/swissesg/"></a>
            </div>
        </div>
        <a href="http://www.swisse.com.sg/products">
            <img src="<%=this.CDNdomain %>images/eDM_findOutMore.jpg" alt="Find Out More" />
        </a>
        <a href="http://www.swisse.com.sg/our-story/swisse-ambassadors">
            <img src="<%=this.CDNdomain %>images/eDM_ambassador.jpg" alt="Swisse Ambassadors" />
        </a>
        <a href="http://www.swisse.com.sg/offers-and-promos">
            <img src="/images/eDM_citibank.jpg" alt="Enjoy $5 Off" />
        </a>
        <a href="http://www.swisse.com.sg/offers-and-promos">
            <img src="<%=this.CDNdomain %>images/eDM_promo.jpg" alt="Offers and Promos" />
        </a>
        <a href="http://www.swisse.com.sg/products">
            <img src="<%=this.CDNdomain %>images/eDM_products.jpg" alt="Swisse Products" />
        </a>
        <a href="http://www.swisse.com.sg">
            <img src="<%=this.CDNdomain %>images/eDM_footer.jpg" />
        </a>
    </div>
</body>
</html>
