﻿<%@ Page Title="Swisse Ambassadors" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ViewAmbassadors.aspx.cs" Inherits="ViewAmbassadors" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:Button ID="btnAdd" runat="server" Text="ADD AMBASSADOR" onclick="btnAdd_Click" />
    <p></p>
     <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvBlog" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvBlogRowCommand"
            OnRowEditing="gvBlog_RowEditing"
            OnPageIndexChanging="gvBlog_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
            OnRowDeleting="PendingRecordsGridview_RowDeleting">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
              <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                         <asp:LinkButton Text="Edit" ID="lnkEdit" runat="server" CommandName="Edit"
                        CommandArgument='<%# Eval("id") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("id") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Ambassador Name" SortExpression="title">
                    <ItemTemplate>
                        <asp:Label ID="lblblogTitle" runat="server" Text='<%# Bind("title") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>
</asp:Content>
