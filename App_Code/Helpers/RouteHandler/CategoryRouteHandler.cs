﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class CategoryRouteHandler : IRouteHandler
{

    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        var categoryName = requestContext.RouteData.Values["CategoryName"] as string;

        if (string.IsNullOrEmpty(categoryName))
            return BuildManager.CreateInstanceFromVirtualPath("~/AllCategories.aspx", typeof(Page)) as Page;
        else
        {
            var categoryService= new CategoryService();
            // Get information about this category 
            var item = categoryService.GetProduct(categoryName);

            if (item == null)
                return UrlHelper.GetNotFoundHttpHandler();
            else
            {
                // Store the Category object in the Items collection
                HttpContext.Current.Items["Category"] = item;
                return BuildManager.CreateInstanceFromVirtualPath("~/ProductCategory.aspx", typeof(Page)) as Page;
            }
        }
    }



}