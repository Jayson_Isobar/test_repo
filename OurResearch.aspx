﻿<%@ Page Title="Our Research" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OurResearch.aspx.cs" Inherits="HeaderNavigationFlyout_OurResearch" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Our Research | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Vitamins - Our Research" />
<meta name="keywords" content="vitamins, multivitamins, scientific research, integrative health, clinical trials, vitamin research, health research, research and development, vitamin science" />
<meta name="description" content="At Swisse our research focus is developing scientifically validated vitamin & supplement products that help people maintain optimal health and wellbeing." />
<link rel="canonical" href="http://www.swisse.com.sg/the-science-of-swisse/our-research" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/the-science-of-swisse">The Science of Swisse</a>
                <span class="mlr">></span>
                Our Research
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>

<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litRelatedProd" runat="server"></asp:Literal>
          <div class="clearfloat"></div>
          </div>    
    </div>
	
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>
<li><a href="/our-story/about-swisse">About Swisse</a></li>
<li><a href="/the-science-of-swisse/our-scientific-partnerships">Our Scientific Partnerships</a></li>
<li><a href="/the-science-of-swisse/clinical-trials">Clinical Trials</a></li>
<%--<li><a href="FAQs.aspx">Frequently Asked Questions</a></li>--%>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>


</asp:Content>
