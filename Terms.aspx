﻿<%@ Page Title="Terms & Conditions for Swisse Sample Coupons" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Terms.aspx.cs" Inherits="Terms" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Contact Us / Swisse Vitamins</title>
<meta property="og:title" content="The Music Run Swisse Promotion – Terms" />
<meta name="keywords" content="The Music Run Swisse Promotion – Terms" />
<meta name="description" content="The Music Run Swisse Promotion – Terms" />
<link rel="canonical" href="" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="breadcrumbs color-blck">
    <div class="container">
    <p>Home &gt; Terms &amp; Conditions for Swisse Sample Coupons</p>
    </div>
</div>
<section class="container">
    <div class="termsContent">
        <h2>TERMS & CONDITIONS:</h2>
        <ul>
            <li>Coupon is valid with any purchase of any Swisse product.</li>
            <li>Coupon is to be presented upon purchase.</li>
            <li>Only original coupon is accepted.</li>
            <li>Coupon is not exchangeable for cash.</li>
            <li>No refund of unused portion of coupon.</li>
            <li>Coupon can be used with existing promotion of Swisse.</li>
            <li>Coupon can be used once for each receipt.</li>
            <li>P&G reserves the right to change the terms of use of coupon without prior notice.</li>
            <li>Coupon can be redeemed al all participating Watsons, Guardian, Unity and Robinson stores in Singapore.</li>
        </ul>
    </div>
    
<div class="clearfloat"></div>
</section>

</asp:Content>
