﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewProfile : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadProfile();
        }
    }
    public void LoadProfile() {

        UserCls GU = new UserCls();

        string Email = Session["Email"].ToString();
        GU = UserCls.GetUserProfile(Email);

        lblFN.Text = GU.userfName;
        lblLN.Text = GU.userlName;
        lblEmail.Text = Email;
        lblBDate.Text = GU.userBirthMonth + " " + GU.userBirthYear;
        lblPostCode.Text = GU.userPostCode;

        if (GU.checkAgreed == 1)
        {
            cbSignUp.Checked = true;
        }else{
            cbSignUp.Checked = false;
        }

    }
    protected void btnEditProfile_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditProfile.aspx");
    }
}
