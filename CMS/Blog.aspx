﻿<%@ Page Title="Blog" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="Blog.aspx.cs" Inherits="Blog" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
   
     <asp:Button ID="btnRelatedThemes" runat="server" Text="Related Themes" 
        onclick="btnRelatedThemes_Click" />
     <asp:Button ID="btnRelatedFormats" runat="server" Text="Related Formats" 
        onclick="btnRelatedFormats_Click" />
     <asp:Button ID="btnRelatedHealthAndHappiness" runat="server" 
        Text="Related Health And Happiness" 
        onclick="btnRelatedHealthAndHappiness_Click" />
     <asp:Button ID="btnRelatedProducts" runat="server" Text="Related Products" 
        onclick="btnRelatedProducts_Click" />
       <asp:Button ID="btnRelatedBlogs" runat="server" Text="Related Blogs" 
        onclick="btnRelatedBlogs_Click" />
    <p></p>
    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
    <p></p>
    <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
    <p></p>
      <asp:Label ID="lblContent" runat="server" Text="Content"></asp:Label>
      <p></p>
      <cc1:Editor ID="txtContent" runat="server" />
      <p></p>
       <asp:Label ID="lblLink" runat="server" Text="Link"></asp:Label>
    <p></p>
    <asp:TextBox ID="txtLink" runat="server"></asp:TextBox>
    <p></p>
   <asp:Label ID="Image" runat="server" Text="Image"></asp:Label>
    <p></p>
    <asp:TextBox ID="txtImage" runat="server"></asp:TextBox>
    <p></p>
    <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="CANCEL" 
        onclick="btnCancel_Click" />
</asp:Content>
