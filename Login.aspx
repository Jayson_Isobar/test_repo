﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Account_Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Member Login | Swisse Vitamins</title>
<meta property="og:title" content="Member Login | Swisse Vitamins" />
<meta name="keywords" content="vitamins, multivitamins, natural health, integrative health, vitamin supplements, subscribe, sign-up, membership, database, newsletters" />
<meta name="description" content="Log-in to Swisse to get the latest research news, lifestyle articles, recipes and tips from Australia’s No.1 Multivitamin Brand*." />
<link rel="canonical" href="http://www.swisse.com.sg/login" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                Login
            </p>
        </div>
    </div>
    
<section class="container" id="login">
  <div class="col_60 fltlft">
    <h2>sign in</h2>
    <h4 class="mtop15">sign in to your account</h4>
    <asp:Label ID="lblRegSuccess" runat="server" Visible="False" ForeColor="Red">You have successfully registered, please check your e-mail for confirmation</asp:Label>
    <div id="create-account-wrap" class="mtop20">
    <div class="col_32 fltlft">
    <label>Email</label>
     <asp:Label ID="lblErr" Visible="false" runat="server" ForeColor="Red">Incorrect Email or Password</asp:Label>
                
                <asp:TextBox ID="txtEmail" runat="server" CssClass="textEntry"></asp:TextBox>
               <asp:RegularExpressionValidator ForeColor = "Red" ID="REVEmail" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtEmail" ValidationExpression="^[_.@a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
    
                <asp:RequiredFieldValidator ForeColor = "Red" ID="UserNameRequired" runat="server" ControlToValidate="txtEmail"
                    CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="Email is required."
                    ValidationGroup="LoginUserValidationGroup">Email is required</asp:RequiredFieldValidator> 
    </div>    
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
    <label class="mtop20">Password</label>
     
                <asp:TextBox ID="txtPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ForeColor = "Red" ID="REVPass" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtPassword" ValidationExpression="^[a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
    
                <asp:RequiredFieldValidator ForeColor = "Red" ID="PasswordRequired" runat="server" ControlToValidate="txtPassword"
                    CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                    ValidationGroup="LoginUserValidationGroup">Password is required</asp:RequiredFieldValidator>
	</div>				
    <div class="clearfloat"></div>     
    <p class="mtop20">
	<asp:HyperLink ID="lnkForgotPass" runat="server" class="txt-bl">
                     <asp:Label ID="lblForgotPass" Visible="true" runat="server" ForeColor="Blue">Need help remembering your password?</asp:Label>
                </asp:HyperLink>
    </p>
	</div>
	      
    
	 <asp:Button ID="btnLogin" runat="server" class="btn color-rd" Text="Sign In" ValidationGroup="LoginUserValidationGroup"
                OnClick="btnLogin_Click" />
	
    <p class="mtop15">Don't have an account yet? </p>

	  <asp:HyperLink ID="lnkRegister" runat="server">
                     <asp:Label ID="lblRegisterText" Visible="true" runat="server" ForeColor="Blue">Create One!</asp:Label>
                </asp:HyperLink>
	
    <p class="mtop10"> <asp:CheckBox ID="RememberMe" runat="server" />
                <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Keep me signed in</asp:Label>   
  </p>
    </div>
   
    <div class="clearfloat"></div>
</section>

</asp:Content>
