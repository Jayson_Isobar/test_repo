
GO
/****** Object:  StoredProcedure [dbo].[sp_CountRecommended]    Script Date: 2/20/2015 5:04:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 20, 2015>
-- Description:	<COUNT Recommended>
-- exec sp_CountRecommended
-- =============================================
CREATE PROCEDURE [dbo].[sp_CountRecommended]
   
    @blogid int
AS
BEGIN
	
	SET NOCOUNT ON;


		SELECT COUNT(*) FROM tbl_Blog_Recommended 
	    WHERE blogid = @blogid
	


END
