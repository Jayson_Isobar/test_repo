﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class FAQRelatedProducts : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
            LoadDDLTheme();
        }
    }
    public void LoadDDLTheme()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProductList";
        SqlCommand cmd1 = new SqlCommand(spName, con);
        cmd1.CommandType = CommandType.StoredProcedure;
        con.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
        DataTable dtPC = new DataTable();
        adapter.Fill(dtPC);

        ddlTheme.DataSource = dtPC;

        ddlTheme.DataTextField = "productName";
        ddlTheme.DataValueField = "productid";
        ddlTheme.DataBind();

        con.Close();
    }
    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetRelatedFAQProducts";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@faqid", Request.QueryString["id"].ToString());
                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvBlog.DataSource = ds;
        gvBlog.DataBind();
        con.Close();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_InsertFAQProducts";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@faqid", Request.QueryString["id"].ToString());
        cmd.Parameters.AddWithValue("@productid", ddlTheme.SelectedValue);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        LoadData();
    }

    protected void gvBlog_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvBlog.EditIndex = e.NewEditIndex;
        LoadData();
    }

    protected void gvBlog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBlog.PageIndex = e.NewPageIndex;
        LoadData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("FAQEdit.aspx?id=" + Request.QueryString["id"].ToString());
    }
}
