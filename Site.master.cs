﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

public partial class Site : System.Web.UI.MasterPage
{
    String lnkLoginURL = ConfigurationManager.AppSettings["lnkLogin"];
    String lnkLogoutURL = ConfigurationManager.AppSettings["lnkLogout"];
    String imgLogo = ConfigurationManager.AppSettings["imgLogo"];
    String siteUrl = ConfigurationManager.AppSettings["siteUrl"];

    string menulink = "";
    string selected = "";
    string selected2 = "";
    string selected3 = "";
    string selected4 = "";
    string selected5 = "";
    string currentPage = HttpContext.Current.Request.Url.LocalPath;
    
    public string GetCDN()
    {
        string cdn = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["CDNdomain"]) ? ConfigurationManager.AppSettings["CDNdomain"] : "/";

        if (currentPage == "/login" || currentPage == "/register")
        {
            cdn = "/";
        }

        return cdn;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        litLogo.Text = "<a href='" + siteUrl + "'><img src='" + GetCDN() + imgLogo + "' width='141' height='53' alt='Logo Swisse'></a>";


        lnkLogin.NavigateUrl = lnkLoginURL;
        lnkLogout.NavigateUrl = lnkLogoutURL;



        if (Session["Email"] != null)
        {
            lblLogin.Visible = false;
            lblPipe.Visible = false;
            lblUN.Visible = true;
            lblUN.Text = "<strong>Welcome " + "<a href='ViewProfile'>" + Session["Email"].ToString() + "</a> " + "</strong>|";
            lblPipe2.Visible = true;
        }
        else
        {
            lblLogout.Visible = false;
        }

        menulink = Path.GetFileNameWithoutExtension(HttpContext.Current.Request.PhysicalPath);

        conditionalMenu();

        litShopProductsMenu.Text = "<a href='/products' class='" + selected + "'>Our Products</a>";
        litTheScienceOfSwisseMenu.Text = "<a href='/the-science-of-swisse' class='" + selected2 + "'>The Science of Swisse</a>";
        litOurStoryMenu.Text = "<a href='/our-story' class='" + selected3 + "'>Our Story</a>";
        litHealthAndHappinessBlogMenu.Text = "<a href='/health-happiness-blog' class='" + selected4 + "'>Health & Happiness Blog</a>";
        litOffersAndPromos.Text = "<a href='/offers-and-promos' class='last " + selected5 + "'>Offers & Promos</a>";

        LoadMenuHover();
        LoadProdCat();
        LoadProdIngredient();
        LoadProdConcern();
    }

    public void LoadMenuHover()
    {
        ShopProductsCls pc = new ShopProductsCls();
        pc = ShopProductsCls.GetShopProductHover();
        if(pc != null){
            string cat = pc.prodcatName.Replace("<img src='", "<img src='" + GetCDN());

        litShopProductsHover.Text = cat;
        }
    }

    public void conditionalMenu()
    {
        if (menulink == "ShopProducts" || menulink == "ProductDetails" || menulink == "ProductCategory" || menulink == "ProductIngredient" || menulink == "ProductConcern")
        {
            selected = "current";
        }
        if (menulink == "TheScienceOfSwisse")
        {
            selected2 = "current";
        }
        if (menulink == "OurStory")
        {
            selected3 = "current";
        }
        if (menulink == "HealthAndHappinessBlog")
        {
            selected4 = "current";
        }
        if (menulink == "OffersAndPromos")
        {
            selected5 = "current";
        }

    }

    public void LoadProdCat()
    {
        ShopProductsCls pc = new ShopProductsCls();
        pc = ShopProductsCls.GetProdCategories();
        string cat = pc.prodcatName;

        litProdCat.Text = cat;
        litProdCatHover.Text = cat;
    }
    public void LoadProdIngredient()
    {
        ShopProductsCls ing = new ShopProductsCls();
        ing = ShopProductsCls.GetProdIngredientsHome();
        string ingredient = ing.prodIngredientName;

        litProdIngredient.Text = ingredient;

        ShopProductsCls ing2 = new ShopProductsCls();
        ing2 = ShopProductsCls.GetProdIngredientsHover();
        string ingredient2 = ing2.prodIngredientName;

        litProdIngHover.Text = ingredient2;
    }
    public void LoadProdConcern()
    {
        ShopProductsCls con = new ShopProductsCls();
        con = ShopProductsCls.GetProdConcernsHome();
        string concern = con.prodConcernName;

        litProdConcern.Text = concern;

        ShopProductsCls con2 = new ShopProductsCls();
        con2 = ShopProductsCls.GetProdConcernsHover();
        string concern2 = con2.prodConcernName;

        litProdConHover.Text = concern2;
    }


    protected void lnkCountry_Click(object sender, EventArgs e)
    {
        //litSingapore.Text = "<a href='javascript:void(0);' data-dismiss='modal'><img src='images/Singapore.png' alt='Singapore'> Singapore</a>";
        //pnlModal.Visible = true;
    }
}
