﻿<%@ Page Title="Rejection Notice" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Rejection.aspx.cs" Inherits="Rejection" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Rejection | Swisse Vitamins</title>
<meta property="og:title" content="Rejection | Swisse Vitamins" />
<meta name="keywords" content="vitamins, multivitamins, natural health, integrative health, vitamin supplements, subscribe, sign-up, membership, database, newsletters" />
<meta name="description" content="Sign-up to Swisse to get the latest research news, lifestyle articles, recipes and tips from Australia’s No.1 Multivitamin Brand*." />
<link rel="canonical" href="http://www.swisse.com.sg/rejection" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                Rejection
            </p>
        </div>
    </div>

<section class="container">
  <span id="MainContent_lblUnderAge" style="color:Red;">We're sorry, but we can't accept your messages or personal information because you do not meet our eligibility requirements. But that doesn't mean you can't explore the rest of our site and learn more about us! Check out <a href="http://www.pg.com" target="_blank">www.pg.com</a> for information about P&amp;G and its brands.</span>
                <asp:Label ID="lblUnderAge" runat="server" 
                    Text="We're sorry, but we can't accept your messages or personal information because you do not meet our eligibility requirements. But that doesn't mean you can't explore the rest of our site and learn more about us! Check out www.pg.com for information about P&G and its brands." 
                    ForeColor="Red" Visible="False"></asp:Label>
           </section>

</asp:Content>
