﻿<%@ Page Title="View Ingredient" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewIng.aspx.cs" Inherits="HeaderNavigationFlyout_ViewIng" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

    <title><%= Item.MetaTitle %></title>
    <meta property="og:title" content="<%= Item.MetaOpenGraphTitle %>"  />
    <meta name="description" content="<%= Item.MetaDescription %>" />
    <meta name="keywords" content="<%= Item.MetaKeywords %>" />
    <link rel="canonical" href="<%= Item.CanonicalUrl %>" />

<link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/the-science-of-swisse">The Science of Swisse</a>
                <span class="mlr">></span>
                <a href="/the-science-of-swisse/ingredients-glossary"">Ingredients Glossary</a>
                <span class="mlr">></span>
                <asp:Literal ID="litBreadCrumbs" runat="server"></asp:Literal>
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>

<section class="blog-content">
    <div class="container">
        <div class="col_60 fltlft">
            <div class="main-content">
                <h4 class="txt-rd bdr-bottom inblock">Ingredients Glossary</h4>
                <asp:Literal ID="litContent" runat="server"></asp:Literal>
            </div>
            <div class="clearfloat"></div>
            <div class="mtop70">
                <h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
                <asp:Literal ID="litBlogRelatedProd" runat="server"></asp:Literal>
                <div class="clearfloat"></div>              
            </div>
        </div>
        <div class="col_32 fltrt">
            <div class="color-dgray txt-w">
                <div class="pd20">
                    <h4>related ingredients</h4>
                    <div class="list">
                        <ul>
                            <asp:Literal ID="litRelatedIng" runat="server"></asp:Literal>
                        </ul>
                    </div>
                </div>
            </div>
            <a href="/products">
                <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" class="mtop20">
            </a>
        </div>
        <div class="clearfloat"></div>
    </div>
</section>

</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>

</asp:Content>