﻿<%@ Page Title="Minor Age" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MinorAge.aspx.cs" Inherits="MinorAge" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs color-blck">
<div class="container">
<p>Home</p>
</div>
</section>

<section class="container">
  

    <h2>Minor Age</h2>
   <%--<asp:Label ID="Label1" runat="server" Text="Age should be 13 years old and above to register!" ForeColor="Red">--%>
    <p>We're sorry, but we can't accept your messages or personal information because you do not meet our eligibility requirements. But that doesn't mean you can't explore the rest of our site and learn more about us! Check out <a href="http://www.pg.com">www.pg.com</a> for information about P&G and its brands.</p>
   </asp:Label>
</section>

</asp:Content>
