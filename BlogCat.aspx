﻿<%@ Page Title="Blog Category" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="BlogCat.aspx.cs" Inherits="BlogCat" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Swisse Health & Happiness Blog | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Multivitamins Health & Happiness Blog" />
<meta name="keywords" content="vitamins, multivitamins, nutrition, recipes, yoga, exercise, weight management, lifestyle hacks, natural health, integrative health, vitamin supplements" />
<meta name="description" content="Check out the Swisse Health & Happiness blog for the latest research news, lifestyle articles, recipes and tips from Australia’s No.1 Multivitamin Brand*." />
<link rel="canonical" href="http://www.swisse.com.sg/health-happiness-blog" />

<link href="<%=this.CDNdomain %>css/featherlight.css" rel="stylesheet" type="text/css">
<script src="<%=this.CDNdomain %>js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="<%=this.CDNdomain %>js/featherlight.js" type="text/javascript"></script>

<script type="text/javascript">
        $(document).ready(function ($) {
            $('.blog-landing-col-img-box').on('click', function () {
                window.location.href = $(this).parents('.fltlft').find('.blog-landing-col-title > h4 > a').attr('href')
            });
        });

</script>
    <style>
        .blog-landing-col-img-box {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/health-happiness-blog">Health & Happiness Blog</a>
                <span class="mlr">></span>
                <span id="cat"></span>
            </p>
        </div>
    </div>

<section class="container">
<h2>Health & Happiness Blog</h2>
<div class="col_39 fltlft color-dgray mtop20">
<div class="sidebar-menu bdr-bottom list">
<h4>Health & Happiness</h4>
<ul>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</ul>
</div>
<div class="sidebar-menu bdr-bottom">
<h4>Themes</h4>
<form action="themes" method="get">
<ul>
<asp:CheckBoxList ID="cbBlogTheme" runat="server"  DataSourceID="sdsBlogTheme" DataTextField="blogTheme"
          DataValueField="blogThemeID"  AutoPostBack="True"  OnSelectedIndexChanged="cbBlogTheme_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsBlogTheme" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetBlogThemesActive" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtBlogTheme" runat="server" Style="visibility: hidden"></asp:TextBox>
</ul>
</form>
</div>
<div class="sidebar-menu">
<h4>Format</h4>
<ul>
<asp:CheckBoxList ID="cbBlogFormat" runat="server"  DataSourceID="sdsBlogFormat" DataTextField="blogFormat"
          DataValueField="blogFormatID"  AutoPostBack="True"  OnSelectedIndexChanged="cbBlogFormat_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsBlogFormat" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetBlogFormatsActive" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtBlogFormat" runat="server" Style="visibility: hidden"></asp:TextBox>
</ul>
</div>
</div>
<div class="col_721 fltrt mtop20">
    <%--<div class="color-blck redefine bdr-bottom"><h4>Viewing</h4>
	<span  id="vs">
	<input name="vsearch" type="text" class="searchbox viewingSearch">
	<img src="<%=this.CDNdomain %>images/x-icon.png" class="sRemoveicon" id="sRI" value="fsfs">
	</span>
  </div>--%>
  <div class="color-blck redefine"><h4><asp:Label ID="lblTotalItems" runat="server"></asp:Label></h4></div>
  <div class="blog-landing-col">
  <asp:Literal ID="litBlog" runat="server"></asp:Literal>
</div>
</div>
<div class="clearfloat"></div>
</section>

</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script type="text/javascript">
        $(document).ready(function () {
            var GetCat = window.location.href.split("cat=")[1];

            if (GetCat == 1) {
                $("#cat").text("Healthy Eating");
            } else if (GetCat == 2) {
                $("#cat").text("Healthy Living");
            };

            $(".blogs h3").height($(".blogs h3:first-child").height());

        });
    </script>

</asp:Content>