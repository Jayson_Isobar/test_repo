﻿<%@ Page Title="FAQs" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FAQs.aspx.cs" Inherits="HeaderNavigationFlyout_FAQs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<section class="container">
<h2>the science of swisse</h2>
<div class="col_14 fltlft color-dgray mtop20">
<div class="sidebar-menu list">
<h4>The science of Swisse</h4>
<ul>
<li><a href="OurResearch.aspx">our research</a></li>
<li><a href="OurScientificPartnership.aspx">our scientific partnerships</a></li>
<li><a href="ClinicalTrials.aspx">clinical trials</a></li>
<li><a href="IngredientsGlossary.aspx">ingredients glossary</a></li>
<li><a href="#" class="current">faqs</a></li>
<li><a href="SwisseScientificAdvisoryPanel.aspx">swisse scientific advisory panel</a></li>
</ul>
</div>
</div>
<div class="col_72 fltrt mtop20">
  <img src="<%=this.CDNdomain %>images/faq.jpg" alt="FAQ" usemap="#Map">
  <map name="Map">
    <area shape="rect" coords="0,0,720,400" href="/our-story/contact-us">
  </map>

 <asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
</section>

</asp:Content>
