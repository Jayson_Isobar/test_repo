﻿<%@ Page Title="Search Result" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SearchResult.aspx.cs" Inherits="SearchResult" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs">
    <div class="container">
        <p>
            <a href="/index.aspx">Home</a>
            <span class="mlr">></span>
            Shop Products
        </p>
    </div>
</section>
<section id="searchResult" class="container">

    <div class="col_76 fltlft">
        <h2>search results for "<asp:label ID="lblSearch" text="Lorem Ipsum" runat="server" />"</h2>
        <div class="tabContainer">
            <ul class="tabs">
			    <li><h3><a href="javascript:void(0);">Product Result (<asp:label ID="lblProductCount" text="6" runat="server" />)</a></h3></li>
			    <li><h3><a href="javascript:void(0);">Content Result (<asp:label ID="lblContentCount" text="6" runat="server" />)</a></h3></li>
		    </ul>
            <div class="clearfloat"></div>

            <ul class="tabsContent">
                <li>
                    <div id="sorting">
                        <div class="sortby fltrt">
                            <div id="view" class="fltlft">
                                <a href="javascript:void(0);" id="gridView"><img src="<%=this.CDNdomain %>images/sort.jpg" alt="Sort" /></a>
                                <a href="javascript:void(0);" id="listView"><img src="<%=this.CDNdomain %>images/sort2.jpg" alt="Sort" /></a>
                            </div>
                            <h4 class="fltlft">SORT</h4>
                            <div id="ddl" class="fltlft">
                                <asp:dropdownlist ID="ddlSort" runat="server" AutoPostBack="True" name="sortby">
                                    <asp:ListItem>Most Popular</asp:ListItem>
                                    <asp:ListItem>Alphabetically</asp:ListItem>
                                </asp:dropdownlist>
                            </div>
                        </div>  
                        <div class="clearfloat"></div>   
                    </div>
                    <asp:literal ID="litProductResult" text="" runat="server" />
                </li>
                <li>

                <!-- LOOP STARTS -->
                <asp:Repeater ID="Repeater_ContentSearchResults" runat="server">
                <ItemTemplate>

              
                    <div class="contents">

                            <a href="<%# Eval("LinkUrl") %>" style='<%# Eval("PhotoUrl") != null ? string.Empty : "display:none;" %>'>
                                <img src="<%# Eval("PhotoUrl") %>" alt="Search" class="contentImg fltlft" />
                            </a>

                   
                            <p class="txt-up txt12 btn"><%# Eval("Header") %></p>
                            
                            <h4>
                                <a href="<%# Eval("LinkUrl") %>">
                                    <%# Eval("Title") %>
                                </a>
                            </h4>
                            
                            <p><%# Eval("Description") %></p>
                        </div>
                        <div class="clearfloat"></div>
                 
                  
                        </ItemTemplate>
                </asp:Repeater>

                    


                    <!-- LOOP ENDS -->



                    <!-- 
                    
                    <div class="contents">
                        <a href="#"><img src="<%=this.CDNdomain %>images/search.jpg" alt="Search" class="contentImg fltlft" /></a>
                        <p class="txt-up txt12 btn">Article</p>
                        <h4><a href="#">Nutrition: Are you eating healthily?</a></h4>
                        <p>... excerpt of page where term appears lorem ipsum dolor sit amet morbi pellentesque orci vel velit blandit, sit amet sollicitudin sapien blandit....</p>
                    </div>
                    <div class="clearfloat"></div>

                    <div class="contents">
                        <a href="#"><img src="<%=this.CDNdomain %>images/search.jpg" alt="Search" class="contentImg fltlft" /></a>
                        <p class="txt-up txt12 btn">Article</p>
                        <h4><a href="#">Nutrition: Are you eating healthily?</a></h4>
                        <p>... excerpt of page where term appears lorem ipsum dolor sit amet morbi pellentesque orci vel velit blandit, sit amet sollicitudin sapien blandit....</p>
                    </div>
                    <div class="clearfloat"></div>

                    <div class="contents">
                        <p class="txt-up txt12 btn">Article</p>
                        <h4><a href="#">Nutrition: Are you eating healthily?</a></h4>
                        <p>... excerpt of page where term appears lorem ipsum dolor sit amet morbi pellentesque orci vel velit blandit, sit amet sollicitudin sapien blandit....</p>
                    </div>
                    <div class="clearfloat"></div>

                    <div class="contents">
                        <p class="txt-up txt12 btn">Article</p>
                        <h4><a href="#">Nutrition: Are you eating healthily?</a></h4>
                        <p>... excerpt of page where term appears lorem ipsum dolor sit amet morbi pellentesque orci vel velit blandit, sit amet sollicitudin sapien blandit....</p>
                    </div>
                    <div class="clearfloat"></div>

                    <div class="contents">
                        <a href="#"><img src="<%=this.CDNdomain %>images/search.jpg" alt="Search" class="contentImg fltlft" /></a>
                        <p class="txt-up txt12 btn">Article</p>
                        <h4><a href="#">Nutrition: Are you eating healthily?</a></h4>
                        <p>... excerpt of page where term appears lorem ipsum dolor sit amet morbi pellentesque orci vel velit blandit, sit amet sollicitudin sapien blandit....</p>
                    </div>
                    <div class="clearfloat"></div>

                    <div class="contents">
                        <a href="#"><img src="<%=this.CDNdomain %>images/search.jpg" alt="Search" class="contentImg fltlft" /></a>
                        <p class="txt-up txt12 btn">Article</p>
                        <h4><a href="#">Nutrition: Are you eating healthily?</a></h4>
                        <p>... excerpt of page where term appears lorem ipsum dolor sit amet morbi pellentesque orci vel velit blandit, sit amet sollicitudin sapien blandit....</p>
                    </div>
                    <div class="clearfloat"></div>
                    -->
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfloat"></div>

</section>


</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script type="text/javascript" src="<%=CDNdomain %>js/tab.js"></script>

    <script type="text/javascript" src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>

    <script type="text/javascript">
        (function ($) {
            var prodContainer = $(".products-container");
            var prodDetail = $(".productDetail");
            var gridView = $("#gridView");
            var listView = $("#listView");
            var prodBox = $(".products-box");
            var view = "grid"; // default
           
            gridView.click(function () {
                if (view != "grid") {
                    prodContainer.hide();
                    prodDetail.addClass("display-none");
                    prodBox.removeClass("list");
                    setTimeout(function () {
                        prodContainer.fadeIn();
                    }, 300);

                    view = "grid";
                };
            });

            listView.click(function () {
                if (view != "list") {
                    prodContainer.hide();
                    prodDetail.removeClass("display-none");
                    prodBox.addClass("list");
                    setTimeout(function () {
                        prodContainer.fadeIn();
                    }, 300);

                    view = "list";
                };
            });


        } (jQuery));


        $(function () {
            $("#txtSearch").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#txtSearchIcon").click();
                }
            });
        });
    </script>

</asp:Content>
