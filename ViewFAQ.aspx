﻿<%@ Page Title="View FAQ" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewFAQ.aspx.cs" Inherits="HeaderNavigationFlyout_ViewFAQ" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  <section class="blog-content">
<section class="breadcrumbs2">
<div class="container">
<ul>
<li>Home &rsaquo;</li>
<li>FAQs</li>
<div class="fltrt">
<p class="txt-up"><a href="#">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a></p>
</div>
</ul>
</div>
</section>
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">FAQs</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litBlogRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related blogs</h4>
<div class="list">
<ul>
<asp:Literal ID="litRelatedBlog" runat="server"></asp:Literal>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="32,297,162,338" href="#">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>

</asp:Content>
