﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_CorporateSocialResponsibility : BaseForm
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetOurStoryContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@link", "CorporateSocialResponsibility.aspx");
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();

        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                var content = myRow[0].ToString().Replace("<img src=\"", "<img src=\"" + base.CDNdomain);
                litContent.Text = content; 
                //litContent.Text = myRow[0].ToString();
            }
        }

        con.Close();
        LoadRelatedProducts();
    }
    public void LoadRelatedProducts()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName2 = "";
        spName2 = "sp_GetRelatedProductsCorporateSocialList";
        SqlCommand cmd2 = new SqlCommand(spName2, con);
        cmd2.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr2 = cmd2.ExecuteReader();
        DataTable dt2 = new DataTable();
        dt2.Load(dr2);

        if (dt2.Rows.Count > 0)
        {
            foreach (DataRow myRow2 in dt2.Rows)
            {
                lblRP.Visible = true;
                litAboutSwisseRelatedProd.Text += "<div class='recentlyviewed-box'>" +
                "<img src='images/" + myRow2[2].ToString() + "' width='128' height='196' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                    " <div class='product-details-wrap-recent'>" +
                    "  <p class='product-name'>" +
                    "<a href='ProductDetails.aspx?id=" + myRow2[0].ToString() + "'>" +
                    myRow2[1].ToString() + 
                    "</a>" +
                    "</p>" +
                     " <div class='product-rate'>" +
                   "  <span class='rating'></span>" +
                       " <div class='clearfloat'></div>" +
                     " </div>" +
                      "<div class='clearfloat'></div>" +
                      "<div class='product-price'><p>SGD " + myRow2[3].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                      "<div class='clearfloat'></div>" +
                      "</div>" +
                "</div>";

            }
        }
        con.Close();
    }
}