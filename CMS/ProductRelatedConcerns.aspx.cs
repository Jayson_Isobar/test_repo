﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ProductRelatedConcerns : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
                LoadData();
                LoadDDLProdConcern();
        }
    }

    public void LoadDDLProdConcern()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProdConcernListActive";
        SqlCommand cmd1 = new SqlCommand(spName, con);
        cmd1.CommandType = CommandType.StoredProcedure;
        con.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
        DataTable dtPC = new DataTable();
        adapter.Fill(dtPC);

        ddlProdConcern.DataSource = dtPC;

        ddlProdConcern.DataTextField = "prodconcernName";
        ddlProdConcern.DataValueField = "prodconcernid";
        ddlProdConcern.DataBind();

        con.Close(); 
    }
 
    protected void btnBackView_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdRelatedConcernList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.Parameters.AddWithValue("@productID", Request.QueryString["cid"]);
                command.CommandType = CommandType.StoredProcedure;
       
                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdConcern.DataSource = ds;
        gvProdConcern.DataBind();
        con.Close();

    }


    protected void gvProdConcernRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProductRelatedConcernByID";
            cmd.Parameters.AddWithValue("@id", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
      
    }

    protected void gvProdConcern_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProdConcern.EditIndex = e.NewEditIndex;
        LoadData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedConcerns.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProdConcern.Visible = false;
        pnlViewProdConcern.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProdRelatedConcern";
        cmd.Parameters.AddWithValue("@productID", Request.QueryString["cid"].ToString());
        cmd.Parameters.AddWithValue("@prodconcernID", ddlProdConcern.SelectedValue);
        

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("Products.aspx?cid="+ Request.QueryString["cid"].ToString());
    }

    protected void gvProdConcern_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProdConcern.PageIndex = e.NewPageIndex;
        LoadData();
    }
    public void PendingRecordsGridview_RowDeleting(Object sender, GridViewDeleteEventArgs e)
    {
        Response.Redirect("ProductRelatedConcerns.aspx?cid=" + Request.QueryString["cid"].ToString());
    } 
}
