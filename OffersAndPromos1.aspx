﻿<%@ Page Title="Offers And Promos" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OffersAndPromos1.aspx.cs" Inherits="HeaderNavigationFlyout_OffersAndPromos1" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Win With Swisse! | Swisse Vitamins & Supplements</title>
<meta property="og:title" content="The Music Run - Swisse Terms" />
<meta name="keywords" content="vitamins, multivitamins, nutrition, recipes, yoga, exercise, weight management, lifestyle hacks, natural health, integrative health, vitamin supplements, the music run, singapore" />
<meta name="description" content="Terms & Conditions for The Music Run promotion from Swisse Singapore." />
<link rel="canonical" href="http://www.swisse.com.sg/tmrterms" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/offersandpromos">Offers & Promos</a>
                <span class="mlr">></span>
                Win a Trip for 2 to an Exclusive Private Island in Australia
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>

<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
    <h4 class="txt-rd bdr-bottom inblock">offers & promos</h4>
    <asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>

</div>
<div class="col_32 fltrt">
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>

</asp:Content>
