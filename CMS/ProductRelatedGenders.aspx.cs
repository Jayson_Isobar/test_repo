﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ProductRelatedGenders : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
                LoadData();
                LoadDDLProdGender();
        }
    }

    public void LoadDDLProdGender()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetGenderList";
        SqlCommand cmd1 = new SqlCommand(spName, con);
        cmd1.CommandType = CommandType.StoredProcedure;
        con.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
        DataTable dtPC = new DataTable();
        adapter.Fill(dtPC);

        ddlProdGender.DataSource = dtPC;

        ddlProdGender.DataTextField = "gender";
        ddlProdGender.DataValueField = "id";
        ddlProdGender.DataBind();

        con.Close(); 
    }
 
    protected void btnBackView_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdRelatedGenderList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.Parameters.AddWithValue("@productID", Request.QueryString["cid"]);
                command.CommandType = CommandType.StoredProcedure;
       
                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdGender.DataSource = ds;
        gvProdGender.DataBind();
        con.Close();

    }


    protected void gvProdGenderRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProductRelatedGenderByID";
            cmd.Parameters.AddWithValue("@id", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
      
    }

    protected void gvProdGender_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProdGender.EditIndex = e.NewEditIndex;
        LoadData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedGenders.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProdGender.Visible = false;
        pnlViewProdGender.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProdRelatedGender";
        cmd.Parameters.AddWithValue("@productID", Request.QueryString["cid"].ToString());
        cmd.Parameters.AddWithValue("@prodgenderID", ddlProdGender.SelectedValue);
        

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("Products.aspx?cid="+ Request.QueryString["cid"].ToString());
    }

    protected void gvProdGender_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProdGender.PageIndex = e.NewPageIndex;
        LoadData();
    }
    public void PendingRecordsGridview_RowDeleting(Object sender, GridViewDeleteEventArgs e)
    {
        Response.Redirect("ProductRelatedGenders.aspx?cid=" + Request.QueryString["cid"].ToString());
    } 
}
