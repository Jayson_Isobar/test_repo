﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Products : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["cid"] != null)
            {
                LoadViewpnlViewProd();
                btnViewProdIngredients.Visible = true;
                btnViewProdConcerns.Visible = true;
                btnViewProdAge.Visible = true;
                btnViewProdGender.Visible = true;
                btnViewProdProduct.Visible = true;
                btnViewProdTitleTags.Visible = true;
                btnViewProdKeywords.Visible = true;
                btnViewProdMetaOpen.Visible = true;
            }
            else
            {
                LoadData();
            }
            LoadDDLProdCat();

        }

    }

    public void LoadDDLProdCat() {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProdCatListActive";
        SqlCommand cmd1 = new SqlCommand(spName, con);
        cmd1.CommandType = CommandType.StoredProcedure;
        con.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
        DataTable dtPC = new DataTable();
        adapter.Fill(dtPC);

        ddlProdCat.DataSource = dtPC;

        ddlProdCat.DataTextField = "prodcatName";
        ddlProdCat.DataValueField = "prodcatid";
        ddlProdCat.DataBind();

        con.Close(); 
    }

    protected void btnViewDeleted_Click(object sender, EventArgs e)
    {
        gvProd.Columns[0].Visible = false;
        btnBackView.Visible = true;
        btnViewDeleted.Visible = false;
        LoadData2();
    }

    protected void btnBackView_Click(object sender, EventArgs e)
    {
        gvProd.Columns[0].Visible = true;
        btnBackView.Visible = false;
        btnViewDeleted.Visible = true;
        LoadData();
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProductList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProd.DataSource = ds;
        gvProd.DataBind();
        con.Close();

    }

    public void LoadData2()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProductDeletedList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProd.DataSource = ds;
        gvProd.DataBind();
        con.Close();

    }

    protected void gvProdRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            var someId = e.CommandArgument;
            Response.Redirect("Products.aspx?cid=" + someId.ToString(), false);
        }
        else if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProductByID";
            cmd.Parameters.AddWithValue("@productID", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Redirect("Products.aspx");
        }
    }

    protected void gvProd_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProd.EditIndex = e.NewEditIndex;
        LoadData();
    }

    public void LoadViewpnlViewProd()
    {
        pnlGVProd.Visible = false;
        pnlViewProd.Visible = true;
        btnUpdate.Visible = true;

        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProductDetailsByID";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@productID", Request.QueryString["cid"].ToString());
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            ddlProdCat.SelectedValue = dt.Rows[0]["prodcatid"].ToString().Trim();
            txtProdName.Text = dt.Rows[0]["productName"].ToString().Trim();
            txtProdDesc.Content = dt.Rows[0]["productDesc"].ToString().Trim();
            txtProdShortDesc.Text = dt.Rows[0]["productShortDesc"].ToString().Trim();
            txtProdImage.Text = dt.Rows[0]["productimage"].ToString().Trim();

txtProdSize.Text = dt.Rows[0]["productSize"].ToString().Trim();
txtProdSize2.Text = dt.Rows[0]["productSize2"].ToString().Trim();
 txtPrice.Text = dt.Rows[0]["productPrice"].ToString().Trim();
 txtPrice2.Text = dt.Rows[0]["productPrice2"].ToString().Trim();
 txtGTIN.Text = dt.Rows[0]["productGTIN"].ToString().Trim();
 txtGTIN2.Text = dt.Rows[0]["productGTIN2"].ToString().Trim();
 txtProdForm.Text = dt.Rows[0]["productForm"].ToString().Trim();
 txtCanonicalURL.Text = dt.Rows[0]["productCanonicalURL"].ToString().Trim();
 txtMetaDesc.Text = dt.Rows[0]["productMetaDescription"].ToString().Trim();

            if (dt.Rows[0]["productStatus"].ToString().Trim() == "1")
            {
                cbStatus.Checked = true;
            }
            else {
                cbStatus.Checked = false;
            }

            txtProdBenefits.Content = dt.Rows[0]["productBenefit"].ToString().Trim();
            txtProdIng.Content = dt.Rows[0]["productIngredient"].ToString().Trim();
            txtProdDir.Content = dt.Rows[0]["productDirection"].ToString().Trim();
        }
        con.Close();

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_UpdateProductByID";
        cmd.Parameters.AddWithValue("@prodcatID", ddlProdCat.SelectedValue);
        cmd.Parameters.AddWithValue("@productID", Request.QueryString["cid"]);
        cmd.Parameters.AddWithValue("@productName", txtProdName.Text.Trim());
        cmd.Parameters.AddWithValue("@productDesc", txtProdDesc.Content.Trim());
        cmd.Parameters.AddWithValue("@productShortDesc", txtProdShortDesc.Text.Trim());
        cmd.Parameters.AddWithValue("@productImage", txtProdImage.Text.Trim());
	cmd.Parameters.AddWithValue("@productSize", txtProdSize.Text.Trim());
        cmd.Parameters.AddWithValue("@productSize2", txtProdSize2.Text.Trim());
        cmd.Parameters.AddWithValue("@productPrice", txtPrice.Text.Trim());
        cmd.Parameters.AddWithValue("@productPrice2", txtPrice2.Text.Trim());
        cmd.Parameters.AddWithValue("@productGTIN", txtGTIN.Text.Trim());
        cmd.Parameters.AddWithValue("productGTIN2", txtGTIN2.Text.Trim());
        cmd.Parameters.AddWithValue("@productForm", txtProdForm.Text.Trim());
        cmd.Parameters.AddWithValue("@productCanonicalURL", txtCanonicalURL.Text.Trim());
        cmd.Parameters.AddWithValue("@productMetaDescription", txtMetaDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else{
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@productStatus", cbStat);

        cmd.Parameters.AddWithValue("@productBenefit", txtProdBenefits.Content.Trim());
        cmd.Parameters.AddWithValue("@productIngredient", txtProdIng.Content.Trim());
        cmd.Parameters.AddWithValue("@productDirection", txtProdDir.Content.Trim());

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("Products.aspx"); 
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProd.Visible = false;
        pnlViewProd.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProduct";
        cmd.Parameters.AddWithValue("@productCat", ddlProdCat.SelectedValue);
        cmd.Parameters.AddWithValue("@productName", txtProdName.Text.Trim());
        cmd.Parameters.AddWithValue("@productDesc", txtProdDesc.Content.Trim());
        cmd.Parameters.AddWithValue("@productShortDesc", txtProdShortDesc.Text.Trim());
        cmd.Parameters.AddWithValue("@productImage", txtProdImage.Text.Trim());
        cmd.Parameters.AddWithValue("@productSize", txtProdSize.Text.Trim());
        cmd.Parameters.AddWithValue("@productSize2", txtProdSize2.Text.Trim());
        cmd.Parameters.AddWithValue("@productPrice", txtPrice.Text.Trim());
        cmd.Parameters.AddWithValue("@productPrice2", txtPrice2.Text.Trim());
        cmd.Parameters.AddWithValue("@productGTIN", txtGTIN.Text.Trim());
        cmd.Parameters.AddWithValue("productGTIN2", txtGTIN2.Text.Trim());
        cmd.Parameters.AddWithValue("@productForm", txtProdForm.Text.Trim());
        cmd.Parameters.AddWithValue("@productCanonicalURL", txtCanonicalURL.Text.Trim());
        cmd.Parameters.AddWithValue("@productMetaDescription", txtMetaDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else
        {
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@productStatus", cbStat);
        cmd.Parameters.AddWithValue("@productBenefit", txtProdBenefits.Content.Trim());
        cmd.Parameters.AddWithValue("@productIngredient", txtProdIng.Content.Trim());
        cmd.Parameters.AddWithValue("@productDirection", txtProdDir.Content.Trim());

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("Products.aspx"); 
    }

    protected void gvProd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProd.PageIndex = e.NewPageIndex;
        LoadData();
    }

    protected void btnViewProdIngredients_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedIngredients.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdConcerns_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedConcerns.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdAge_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedAges.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdGender_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedGenders.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdProduct_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedProducts.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdTitleTags_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedTitleTags.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdKeywords_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedKeywords.aspx?cid=" + Request.QueryString["cid"]);
    }
    protected void btnViewProdMetaOpen_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedMetaOpen.aspx?cid=" + Request.QueryString["cid"]);
    }
}
