﻿<%@ Page Title="Product Concerns" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductConcerns.aspx.cs" Inherits="ProductConcerns" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT CONCERN" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Product Concerns List" 
        onclick="btnBackView_Click" Visible="False" />
        <p></p>
         <asp:Button ID="btnViewDeleted" runat="server" Text="View Deleted Product Concerns List" 
        onclick="btnViewDeleted_Click" />
        <p></p>
    <asp:Panel ID="pnlGVProdConcern" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdConcern" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdConcernRowCommand"
            OnRowEditing="gvProdConcern_RowEditing"
            OnPageIndexChanging="gvProdConcern_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" AllowSorting="False" PageSize="10">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                        <asp:LinkButton Text="Update Details" ID="lnkUpdate" runat="server" CommandName="Edit"
                        CommandArgument='<%# Eval("prodconcernid") %>'/>    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("prodconcernid") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Concern Name" SortExpression="prodconcernName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodconcernName" runat="server" Text='<%# Bind("prodconcernName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Product Concern Description" SortExpression="prodconcernDesc">
                    <ItemTemplate>
                        <asp:Label ID="lblprodconcernDesc" runat="server" Text='<%# Bind("prodconcernDesc") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Status" SortExpression="status">
                    <ItemTemplate>
                        <asp:Label ID="lblprodconcernStatus" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdConcern" runat="server" Visible="False">
             <asp:Label ID="Label1" runat="server" Text="Product Concern Name"></asp:Label>
             <asp:TextBox ID="txtProdConcernName" runat="server"></asp:TextBox>
             <p></p>
             <asp:Label ID="Label2" runat="server" Text="Product Concern Description"></asp:Label>
             <asp:TextBox ID="txtProdConcernDesc" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label3" runat="server" Text="Active ?"></asp:Label>
             <asp:CheckBox ID="cbStatus" runat="server" Checked="True" />
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnUpdate" runat="server" Text="UPDATE" onclick="btnUpdate_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
