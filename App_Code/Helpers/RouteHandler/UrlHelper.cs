﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class UrlHelper
{

   
    public static string FormatPageUrl(string id,PageType.Type pageType)
    {
        var pageUrl = XmlHelper.GetPageUrl(id, pageType);
        VirtualPathData virtualPathData = null;
        if (pageType == PageType.Type.Product || pageType == PageType.Type.Category || pageType == PageType.Type.ProductIngredient || pageType == PageType.Type.Concern || pageType == PageType.Type.Ingredient)
        {
          virtualPathData = RouteTable.Routes.GetVirtualPath(null, "View Item", new RouteValueDictionary {{ "Page", "products" } , { "PageUrl", pageUrl } });
        }

        else if (pageType == PageType.Type.Ingredients)
        {
            virtualPathData = RouteTable.Routes.GetVirtualPath(null, "View Item", new RouteValueDictionary { { "Page", "the-science-of-swisse" }, { "PageUrl", pageUrl } });
        }

        else if (pageType == PageType.Type.BlogItem || pageType == PageType.Type.BlogCategory)
        {
            virtualPathData = RouteTable.Routes.GetVirtualPath(null, "View Item", new RouteValueDictionary { { "Page", "health-happiness-blog" }, { "PageUrl", pageUrl } });
        }

        else if (pageType == PageType.Type.Promo)
        {
            virtualPathData = RouteTable.Routes.GetVirtualPath(null, "View Item", new RouteValueDictionary { { "Page", "offers-promos" }, { "PageUrl", pageUrl } });
        }
       
        if (virtualPathData != null)
        {
            return virtualPathData.VirtualPath;
        }

        return string.Empty;
    }

  

    public static IHttpHandler GetNotFoundHttpHandler()
    {
        return BuildManager.CreateInstanceFromVirtualPath("~/404.aspx", typeof(Page)) as Page;
    }
}