﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ForgotPass.aspx.cs" Inherits="ForgotPass" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs color-blck">
<div class="container">
<p>Home</p>
</div>
</section>

<section class="container">
  
    <div class="col_60 fltlft">

    <h2>Recover your password</h2>
    <div id="create-account-wrap" class="mtop20">
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
    <label>Email</label>
     <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
     <asp:RegularExpressionValidator ForeColor = "Red" ID="REVEmail" runat="server" ErrorMessage="Please enter only alphanumeric." 
          ControlToValidate="txtEmail" ValidationExpression="^[_.@a-zA-Z0-9]+$" ValidationGroup="LoginUserValidationGroup"></asp:RegularExpressionValidator>
    
     <asp:RequiredFieldValidator ForeColor = "Red" ID="UserNameRequired" runat="server" ControlToValidate="txtEmail"
                    CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="Email is required."
                    ValidationGroup="LoginUserValidationGroup">Email is required</asp:RequiredFieldValidator> 
    </div>
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
    <label>New Password</label>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>
        <asp:RegularExpressionValidator ForeColor = "Red" ID="REVPass" runat="server" ErrorMessage="Please enter only alphanumeric." 
          ControlToValidate="txtPassword" ValidationExpression="^[a-zA-Z0-9]+$" ValidationGroup="LoginUserValidationGroup"></asp:RegularExpressionValidator>
    
       <asp:RequiredFieldValidator ForeColor = "Red" ID="PasswordRequired" runat="server" ControlToValidate="txtPassword"
                    CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                    ValidationGroup="LoginUserValidationGroup">Password is required</asp:RequiredFieldValidator>
    </div>
    <div class="clearfloat"></div>
    
    </div>
    <div class="clearfloat"></div>

    <asp:Button ID="btnSave" runat="server" class="btn color-rd" Text="Save" ValidationGroup="LoginUserValidationGroup"
        onclick="btnSave_Click"></asp:Button>
    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>

</asp:Content>
