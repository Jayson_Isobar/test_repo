﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for HealthAndHappinessBlogCls
/// </summary>
public class HealthAndHappinessBlogCls
{
 
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;

    public string healthandhappinessblog { get; set; }
    public string blogTheme { get; set; }
    public string blogFormat { get; set; }
    public string totalBlog { get; set; }
    public string userid { get; set; }
    public string blogID { get; set; }

    public static HealthAndHappinessBlogCls GetHealthAndHappinessBlogCls()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetHealthAndHappinessBlogList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        HealthAndHappinessBlogCls rc;
        rc = new HealthAndHappinessBlogCls();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                rc.healthandhappinessblog += "<li><a href='" + UrlHelper.FormatPageUrl(myRow[0].ToString(), PageType.Type.BlogCategory) + "''>" + myRow[1].ToString() + "</a></li>";
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }

    public static HealthAndHappinessBlogCls GetBlogFilter(string Theme, string Format)
    {
        if (string.IsNullOrEmpty(Theme))
            Theme = null;
        if (string.IsNullOrEmpty(Format))
            Format = null;
        string blogs = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetHealthAndHappinessBlogList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        HealthAndHappinessBlogCls rc;

        rc = new HealthAndHappinessBlogCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }

                rc.healthandhappinessblog += "<h3 class='mlft12'>" + myRow[1].ToString() + "</h3>";

                SqlCommand cmd2 = new SqlCommand("sp_GetBlogsHealthHappinessCategory2", con);
                SqlDataAdapter da2 = new SqlDataAdapter();
                DataSet ds2 = new DataSet();
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@Category", myRow[1].ToString());
                cmd2.Parameters.AddWithValue("@pblogThemeID", Theme);
                cmd2.Parameters.AddWithValue("@pblogFormatID", Format);
                SqlDataReader dr2 = cmd2.ExecuteReader();
                DataTable dt2 = new DataTable();
                dt2.Load(dr2);

                da2 = new SqlDataAdapter(cmd2);
                da2.Fill(ds2);

                string blog = "";

               

                if (dt2.Rows.Count > 0)
                {
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 2 == 0)
                        {
                            i = " last";
                            i2 = "<div class='clearfloat'></div>";
                        }
                        openDiv = "<div class='col_36 fltlft color-dgray mbot1'>";
                        closeDiv = "</div>";


                        SqlCommand cmdCount = new SqlCommand("sp_CountRecommended", con);
                        SqlDataAdapter daCount = new SqlDataAdapter();
                        DataSet dsCount = new DataSet();
                        cmdCount.CommandType = CommandType.StoredProcedure;
                        cmdCount.Parameters.AddWithValue("@blogid", myRow2[0].ToString());

                        SqlDataReader drCount = cmdCount.ExecuteReader();
                        DataTable dtCount = new DataTable();
                        dtCount.Load(drCount);

                        daCount = new SqlDataAdapter(cmdCount);
                        daCount.Fill(dsCount);


                        string totalrecblog = "";
                        string totaltimes = "";
                        foreach (DataRow myRowCount in dtCount.Rows)
                        {
                            totalrecblog = myRowCount[0].ToString();
                            if (Int32.Parse(totalrecblog) == 0 || Int32.Parse(totalrecblog) == 1)
                            {
                                totaltimes = " time";
                            }
                            else
                            {
                                totaltimes = " times";
                            }
                        }

                        blog += openDiv +
                            "<div class='blog-landing-col-img-box'>"+
                              "<img src='images/" + myRow2[3].ToString() + "' width='360' height='190' alt='" + myRow2[1].ToString() + "'>" +
                              "<div class='article-icon'><a href='#'><img src='images/article-icon.png' alt='Article'></a></div>"+
                              "</div>"+
                            "<div class='blog-landing-col-title'>"+
                            "<h4><a href='" + UrlHelper.FormatPageUrl(myRow2[4].ToString(), PageType.Type.BlogItem) + "'>" + myRow2[1].ToString() + "</a></h4>" +
                            "<p class='txt12'>Recommended " + totalrecblog + totaltimes + "</p>" +
                            "</div>" + closeDiv +
                            i2;
                    }

                    blogs = rc.healthandhappinessblog +  blog  + divClear;

                }
                rc.healthandhappinessblog = blogs;

                oldcounter = counter;
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }

    public static HealthAndHappinessBlogCls GetBlogFilterCat(string Cat,string Theme,string Format)
    {
        string blogs = "";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetBlogCatFilter", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pblogid", Cat);
        cmd.Parameters.AddWithValue("@pblogThemeID", Theme);
        cmd.Parameters.AddWithValue("@pblogFormatID", Format);
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        HealthAndHappinessBlogCls rc;

        rc = new HealthAndHappinessBlogCls();
        if (tbRow.Rows.Count > 0)
        {
            int counter = 0;
            int oldcounter = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                counter = counter + 1;

                string divClear = "";
                if (oldcounter != counter)
                {
                    divClear = "<div class='clearfloat'></div>";
                }

                rc.healthandhappinessblog += "<div class='blogs'><h3 class='title'>" + myRow[1].ToString() + "</h3>";

                
                    int ctr = 0;
                    string openDiv = "";
                    string closeDiv = "";

                   
                        string i = "";
                        string i2 = "";

                        ctr = ctr + 1;

                        if (ctr % 2 == 0)
                        {
                            i = " last";
                            i2 = "<div class='clearfloat'></div>";
                        }
                        openDiv = "<div class='col_36 fltlft color-dgray mbot1'>";
                        closeDiv = "</div></div>";

                        string blog = "";


                        SqlCommand cmdCount = new SqlCommand("sp_CountRecommended", con);
                        SqlDataAdapter daCount = new SqlDataAdapter();
                        DataSet dsCount = new DataSet();
                        cmdCount.CommandType = CommandType.StoredProcedure;
                        cmdCount.Parameters.AddWithValue("@blogid", myRow[0].ToString());

                        SqlDataReader drCount = cmdCount.ExecuteReader();
                        DataTable dtCount = new DataTable();
                        dtCount.Load(drCount);

                        daCount = new SqlDataAdapter(cmdCount);
                        daCount.Fill(dsCount);


                        string totalrecblog = "";
                        string totaltimes = "";
                        foreach (DataRow myRowCount in dtCount.Rows)
                        {
                            totalrecblog = myRowCount[0].ToString();
                            if (Int32.Parse(totalrecblog) == 0 || Int32.Parse(totalrecblog) == 1)
                            {
                                totaltimes = " time";
                            }
                            else {
                                totaltimes = " times";
                            }
                        }

                        blog += openDiv +
                            "<div class='blog-landing-col-img-box'>" +
                              "<img src='images/" + myRow[3].ToString() + "' width='360' height='190' alt='" + myRow[1].ToString() + "'>" +
                              "<div class='article-icon'><a href='#'><img src='images/article-icon.png' alt='Article'></a></div>" +
                              "</div>" +
                            "<div class='blog-landing-col-title'>" +
                            "<h4><a href='" + UrlHelper.FormatPageUrl(myRow[4].ToString(), PageType.Type.BlogItem) + "'>" + myRow[1].ToString() + "</a></h4>" +
                            "<p class='txt12'>Recommended " + totalrecblog + totaltimes + "</p>" +
                            "</div>" + closeDiv +
                            i2;
                   

                    blogs = rc.healthandhappinessblog + blog;

                
                rc.healthandhappinessblog = blogs;

                oldcounter = counter;
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }
    public static HealthAndHappinessBlogCls GetTotalBlogByCategory(string id)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetBlogCountPerCategory", con);
        cmd.Parameters.AddWithValue("@blogcatID", id);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        HealthAndHappinessBlogCls rc;
        rc = new HealthAndHappinessBlogCls();
        string totalBlog = "";
        if (tbRow.Rows.Count > 0)
        {

            foreach (DataRow myRow in tbRow.Rows)
            {
                totalBlog += myRow[0].ToString();

            }
            rc.totalBlog += totalBlog;


            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }

    public bool InsertToRecommended()
    {
        bool result = false;

        _helper = new SqlHelper();
        try
        {

            if (_helper.CreateConnection())
            {
                _helper.BeginTransaction();
                _helper.CreateCommand("sp_InsertToRecommended", true);
                _helper.AddParameter("@userid", this.userid, DbType.String, ParameterDirection.Input);
                _helper.AddParameter("@blogid", this.blogID, DbType.String, ParameterDirection.Input);

                result = _helper.ExecuteNonQuery();

                _helper.ClearCommandParameters();

                if (result)
                {
                    _helper.CommitTransaction();

                }


            }

        }
        catch (Exception ex)
        {

            _helper.RollbackTransaction();
            ErrorHandler.Handle(ex);


        }
        finally
        {
            _helper.CloseConnection();

        }


        return result;

    }

    public static HealthAndHappinessBlogCls GetTotalRecommended(string blogID)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_CountRecommended", con);
        cmd.Parameters.AddWithValue("@blogid", blogID);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable tbRow = ds.Tables[0];
        HealthAndHappinessBlogCls rc;
        rc = new HealthAndHappinessBlogCls();
        string totalBlog = "";
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                totalBlog += myRow[0].ToString();

            }
            rc.totalBlog += totalBlog;


            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }
}


