﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for Item
/// </summary>
public class Item
{
    [XmlElement("id")]
    public string Id { get; set; }

    [XmlElement("name")]
    public string Name { get; set; }

    [XmlElement("url")]
    public string Url { get; set; }

    [XmlElement("metaTitle")]
    public string MetaTitle { get; set; }

    [XmlElement("metaDescription")]
    public string MetaDescription { get; set; }

    [XmlElement("metaKeywords")]
    public string MetaKeywords { get; set; }

    [XmlElement("metaOpenGraphTitle")]
    public string MetaOpenGraphTitle { get; set; }

    [XmlElement("type")]
    public string Type { get; set; }

    public string CanonicalUrl { get; set; }

}

[Serializable,XmlRoot("Items")]
public class Items
{
    [XmlElement("Item", typeof(Item))]
    public List<Item> ItemList { get; set; }
}