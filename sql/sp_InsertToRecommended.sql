
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertToRecommended]    Script Date: 2/20/2015 5:04:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 20, 2015>
-- Description:	<Insert Recommended>
-- exec sp_InsertToRecommended
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertToRecommended]
   
    @userid int,
    @blogid int
AS
BEGIN
	
	SET NOCOUNT ON;


	--IF EXISTS (SELECT 1 FROM tbl_Blog_Recommended WHERE blogid = @blogid)  
	--BEGIN
	--	UPDATE tbl_Blog_Recommended 
	--	SET userid = @userid, productid = @blogid
	--	WHERE blogid = @blogid;
	--END
	--ELSE 
	--BEGIN
		INSERT INTO tbl_Blog_Recommended (userid, blogid) 
		VALUES ( @userid,
	    @blogid);
	--END


END
