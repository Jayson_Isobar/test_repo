﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CategoryService
/// </summary>
public class CategoryService
{
	public CategoryService()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    /// <summary>
    /// Get cache or new category list
    /// </summary>
    /// <returns></returns>
    public List<CategoryModel> GetCategoryList()
    {
        const string key = "category_List";
        var products = CacheHelper.Get<List<CategoryModel>>(key);
        if (products != null) return products;
        var categoryList = ShopProductsCls.GetCategoryList();
        CacheHelper.Add(categoryList, key);
        return categoryList;
    }


    /// <summary>
    /// Get cache or new category model
    /// </summary>
    /// <returns></returns>
    public CategoryModel GetProduct(string categoryName)
    {
        var categoryList = GetCategoryList();
        var categoryModel = new CategoryModel();
        if (categoryList != null)
        {
            categoryModel = categoryList.FirstOrDefault(x => x.CategoryName.Equals(categoryName, StringComparison.OrdinalIgnoreCase));

        }
        return categoryModel;
    }

}