﻿<%@ Page Title="View Profile" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewProfile.aspx.cs" Inherits="ViewProfile" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   
<section class="breadcrumbs color-blck">
<div class="container">
<p>Home</p>
</div>
</section>

<section class="container">
  <div class="col_60 fltlft">
  <asp:Button ID="btnEditProfile" class="btn color-rd fltrt" runat="server" 
          Text="Edit" onclick="btnEditProfile_Click"></asp:Button>
     
    <h2>edit your profile</h2>
    <h4 class="mtop15">Your basic information</h4>
    <div id="create-account-wrap" class="mtop20">
    <div class="col_32 fltlft">
    <label>First Name</label>
    <p><asp:Label ID="lblFN" runat="server"></asp:Label></p>
    </div>
    <div class="col_14 fltrt">
    <label>Last Name</label>
    <p><asp:Label ID="lblLN" runat="server"></asp:Label></p>   
    </div>
    <div class="clearfloat"></div>
    <div class="col_32 fltlft mtop20">
    <label>Email</label>
    <p><asp:Label ID="lblEmail" runat="server"></asp:Label></p>
    </div>    
    <div class="clearfloat"></div>
    <label class="mtop20">Birthday</label>
    <p><asp:Label ID="lblBDate" runat="server"></asp:Label></p>

    <label class="mtop20">Post Code</label>
   <p><asp:Label ID="lblPostCode" runat="server"></asp:Label></p>
    <div class="clearfloat"></div>     
    <p class="mtop20"><asp:CheckBox ID="cbSignUp" runat="server" Enabled="False"></asp:CheckBox>Yes! Yes sign me up to receive occasional emails from Swisse as well as emails from other trusted P&G Brands which will help me:</p>
    <ul class="mtop15">
    <li>Save money with exclusive coupons</li>
    <li>Get inspired with useful tips & ideas</li>
    <li>Learn about free samples, new products, exciting promotions and other information from P&G and carefully-selected business partners</li>
    </ul>
    </div>
    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>

</asp:Content>
