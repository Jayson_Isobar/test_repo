﻿<%@ Page Title="Reset Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ResetPassword.aspx.cs" Inherits="ResetPassword" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs color-blck">
<div class="container">
<p>Home</p>
</div>
</section>

<section class="container">
  
    <div class="col_60 fltlft">

   
    <h2>Change your password</h2>
    <div id="create-account-wrap" class="mtop20">
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
    <label>New Password</label>
     <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>
    </div>
    <div class="clearfloat"></div>
    <label class="mtop20">Confirm Password</label>
    <asp:TextBox ID="txtConfirmPass" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>    
    <div class="clearfloat"></div>     
    </div>
    
    
  
    <div class="clearfloat"></div>


    <asp:Button ID="btnSave" runat="server" class="btn color-rd" Text="Save" 
        onclick="btnSave_Click"></asp:Button>
    
    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>

</asp:Content>
