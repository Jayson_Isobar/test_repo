﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ProductConcerns : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["cid"] != null)
            {
                LoadViewpnlViewProdConcern();
            }
            else
            {
                LoadData();
            }
        }
    }

    protected void btnViewDeleted_Click(object sender, EventArgs e)
    {
        pnlGVProdConcern.Visible = false;
        btnBackView.Visible = true;
        btnViewDeleted.Visible = false;
        LoadData2();
    }

    protected void btnBackView_Click(object sender, EventArgs e)
    {
        pnlGVProdConcern.Visible = true;
        btnBackView.Visible = false;
        btnViewDeleted.Visible = true;
        LoadData();
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdConcernList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdConcern.DataSource = ds;
        gvProdConcern.DataBind();
        con.Close();

    }

    public void LoadData2()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdConcernDeletedList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdConcern.DataSource = ds;
        gvProdConcern.DataBind();
        con.Close();

    }

    protected void gvProdConcernRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            var someId = e.CommandArgument;
            Response.Redirect("ProductConcerns.aspx?cid=" + someId.ToString(), false);
        }
        else if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProdConcernByID";
            cmd.Parameters.AddWithValue("@prodconcernID", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Redirect("ProductConcerns.aspx");
        }
    }

    protected void gvProdConcern_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProdConcern.EditIndex = e.NewEditIndex;
        LoadData();
    }

    public void LoadViewpnlViewProdConcern()
    {
        pnlGVProdConcern.Visible = false;
        pnlViewProdConcern.Visible = true;
        btnUpdate.Visible = true;

        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProdConcernDetailsByID";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@prodconcernID", Request.QueryString["cid"].ToString());
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            txtProdConcernName.Text = dt.Rows[0]["prodconcernName"].ToString().Trim();
            txtProdConcernDesc.Text = dt.Rows[0]["prodconcernDesc"].ToString().Trim();
            if (dt.Rows[0]["prodconcernStatus"].ToString().Trim() == "1")
            {
                cbStatus.Checked = true;
            }
            else {
                cbStatus.Checked = false;
            }
        }
        con.Close();
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_UpdateProdConcernByID";
        cmd.Parameters.AddWithValue("@prodconcernID", Request.QueryString["cid"]);
        cmd.Parameters.AddWithValue("@prodconcernName", txtProdConcernName.Text.Trim());
        cmd.Parameters.AddWithValue("@prodconcernDesc", txtProdConcernDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else{
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@prodconcernStatus", cbStat);

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ProductConcerns.aspx"); 
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductConcerns.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProdConcern.Visible = false;
        pnlViewProdConcern.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProdConcern";
        cmd.Parameters.AddWithValue("@prodconcernName", txtProdConcernName.Text.Trim());
        cmd.Parameters.AddWithValue("@prodconcernDesc", txtProdConcernDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else
        {
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@prodconcernStatus", cbStat);

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ProductConcerns.aspx"); 
    }

    protected void gvProdConcern_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProdConcern.PageIndex = e.NewPageIndex;
        LoadData();
    }
}
