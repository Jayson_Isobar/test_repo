﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class index : BaseForm
{
    String siteUrl = ConfigurationManager.AppSettings["siteUrl"];

    protected void Page_Load(object sender, EventArgs e)
    {
       
      LoadBanner();
    }

    public void LoadBanner() {

        ShopProductsCls banners = new ShopProductsCls();
        banners = ShopProductsCls.GetBanners();
        string banner = banners.bannerName;
        banner = banner.Replace("url(", "url(" + base.CDNdomain);
        litBanner.Text = banner;
    }
}