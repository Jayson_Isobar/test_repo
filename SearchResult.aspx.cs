﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Net;
using System.Text;
using System.IO;
using System.Xml.Linq;

public partial class SearchResult : BaseForm
{
   
    
    string id = "1";
    string Ing;
    string Con;
    string Age;
    string Gender;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadSearchResult();
    }


    public void LoadSearchResult()
    {
        ShopProductsCls pc = new ShopProductsCls();

        pc = ShopProductsCls.GetSearchResult(id, Ing, Con, Age, Gender);
        string cat = pc.prodcatName;

        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProductResult.Text = cat;

        List<GSAContentResultModel> ContentResults = new List<GSAContentResultModel>();

        string keyword = Request.QueryString["p"];
        ContentResults = RetrieveGSAResults(keyword, 0, int.Parse(ConfigurationManager.AppSettings["GsaSearchResultPerPage"]));


        lblContentCount.Text = ContentResults.Count() == 0 ? "0" : ContentResults.Count().ToString("#,###") ?? "0";
        lblSearch.Text = keyword;

        this.Repeater_ContentSearchResults.DataSource = ContentResults;
        this.Repeater_ContentSearchResults.DataBind();

    }

    //TODO: Let us put this into AppSettings
    

    
    List<GSAContentResultModel> RetrieveGSAResults(string keyword, int pageIndex, int recsPerPage)
    {
        List<GSAContentResultModel> results = new List<GSAContentResultModel>();
        var searcher = new GSASearch();
        HttpWebResponse webResponse = searcher.SearchQuery(Request.QueryString["p"], ConfigurationManager.AppSettings["GsaSearchSite"] ?? string.Empty, pageIndex, recsPerPage);
        if (webResponse != null)
        {
            using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
            {
                XDocument xmlResponse = XDocument.Load(reader);
                IEnumerable<XElement> res = xmlResponse.Elements();
                foreach (var r in res.Elements("RES").Elements("R"))
                {
                    results.Add(new GSAContentResultModel
                    {
                        LinkUrl = r.Element("U").Value.Replace("<b>", "<strong>").Replace("</b>", "</strong>"),
                        Description = r.Element("S").Value.Replace("<b>", "<strong>").Replace("</b>", "</strong>"),
                        Header = "Article",
                        PhotoUrl = this.CDNdomain + "images/search.jpg",
                        Title = r.Element("T").Value.Replace("<b>", "<strong>").Replace("</b>", "</strong>"),
                        ResultIndex = int.Parse(r.Attribute("N").Value)
                    });
                }

                
            }
        }



        return results;
    }
}


//To be refactored and place to it's own class file
public class GSASearch
{
    public HttpWebResponse SearchQuery(string keyword, string siteToSearch, int start, int pageCount)
    {
        try
        {
            string requestUrl = string.Format("http://pg-googleit-01.isc.savvis.net/search?site=default_collection&client=default_frontend&output=xml_no_dtd&sitesearch={0}&q={1}&start={2}&num={3}",siteToSearch,keyword, start, pageCount);
            HttpWebRequest request = (WebRequest.Create(requestUrl) as HttpWebRequest);
            request.Method = "GET";
            request.ContentType = "text/xml";
            request.Timeout = -1;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            return response;
        }
        catch
        {
            return null;
        }
    }
}

//To be refactored and place to it's own class file
public class GSAContentResultModel
{
    public string PhotoUrl { get; set; }
    public string LinkUrl { get; set; }
    public string Header { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    
    //Not used
    public int ResultIndex { get; set; }

}
