﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BINctrl.aspx.cs" Inherits="BINctrl" %>

<asp:Repeater id="retailer_table" runat="server">
                        
    <HeaderTemplate>
        <table id="bin_tbl">
    <tr>
        <td>
                Retailer
        </td>
        <td>
                purchase
        </td>
    </tr>
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
        <td>
            <a class="bin" target="_blank" href="<%#Eval("buy_url")%>"><img src="<%#Eval("dealer_logo")%>" alt=""/></a>
        </td>
        <td>
            <a class="bin buynow" target="_blank" href="<%#Eval("buy_url")%>">
                BUY NOW
            </a>
        </td>
    </tr>
    </ItemTemplate>
                        
    <FooterTemplate>
    </table>
    </FooterTemplate>
    </asp:Repeater>

    <div id="noStockMsg" class="exFromTab noStock" runat="server">
        <p>NO STOCK AVAILABLE</p>
    </div>