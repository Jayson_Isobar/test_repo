﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HeaderNavigationFlyout_OurStory : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadOurStory();
    }
    public void LoadOurStory()
    {
        OurStoryCls os = new OurStoryCls();
        os = OurStoryCls.GetOurStoryCls();
        string ourstory = os.ourstory;

        litContent.Text = ourstory;
    }
}