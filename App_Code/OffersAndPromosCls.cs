﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for OffersAndPromosCls
/// </summary>
public class OffersAndPromosCls
{
 
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;

    public string offersandpromos { get; set; }

    public static OffersAndPromosCls GetOffersAndPromosCls()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetOffersAndPromosList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        OffersAndPromosCls rc;
        rc = new OffersAndPromosCls();
        if (tbRow.Rows.Count > 0)
        {
            var numrow = 0;
            foreach (DataRow myRow in tbRow.Rows)
            {
                numrow++;
                if (numrow > 3) break;
                var url = myRow[1].ToString().Equals("Offers and Promos") ? UrlHelper.FormatPageUrl(myRow[2].ToString(), PageType.Type.Promo) : myRow[2].ToString();
                rc.offersandpromos += "<a href='" + url + "'>" +
                                        "<img src='images/" + myRow[3].ToString() + "' alt='" + myRow[1].ToString() + "' width='" + myRow[5].ToString() + "' height='" + myRow[6].ToString() + "' class='fltlft'> " +
                                    "</a>" +
                                    myRow[8].ToString();

                
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }


}


