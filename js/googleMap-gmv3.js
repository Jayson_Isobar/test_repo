var map;
var esMarker
var fsMarker;
var markersArray = [];
var currmarker;
var minZoomLevel = 10;
var imgPath = "/images/";

function clearOverlays() {
  for (var i = 0; i < markersArray.length; i++ ) {
    markersArray[i].setMap(null);
  }
  markersArray.length = 0;
  $( ".infoBox" ).remove();
}

function moveCenter(lat, lng) {
  	var newPos =  new google.maps.LatLng(lat, lng);
	map.panTo(newPos); 
}

function zoomMoveToLocation(lat, lng, zoom) {
	if ($( window ).width() <  768) {
		tipboxbg = "url('/img/tipbox-mobile.png') bottom left no-repeat";
		tipoffsetx = -160; 
	}
  	var newPos =  new google.maps.LatLng(parseFloat(lat) +  parseFloat(0.001), parseFloat(lng));
	map.panTo(newPos); 
	map.setZoom(parseInt(zoom));
}
  

//calculates distance between two points in km's
function calcDistance(lat1, lng1, lat2, lng2){

	var p1 = new google.maps.LatLng(lat1, lng1);
	var p2 = new google.maps.LatLng(lat2, lng2);

	return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
}

function zoomMap(nzoom) {
	var myTvar3=setInterval(function(){
		map.setZoom(nzoom); 
		var currcenter =map.getCenter();
		var nlat = currcenter.lat();
		var nlng = currcenter.lng();
		moveCenter(nlat, parseFloat(nlng)) ;
		clearInterval(myTvar3);
	},1000);
}	
	

function checkBounds() {    
	if(! allowedBounds.contains(map.getCenter()))
	{
		var C = map.getCenter();
		var X = C.lng();
		var Y = C.lat();
		var AmaxX = allowedBounds.getNorthEast().lng();
		var AmaxY = allowedBounds.getNorthEast().lat();
		var AminX = allowedBounds.getSouthWest().lng();
		var AminY = allowedBounds.getSouthWest().lat();
		if (X < AminX) {X = AminX;}
		if (X > AmaxX) {X = AmaxX;}
		if (Y < AminY) {Y = AminY;}
		if (Y > AmaxY) {Y = AmaxY;}
		map.panTo(new google.maps.LatLng(Y,X));
	} 
}


function initialize() {
	
	// Default location - SG
	var Lat = 1.3011965;
	var Lng = 103.838532;

	position = new google.maps.LatLng(Lat, Lng)

	var mapOptions = {
		zoom: minZoomLevel,
		center: position
	};

map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

   // Limit the zoom level
   google.maps.event.addListener(map, 'zoom_changed', function() {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
   });

	initBranchList(0);
	
	//google.maps.event.addListener(map, "rightclick", function(event) {
	//	var dlat = event.latLng.lat();
	//	var dlng = event.latLng.lng();
	//  alert("Lat=" + dlat + "; Lng=" + dlng); 
	//});
	
    google.maps.event.addListenerOnce(map,'idle',function() {
        allowedBounds = map.getBounds();
    });
    google.maps.event.addListener(map,'drag',function() {
        checkBounds(); 
    });	

	//if(qs["branch"]) {
	//		var myTvar2=setInterval(function(){showTheShop(qs["branch"]); clearInterval(myTvar2);},2000);
	//}		 	
	
	//if ($( window ).width() <  768 || detectBrowser() == "iPhone") {
		//var myVar=setInterval(function(){$("#map").hide(); clearInterval(myVar);},200);
	//}	
}

google.maps.event.addDomListener(window, 'load', initialize);

