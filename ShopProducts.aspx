﻿<%@ Page Title="Shop Products" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ShopProducts.aspx.cs" Inherits="HeaderNavigationFlyout_ShopProducts" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>Swisse Vitamins | Australian's #1 Multivitamin Range</title>
<meta property="og:title" content="Swisse Vitamins | Australian's #1 Multivitamin Range" />
<meta name="keywords" content="multivitamins, multivite, multivitamin tablets, women's health, men's health, vitamins, supplements, vitamin B, vitamin C, calcium, folic acid, magnesium" />
<meta name="description" content="Australia's #1 Multivitamin Brand, discover the full range of Swisse vitamins & supplements specially tailored to individual age, gender and health concerns." />
<link rel="canonical" href="http://www.swisse.com.sg/products" />

    <link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="banner-pr" id="shopProducts">
        <div class="breadcrumbs">
            <div class="container">
                <p>
                    <a href="/index.aspx">Home</a>
                    <span class="mlr">></span>
                    Our Products
                </p>
            </div>
        </div>
    </div>
<div class="banner-pr-bottom color-blck">
</div>
<div id="product-wrap">
<div class="container">
    	<div id="sp-sidebar" class="color-dgray">
       	<div class="sidebar-lft list">
        <h3>Browse by Category</h3>
        <ul class="by-category">
            <asp:Literal ID="litProdCat" runat="server"></asp:Literal>
        </ul>
        </div>
       	<div class="sidebar-lft">
        <h3>Refine by Ingredient</h3>
        <ul class="by-category">
           <asp:CheckBoxList ID="cbIng" runat="server"  DataSourceID="sdsIngredients" DataTextField="prodingredientName"
          DataValueField="prodingredientid"  AutoPostBack="True"  
                OnSelectedIndexChanged="cbIng_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsIngredients" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetProdIngredientListActive" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtIngredients" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div>
       	<div class="sidebar-lft">
        <h3>Refine by Concern</h3>
        <ul class="by-category">
           <asp:CheckBoxList ID="cbConcern" runat="server"  DataSourceID="sdsConcerns" DataTextField="prodconcernName"
          DataValueField="prodconcernid"  AutoPostBack="True"  
                OnSelectedIndexChanged="cbConcern_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsConcerns" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetProdConcernListActive" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtConcerns" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div> 
        <div class="sidebar-lft" style="display:block;">
        <h3>Refine by Age</h3>
        <ul class="by-category refineAge">
            <asp:CheckBoxList ID="cbAge" runat="server"  DataSourceID="sdsAge" DataTextField="agerange"
          DataValueField="id"  AutoPostBack="True"  
                OnSelectedIndexChanged="cbAge_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsAge" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetAgeList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtAges" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div>  
        	<div class="sidebar-lft" style="display:block;">
        <h3>Refine by Gender</h3>
        <ul class="by-category refineGender">
            <asp:CheckBoxList ID="cbGender" runat="server"  DataSourceID="sdsGender" DataTextField="gender"
          DataValueField="id"  AutoPostBack="True"  
                OnSelectedIndexChanged="cbGender_SelectedIndexChanged"></asp:CheckBoxList>
           <asp:SqlDataSource ID="sdsGender" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
               SelectCommand="sp_GetGenderList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
          <asp:TextBox ID="txtGenders" runat="server" Style="visibility: hidden"></asp:TextBox>
        </ul>
        </div>                  
      </div>
        <div id="sp-content">
            <div class="sp-content-banner">
                <img src="<%=this.CDNdomain %>images/banner-img.png" alt="Discover the Swisse difference" />
            </div>

            <div class="viewing-filter">
                <asp:Panel ID="viewingFilterPanel" runat="server" Visible="false" >
                    <h4>Viewing:
                        <asp:Repeater ID="filterRepeater" runat="server">
                            <ItemTemplate>
                                <span>
                                    <%# Eval("Name").ToString() %>
                                    <asp:LinkButton CommandName="FilterRemove" CommandArgument='<%#Container.ItemIndex + "," + Eval("Value")+","+ Eval("Type") %>' OnCommand="FilterRemove_Command" runat="server">
                                        <img src="<%=this.CDNdomain %>images/btnX.png" alt="" />
                                    </asp:LinkButton>
                                </span>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:LinkButton Text="Clear All" CommandName="FilterRemoveAll" OnCommand="FilterRemove_Command" runat="server" />
                    </h4>
                </asp:Panel>
            </div>

            <div id="sorting" class=" color-blck">

                <h4 class="fltlft">
                    <asp:Label ID="lblTotalItems" runat="server"></asp:Label>
                    <%--<img src="<%=this.CDNdomain %>images/sort.jpg" class="mlft12" alt="Sort">
                    <img src="<%=this.CDNdomain %>images/sort2.jpg" width="20" height="20" alt="Sort">--%>
                </h4>

                <div class="sortby fltrt">
                    <h4 class="fltlft">SORT</h4>
                    <div class="fltlft">
                        <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="True" onselectedindexchanged="ddlSort_SelectedIndexChanged" name="sortby">
                            <asp:ListItem>Most Popular</asp:ListItem>
                            <%--<asp:ListItem>Price</asp:ListItem>--%>
                            <asp:ListItem>Alphabetically</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>  
                <div class="clearfloat"></div>   
            </div>

            <asp:Literal ID="litProduct" runat="server"></asp:Literal>
        </div><!--Product Wrap -->

        <div class="clearfloat"></div>
    </div>
</div>
<div class="recentlyviewed">
<div class="container">
<h3>RECENTLY VIEWED PRODUCTS</h3>
<div class="recentlyviewed-wrap mtop15">
	<asp:Literal ID="litProdRecentView" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
</div>
</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>
</asp:Content>
