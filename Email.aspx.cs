﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;

public partial class Account_Register : BaseForm
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }


    protected void btnRegister_Click(object sender, EventArgs e)
    {
    
        if (txtPassword.Text.Length < 8 || txtPassword.Text.Length > 15)
        {
            lblErrPass.Visible = true;
            lblErrReq.Visible = true;
        }
        else
        {
            
            UserCls uAdd = new UserCls();
            uAdd.userEmail = txtEmail.Text;
            uAdd.userfName = txtfName.Text;
            uAdd.usermName = txtmName.Text;
            uAdd.userlName = txtlName.Text;
            uAdd.userName = txtuName.Text;
            uAdd.userPassword = Encrypt(txtPassword.Text);
            uAdd.userStatus = 0;
            uAdd.AddUser();

            lblErrReq.Visible = false;
            lblErrPass.Visible = false;
        }

//        protected void btnSubmit_Click(object sender, EventArgs e)
//{
//string strpassword = Encryptdata(txtPassword.Text);
//con.Open();
//SqlCommand cmd = new SqlCommand("insert into SampleUserdetails(UserName,Password,FirstName,LastName) values('" + txtname.Text + "','" + strpassword + "','" + txtfname.Text + "','" + txtlname.Text + "')", con);
//cmd.ExecuteNonQuery();
//con.Close();

//}


//private string Encryptdata(string password)
//{
//string strmsg = string.Empty;
//byte[] encode = new byte[password.Length];
//encode = Encoding.UTF8.GetBytes(password);
//strmsg = Convert.ToBase64String(encode);
//return strmsg;
//}



    }
}
