﻿<%@ Page Title="Product Related Ingredients" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductRelatedIngredients.aspx.cs" Inherits="ProductRelatedIngredients" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT INGREDIENT" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Products List" 
        onclick="btnBackView_Click" />
        <p></p>
        
    <asp:Panel ID="pnlGVProdIngredient" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdIngredient" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdIngredientRowCommand"
            OnRowEditing="gvProdIngredient_RowEditing"
            OnPageIndexChanging="gvProdIngredient_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
            OnRowDeleting="PendingRecordsGridview_RowDeleting">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("id") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Ingredient Name" SortExpression="prodIngredientName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodingredientName" runat="server" Text='<%# Bind("prodIngredientName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

               
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdIngredient" runat="server" Visible="False">
            
             <asp:Label ID="Label1" runat="server" Text="Product Ingredient Name"></asp:Label>
             <asp:DropDownList ID="ddlProdIngredient" runat="server" AutoPostBack="True">
             </asp:DropDownList>
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
