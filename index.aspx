﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Swisse Vitamins | Australia's #1 Multivitamin Brand</title>
<meta property="og:title" content="Swisse Vitamins | Australia's #1 Multivitamin Brand" />
<meta name="keywords" content="multivitamins, multivite, multivitamin tablets, women's health, men's health, vitamins, supplements, integrative health, natural medicine, traditional chinese medicine, vitamin supplements" />
<meta name="description" content="At Swisse we develop scientifically validated vitamins and supplements specially formulated to help fill nutritional gaps. Australia’s #1 Multivitamin Brand." />
<link rel="canonical" href="http://www.swisse.com.sg" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">


<div class="banner">
<ul class="bxslider">
    <asp:Literal ID="litBanner" runat="server"></asp:Literal>
</ul>
</div>
<%--<div class="container">
</div>
--%><section class="container">
    <a href="/our-story/swisse-ambassadors/rebecca-lim">
        <img src="<%=this.CDNdomain %>images/playvideo.jpg" alt="Rebecca Lim's Secret to Natural Beauty" class="fltlft homethumb" />
    </a>

    <a href="/products">
        <img src="<%=this.CDNdomain %>images/shoprange.jpg" alt="Our Unique Formula" width="320" height="400" class="fltrt homethumb" />
    </a>

    <a href="/the-science-of-swisse">
        <img src="<%=this.CDNdomain %>images/scienceofswisse.jpg" alt="The Science of Swisse" class="fltlft" />
    </a>

    <a href="/health-happiness-blog/diy-detox-cut-down-on-coffee-today">
        <img src="<%=this.CDNdomain %>images/doityourselfdetox.jpg" alt="Do it yourself detox" class="fltlft" />
    </a>

    <a href="/OffersAndPromos1">
        <img src="<%=this.CDNdomain %>images/winatrip_2.jpg" alt="Win a trip to Australia" class="fltlft" />
    </a>
<div class="clearfloat"></div>
</section>
<div class="container mtop102" id="bottomLinks" style="display:none !important">
<div class="col_14 fltlft">
    <div class="colorbox">
    <h3>SEEKING <br>SWISSE?</h3>
    <div class="color-sep color-rd"></div>
    <div class="clearfloat"></div>
    <p>Locate your nearest Swisse retailer in Singapore with our store finder</p>
    <div class="btn color-rd"><a href="SwisseProductFinder.aspx">Search Now</a></div>
    </div>
</div>
<%--<div class="col_14 fltlft">
    <div class="colorbox">
    <h3>GOT A <br>QUESTION?</h3>
    <div class="color-sep color-y"></div>
    <div class="clearfloat"></div>
    <p>Check out our FAQs section for answers, or submit a question yourself</p>
    <div class="btn color-y"><a>Visit FAQ's</a></div>
    </div>
</div>--%>
<div class="col_14 fltlft">
    <div class="colorbox">
    <h3>Spotlight<br>On Sleep </h3>
    <div class="color-sep color-lvdr"></div>
    <div class="clearfloat"></div>
    <p>Top 5 tips for a better night’s rest</p>
    <div class="btn color-lvdr"><a href="#">Read More</a></div>
    </div>
</div>
<div class="col_14 fltlft">
    <div class="colorbox">
    <h3>The Other<br>Omega 3 </h3>
    <div class="color-sep color-or"></div>
    <div class="clearfloat"></div>
    <p>Swisse Krill Oil includes the natural antioxidant astaxanthin </p>
    <div class="btn color-or"><a href="#">BROWSE THE RANGE</a></div>
    </div>
</div>
<div class="clearfloat"></div>
</div>
</asp:Content>


<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="<%=CDNdomain %>js/jquery.bxslider.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('.bxslider').bxSlider();
            //$('#overlay').modal('show');
        });
   
    </script>
</asp:Content>
