﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class HeaderNavigationFlyout_OffersAndPromos : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadOffersAndPromos();
    }
    public void LoadOffersAndPromos()
    {
        OffersAndPromosCls oap = new OffersAndPromosCls();
        oap = OffersAndPromosCls.GetOffersAndPromosCls();
        string offersandpromos = oap.offersandpromos;
        offersandpromos = offersandpromos.Replace("<img src='", "<img src='" + base.CDNdomain);
        litContent.Text = offersandpromos;
    }
}