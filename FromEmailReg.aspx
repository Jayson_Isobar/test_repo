﻿<%@ Page Title="From Email Registration" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FromEmailReg.aspx.cs" Inherits="FromEmailReg" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs color-blck">
<div class="container">
<p>Home</p>
</div>
</section>

<section class="container">
  <div class="col_60 fltlft">
    <h2>Verify Personal Details</h2>
    <h4 class="mtop15">Enter your basic information</h4>
    
    <div id="create-account-wrap" class="mtop20">
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
    <label>Email</label>
    <input type="email" size="32">   
    </div>
    <div class="clearfloat"></div>
    <label class="mtop20">Birthday</label>
    <select name="Month" class="fltlft">
      <option>Month</option>
    </select>
    <select name="Month" class="fltlft">
      <option>Year</option>
    </select>
    <div class="clearfloat"></div>
    <label class="mtop20">Post Code</label>
    <input type="text"> 
    <div class="clearfloat"></div>     
   
   </div>
    <div class="col_60 fltlft">

   
    <h2>Create your password</h2>
    <h4 class="mtop15">Enter your basic information</h4>
    <div id="create-account-wrap" class="mtop20">
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
    <label>New Password</label>
    <input type="password">   
    </div>
    <div class="clearfloat"></div>
    <label class="mtop20">Confirm Password</label>
    <input type="password"> 
    <div class="clearfloat"></div>     
    </div>
    <div class="btn color-rd"><a href="#">Sign In</a></div>
    
    
     </div>
  
    <div class="clearfloat"></div>


    <asp:Button ID="btnNext" runat="server" class="btn color-rd" Text="Next"></asp:Button>
    <asp:Button ID="btnSave" runat="server" class="btn color-rd" Text="Save" 
        onclick="btnSave_Click"></asp:Button>
    
    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>

</asp:Content>
