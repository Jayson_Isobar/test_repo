USE [mina]
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckDuplicationEmail]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 12, 2015>
-- Description:	<Get User Email>
-- exec sp_GetUserEmail
-- =============================================
create PROCEDURE [dbo].[sp_CheckDuplicationEmail]
	@Email nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
        userID,
        userEmail,
        userName,
        userPassword  
    FROM tbl_User 
    WHERE userEmail = @Email


    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAmbassadorByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Delete Ambassador By ID>
-- exec sp_DeleteAmbassadorByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteAmbassadorByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_SwisseAmbassador
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteBlogByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Delete Blog By ID>
-- exec sp_DeleteBlogByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteBlogByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_Blog
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteFAQByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 12, 2015>
-- Description:	<Delete FAQ By ID>
-- exec sp_DeleteFAQByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteFAQByID]
   @id as int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_FAQs
    
    WHERE id= @id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProdCatByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Delete Product Category By ID>
-- exec sp_DeleteProdCatByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProdCatByID]
   @prodcatID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_ProductCategory
    SET deleted = '1'
    WHERE prodcatID= @prodcatID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProdConcernByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Delete Product Concern By ID>
-- exec sp_DeleteProdConcernByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProdConcernByID]
   @prodconcernID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_ProductConcern
    SET deleted = '1'
    WHERE prodconcernID= @prodconcernID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProdIngredientByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Delete Product Ingredient By ID>
-- exec sp_DeleteProdIngredientByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProdIngredientByID]
   @prodingredientID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_ProductIngredient
    SET deleted = '1'
    WHERE prodingredientID= @prodingredientID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProductByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<Delete Product By ID>
-- exec sp_DeleteProductByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProductByID]
   @productID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_Product
    SET deleted = '1'
    WHERE productID= @productID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProductRelatedAgeByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Delete Product Related Age By ID>
-- exec sp_DeleteProductRelatedAgeByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProductRelatedAgeByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_Product_Age
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProductRelatedConcernByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Delete Product Related Concern By ID>
-- exec sp_DeleteProductRelatedConcernByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProductRelatedConcernByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_Product_ProductConcern
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProductRelatedGenderByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Delete Product Related Gender By ID>
-- exec tbl_Product_Gender
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProductRelatedGenderByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_Product_Gender
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProductRelatedIngredientByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 15, 2015>
-- Description:	<Delete Product Related Ingredient By ID>
-- exec sp_DeleteProductRelatedIngredientByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProductRelatedIngredientByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_Product_ProductIngredient
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteProductRelatedProductByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Delete Product Related Product By ID>
-- exec sp_DeleteProductRelatedProductByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProductRelatedProductByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM tbl_Product_RelatedProducts
    WHERE id = @id 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUserByID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Delete User>
-- exec sp_DeleteUserByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteUserByID]
	@userID int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User 
    SET userStatus = '0'
    WHERE userID = @userID
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAgeList]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Ages>
-- exec sp_GetAgeList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAgeList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, a.agerange + ' (' +
	CONVERT(NVARCHAR,(SELECT COUNT(productid) FROM tbl_Product_Age WHERE ageid = a.id)) + ')' as agerange
    FROM tbl_Age a
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAgePerProductByProductID]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Ages per Product>
-- exec sp_GetAgePerProductByProductID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAgePerProductByProductID]
   @productID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT b.ageRange 
    FROM tbl_Product_Age a
    INNER JOIN tbl_Age b
    ON a.ageID = b.id 
    WHERE a.productID = @productID
    ORDER BY b.ageRange ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAmbassador]    Script Date: 2/12/2015 2:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Content per item in Blog tab>
-- exec sp_GetAmbassador
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAmbassador]
   @id int
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, ambassadorContent, ambassadorImage, ambassadorLink
    FROM tbl_SwisseAmbassadors
    WHERE id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAmbassadorsContents]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Content per item in Blog tab>
-- exec sp_GetAmbassadorsContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAmbassadorsContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, ambassadorContent, ambassadorImage
    FROM tbl_SwisseAmbassadors
    WHERE ambassadorLink = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBannerListActive]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<All Banners>
-- exec sp_GetBannerListActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBannerListActive]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, bannerName, bannerImage
    FROM tbl_Banner
    WHERE deleted = '0' AND [status] = '1'
    ORDER BY bannerName ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlog]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlog
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlog]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, a.title, a.blogContent, a.blogImage , a.blogLink
    FROM tbl_Blog a
    
	WHERE a.id = @id
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogCatFilter]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlogCatFilter
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogCatFilter]
   @pblogid int,
   @pblogThemeID nvarchar(500) = null,
   @pblogFormatID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;


	DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT 
			a.id, a.title, a.blogContent, a.blogImage, a.blogLink
			FROM tbl_Blog a
			INNER JOIN tbl_Blog_HealthAndHappiness b
			ON a.id = b.blogid
			LEFT JOIN tbl_Blog_Themes c
			ON a.id = c.blogid
			LEFT JOIN tbl_Blog_Formats d
			ON a.id = d.blogid
			'
    , @ParamList nvarchar(max) = N'@blogID int,@blogThemeID nvarchar(500),@blogFormatID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE b.healthandhappinessblogid = @blogID ';

	IF @pblogThemeID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.themeid IN  ('+ Convert(VARCHAR(500), @pblogThemeID)+') ';
    END

	IF @pblogFormatID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.formatid IN  ('+ Convert(VARCHAR(500), @pblogFormatID)+') ';
    END

	
	 SET @BaseQuery = @BaseQuery + @WhereClause;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @blogID = @pblogid, @blogThemeID = @pblogThemeID, @blogFormatID = @pblogFormatID;
     
	

    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogCatFilterThemeFormat]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 12, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlogCatFilterThemeFormat
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogCatFilterThemeFormat]
  
   @pblogThemeID nvarchar(500) = null,
   @pblogFormatID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;


	DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT 
			a.id, a.title, a.blogContent, a.blogImage, a.blogLink
			FROM tbl_Blog a
			INNER JOIN tbl_Blog_HealthAndHappiness b
			ON a.id = b.blogid
			LEFT JOIN tbl_Blog_Themes c
			ON a.id = c.blogid
			LEFT JOIN tbl_Blog_Formats d
			ON a.id = d.blogid
			'
    , @ParamList nvarchar(max) = N'@blogThemeID nvarchar(500),@blogFormatID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' ';

	

	IF @pblogThemeID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + 'WHERE c.themeid IN  ('+ Convert(VARCHAR(500), @pblogThemeID)+') ';
    END

	IF @pblogFormatID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.formatid IN  ('+ Convert(VARCHAR(500), @pblogFormatID)+') ';
    END

	
	 SET @BaseQuery = @BaseQuery + @WhereClause;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @blogThemeID = @pblogThemeID, @blogFormatID = @pblogFormatID;
     
	

    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogContents]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 08, 2015>
-- Description:	<Content per item in Blog tab>
-- exec sp_GetBlogContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, blogContent, blogImage
    FROM tbl_Blog
    WHERE blogLink = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogCountPerCategory]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlogCountPerCategory
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogCountPerCategory]
   @blogcatID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT COUNT(a.id) as total
    FROM tbl_Blog a
	INNER JOIN tbl_Blog_HealthAndHappiness b
	ON a.id = b.blogid
    WHERE 
	b.healthandhappinessblogid = @blogcatID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogFormatsActive]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<All Blog Formats>
-- exec sp_GetBlogFormatsActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogFormatsActive]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT blogFormatID, blogFormat, blogFormatDesc
    FROM tbl_BLogFormat
    WHERE deleted = '0' AND blogFormatStatus = '1'
    ORDER BY blogFormat ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogs]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlogs
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogs]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, a.title, a.blogContent, a.blogImage 
    FROM tbl_Blog a
    
    ORDER BY a.title asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogsHealthHappinessCategory]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 08, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlogsHealthHappinessCategory
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogsHealthHappinessCategory]
 @Category nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT TOP 6 a.id, a.title, a.blogContent, a.blogImage, a.blogLink, c.title as Category  
    FROM tbl_Blog a
	INNER JOIN tbl_Blog_HealthAndHappiness b
	ON a.id = b.blogid
	INNER JOIN tbl_HealthAndHappinessBlog c
	ON b.healthandhappinessblogid = c.id
    WHERE c.title = @Category
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogsHealthHappinessCategoryAlphabetical]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 08, 2015>
-- Description:	<All Blogs>
-- exec sp_GetBlogsHealthHappinessCategory
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogsHealthHappinessCategoryAlphabetical]
 @Category nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT TOP 6 a.id, a.title, a.blogContent, a.blogImage, a.blogLink, c.title as Category  
    FROM tbl_Blog a
	INNER JOIN tbl_Blog_HealthAndHappiness b
	ON a.id = b.blogid
	INNER JOIN tbl_HealthAndHappinessBlog c
	ON b.healthandhappinessblogid = c.id
    WHERE c.title = @Category
    ORDER BY a.title
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetBlogThemesActive]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<All Blog Themes>
-- exec sp_GetBlogThemesActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBlogThemesActive]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT blogThemeID, blogTheme, blogThemeDesc
    FROM tbl_BLogTheme
    WHERE deleted = '0' AND blogThemeStatus = '1'
    ORDER BY blogTheme ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetFAQContents]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 12, 2015>
-- Description:	<Content per item in FAQ tab>
-- exec sp_GetFAQContents]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetFAQContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, [content], [image], link
    FROM tbl_FAQs
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetFAQsList]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 22, 2015>
-- Description:	<All FAQs Items>
-- exec sp_GetFAQsList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetFAQsList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		id, 
		title, 
		link,
		[image],
		usemap,
		coords,
		divclass,
		[content]
    FROM tbl_FAQs
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetFAQsListByID]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 22, 2015>
-- Description:	<All FAQs Items>
-- exec sp_GetFAQsListByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetFAQsListByID]
   @id int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		id, 
		title, 
		link,
		[image],
		usemap,
		coords,
		divclass,
		[content]
    FROM tbl_FAQs
	WHERE id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetGenderList]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Genders>
-- exec sp_GetGenderList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetGenderList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, a.gender  + ' (' +
	CONVERT(NVARCHAR,(SELECT COUNT(productid) FROM tbl_Product_Gender WHERE genderid = a.id)) + ')' as gender
    FROM tbl_Gender a
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetHealthAndHappinessBlogContents]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<Content per item in Health and Happiness Blog tab>
-- exec sp_GetHealthAndHappinessBlogContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHealthAndHappinessBlogContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT content
    FROM tbl_HealthAndHappinessBlog
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetHealthAndHappinessBlogList]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<All Health and Happiness Blog Items>
-- exec sp_GetHealthAndHappinessBlogList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHealthAndHappinessBlogList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, link
    FROM tbl_HealthAndHappinessBlog
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetIngredient]    Script Date: 2/12/2015 2:39:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Content per item in Blog tab>
-- exec sp_GetIngredient
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetIngredient]
   @id int
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, ingContent, ingImage, ingLink
    FROM tbl_Ingredients
    WHERE id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetIngredients]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Content per item in Blog tab>
-- exec sp_GetIngredients
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetIngredients]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, ingContent, ingImage, ingLink
    FROM tbl_Ingredients
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetIngredientsContents]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Content per item in Blog tab>
-- exec sp_GetIngredientsContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetIngredientsContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, ingContent, ingImage
    FROM tbl_Ingredients
    WHERE ingLink = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetIngredientsGlossaryContents]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 22, 2015>
-- Description:	<Content per item in Ingredients Glossary tab>
-- exec sp_GetIngredientsGlossaryContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetIngredientsGlossaryContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT content
    FROM tbl_IngredientsGlossary
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetIngredientsGlossaryList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 22, 2015>
-- Description:	<All Ingredients Glossary Items>
-- exec sp_GetIngredientsGlossaryList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetIngredientsGlossaryList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		id, 
		title, 
		link,
		borderClass,
		divLastClass,
		divclass
    FROM tbl_IngredientsGlossary 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetOffersAndPromosContents]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<Content per item in Offers And Promos tab>
-- exec sp_GetOffersAndPromosContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetOffersAndPromosContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT content
    FROM tbl_OffersAndPromos
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetOffersAndPromosList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<All Offers And Promos Items>
-- exec sp_GetOffersAndPromosList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetOffersAndPromosList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		id, 
		title, 
		link,
		[image],
		usemap,
		width,
		height,
		coords,
		divclass
    FROM tbl_OffersAndPromos
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetOurStoryContents]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 13, 2015>
-- Description:	<Content per item in Our Story tab>
-- exec sp_GetOurStoryContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetOurStoryContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT content
    FROM tbl_OurStory
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetOurStoryList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 13, 2015>
-- Description:	<All Our Story Items>
-- exec sp_GetOurStoryList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetOurStoryList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, link
    FROM tbl_OurStory
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdCatDeletedList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Deleted Product Categories>
-- exec sp_GetProdCatDeletedList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdCatDeletedList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodcatid, prodcatName, prodcatDesc, 
    'Deleted'as [status]
    FROM tbl_ProductCategory
    WHERE deleted = '1'
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdCatDetailsByID]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Get Product Category Details By ID>
-- exec sp_GetProdCatDetailsByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdCatDetailsByID]
   @prodcatID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodcatName, prodcatDesc, prodcatStatus
    FROM tbl_ProductCategory
    WHERE prodcatID= @prodcatID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdCategoryName]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 23, 2015>
-- Description:	<All Products>
-- exec sp_GetProdCategoryName[2]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdCategoryName]
   @prodcatID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
	b.prodcatName
    FROM tbl_ProductCategory b
    WHERE b.deleted = '0' 
	AND b.prodcatID = @prodcatID
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdCatList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<All Product Categories>
-- exec sp_GetProdCatList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdCatList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodcatid, prodcatName, prodcatDesc, 
    CASE prodcatStatus WHEN '1' THEN 'Active' ELSE 'Inactive' END as [status]
    FROM tbl_ProductCategory
    WHERE deleted = '0'
    ORDER BY prodcatName asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdCatListActive]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Product Categories>
-- exec sp_GetProdCatListActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdCatListActive]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
	a.prodcatid,a. prodcatName, a.prodcatDesc,
	(SELECT COUNT(productID) FROM tbl_Product WHERE prodcatid = a.prodcatid) as totalCount
    FROM tbl_ProductCategory a
    WHERE a.deleted = '0' AND a.prodcatStatus = '1'
    ORDER BY a.rank ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdConcernDeletedList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Deleted Product Categories>
-- exec sp_GetProdConcernDeletedList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdConcernDeletedList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodconcernid, prodconcernName, prodconcernDesc, 
    'Deleted'as [status]
    FROM tbl_ProductConcern
    WHERE deleted = '1'
     ORDER BY prodconcernName ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdConcernDetailsByID]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Get Product Concern Details By ID>
-- exec sp_GetProdConcernDetailsByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdConcernDetailsByID]
   @prodconcernID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodconcernName, prodconcernDesc, prodconcernStatus
    FROM tbl_ProductConcern
    WHERE prodconcernID= @prodconcernID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdConcernList]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Product Concerns>
-- exec sp_GetProdConcernList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdConcernList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodconcernID, prodconcernName, prodconcernDesc, 
    CASE prodconcernStatus WHEN '1' THEN 'Active' ELSE 'Inactive' END as [status]
    FROM tbl_ProductConcern
    WHERE deleted = '0'
    ORDER BY prodconcernName ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdConcernListActive]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Product Concerns>
-- exec sp_GetProdConcernListActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdConcernListActive]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.prodconcernid, a.prodconcernName + 
	' (' +
	CONVERT(NVARCHAR,(SELECT COUNT(productid) FROM tbl_Product_ProductConcern WHERE prodconcernid = a.prodconcernid)) + ')' as prodconcernName
	, a.prodconcernDesc
    FROM tbl_ProductConcern a
    WHERE a.deleted = '0' AND a.prodconcernStatus = '1'
    ORDER BY a.rank ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdConcernListHover]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 02, 2015>
-- Description:	<Top 5 Product Concerns>
-- exec sp_GetProdConcernListHover
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdConcernListHover]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT TOP 5
	prodconcernid, prodconcernName
    FROM tbl_ProductConcern
    WHERE deleted = '0' AND prodconcernStatus = '1'
    ORDER BY rank ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdConcernName]    Script Date: 2/12/2015 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 03, 2015>
-- Description:	<Product Concern Name>
-- exec sp_GetProdConcernName[2]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdConcernName]
   @prodconID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
	b.prodconcernName
    FROM tbl_ProductConcern b
    WHERE b.deleted = '0' 
	AND b.prodconcernID = @prodconID
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdIngredientDeletedList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Deleted Product Ingredients>
-- exec sp_GetProdIngredientDeletedList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdIngredientDeletedList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodingredientid, prodingredientName, prodingredientDesc, 
    'Deleted'as [status]
    FROM tbl_ProductIngredient
    WHERE deleted = '1'
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdIngredientDetailsByID]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Get Product Ingredient Details By ID>
-- exec sp_GetProdIngredientDetailsByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdIngredientDetailsByID]
   @prodingredientid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodingredientName, prodingredientDesc, prodingredientStatus
    FROM tbl_ProductIngredient
    WHERE prodingredientid= @prodingredientid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdIngredientList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Product Ingredients>
-- exec sp_GetProdIngredientList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdIngredientList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT prodingredientid, prodingredientName, prodingredientDesc, 
    CASE prodingredientStatus WHEN '1' THEN 'Active' ELSE 'Inactive' END as [status]
    FROM tbl_ProductIngredient
    WHERE deleted = '0'
    ORDER BY prodingredientName asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdIngredientListActive]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Product Ingredients>
-- exec sp_GetProdIngredientListActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdIngredientListActive]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.prodingredientid, a.prodingredientName + ' (' +
	CONVERT(NVARCHAR,(SELECT COUNT(productid) FROM tbl_Product_ProductIngredient WHERE productingredientID = a.prodingredientid)) + ')' as prodingredientName,
	a.prodingredientDesc
    FROM tbl_ProductIngredient a
    WHERE a.deleted = '0' AND a.prodingredientStatus = '1'
    ORDER BY a.rank ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdIngredientListHover]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 02, 2015>
-- Description:	<Top 5 Product Ingredients Hover>
-- exec sp_GetProdIngredientListHover
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdIngredientListHover]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT TOP 5 prodingredientid, prodingredientName
    FROM tbl_ProductIngredient
    WHERE deleted = '0' AND prodingredientStatus = '1'
    ORDER BY rank ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdIngredientName]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 03, 2015>
-- Description:	<Product Ingredient Name>
-- exec sp_GetProdIngredientName[2]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdIngredientName]
   @prodingID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
	b.prodingredientName
    FROM tbl_ProductIngredient b
    WHERE b.deleted = '0' 
	AND b.prodingredientID = @prodingID
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdPerCategory]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 23, 2015>
-- Description:	<All Products>
-- exec sp_GetProdPerCategory[2]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdPerCategory]
  @pcatid nvarchar(500) = null,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null,
   @prodAgeID nvarchar(500) = null,
   @prodGenderID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT 
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN, a.productSize, a.productForm
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID
			LEFT JOIN tbl_Product_Age e
			ON a.productID = e.productID
			LEFT JOIN tbl_Product_Gender f
			ON a.productID = f.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500),@prodaID nvarchar(500),@prodgID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 ';

	IF @pcatid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND a.prodcatID IN  ('+ Convert(VARCHAR(500), @pcatid)+') ';
    END


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END

	IF @prodAgeID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND e.ageid IN  ('+ Convert(VARCHAR(500), @prodAgeID)+') ';
    END

	IF @prodGenderID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND f.genderid IN  ('+ Convert(VARCHAR(500), @prodGenderID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID, @prodaID = @prodAgeID, @prodgID = @prodGenderID;
     
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdRelatedAgeList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<All Product Related Age>
-- exec sp_GetProdRelatedAgeList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdRelatedAgeList]
   @productID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.ageRange
    FROM tbl_Product_Age a
    INNER JOIN tbl_Age b
	ON a.ageID  = b.id
	WHERE a.productID = @productID
    ORDER BY b.id asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdRelatedConcernList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 15, 2015>
-- Description:	<All Product Related Concerns>
-- exec sp_GetProdRelatedConcernList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdRelatedConcernList]
   @productID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.prodconcernName
    FROM tbl_Product_ProductConcern a
    INNER JOIN tbl_ProductConcern b
	ON a.prodconcernID  = b.prodconcernID
	WHERE a.productID = @productID
    ORDER BY b.prodconcernName asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdRelatedGenderList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<All Product Related Gender>
-- exec sp_GetProdRelatedGenderList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdRelatedGenderList]
   @productID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.gender
    FROM tbl_Product_Gender a
    INNER JOIN tbl_Gender b
	ON a.genderID  = b.id
	WHERE a.productID = @productID
    ORDER BY b.id asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdRelatedIngredientList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<All Product Related Ingredients>
-- exec sp_GetProdRelatedIngredientList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProdRelatedIngredientList]
   @productID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.prodIngredientName
    FROM tbl_Product_ProductIngredient a
    INNER JOIN tbl_ProductIngredient b
	ON a.productingredientID  = b.prodingredientID
	WHERE a.productID = @productID
    ORDER BY b.prodIngredientName asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductByProductCategory]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Product per Category>
-- exec sp_GetProductByProductCategory
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductByProductCategory]
   @prodcatID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT productID, productName, productDesc, productImage 
    FROM tbl_Product
    WHERE productStatus = '1' AND prodcatID = @prodcatID
    ORDER BY productName ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductCount]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 23, 2015>
-- Description:	<All Products>
-- exec sp_GetProductCount
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductCount]
 @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null,
   @prodAgeID nvarchar(500) = null,
   @prodGenderID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;

 
	  DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT TOP 6
	 COUNT(a.productid)
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			'
	, @ParamList nvarchar(max) = N'@prodingredientID nvarchar(500),@prodcnID nvarchar(500),@prodaID nvarchar(500),@prodgID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1' 

	IF @prodIngID IS NOT NULL
    BEGIN
	 SET @BaseQuery = @BaseQuery + ' LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID ';
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END
	IF @prodConcernID IS NOT NULL
    BEGIN
	 SET @BaseQuery = @BaseQuery + ' LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID ';
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END
	IF @prodAgeID IS NOT NULL
    BEGIN
	 SET @BaseQuery = @BaseQuery + 'LEFT JOIN tbl_Product_Age e
			ON a.productID = e.productID ';
        SET @WhereClause = @WhereClause + ' AND e.ageid IN  ('+ Convert(VARCHAR(500), @prodAgeID)+') ';
    END
	IF @prodGenderID IS NOT NULL
    BEGIN
	 SET @BaseQuery = @BaseQuery + ' LEFT JOIN tbl_Product_Gender f
			ON a.productID = f.productID ';
        SET @WhereClause = @WhereClause + ' AND f.genderid IN  ('+ Convert(VARCHAR(500), @prodGenderID)+') ';
    END

	 SET @BaseQuery = @BaseQuery + @WhereClause ;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID, @prodaID = @prodAgeID, @prodgID = @prodGenderID;
     
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductCountPerCategory]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 22, 2015>
-- Description:	<All Products>
-- exec sp_GetProductCountPerCategory
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductCountPerCategory]
   @prodcatID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT COUNT(a.productid) as total
    FROM tbl_Product a
	
    WHERE a.deleted = '0' AND 
	a.productStatus = '1' AND
	a.prodcatID = @prodcatID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductDeletedList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<All Deleted Products>
-- exec sp_GetProductDeletedList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductDeletedList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT productid, productName, productDesc, 
    'Deleted'as [status]
    FROM tbl_Product
    WHERE deleted = '1'
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductDetailsByID]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<Get Product Details By ID>
-- exec sp_GetProductDetailsByID[23]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductDetailsByID]
   @productid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		pc.prodcatName, a.productName, a.productDesc, a.productimage, a.productStatus,
		b.productBenefit, b.productIngredient, b.productDirection,
		a.productPrice, a.productPrice2, a.productSize, a.productSize2,
		a.productGTIN, a.productGTIN2, a.productForm, a.productCanonicalURL, a.productMetaDescription,
		pc.prodcatid, a.productShortDesc   
    FROM tbl_Product a
	INNER JOIN tbl_ProductCategory pc
	ON a.prodcatid = pc.prodcatid
	LEFT JOIN tbl_ProductDetail b
	ON a.productid = b.productid
    WHERE a.productid= @productid AND a.deleted = '0' AND a.productStatus = '1'
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductList]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Products>
-- exec sp_GetProductList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.productid, a.productName, a.productDesc, a.productImage,
    CASE a.productStatus WHEN '1' THEN 'Active' ELSE 'Inactive' END as [status],
	b.prodcatName, a.productShortDesc
    FROM tbl_Product a
	LEFT JOIN tbl_ProductCategory b
	ON a.prodcatID = b.prodcatID
    WHERE a.deleted = '0'
    ORDER BY b.prodcatname asc, a.productName asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListActive]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Products>
-- exec sp_GetProductListActive
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListActive]
   @prodcatID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT TOP 15
	a.productid, a.productName, a.productDesc, a.productImage, a.productPrice, a.productCount, 
	b.prodcatName
    FROM tbl_Product a
	INNER JOIN tbl_ProductCategory b
	ON a.prodcatid = b.prodcatid
    WHERE a.deleted = '0' 
	AND a.productStatus = '1'
    GROUP BY b.prodcatName, a.productID , a.productName , a.productDesc, a.productPrice, a.productCount, a.productImage
     ORDER BY b.prodcatName ASC, a.productName  ASC

	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListActiveSample]    Script Date: 2/12/2015 2:39:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Products>
-- exec sp_GetProductListActiveSample
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListActiveSample]
   @pcatid int,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null,
   @prodAgeID nvarchar(500) = null,
   @prodGenderID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT TOP 6
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID
			LEFT JOIN tbl_Product_Age e
			ON a.productID = e.productID
			LEFT JOIN tbl_Product_Gender f
			ON a.productID = f.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500),@prodaID nvarchar(500),@prodgID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 AND a.prodcatID = @prodcatID';


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END

	IF @prodAgeID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND e.ageid IN  ('+ Convert(VARCHAR(500), @prodAgeID)+') ';
    END

	IF @prodGenderID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND f.genderid IN  ('+ Convert(VARCHAR(500), @prodGenderID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID, @prodaID = @prodAgeID, @prodgID = @prodGenderID;
     
	

    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListAlphabetically]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 29, 2015>
-- Description:	<All Products Alphabetically>
-- exec sp_GetProductListAlphabetically
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListAlphabetically]
   @pcatid int,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT TOP 6
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 AND a.prodcatID = @prodcatID';


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause + ' ORDER BY a.productName ASC ';
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID;
     
	

    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListPopular]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 3, 2015>
-- Description:	<All Products Popular>
-- exec sp_GetProductListPopular
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListPopular]
   @pcatid int,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT TOP 6
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN,
			(SELECT COUNT (b.productid) FROM tbl_ProductRecentlyViewed b WHERE b.productid = a.productid) as countPopular
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 AND a.prodcatID = @prodcatID';


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause + ' ORDER BY countPopular DESC ';
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID;
     
	

    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListPriceASC]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 29, 2015>
-- Description:	<All Products Price ASC>
-- exec sp_GetProductListPriceASC
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListPriceASC]
   @pcatid int,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT TOP 6
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 AND a.prodcatID = @prodcatID';


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause + ' ORDER BY a.productPrice ASC ';
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID;
     
	

    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductListRecentlyViewed]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<All Products Recently Viewed>
-- exec sp_GetProductListRecentlyViewed
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductListRecentlyViewed]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT DISTINCT TOP 5
	a.productid, b.productName, b.productShortDesc, b.productImage, b.productPrice, b.productCount, c.userName, a.datetime, b.productGTIN
    FROM tbl_ProductRecentlyViewed a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
	LEFT JOIN tbl_User c
	ON a.userid = c.userID
	ORDER BY a.datetime DESC
        
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductRetailersByID]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 30, 2015>
-- Description:	<Get Product Retailers By ID>
-- exec sp_GetProductRetailersByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductRetailersByID]
   @productid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		a.retailerName, a.retailerImage, a.retailerStock, a.retailerLink
    FROM tbl_Product_Retailers a
	
    WHERE a.productid= @productid 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductsByFilter]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Products per Filter>
-- exec sp_GetProductsByFilter[1,'2']
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductsByFilter]
    @pcatid int,
    @pageid nvarchar(500),
    @pconcernid nvarchar(500),
    @pingredientid nvarchar(500),
    @pgenderid nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    DECLARE 
    
    @BaseQuery nvarchar(max) = N'SELECT DISTINCT a.productName
    FROM tbl_Product a
    LEFT JOIN tbl_Product_Age b
    ON a.productID = b.productID
    LEFT JOIN tbl_Product_ProductConcern c
    ON a.productID = c.productID
    LEFT JOIN tbl_Product_ProductIngredient d
    ON a.productID = d.productID
    LEFT JOIN tbl_Product_Gender e
    ON a.productID = e.genderID'
    , @ParamList nvarchar(max) = N'@prodcatID int, @ageID nvarchar(500), @prodconcernID nvarchar(500), @prodingredientID nvarchar(500), @prodgenderID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.productStatus=1';

       
    IF @pcatid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND a.prodcatID = @prodcatID';
    END
    
    IF @pageid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND b.ageID IN (@ageID)';
    END
    
    IF @pconcernid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.prodconcernID IN (@prodconcernID)';
    END
    
     IF @pingredientid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodingredientID IN (@prodingredientID)';
    END
    
     IF @pgenderid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND e.genderID IN (@prodgenderID)';
    END
 
    SET @BaseQuery = @BaseQuery + @WhereClause;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @ageId = @pageid, @prodconcernId = @pconcernid, @prodingredientId = @pingredientid, @prodgenderId = @pgenderid;
      
        
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedAmbassadors]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<All Related Ambassadors>
-- exec sp_GetRelatedAmbassadors[4]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedAmbassadors]
@ambassadorID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.title, b.ambassadorLink
    FROM tbl_SwisseAmbassadors_RelatedLinks a
	LEFT JOIN tbl_SwisseAmbassadors b
	ON a.relatedLinkID = b.id
    WHERE a.ambassadorID = @ambassadorID
    ORDER BY title asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedBlogFormats]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Related Blog Formats>
-- exec sp_GetRelatedBlogFormats
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedBlogFormats]
@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.blogFormat
    FROM tbl_Blog_Formats a
	LEFT JOIN tbl_BlogFormat b
	ON a.formatid = b.blogFormatID
    WHERE a.blogid = @blogid
    ORDER BY blogFormat asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedBlogHealthAndHappiness]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Related Blog Health And Happiness>
-- exec sp_GetRelatedBlogHealthAndHappiness
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedBlogHealthAndHappiness]
@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.title
    FROM tbl_Blog_HealthAndHappiness a
	LEFT JOIN tbl_HealthAndHappinessBlog b
	ON a.healthandhappinessblogid = b.id
    WHERE a.blogid = @blogid
    ORDER BY title asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedBlogProducts]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Related Blog Products>
-- exec sp_GetRelatedBlogProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedBlogProducts]
@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.productName
    FROM tbl_Blog_RelatedProducts a
	LEFT JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.blogid = @blogid
    ORDER BY productName asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedBlogs]    Script Date: 2/12/2015 2:39:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Related Blogs>
-- exec sp_GetRelatedBlogs[4]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedBlogs]
@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.title, b.blogLink
    FROM tbl_Blog_RelatedBlogs a
	LEFT JOIN tbl_Blog b
	ON a.relatedblogid = b.id
    WHERE a.blogid = @blogid
    ORDER BY title asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedBlogThemes]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<All Related Blog Themes>
-- exec sp_GetRelatedBlogThemes
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedBlogThemes]
@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.blogTheme 
    FROM tbl_Blog_Themes a
	LEFT JOIN tbl_BlogTheme b
	ON a.themeid = b.blogThemeID
    WHERE a.blogid = @blogid
    ORDER BY blogTheme asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedIngredients]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<All Related Ingredients>
-- exec sp_GetRelatedIngredients[4]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedIngredients]
@ingid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.title, b.ingLink
    FROM tbl_Ingredients_RelatedIngredients a
	LEFT JOIN tbl_Ingredients b
	ON a.relatedingid = b.id
    WHERE a.ingid = @ingid
    ORDER BY title asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsAboutSwisseList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get About Swisse Related Products By ID>
-- exec sp_GetRelatedProductsAboutSwisseList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsAboutSwisseList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_AboutSwisse_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsAmbassadorList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get Ambassadors Related Products By ID>
-- exec sp_GetRelatedProductsAmbassadorList[1]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsAmbassadorList]
   @ambassadorID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_SwisseAmbassadors_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.ambassadorID= @ambassadorID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsBlogList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 08, 2015>
-- Description:	<Get Blog Related Products By ID>
-- exec sp_GetRelatedProductsBLogList[1]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsBlogList]
   @blogid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_Blog_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.blogid= @blogid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsClinicalTrialsList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Get Clinical Trials Related Products By ID>
-- exec sp_GetRelatedProductsClinicalTrialsList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsClinicalTrialsList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_ClinicalTrials_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsCorporateSocialList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get Corporate Social Related Products By ID>
-- exec sp_GetRelatedProductsCorporateSocialList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsCorporateSocialList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_CorporateSocial_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsIngList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get Ingredients Related Products By ID>
-- exec sp_GetRelatedProductsIngList[1]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsIngList]
   @ingid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_Ingredients_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.ingID= @ingid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsIngredientList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get Ingredients Related Products By ID>
-- exec sp_GetRelatedProductsIngredientList[1]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsIngredientList]
   @ingID as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		a.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_Ingredients_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.ingID= @ingID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 30, 2015>
-- Description:	<Get Product Related Products By ID>
-- exec sp_GetRelatedProductsList[1]
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsList]
   @productid as int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_Product_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.relatedproductID = b.productid
    WHERE a.productid= @productid 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsOurHistoryList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get Our History Related Products By ID>
-- exec sp_GetRelatedProductsOurHistoryList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsOurHistoryList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_OurHistory_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsOurPhilosophyList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Get Our Philosophy Related Products By ID>
-- exec sp_GetRelatedProductsOurPhilosophyList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsOurPhilosophyList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_OurPhilosophy_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsOurResearchList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Get Our Research Related Products By ID>
-- exec sp_GetRelatedProductsOurResearchList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsOurResearchList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_OurResearch_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsOurScientificPartnershipList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Get Our Scientific Partnership Related Products By ID>
-- exec sp_GetRelatedProductsOurScientificPartnershipList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsOurScientificPartnershipList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_OurScientificPartnership_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedProductsSwisseAdvisoryList]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Get Swisse Advisory Related Products By ID>
-- exec sp_GetRelatedProductsSwisseAdvisoryList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedProductsSwisseAdvisoryList]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
		b.productID, b.productName, b.productImage, b.productPrice, b.productGTIN
    FROM tbl_SwisseAdvisory_RelatedProducts a
	INNER JOIN tbl_Product b
	ON a.productid = b.productid
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetScienceOfSwisseContents]    Script Date: 2/12/2015 2:39:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Content per item in Science Of Swisse tab>
-- exec sp_GetScienceOfSwisseContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetScienceOfSwisseContents]
   @link nvarchar(500)
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT content
    FROM tbl_TheScienceOfSwisse
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetScienceOfSwisseList]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Science Of Swisse Items>
-- exec sp_GetScienceOfSwisseList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetScienceOfSwisseList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, link
    FROM tbl_TheScienceOfSwisse
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetShopProductMenuHover_Cat]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 02, 2015>
-- Description:	<Top 6 Product Categories>
-- exec sp_GetShopProductMenuHover_Cat
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetShopProductMenuHover_Cat]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT TOP 5
	a.prodcatid, a.prodcatName, a.prodcatImage
    FROM tbl_ProductCategory a
    WHERE a.deleted = '0' AND a.prodcatStatus = '1'
	AND a.prodcatImage != ''
    ORDER BY a.rank ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetShopProductsList]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<All Shop Products>
-- exec sp_GetShopProductsList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetShopProductsList]
   
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT id, title, link
    FROM tbl_ShopProducts
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetSwisseAmbassadors]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<All Ambassadors>
-- exec sp_GetSwisseAmbassadors
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetSwisseAmbassadors]
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, a.title, a.ambassadorContent, a.ambassadorImage 
    FROM tbl_SwisseAmbassadors a
    
    ORDER BY a.title asc
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserByEmailPassword]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Get User By Email and Password>
-- exec sp_GetUserByEmailPassword
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserByEmailPassword]
	@Email nvarchar(500),
	@Password nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
        userID,
        userEmail,
        userName,
        userPassword  
    FROM tbl_User 
    WHERE userEmail = @Email
    AND userPassword = @Password
    AND userStatus = '1'
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserDetailsByID]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Get User Details By ID>
-- exec sp_getUserDetailsByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserDetailsByID]
	@userID int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT userID, userEmail, userName, userFN, userMN, userLN, userPassword 
    FROM tbl_User
    WHERE userID = @userID
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserEmail]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 12, 2015>
-- Description:	<Get User Email>
-- exec sp_GetUserEmail
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserEmail]
	@Email nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
        userID,
        userEmail,
        userName,
        userPassword  
    FROM tbl_User 
    WHERE userEmail = @Email

    AND userStatus = '1'
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserFirstLogged]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Users First Logged>
-- exec sp_GetUserFirstLogged
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserFirstLogged]
	@Email nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
    firstLogged
    FROM tbl_User
    WHERE userEmail = @Email
      
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserList]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<All Users>
-- exec sp_getUserList
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserList]
	
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT 
    userID, 
    userEmail, 
    userName, 
    userFN, 
    userMN, 
    userLN, 
    userPassword,
    userStatus
    FROM tbl_User
    WHERE userStatus = '1'
      
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAboutSwisseProducts]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New About Swisse Products>
-- exec sp_InsertAboutSwisseProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertAboutSwisseProducts]
    
	@aboutswisseproductid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_AboutSwisse_RelatedProducts (productid)
    VALUES  (
        @aboutswisseproductid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAmbassadors]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Ambassador>
-- exec sp_InsertAmbassadors
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertAmbassadors]
    @Title nvarchar(500),
    @Content text,
    @Image nvarchar(50),
    @Link nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_SwisseAmbassadors(title ,ambassadorContent , ambassadorImage, ambassadorLink)
    VALUES  (
        @Title,
	    @Content,
	    @Image,
		@Link
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAmbassadorsProducts]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Ambassadors Products>
-- exec sp_InsertAmbassadorsProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertAmbassadorsProducts]
    @productid int,
	@ambassadorid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_SwisseAmbassadors_RelatedProducts (productid,ambassadorid)
    VALUES  (
        @productid,
	    @ambassadorid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBlog]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Insert New Blog>
-- exec sp_InsertBlog
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlog]
    @blogTitle nvarchar(500),
    @blogContent text,
    @blogImage nvarchar(50),
    @blogLink nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Blog (title ,blogContent , blogImage, blogLink)
    VALUES  (
        @blogTitle,
	    @blogContent,
	    @blogImage,
		@blogLink
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBlogFormat]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Insert New Blog Format>
-- exec sp_InsertBlogFormat
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlogFormat]
    @formatid int,
	@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Blog_Formats (formatid,blogid)
    VALUES  (
        @formatid,
	    @blogid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBlogHealthAndHappiness]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Insert New Blog Health And Happiness>
-- exec sp_InsertBlogHealthAndHappiness
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlogHealthAndHappiness]
    @healthandhappinessblogid int,
	@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Blog_HealthAndHappiness (healthandhappinessblogid,blogid)
    VALUES  (
        @healthandhappinessblogid,
	    @blogid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBlogProducts]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Insert New Blog Products>
-- exec sp_InsertBlogProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlogProducts]
    @productid int,
	@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Blog_RelatedProducts (productid,blogid)
    VALUES  (
        @productid,
	    @blogid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBlogRelatedBlogs]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Insert New Blog Related Blogs>
-- exec sp_InsertBlogRelatedBlogs
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlogRelatedBlogs]
    @relatedblogid int,
	@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Blog_RelatedBlogs(relatedblogid,blogid)
    VALUES  (
        @relatedblogid,
	    @blogid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertBlogTheme]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Insert New Blog Theme>
-- exec sp_InsertBlogTheme
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlogTheme]
    @themeid int,
	@blogid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Blog_Themes (themeid,blogid)
    VALUES  (
        @themeid,
	    @blogid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertClinicalTrialsProducts]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Insert New Clinical Trials Products>
-- exec sp_InsertClinicalTrialsProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertClinicalTrialsProducts]
    
	@productid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_ClinicalTrials_RelatedProducts (productid)
    VALUES  (
        @productid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertCorporateSocialProducts]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Corporate Social Swisse Products>
-- exec sp_InsertCorporateSocialProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertCorporateSocialProducts]
    
	@aboutswisseproductid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_CorporateSocial_RelatedProducts (productid)
    VALUES  (
        @aboutswisseproductid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertFAQsList]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 12, 2015>
-- Description:	<Insert New FAQs>
-- exec sp_InsertFAQsList
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertFAQsList]
    @title nvarchar(500),
    @content text,
    @image nvarchar(50),
    @link nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_FAQs (title ,[content] , [image], link)
    VALUES  (
        @title,
	    @content,
	    @image,
		@link
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertIngredient]    Script Date: 2/12/2015 2:39:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Ingredient>
-- exec sp_InsertIngredient
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertIngredient]
    @Title nvarchar(500),
    @Content text,
    @Image nvarchar(50),
    @Link nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Ingredients(title ,ingContent , ingImage, ingLink)
    VALUES  (
        @Title,
	    @Content,
	    @Image,
		@Link
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertIngredientsProducts]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Ingredients Products>
-- exec sp_InsertIngredientsProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertIngredientsProducts]
    @productid int,
	@ingid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Ingredients_RelatedProducts (productid,ingid)
    VALUES  (
        @productid,
	    @ingid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertOurHistoryProducts]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Our History Swisse Products>
-- exec sp_InsertOurHistoryProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOurHistoryProducts]
    
	@aboutswisseproductid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_OurHistory_RelatedProducts (productid)
    VALUES  (
        @aboutswisseproductid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertOurPhilosophyProducts]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert New Our Philosophy Swisse Products>
-- exec sp_InsertOurPhilosophyProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOurPhilosophyProducts]
    
	@aboutswisseproductid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_OurPhilosophy_RelatedProducts (productid)
    VALUES  (
        @aboutswisseproductid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertOurResearchProducts]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Insert New Our Research Products>
-- exec sp_InsertOurResearchProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOurResearchProducts]
    
	@productid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_OurResearch_RelatedProducts (productid)
    VALUES  (
        @productid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertOurScientificPartnershipProducts]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Insert New Our Scientific Partnership Products>
-- exec sp_InsertOurScientificPartnershipProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOurScientificPartnershipProducts]
    
	@productid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_OurScientificPartnership_RelatedProducts (productid)
    VALUES  (
        @productid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdCat]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Insert New Product Category>
-- exec sp_InsertProdCat
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdCat]
   
    @prodcatName nvarchar(500),
    @prodcatDesc nvarchar(500),
    @prodcatStatus int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_ProductCategory (prodcatName, prodcatDesc, prodcatStatus)
    VALUES  (
      
	    @prodcatName,
	    @prodcatDesc,
	    @prodcatStatus
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdConcern]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Insert New Product Concern>
-- exec sp_InsertProdConcern
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdConcern]
   
    @prodconcernName nvarchar(500),
    @prodconcernDesc nvarchar(500),
    @prodconcernStatus int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_ProductConcern (prodconcernName, prodconcernDesc, prodconcernStatus)
    VALUES  (
      
	    @prodconcernName,
	    @prodconcernDesc,
	    @prodconcernStatus
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdIngredient]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Insert New Product Ingredient>
-- exec sp_InsertProdIngredient
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdIngredient]
   
    @prodingredientName nvarchar(500),
    @prodingredientDesc nvarchar(500),
    @prodingredientStatus int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_ProductIngredient (prodingredientName, prodingredientDesc, prodingredientStatus)
    VALUES  (
      
	    @prodingredientName,
	    @prodingredientDesc,
	    @prodingredientStatus
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdRelatedAge]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 29, 2015>
-- Description:	<Insert New Product Age>
-- exec sp_InsertProdRelatedAge
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdRelatedAge]
   
    @productID int,
    @prodageID int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Product_Age(productID,ageID)
    VALUES  (
      
	   @productID,
	   @prodageID
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdRelatedConcern]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 15, 2015>
-- Description:	<Insert New Product Concern>
-- exec sp_InsertProdRelatedConcern
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdRelatedConcern]
   
    @productID int,
    @prodconcernID int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Product_ProductConcern(productID,prodconcernID)
    VALUES  (
      
	   @productID,
	   @prodconcernID
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdRelatedGender]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Insert New Product Gender>
-- exec sp_InsertProdRelatedGender
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdRelatedGender]
   
    @productID int,
    @prodgenderID int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Product_Gender(productID,genderID)
    VALUES  (
      
	   @productID,
	   @prodgenderID
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdRelatedIngredient]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Insert New Product Ingredient>
-- exec sp_InsertProdRelatedIngredient
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdRelatedIngredient]
   
    @productID int,
    @prodingredientID int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Product_ProductIngredient (productID,productingredientID)
    VALUES  (
      
	   @productID,
	   @prodingredientID
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProdRelatedProduct]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Insert New Product Product>
-- exec sp_InsertProdRelatedProduct
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProdRelatedProduct]
   
    @productID int,
    @prodRelatedprodID int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Product_RelatedProducts(productID,relatedproductID)
    VALUES  (
      
	   @productID,
	   @prodRelatedprodID
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProduct]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<Insert New Product>
-- exec sp_InsertProduct
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProduct]
    @productCat nvarchar(500),
    @productName nvarchar(500),
    @productDesc text,
	@productShortDesc nvarchar(500),
	@productImage nvarchar(500),
    @productStatus int,
	@productSize nvarchar(500),
	@productSize2 nvarchar(500),
	@productPrice nvarchar(500),
	@productPrice2 nvarchar(500),
	@productGTIN nvarchar(500),
	@productGTIN2 nvarchar(500),
	@productForm nvarchar(500),
	@productCanonicalURL nvarchar(500),
	@productMetaDescription nvarchar(500),
	@productBenefit text,
    @productIngredient text,
    @productDirection text
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_Product (prodcatID ,productName , productDesc, productImage , productStatus,
				productSize,productSize2,productPrice,productPrice2,productGTIN,productGTIN2,productForm,productCanonicalURL,productMetaDescription,productShortDesc)
    VALUES  (
        @productCat,
	    @productName,
	    @productDesc,
		@productImage,
	    @productStatus,
		@productSize,
		@productSize2,
		@productPrice,
		@productPrice2,
		@productGTIN,
		@productGTIN2,
		@productForm,
		@productCanonicalURL,
		@productMetaDescription,
		@productShortDesc
  );

  
  INSERT INTO tbl_ProductDetail(productID,productBenefit,productIngredient,productDirection)
  VALUES(scope_identity(),
   @productBenefit,
   @productIngredient,
   @productDirection);
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProductDetail]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 30, 2015>
-- Description:	<Insert New Product Detail>
-- exec sp_InsertProductDetail
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProductDetail]
    @productid int,
    @productBenefit text,
	@productIngredient text,
	@productDirection text
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_ProductDetail (productid, productBenefit, productIngredient, productDirection)
    VALUES  (
      
	    @productid,
	    @productBenefit,
	    @productIngredient,
		@productDirection
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertRecentlyViewed]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Insert Recently Viewed>
-- exec sp_InsertRecentlyViewed
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertRecentlyViewed]
   
    @userid int,
    @productid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_ProductRecentlyViewed (userid, productid)
	    VALUES  (
      
	    @userid,
	    @productid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertRelatedAmbassadors]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Insert Related Ambassadors>
-- exec sp_InsertRelatedAmbassadors
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertRelatedAmbassadors]
    @relatedid int,
	@ambassadorID int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_SwisseAmbassadors_RelatedLinks(relatedLinkID,ambassadorID)
    VALUES  (
        @relatedid,
	    @ambassadorID
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertSwisseAdvisoryProducts]    Script Date: 2/12/2015 2:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 11, 2015>
-- Description:	<Insert New Swisse Advisory Products>
-- exec sp_InsertSwisseAdvisoryProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertSwisseAdvisoryProducts]
    
	@productid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_SwisseAdvisory_RelatedProducts (productid)
    VALUES  (
        @productid
  )
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUser]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Insert New User>
-- exec sp_InsertUser
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertUser]

    @UserName nvarchar(500),
	@Email nvarchar(500),
	@Password nvarchar(500),
	@FirstName nvarchar(500),
	@MiddleName nvarchar(500),
	@LastName nvarchar(500),
	@Status int,
	@userBirthMonth nvarchar(500),
	@userBirthYear nvarchar(500),
	@userPostCode nvarchar(500),
	@checkAgreed int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_User (userName,userEmail,userPassword,userFN,userMN,userLN,userStatus,userBirthMonth,userBirthYear,userPostCode,checkAgreed)
    VALUES  (
        @UserName,
	    @Email,
	    @Password,
	    @FirstName,
	    @MiddleName,
	    @LastName,
	    @Status,
		@userBirthMonth,
		@userBirthYear,
		@userPostCode,
		@checkAgreed
     );
     

     --INSERT INTO tbl_User_Country(userid,countryid)
     --VALUES (SCOPE_IDENTITY(),'0');
END

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUserLogs]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<User Logs>
-- exec sp_InsertUserLogs
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertUserLogs]
	@userID int,
	@activity nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_UserLogs (userID, activity)
    VALUES(@userID, @activity);
      
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateAmbassador]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Update Ambassador>
-- exec sp_UpdateAmbassador
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateAmbassador]
	@id int,
    @Title nvarchar(500),
    @Content text,
    @Image nvarchar(50),
    @Link nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_SwisseAmbassadors 
	SET title = @Title,
	ambassadorContent = @Content, 
	ambassadorImage = @Image,
	ambassadorLink = @Link
    
	WHERE id = @id  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateBlog]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 07, 2015>
-- Description:	<Update Blog>
-- exec sp_UpdateBlog
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateBlog]
	@id int,
    @blogTitle nvarchar(500),
    @blogContent text,
    @blogImage nvarchar(50),
    @blogLink nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_Blog 
	SET title = @blogTitle,
	blogContent = @blogContent, 
	blogImage = @blogImage,
	blogLink = @blogLink
    
	WHERE id = @id  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateFAQ]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 12, 2015>
-- Description:	<Update FAQ>
-- exec sp_UpdateFAQ
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateFAQ]
	@id int,
    @title nvarchar(500),
    @content text,
    @image nvarchar(50),
    @link nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_FAQs 
	SET title = @title,
	[content] = @content, 
	[image] = @image,
	link = @link
    
	WHERE id = @id  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateHealthAndHappinessBlogContents]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<Content update per item in Health And Happiness Blog tab>
-- exec sp_UpdateHealthAndHappinessBlogContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateHealthAndHappinessBlogContents]
   @link nvarchar(500),
   @content text
   
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_HealthAndHappinessBlog
    SET content = @content
     
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateIngredient]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 10, 2015>
-- Description:	<Update Ingredient>
-- exec sp_UpdateIngredient
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateIngredient]
	@id int,
    @Title nvarchar(500),
    @Content text,
    @Image nvarchar(50),
    @Link nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_Ingredients 
	SET title = @Title,
	ingContent = @Content, 
	ingImage = @Image,
	ingLink = @Link
    
	WHERE id = @id  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateIngredientsGlossaryContents]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 22, 2015>
-- Description:	<Content update per item in Ingredients Glossary tab>
-- exec sp_UpdateIngredientsGlossaryContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateIngredientsGlossaryContents]
   @link nvarchar(500),
   @content text
   
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_IngredientsGlossary
    SET content = @content
     
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateOffersAndPromosContents]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 20, 2015>
-- Description:	<Content update per item in Offers And Promos tab>
-- exec sp_UpdateOffersAndPromosContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateOffersAndPromosContents]
   @link nvarchar(500),
   @content text
   
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_OffersAndPromos
    SET content = @content
     
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateOurStoryContents]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 13, 2015>
-- Description:	<Content update per item in Our Story tab>
-- exec sp_UpdateOurStoryContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateOurStoryContents]
   @link nvarchar(500),
   @content text
   
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_OurStory
    SET content = @content
     
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateProdCatByID]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Update Product Category By ID>
-- exec sp_UpdateProdCatByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateProdCatByID]
   @prodcatID as int,
   @prodcatName as nvarchar(500),
   @prodcatDesc nvarchar(500),
   @prodcatStatus int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_ProductCategory
    SET prodcatName = @prodcatName,
    prodcatDesc = @prodcatDesc,
    prodcatStatus = @prodcatStatus 
    WHERE prodcatID= @prodcatID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateProdConcernByID]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Update Product Concern By ID>
-- exec sp_UpdateProdConcernByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateProdConcernByID]
   @prodconcernID as int,
   @prodconcernName as nvarchar(500),
   @prodconcernDesc nvarchar(500),
   @prodconcernStatus int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_ProductConcern
    SET prodconcernName = @prodconcernName,
    prodconcernDesc = @prodconcernDesc,
    prodconcernStatus = @prodconcernStatus 
    WHERE prodconcernID= @prodconcernID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateProdIngredientByID]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Update Product Ingredient By ID>
-- exec sp_UpdateProdIngredientByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateProdIngredientByID]
   @prodingredientID as int,
   @prodingredientName as nvarchar(500),
   @prodingredientDesc nvarchar(500),
   @prodingredientStatus int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_ProductIngredient
    SET prodingredientName = @prodingredientName,
    prodingredientDesc = @prodingredientDesc,
    prodingredientStatus = @prodingredientStatus 
    WHERE prodingredientID= @prodingredientID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateProductByID]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 14, 2015>
-- Description:	<Update Product By ID>
-- exec sp_UpdateProductByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateProductByID]
   @prodcatID as int,
   @productID as int,
   @productName as nvarchar(500),
   @productDesc text,
   @productShortDesc nvarchar(500),
   @productImage nvarchar(500),
   @productStatus int,
   @productBenefit text,
   @productIngredient text,
   @productDirection text
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_Product
    SET 
	prodcatID = @prodcatID,
	productName = @productName,
    productDesc = @productDesc,
	productShortDesc = @productShortDesc,
	productImage = @productImage,
    productStatus = @productStatus 
    WHERE productID= @productID;

	 UPDATE tbl_ProductDetail
    SET 
	
	productBenefit = @productBenefit,
	productIngredient = @productIngredient,
    productDirection = @productDirection 
    WHERE productID= @productID;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateProfile]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 27, 2015>
-- Description:	<Update User>
-- exec sp_UpdateProfile
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateProfile]
	@Email nvarchar(500),
	@FirstName nvarchar(500),
	@LastName nvarchar(500),
    @UserBMonth nvarchar(500),
    @UserBYear nvarchar(500),
    @UserPostCode nvarchar(500),
	@checkAgreed int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User
    SET
	    userFN = @FirstName,
	    userLN = @LastName,
		userBirthMonth = @UserBMonth,
		userBirthYear = @UserBYear,
		userPostCode = @UserPostCode, 
	    checkAgreed = @checkAgreed
	 WHERE userEmail = @Email
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateScienceOfSwisseContents]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 12, 2015>
-- Description:	<Content update per item in Science Of Swisse tab>
-- exec sp_UpdateScienceOfSwisseContents
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateScienceOfSwisseContents]
   @link nvarchar(500),
   @content text
   
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_TheScienceOfSwisse
    SET content = @content
     
    WHERE link = @link
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserByEmailRegistration]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Update User Status By Email Confirmation>
-- exec sp_UpdateUserByEmailRegistration
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUserByEmailRegistration]
	@Email nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User 
    SET userStatus = '1'
    WHERE userEmail = @Email
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserByID]    Script Date: 2/12/2015 2:39:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Update User>
-- exec sp_UpdateUserByID
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUserByID]
    @userID int,
    @UserName nvarchar(500),
	@Email nvarchar(500),
	@Password nvarchar(500),
	@FirstName nvarchar(500),
	@MiddleName nvarchar(500),
	@LastName nvarchar(500),
	@Status int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User
    SET
        userName = @UserName,
	    userPassword = @Password,
	    userFN = @FirstName,
	    userMN = @MiddleName,
	    userLN = @LastName,
	    userStatus = @Status 
	 WHERE userID = @userID
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserCountry]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Update User Country>
-- exec sp_UpdateUserCountry
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUserCountry]
   @userid int,
   @countryid int
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User_Country
    SET
        countryid = @countryid
	 WHERE userid = @userid
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserFirstLogged]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Update User First Logged>
-- exec sp_UpdateUserFirstLogged
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUserFirstLogged]
   @Email nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User
    SET
        firstLogged = '1' 
	 WHERE userEmail = @Email
  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserOldPassword]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 10, 2015>
-- Description:	<Update User Old Password>
-- exec sp_UpdateUserOldPassword
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUserOldPassword]
	@Email nvarchar(500),
	@newPassword nvarchar(500),
	@oldPassword nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User 
    SET userPassword = @newPassword
    WHERE userEmail = @Email
    AND userPassword = @oldPassword
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserPasswordByEmail]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 11, 2015>
-- Description:	<Update User Password>
-- exec sp_UpdateUserPasswordByEmail
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUserPasswordByEmail]
	@Email nvarchar(500),
	@pass nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE tbl_User 
    SET userPassword = @pass
    WHERE userEmail = @Email
    
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ViewProfile]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 27, 2015>
-- Description:	<View User>
-- exec sp_ViewProfile
-- =============================================
CREATE PROCEDURE [dbo].[sp_ViewProfile]
    @Email nvarchar(500)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT
	userFN, userLN, userPostCode, userBirthMonth , userBirthYear, checkAgreed
	
	FROM tbl_User
    WHERE userEmail = @Email
  
END

GO
/****** Object:  Table [dbo].[tbl_AboutSwisse_RelatedProducts]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_AboutSwisse_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_AboutSwisse_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Age]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Age](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ageRange] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_Age] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Banner]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Banner](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bannerName] [nvarchar](500) NULL,
	[bannerImage] [nvarchar](500) NULL,
	[bannerLink] [nvarchar](500) NULL,
	[status] [int] NULL,
	[deleted] [int] NULL,
 CONSTRAINT [PK_tbl_Banner] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Blog]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Blog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[blogContent] [text] NULL,
	[blogImage] [nvarchar](50) NULL,
	[blogLink] [nvarchar](50) NULL,
	[deleted] [int] NULL,
 CONSTRAINT [PK_tbl_Blog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Blog_Formats]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Blog_Formats](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blogid] [int] NULL,
	[formatid] [int] NULL,
 CONSTRAINT [PK_tbl_Blog_Formats] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Blog_HealthAndHappiness]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Blog_HealthAndHappiness](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blogid] [int] NULL,
	[healthandhappinessblogid] [int] NULL,
 CONSTRAINT [PK_tbl_Blog_HealthAndHappiness] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Blog_RelatedBlogs]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Blog_RelatedBlogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blogid] [int] NULL,
	[relatedblogid] [int] NULL,
 CONSTRAINT [PK_tbl_Blog_RelatedBlogs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Blog_RelatedProducts]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Blog_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blogid] [int] NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_Blog_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Blog_Themes]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Blog_Themes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blogid] [int] NULL,
	[themeid] [int] NULL,
 CONSTRAINT [PK_tbl_Blog_Themes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_BlogFormat]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BlogFormat](
	[blogFormatID] [int] IDENTITY(1,1) NOT NULL,
	[blogFormat] [nvarchar](500) NULL,
	[blogFormatDesc] [nvarchar](500) NULL,
	[blogFormatStatus] [int] NULL,
	[deleted] [int] NULL,
 CONSTRAINT [PK_tbl_BlogFormat] PRIMARY KEY CLUSTERED 
(
	[blogFormatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_BlogTheme]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BlogTheme](
	[blogThemeID] [int] IDENTITY(1,1) NOT NULL,
	[blogTheme] [nvarchar](500) NULL,
	[blogThemeDesc] [nvarchar](500) NULL,
	[blogThemeStatus] [int] NULL,
	[deleted] [int] NULL,
 CONSTRAINT [PK_tbl_BlogTheme] PRIMARY KEY CLUSTERED 
(
	[blogThemeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ClinicalTrials_RelatedProducts]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ClinicalTrials_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_ClinicalTrials_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_CMSUser]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CMSUser](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](500) NULL,
	[password] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_CMSUser] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ContactUs]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ContactUs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [nvarchar](500) NULL,
	[lastName] [nvarchar](500) NULL,
	[email] [nvarchar](500) NOT NULL,
	[gender] [nvarchar](50) NULL,
	[dobMonth] [nvarchar](50) NULL,
	[dobDate] [nvarchar](50) NULL,
	[dobYear] [nvarchar](50) NULL,
	[mobile] [nvarchar](50) NOT NULL,
	[postCode] [nvarchar](50) NULL,
	[country] [nvarchar](50) NOT NULL,
	[typeOfQuery] [nvarchar](50) NOT NULL,
	[message] [text] NOT NULL,
	[checkAgreed] [int] NULL,
 CONSTRAINT [PK_tbl_ContactUs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_CorporateSocial_RelatedProducts]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CorporateSocial_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_CorporateSocial_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Country]    Script Date: 2/12/2015 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Country](
	[countryID] [int] IDENTITY(1,1) NOT NULL,
	[countryName] [nvarchar](500) NULL,
	[countryLanguage] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED 
(
	[countryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_FAQs]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FAQs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
	[content] [text] NULL,
	[image] [nvarchar](50) NULL,
	[usemap] [nvarchar](50) NULL,
	[coords] [nvarchar](50) NULL,
	[divclass] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_FAQs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Gender]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Gender](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[gender] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_Gender] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_HealthAndHappinessBlog]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_HealthAndHappinessBlog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
	[content] [text] NULL,
 CONSTRAINT [PK_tbl_HealthAndHappinessBlog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Ingredients]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Ingredients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[ingContent] [text] NULL,
	[ingImage] [nvarchar](50) NULL,
	[ingLink] [nvarchar](50) NULL,
	[deleted] [int] NULL,
 CONSTRAINT [PK_tbl_Ingredients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Ingredients_RelatedIngredients]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Ingredients_RelatedIngredients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ingid] [int] NULL,
	[relatedingid] [int] NULL,
 CONSTRAINT [PK_tbl_Ingredient_RelatedIngredients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Ingredients_RelatedProducts]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Ingredients_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ingID] [int] NULL,
	[productID] [int] NULL,
 CONSTRAINT [PK_tbl_Ingredients_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_IngredientsGlossary]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_IngredientsGlossary](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
	[content] [text] NULL,
	[borderClass] [nvarchar](50) NULL,
	[divLastClass] [nvarchar](50) NULL,
	[divClass] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_IngredientsGlossary] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_OffersAndPromos]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OffersAndPromos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
	[content] [text] NULL,
	[image] [nvarchar](50) NULL,
	[usemap] [nvarchar](50) NULL,
	[width] [nvarchar](50) NULL,
	[height] [nvarchar](50) NULL,
	[coords] [nvarchar](50) NULL,
	[divclass] [nvarchar](255) NULL,
 CONSTRAINT [PK_tbl_OffersAndPromos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_OurHistory_RelatedProducts]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OurHistory_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_OurHistory_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_OurPhilosophy_RelatedProducts]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OurPhilosophy_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_OurPhilosophy_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_OurResearch_RelatedProducts]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OurResearch_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_OurResearch_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_OurScientificPartnership_RelatedProducts]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OurScientificPartnership_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_OurScientificPartnership_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_OurStory]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_OurStory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
	[content] [text] NULL,
 CONSTRAINT [PK_tbl_OurStory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product](
	[productID] [int] IDENTITY(1,1) NOT NULL,
	[prodcatID] [int] NULL,
	[productName] [nvarchar](500) NULL,
	[productDesc] [text] NULL,
	[productImage] [nvarchar](500) NULL,
	[productPrice] [nvarchar](500) NULL,
	[productPrice2] [nvarchar](500) NULL,
	[productManufacturer] [nvarchar](500) NULL,
	[productSize] [nvarchar](50) NULL,
	[productSize2] [nvarchar](50) NULL,
	[productGTIN] [nvarchar](500) NULL,
	[productGTIN2] [nvarchar](500) NULL,
	[productForm] [nvarchar](500) NULL,
	[productCanonicalURL] [nvarchar](500) NULL,
	[productMetaDescription] [nvarchar](500) NULL,
	[productCount] [nvarchar](50) NULL,
	[productStatus] [int] NULL,
	[deleted] [int] NULL,
	[productShortDesc] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_Product] PRIMARY KEY CLUSTERED 
(
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_Age]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_Age](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[ageID] [int] NULL,
 CONSTRAINT [PK_tbl_Product_Age] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_Gender]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_Gender](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[genderID] [int] NULL,
 CONSTRAINT [PK_tbl_Product_Gender] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_Keywords]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_Keywords](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[keywords] [text] NULL,
 CONSTRAINT [PK_tbl_ProductKeywords] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_MetaOpenGraphTitle]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_MetaOpenGraphTitle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
	[metaopengraphtitle] [text] NULL,
 CONSTRAINT [PK_tbl_Product_MetaOpenGraphTitle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_ProductConcern]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_ProductConcern](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[prodconcernID] [int] NULL,
 CONSTRAINT [PK_tbl_Product_ProductConcern] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_ProductIngredient]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_ProductIngredient](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[productingredientID] [int] NULL,
 CONSTRAINT [PK_tbl_Product_ProductIngredient] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_RelatedProducts]    Script Date: 2/12/2015 2:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[relatedproductID] [int] NULL,
 CONSTRAINT [PK_tbl_Product_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_Retailers]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_Retailers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[retailerName] [nvarchar](255) NULL,
	[retailerImage] [nvarchar](255) NULL,
	[retailerStock] [nvarchar](150) NULL,
	[retailerLink] [nvarchar](255) NULL,
 CONSTRAINT [PK_tbl_ProductRetailer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_TitleTag]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_TitleTag](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NULL,
	[titleTag] [text] NULL,
 CONSTRAINT [PK_tbl_Product_TitleTag] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ProductCategory]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductCategory](
	[prodcatID] [int] IDENTITY(1,1) NOT NULL,
	[prodcatName] [nvarchar](500) NULL,
	[prodcatDesc] [nvarchar](500) NULL,
	[prodcatImage] [nvarchar](255) NULL,
	[prodcatStatus] [int] NULL,
	[deleted] [int] NULL,
	[rank] [int] NULL,
 CONSTRAINT [PK_tbl_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[prodcatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ProductConcern]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductConcern](
	[prodconcernID] [int] IDENTITY(1,1) NOT NULL,
	[prodconcernName] [nvarchar](500) NULL,
	[prodconcernDesc] [nvarchar](500) NULL,
	[prodconcernStatus] [int] NULL,
	[deleted] [int] NULL,
	[rank] [int] NULL,
 CONSTRAINT [PK_tbl_ProductConcern] PRIMARY KEY CLUSTERED 
(
	[prodconcernID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ProductDetail]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
	[productBenefit] [text] NULL,
	[productIngredient] [text] NULL,
	[productDirection] [text] NULL,
 CONSTRAINT [PK_tbl_ProductDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ProductIngredient]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductIngredient](
	[prodingredientID] [int] IDENTITY(1,1) NOT NULL,
	[prodingredientName] [nvarchar](500) NULL,
	[prodingredientDesc] [nvarchar](500) NULL,
	[prodingredientStatus] [int] NULL,
	[deleted] [int] NULL,
	[rank] [int] NULL,
 CONSTRAINT [PK_tbl_ProductIngredient] PRIMARY KEY CLUSTERED 
(
	[prodingredientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ProductRecentlyViewed]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductRecentlyViewed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[productid] [int] NULL,
	[datetime] [timestamp] NULL,
 CONSTRAINT [PK_tbl_ProductRecentlyViewed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ShopProducts]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ShopProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_ShopProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_SwisseAdvisory_RelatedProducts]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_SwisseAdvisory_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[productid] [int] NULL,
 CONSTRAINT [PK_tbl_SwisseAdvisory_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_SwisseAmbassadors]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_SwisseAmbassadors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[ambassadorContent] [text] NULL,
	[ambassadorImage] [nvarchar](50) NULL,
	[ambassadorLink] [nvarchar](50) NULL,
	[deleted] [int] NULL,
 CONSTRAINT [PK_tbl_SwisseAmbassadors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_SwisseAmbassadors_RelatedLinks]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_SwisseAmbassadors_RelatedLinks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ambassadorID] [int] NULL,
	[relatedLinkID] [int] NULL,
 CONSTRAINT [PK_tbl_SwisseAmbassadors_RelatedLinks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_SwisseAmbassadors_RelatedProducts]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_SwisseAmbassadors_RelatedProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ambassadorID] [int] NULL,
	[productID] [int] NULL,
 CONSTRAINT [PK_tbl_SwisseAmbassadors_RelatedProducts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_TheScienceOfSwisse]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TheScienceOfSwisse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[link] [nvarchar](500) NULL,
	[content] [text] NULL,
 CONSTRAINT [PK_tbl_TheScienceOfSwisse] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[userName] [nvarchar](500) NULL,
	[userEmail] [nvarchar](500) NOT NULL,
	[userPassword] [nvarchar](500) NOT NULL,
	[userFN] [nvarchar](500) NULL,
	[userMN] [nvarchar](500) NULL,
	[userLN] [nvarchar](500) NULL,
	[userBirthMonth] [nvarchar](50) NULL,
	[userBirthYear] [nvarchar](50) NULL,
	[userPostCode] [nvarchar](50) NULL,
	[userStatus] [int] NULL,
	[firstLogged] [int] NULL,
	[checkAgreed] [int] NULL,
 CONSTRAINT [PK_tbl_Users] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_User_Country]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User_Country](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[countryid] [int] NULL,
 CONSTRAINT [PK_tbl_UserCountry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_UserLogs]    Script Date: 2/12/2015 2:39:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_UserLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NULL,
	[activity] [nvarchar](500) NULL,
	[datetime] [timestamp] NULL,
 CONSTRAINT [PK_tbl_UserLogs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_Age] ON 

INSERT [dbo].[tbl_Age] ([id], [ageRange]) VALUES (1, N'2 - 12')
INSERT [dbo].[tbl_Age] ([id], [ageRange]) VALUES (2, N'12 - 18')
INSERT [dbo].[tbl_Age] ([id], [ageRange]) VALUES (3, N'18 - 29')
INSERT [dbo].[tbl_Age] ([id], [ageRange]) VALUES (4, N'30 - 39')
INSERT [dbo].[tbl_Age] ([id], [ageRange]) VALUES (5, N'40 - 49')
INSERT [dbo].[tbl_Age] ([id], [ageRange]) VALUES (6, N'50 +')
SET IDENTITY_INSERT [dbo].[tbl_Age] OFF
SET IDENTITY_INSERT [dbo].[tbl_Banner] ON 

INSERT [dbo].[tbl_Banner] ([id], [bannerName], [bannerImage], [bannerLink], [status], [deleted]) VALUES (1, N'Banner  1', N'banner1.png', NULL, 1, 0)
INSERT [dbo].[tbl_Banner] ([id], [bannerName], [bannerImage], [bannerLink], [status], [deleted]) VALUES (2, N'Banner  2', N'banner1.png', NULL, 1, 0)
INSERT [dbo].[tbl_Banner] ([id], [bannerName], [bannerImage], [bannerLink], [status], [deleted]) VALUES (3, N'Banner  3', N'banner1.png', NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[tbl_Banner] OFF
SET IDENTITY_INSERT [dbo].[tbl_Blog] ON 

INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (1, N'DIY Detox – Cut down on coffee today', N'<img src="images/coffee_detox.jpg" /><br />
<br />
 <span style="font-weight: bold;">You’ll be surprised at the benefits a caffeine-free lifestyle can bring. (We were!)</span><br />
<br />
 If you’re one of those people who can’t start the day without a large double latte then giving up coffee can seem like a daunting and downright impossible task.<br />
<br />
  But detoxing from caffeine means you’ll sleep better, have lower blood pressure, get fewer headaches and generally feel happier. Not to mention all the money you’ll save!<br />
<br />
 Here’s how to do it. <br />
<br />
<span style="font-weight: bold;">Cut down by one coffee a day.</span><br />
As with any detox you can expect headaches, fatigue and crankiness in the first few days. Cutting down gradually will decrease the severity of detox symptoms. (If you only have one coffee per day you can start right away).<br />
<br />
<span style="font-weight: bold;">Order a chai latte or hot chocolate instead.</span>  <br />
We know there’s nothing quite like the morning ritual of a hot takeaway latte on your way to work, so order a caffeine free alternative instead.<br />
<br />
<span style="font-weight: bold;">Drink green tea.</span> <br />
While it’s still caffeinated it’s also packed with antioxidants, increases metabolism and can reduce signs of aging. What’s not to love!<br />
<br />
<span style="font-weight: bold;">Try hot water with a slice of lemon.</span> <br />
Another great coffee replacement, lemon water aids digestion, cleanses your system, boosts immunity and is high in Vitamin C. <br />
<br />
<span style="font-weight: bold;">Go Herbal.</span><br />
Swap coffee breath and stained teeth for a healthy smile. Herbal teas, like rooibos and white tea, both contain flavonoids which act as antioxidants, and have been shown to protect skin, teeth and gums. <br />
<br />
<span style="font-weight: bold;">Miso soup.</span> <br />
Shown to strengthen the immune system, miso soup has an alkalizing effect on the body and helps maintain nutritional balance. Keep a few sachets handy for when coffee cravings strike.<br />
<br />
 While detoxing remember to:<br />
<br />
 <span style="font-weight: bold;">Drink Water</span>. <br />
At least 8 glasses per day will help flush out the toxins. <br />
<br />
 <span style="font-weight: bold;">Sleep.</span>  <br />
You’ll be tired anyway so listen to your body and get plenty of rest. <br />
<br />
 If you find you’re having trouble sleeping try Swisse Ultiboost Sleep, a comprehensive formula that assists natural restful sleep &amp; helps relieve nervous tension.  <br />
<br />
 <span style="font-weight: bold;">Sweat.</span>  <br />
Sweating eliminates nasty toxins and speeds up the detox process so go for a jog, get to the gym, sit in a sauna – whatever makes you sweat! <br />
<br />
 <span style="font-weight: bold;">Eat Well.</span>  <br />
Fibre helps get rid of toxins via your digestive tract so resist the temptation to overload on carbs and go for high-fibre foods like fruits, veggies, legumes and grains instead.<br />
<br />
  The first few days of any detox will challenge your resolve but stick with it and by the 4th or 5th caffeine-free day you can expect to feel more energetic, more alert and happier. And with the money you’ll save by not buying a daily coffee you can treat yourself to something special!<br />
<br />', N'coffee_detox.jpg', N'Detox.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (2, N'Cold or flu – Do you know the difference?', N'<img src="images/cold_flu.jpg"><br><br>
<p class="MsoNormal">While the common cold and the flu share some symptoms they’re actually quite different. We had our Swisse science and research team explain exactly how they differ. &nbsp;</p>
<p class="MsoNormal"><b>So what is the common cold?</b></p>
<p class="MsoNormal">Once exposed to the common cold virus, your body’s immune system responds and you’ll notice symptoms like a sore throat and runny nose, before the virus is eventually neutralised.</p>
<p class="MsoNormal"><b>How can you tell thedifference between cold and flu?</b></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="539" style="width: 404pt; border-collapse: collapse; border: none;"> 
<tbody>
<tr>  
<td width="95" style="width: 70.9pt; border: 1pt solid #6d6d6d; padding: 0in 5.4pt;">  
<p class="MsoNormal" style="margin-left:-76.3pt;mso-pagination:none;mso-layout-grid-align:  none;text-autospace:none"><b><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">&nbsp;<o:p /></span></b></p>  </td>  
<td width="198" style="width: 148.85pt; border-style: solid solid solid none; border-top-color: #6d6d6d; border-right-color: #6d6d6d; border-bottom-color: #6d6d6d; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0in 5.4pt;">  
<p class="MsoNormal"><b><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">COLD &nbsp; &nbsp;</span></b><b><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<o:p /></span></b></p>  </td>  
<td width="246" style="width: 184.25pt; border-style: solid solid solid none; border-top-color: #6d6d6d; border-right-color: #6d6d6d; border-bottom-color: #6d6d6d; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0in 5.4pt;">  
<p class="MsoNormal"><b><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">FLU</span></b><b><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22"><o:p /></span></b></p>  </td> </tr> 
<tr>  
<td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: #6d6d6d; border-bottom-color: #6d6d6d; border-left-color: #6d6d6d; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0in 5.4pt;">  
<p class="MsoNormal"><b><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Common Symptoms</span></b><b><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22"><o:p /></span></b></p>  </td>  
<td width="198" style="width:148.85pt;border-top:none;border-left:none;  border-bottom:solid #6d6d6d 1.0pt;border-right:solid #6d6d6d 1.0pt;  mso-border-top-alt:solid #6d6d6d 1.0pt;mso-border-left-alt:solid #6d6d6d 1.0pt;  padding:0in 5.4pt 0in 5.4pt">  
<p class="MsoNormal" style="margin-left:9.45pt;text-indent:-9.45pt;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:9.45pt 11.0pt;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp; </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">Usually  affects only the head and throat<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">No fever in adults<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:9.45pt;text-indent:-9.45pt;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:9.45pt 11.0pt;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp; </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">Fever in  toddlers and infants (between 37.5 and 39ºC)<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Nasal stuffiness<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Sneezing<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Runny nose<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l0 level1 lfo1;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Throat irritation<o:p /></span></p>  </td>  
<td width="246" style="width:184.25pt;border-top:none;border-left:none;  border-bottom:solid #6d6d6d 1.0pt;border-right:solid #6d6d6d 1.0pt;  mso-border-top-alt:solid #6d6d6d 1.0pt;mso-border-left-alt:solid #6d6d6d 1.0pt;  padding:0in 5.4pt 0in 5.4pt">  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Affects the whole body <o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Sudden fever (between 38.5 to 42ºC)<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Flushed face<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Body aches<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Headache<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Lack of energy<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Dizziness<o:p /></span></p>  
<p class="MsoNormal" style="margin-left:.5in;text-indent:-.5in;mso-pagination:  none;mso-list:l1 level1 lfo2;tab-stops:11.0pt .5in;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;  </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Vomiting<o:p /></span></p>  </td> </tr> 
<tr>  
<td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: #6d6d6d; border-bottom-color: #6d6d6d; border-left-color: #6d6d6d; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0in 5.4pt;">  
<p class="MsoNormal"><b><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Duration</span></b><b><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22"><o:p /></span></b></p>  </td>  
<td width="198" style="width:148.85pt;border-top:none;border-left:none;  border-bottom:solid #6d6d6d 1.0pt;border-right:solid #6d6d6d 1.0pt;  mso-border-top-alt:solid #6d6d6d 1.0pt;mso-border-left-alt:solid #6d6d6d 1.0pt;  padding:0in 5.4pt 0in 5.4pt">  
<p class="MsoNormal" style="margin-left:9.45pt;text-indent:-9.45pt;mso-pagination:  none;mso-list:l2 level1 lfo3;tab-stops:9.45pt 11.0pt;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp; </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">Symptoms  usually last no more than 7 days, although a cough may linger<o:p /></span></p>  </td>  
<td width="246" style="width:184.25pt;border-top:none;border-left:none;  border-bottom:solid #6d6d6d 1.0pt;border-right:solid #6d6d6d 1.0pt;  mso-border-top-alt:solid #6d6d6d 1.0pt;mso-border-left-alt:solid #6d6d6d 1.0pt;  padding:0in 5.4pt 0in 5.4pt">  
<p class="MsoNormal" style="margin-left:8.75pt;text-indent:-8.75pt;mso-pagination:  none;mso-list:l3 level1 lfo4;tab-stops:8.75pt 11.0pt;mso-layout-grid-align:  none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp; </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">Symptoms  usually last between 4 to 7 days, but fever may return and cough and  tiredness may last for a number of weeks<o:p /></span></p>  </td> </tr> 
<tr>  
<td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: #6d6d6d; border-bottom-color: #6d6d6d; border-left-color: #6d6d6d; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0in 5.4pt;">  
<p class="MsoNormal"><b><span style="font-size:10.0pt;mso-bidi-font-family:  arial;color:#111b22">Cause</span></b><b><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22"><o:p /></span></b></p>  </td>  
<td width="198" style="width:148.85pt;border-top:none;border-left:none;  border-bottom:solid #6d6d6d 1.0pt;border-right:solid #6d6d6d 1.0pt;  mso-border-top-alt:solid #6d6d6d 1.0pt;mso-border-left-alt:solid #6d6d6d 1.0pt;  padding:0in 5.4pt 0in 5.4pt">  
<p class="MsoListParagraph" style="margin-left:9.45pt;mso-add-space:auto;  text-indent:-9.45pt;mso-pagination:none;mso-list:l5 level1 lfo6;tab-stops:  11.0pt;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:symbol;mso-fareast-font-family:symbol;  mso-bidi-font-family:symbol;color:#111b22">·<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">Very common  infectious disease caused by more than 200 different types of viruses&nbsp;<o:p /></span></p>  </td>  
<td width="246" style="width:184.25pt;border-top:none;border-left:none;  border-bottom:solid #6d6d6d 1.0pt;border-right:solid #6d6d6d 1.0pt;  mso-border-top-alt:solid #6d6d6d 1.0pt;mso-border-left-alt:solid #6d6d6d 1.0pt;  padding:0in 5.4pt 0in 5.4pt">  
<p class="MsoNormal" style="margin-left:8.75pt;text-indent:-8.75pt;mso-pagination:  none;mso-list:l4 level1 lfo5;tab-stops:11.0pt;mso-layout-grid-align:none;  text-autospace:none"><!--[if !supportLists]--><span style="font-size:10.0pt;  mso-fareast-font-family:cambria;mso-fareast-theme-font:minor-latin;  mso-bidi-font-family:cambria;mso-bidi-theme-font:minor-latin;color:#111b22">?<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp; </span></span><!--[endif]--><span style="font-size:10.0pt;mso-bidi-font-family:arial;color:#111b22">Caused by  the influenza virus, usually classified as type A or B, each of which  includes several subtypes or strains</span></p></td></tr></tbody></table>
<p class="MsoNormal"><br />
</p>
<p class="MsoNormal">To naturally maintain healthy immune function and reduce tiredness and fatigue try <u><span style="color:#548dd4;mso-themecolor:text2;mso-themetint:153">Swisse Ultiboost Immune</span></u>. Containing Vitamin C,Zinc, Magnesium and natural Olive Leaf, it is formulated based on scientific evidence to provide key ingredients beneficial for immune health in a convenient daily dose.&nbsp;</p>
<p class="MsoNormal">Check out the full <u><span style="color:#548dd4;mso-themecolor:text2;mso-themetint:153">Swisse Immune range here.</span></u></p><span style="font-size: 10pt; font-family: verdana, arial, helvetica, sans-serif;">If symptoms worsen or persist, seek expert advice from your healthcare professional.</span>', N'cold_flu.jpg', N'ColdFlu.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (3, N'Fuss-Free Ways to Feed Your Family', N'<img src="images/feed_family.jpg"><br><br>

<p class="MsoNormal"><b>How to influence happy and healthy eating habits for you and your family.<o:p /></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">As a parent, you can have a huge influence on your children(even though at times it feels like you don’t!), especially when it comes todiet and exercise.&nbsp; Children can also begood motivation for you to stay healthy. By sticking to a healthy and balanceddiet, we can help our children develop better eating habits. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Our environment can shape our eating habits and as parentswe decide what our family eats, but it when it comes to our children it can bechallenging to get them to eat well. At mealtimes children can be fussy andvery vocal about their refusal to eat anything green, spicy or any kind of foodthat isn’t recognizable to them. A good way to approach this is to keepintroducing new foods, encourage and persist even when it’s being rejected. Itmight take time for newer healthier meals to become familiar enough for them toenjoy eating, so help them along by slowly incorporating the new and old mealstogether.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Take a step back and re-evaluate what you and your familyeats. Is there anything that can easily be changed? Here are some quick andeasy tips to help introduce happier, healthier eating habits into the family’sdiet.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:wingdings;mso-fareast-font-family:wingdings;mso-bidi-font-family:wingdings">ü<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;</span></span><!--[endif]-->Make your own low-fat fast food such as chipsbrushed with a little olive oil baked in the oven. Serve with a roast chicken, grilledmeat or fish and it will soon become a favorite<o:p /></p>
<p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:wingdings;mso-fareast-font-family:wingdings;mso-bidi-font-family:wingdings">ü<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;</span></span><!--[endif]-->To get the children involved, let them choosethree different colored vegetables to be served with dinner<o:p /></p>
<p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:wingdings;mso-fareast-font-family:wingdings;mso-bidi-font-family:wingdings">ü<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;</span></span><!--[endif]-->Combine some spinach, banana, frozen berries,coconut water or almond milk and blend to create a delicious smoothie. It’s agood way to encourage your children to eat their greens and distracts them fromthe fact that it’s healthy<o:p /></p>
<p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:wingdings;mso-fareast-font-family:wingdings;mso-bidi-font-family:wingdings">ü<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;</span></span><!--[endif]-->Chop up seasonal fruit and have them wheneveryou have ice-cream.<o:p /></p>
<p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:wingdings;mso-fareast-font-family:wingdings;mso-bidi-font-family:wingdings">ü<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;</span></span><!--[endif]-->Freeze favorite seasonal fruit so they’re neverout of season and serve them as frozen sweet treats.<o:p /></p>
<p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:wingdings;mso-fareast-font-family:wingdings;mso-bidi-font-family:wingdings">ü<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;</span></span><!--[endif]-->Have low-fat milk instead of full cream. Alsotry soy or almond milk.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Food can be fresh, healthy and by encouraging your family toparticipate in preparing the meals together, it can also be a lot of fun. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p><span style="font-size:12.0pt;font-family:cambria,serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:ms mincho;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:times new roman;mso-bidi-theme-font:minor-bidi;mso-ansi-language:en-us;mso-fareast-language:en-us;mso-bidi-language:ar-sa">We all strive to be healthier and happier andwhat we eat is a big part of that. But we can’t always get all the nutrition weneed from food. Supplementing your diet with Swisse Multivitamins may help tosupport a healthy diet by bridging those nutritional gaps. Swisse contains moreof the essential vitamins, minerals and&nbsp;</span><span style="font-size: 10pt;">natural herb extracts to help you, and your family, feelgreat!</span>
<p class="MsoNormal"><o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><u>Browse the full range of Swisse multivitamins here</u> –formulated by gender and life stage so they’re tailored to your needs.<o:p /></p>', N'feed_family.jpg', N'FeedFamily.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (4, N'Natural Remedies for Hayfever & Allergies', N'<img src="images/hayfever.jpg"><br /><br />
<span style="font-weight: bold;">Millions of people suffer from the itchy throat, runny nose and sore eyes that allergies and hay fever bring.</span> <br />
<br />
 Allergies can be triggered by many different factors, with pollen, dust and poor air quality from haze being among the worst offenders. Thankfully there are plenty of ways to beat the symptoms so before you reach for the anti-histamines give some of the below natural remedies a try.<br />
<br />
 For an extra immunity boost try one of the Swisse range of immunity products like Swisse Ultiboost Immune, a premium quality formula containing vitamins, minerals and herbs to help maintain healthy immune function and provide symptomatic relief from hay fever and cold symptoms.<br />
<br />
 <span style="font-weight: bold;">Chamomile</span><br />
Try placing cool chamomile tea bags over your eyes for a natural alternative to eye drops. Chamomile soothes the eye area and helps reduce inflammation and redness. Washing your eye area regularly with cold water can also help by flushing away eye irritants like pollen.<br />
<br />
 <span style="font-weight: bold;">Red Onions</span><br />
Quercetin is a powerful antioxidant, anti-inflammatory and natural anti-histamine. It naturally occurs in many foods like red onions, apples, red grapes, berries and parsley.<br />
<br />
 <span style="font-weight: bold;">Fish Oil</span><br />
Fish oils work by blocking the inflammatory pathways and may help reduce the severity of your body’s allergic response to pesky pollen. Fish oil can be found in fatty fish like salmon, sardines and Pacific herring. <br />
<br />
 Try one of the Swisse range of Omega 3s like Swisse Ultiboost Odourless Wild Fish Oil.<br />
<br />
 <span style="font-weight: bold;">Horseradish, Garlic and Vitamin C</span><br />
Touted for its decongestant properties, horseradish helps clear nasal passage, garlic is a good source of quercetin and a potent antibiotic, and Vitamin C can help reduce the effects of histamine. <br />
<br />
 The best cure is always prevention, so be sure to boost your immunity by increasing your Vitamin C intake all year round. <br />
<br />
 Try Swisse Ultiboost High Strength Vitamin C, or easy-to-take Swisse Ultiboost High Strength Vitamin C Effervescent, or check out the rest of our immunity range here.<br />
<br />', N'hayfever.jpg', N'HayFever.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (5, N'Sustainable Krill Oil – the other Omega 3', N'<p class="MsoNormal"><b>The tiny crustaceanmaking big waves in the vitamin and supplement market. <o:p /></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Still relatively new on the omega-3 market, Krill Oil hasbeen shown to benefit a wide range of health issues including helping tosupport normal cholesterol levels in healthy individuals, lessen arthritispain, as well as helping to combat heart disease and other cardiovascularissues.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Krill oil is the nutrient-dense substance extracted from thetiny shrimp-like sea creatures. It’s a rich source of omega-3 fatty acids andprovides many of the same health benefits as fish oil, but can be easier forthe body to absorb meaning fewer tablets a day for the same results.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Several accredited bodies, including the Australian NationalHeart Foundation, recommend that healthy individuals consume 500mg omega-3 (EPA&amp; DHA) per day, and for cardiovascular health the consumption of 1000mg ofomega-3 (EPA &amp; DHA) + at least 2g of ALA per day is required.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">These volumes can be achieve by eating 2 to 3 (150g foradults and 75g for children) serves of oily fish (salmon, mackerel, herring,tuna or trout) per week and/or by supplementing your intake with fish oilsupplements and omega-3 enriched foods and drinks.<span style="background:yellow;mso-highlight:yellow"><o:p /></span></p>
<p class="MsoNormal"><b>&nbsp;</b></p>
<p class="MsoNormal"><b>What makes SwisseKrill Oil the best quality on the market?<o:p /></b></p>
<p class="MsoNormal">&nbsp;<o:p /></p>
<p class="MsoListParagraph" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol">·<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]-->Swisse Krill Oil contains premium quality NeptuneKrill Oil with higher EPA/DHA and astaxanthin values than other forms, whichhelp support healthy cholesterol levels, combat potential free radical damageand relieve the symptoms of arthritis.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoListParagraph" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol">·<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]-->The extraction process used for Swisse products maximisesthe cultivation of these active ingredients found in krill oil making it thebest quality on the market.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoListParagraph" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol">·<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]-->Swisse uses sustainably sourced krill oilproducts originating from Antarctic krill – the most abundant speciesavailable.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoListParagraph" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol">·<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]-->All Swisse Neptune Krill Oil products arecertified by Friends of the Sea for Sustainable Fisheries (Certificate No.305-2012-W).</p>
<p class="MsoListParagraph" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><br />
</p>
<p class="MsoNormal"><o:p /></p>
<p class="MsoListParagraph" style="text-indent:-.25in;mso-list:l0 level1 lfo1"><span style="font-size: 10pt;">The tiny krill can mean big things for health. Check out our</span><u style="font-size: 10pt;">Swisse Ultiboost Joints</u><span style="font-size: 10pt;"> supplement, and our premium range of </span><u style="font-size: 10pt;">Omega 3products</u><span style="font-size: 10pt;"> and try some today.</span>&nbsp;<o:p /></p>', N'hl-krill-v5.jpg', N'KrillOil.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (6, N'The Beauty Supplement – Swisse Hair Skin & Nails', N'<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">The beauty supplement that works from the inside out<o:p /></span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">These days there’s a vitamin or supplement for almost every health andwellbeing issue imaginable. But did you know there’s also a supplement forhealthy skin, hair and nails?<o:p /></span></p>
<p class="MsoNormal" style="margin-right:39.35pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">The benefits of Swisse Hair Skin Nails<o:p /></span></b></p>
<p class="MsoNormal" style="margin-right:39.35pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Swisse Ultiboost Hair Skin Nails combines vitamins and minerals shown tosupport hair, skin and nail health. It contains ingredients essential for theformation of collagen and maintenance of healthy skin.<o:p /></span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Award-winning actress Rebecca Lim uses Swisse Hair Skin Nails to lookgood on camera and off. Check out what she has to say about how the uniquebeauty supplement helps her look and feel at her best everyday. <u><span style="color:blue"><o:p /></span></u></span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><u><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;color:blue;mso-ansi-language:en-au">&nbsp;</span></u></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">[Embed 30 second iVideo for HSN]<o:p /></span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;color:blue;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">How does it work?<o:p /></span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span style="font-size:10.0pt;mso-bidi-font-family:times new roman">Swisse Hair Skin Nails is a unique beauty formula that contains activeingredients like zinc, silica, vitaminC and blood orange extract. <o:p /></span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormalCxSpMiddle" style="margin-top:0in;margin-right:39.4pt;margin-bottom:0in;margin-left:49.65pt;margin-bottom:.0001pt;mso-add-space:auto;text-indent:-14.15pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-AU" style="font-size:10.0pt;font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol;mso-ansi-language:en-au">+<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]--><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Silica supportscollagen structure and helps maintain skin integrity and appearance. <o:p /></span></p>
<p class="MsoNormalCxSpMiddle" style="margin-top:0in;margin-right:39.4pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt;mso-add-space:auto"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormalCxSpMiddle" style="margin-top:0in;margin-right:39.4pt;margin-bottom:0in;margin-left:49.65pt;margin-bottom:.0001pt;mso-add-space:auto;text-indent:-14.2pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-AU" style="font-size:10.0pt;font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol;mso-ansi-language:en-au">+<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]--><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Vitamin C helpsprotect skin cells and improves the appearance of sun damage.<o:p /></span></p>
<p class="MsoNormalCxSpMiddle" style="margin-right:39.4pt;mso-add-space:auto"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;<o:p /></span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.4pt;margin-bottom:0in;margin-left:49.65pt;margin-bottom:.0001pt;text-indent:-14.2pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-AU" style="font-size:10.0pt;font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol;mso-ansi-language:en-au">+<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]--><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Blood orange extractis high in antioxidants and helps protect cells from potential free radicaldamage.<o:p /></span></p>
<p class="MsoNormal" style="margin-right:39.4pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Premium ingredients<o:p /></span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">At Swisse we always try to source the best ingredients available. Ourblood orange extract comes from three varieties of Sicilian blood oranges grownin the rich, volcanic soil surrounding Mount Etna.<o:p /></span></p>
<p class="MsoNormal" style="margin-right:39.35pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:39.35pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Is Swisse Hair Skin Nails for me?<o:p /></span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:46.45pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:46.45pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">If you suffer from frail, splitting or thin nails, hair fall or justwant to have a more radiant complexion try Swisse Hair Skin Nails. It’s thebeauty supplement that starts from within.<o:p /></span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:46.45pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:46.45pt;margin-bottom:0in;margin-left:35.45pt;margin-bottom:.0001pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Discover the full range of premium Swisse products for your healthy,happy lifestyle <u>here</u>.&nbsp;<o:p /></span></p>', N'', N'BeautySupplement.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (7, N'Swisse Ultiboost Joints – The Power of Boswellia', N'<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><b><span style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman">Firstly,what are joints?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><span style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman">Joints are the connections between twobones. They allow you to bend your fingers, wrists, elbows, knees, hips, backand head. Smooth tissue, known as cartilage, and synovial fluid cushion thejoints and prevent bones from rubbing together. Getting older, injuries orcarrying too much weight can all wear away at cartilage, damaging joints andleading to arthritis. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><b><span style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman">Whatare some ways to maintain joint health?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">As well asexercising and maintaining a healthy weight, you can try the Swisse range ofJoints products. They’re specially formulated with premium quality ingredients,like boswellia, to help support the health of joints.<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Check out howformer Wimbledon Champion and Australian Tennis legend Lleyton Hewitt usesSwisse Ultiboost Joints to maintain his joint health. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">[Embed 30second iVideo for Joints]<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><b><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">What is boswellia? <o:p /></span></b></p>
<p class="MsoNormal" style="margin-left:35.45pt"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Boswellia is a herb with a long history of therapeuticuses and is known for its strong anti-inflammatory and analgesic properties.Boswellia may also help prevent cartilage loss and inhibit the autoimmuneprocess giving patients better mobility and an ability to walk longer distanceswithout pain. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><b><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">What other ingredients are found in the Swisse Jointsrange?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><span style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman">Swisse Joints products contain avariety of beneficial vitamins, minerals and natural herb extracts including: <o:p /></span></p>
<p class="MsoListParagraphCxSpFirst" style="margin-left:72.3pt;mso-add-space:auto"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoListParagraphCxSpLast" style="margin-left:70.9pt;mso-add-space:auto;text-indent:-35.45pt;mso-pagination:none;mso-list:l0 level1 lfo2"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol">+<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]--><b><span style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman">G</span></b><b><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">lucosamine Sulphate</span></b><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">.<b> </b>Occurs naturally in thebody and plays a key role in the formation of cartilage. Also helps maintain elasticityand strength of cartilage </span><span style="font-size:10.0pt;mso-bidi-font-family:times new roman"><o:p /></span></p>
<p class="MsoNormal"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoListParagraph" style="margin-left:72.3pt;mso-add-space:auto;text-indent:-36.85pt;mso-list:l1 level1 lfo1"><!--[if !supportLists]--><span lang="EN-AU" style="font-size:10.0pt;font-family:symbol;mso-fareast-font-family:symbol;mso-bidi-font-family:symbol;mso-ansi-language:en-au">+<span style="font-stretch: normal; font-size: 7pt; font-family: times new roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><!--[endif]--><b><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Wild Neptune Krill Oil</span></b><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">. Good source of omega-3 fatty acidsEPA and DHA it helps to reduce joint inflammation and provides temporary relieffrom pain associated with osteoarthritis.<o:p /></span></p>
<p class="MsoNormal"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">&nbsp;</span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:35.45pt"><span lang="EN-AU" style="font-size:10.0pt;font-family:times,serif;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">If you sufferfrom joint pain, arthritis or just want to keep your joints healthy andfunctioning, <u>check out the range of Swisse Joints products here</u>.<o:p /></span></p>', N'', N'Joints.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (8, N'3 reasons the humble lemon is a citrus superstar', N'<p class="MsoNormal"><b><i>High in vitamin C and potassium, these fruits - native to Asia - arepacked with anti-bacterial, antiviral and immune-boosting properties.<o:p /></i></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Here are three (more) reasons to love lemons.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>1. Your digestive system loves them.<o:p /></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The acidity and composition of lemons break down food inyour gut by encouraging the liver to produce bile, which gets your digestivejuices flowing, and eases the symptoms of indigestion and bloating. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Starting your day with lemon and warm water is a great,natural way to kick-start your metabolism while cleansing your system of unwanted toxins and hydrating your lymph system.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Top Tip: Drink a cup of warm water with a tablespoon of lemon juice after meals to aid digestion.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Another great way to help maintain a healthy immune systemand reduce tiredness and fatigue is with Swisse Immune products. Check out therange <u><span style="color:#3366ff">here.</span></u><o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>2. Your immune system loves them.<o:p /></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Lemons are rich in vitamin C and antiviral properties. Taken at the first sign of a sore throat or sniffly nose, a mug of warm water with lemon juice and honey is a naturally delicious way to help relieve the onset of colds. Their natural acidity expels toxins from the blood and reduces phlegm and mucus.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">If a sore throat is accompanied by bad breath, gargling witha mouthful of lemon juice or chewing on a lemon slice will help reduce theodour.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>3. Gardens love them.<o:p /></b></p>
<p class="MsoNormal">&nbsp;<o:p /></p>
<p class="MsoNormal">Lemon trees are easy to grow and make a welcome addition toany garden. You don’t even need a green thumb - just some well-drained soil anda sunny spot. As well as looking great you’ll have a ready supply of thesecitrus superstars on hand to reap their many health benefits!<o:p /></p>
<p class="MsoNormal"><b>+1. Swisse Ambassadorand celebrity chef Willin Low loves them!<o:p /></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:cambria,serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:ms mincho;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:times new roman;mso-bidi-theme-font:minor-bidi;mso-ansi-language:en-us;mso-fareast-language:en-us;mso-bidi-language:ar-sa">The versatile lemon is a kitchen essential providinga refreshing and zesty finish to both sweet and savoury dishes.&nbsp; Willin uses lemons to give a tangy finish tosalads, soups and cocktails and loves the flavour lemon gives to roast chickenand lo mai kai.&nbsp;</span></p>', N'lemon_superstar.jpg', N'HumbleLemons.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (9, N'Dietary Health and the role of Multivitamins', N'<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">What are multivitamins? <o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Multivitamins are supplementscontaining all the key nutrients your body needs for optimal health. They’redesigned to act as a top-up to your daily diet and generally include VitaminsA, C and the B family as well as essential minerals like zinc, magnesium andcalcium. Swisse Multivitamins also contain natural herb extracts such ascoriander, parsley and spearmint. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt;mso-outline-level:2"><b><span lang="EN-AU" style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Why take multivitamins? <o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Singapore lifestyles are busy andfast-paced. And the prevalence of cheap and tasty hawker-style food means ourbodies don’t always get all the nutrients they need from our diets. Even freshfood is not as rich in nutrients as it should be because of chemicalfertilisers and pesticides in the soil. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Why are multivitamins important?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Multivitamins are a simple way togive our bodies the daily nutrients they may be missing to help maintain ahealthy mind and body.<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Those who drink, smoke, have arestricted diet (like vegetarians and vegans), the elderly and pregnant peoplecan also greatly benefit from multivitamins. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Multivitamins play an important role in dietary health<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Multivitamins shouldn’t replace abalanced diet but can play a key role in health and wellbeing.&nbsp; Children can also benefit from the essentialnutrients multivitamins provide - especially kids who are fussy eaters or havean intolerance to certain food groups.<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">As with any medication, it isimportant to follow the label instructions and consult with your healthcarepractitioner if you have any concerns.<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Try the Swisse range ofMultivitamins specifically formulated for men and women of every life stage.You can check out the full range of <u>Swisse multivitamins here</u>.<o:p /></span></p>', N'', N'DietaryHealth.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (10, N'Conquer PMS with These 5 Healthy Habits', N'<img src="images/pms.jpg" /><br />
<br />
When it’s ‘that time of the month’ you usually just want to curl up in the corner of the couch with sugary snacks and plenty of painkillers. But the best way to tackle painful cramps (and those mood swings) is to get on-board with a few healthy habits.<br />
<br />
 <span style="font-weight: bold;">1. Exercise</span><br />
Light to moderate exercise is a great way to help alleviate symptoms by improving circulation and the flow of oxygen around the body. A good work out also reduces the sensation of pain by releasing endorphins while increasing the production of dopamine and serotonin so you feel less cranky! It’s a win for everyone!<br />
<br />
 <span style="font-weight: bold;">2. Stretch</span><br />
Hip and back stretches promote blood flow to the abdominal, lower-back and uterine muscles so they’re great for easing the tension that causes cramps. Try yoga, and in particular moves that open up the legs and hips.<br />
<br />
 <span style="font-weight: bold;">3. Sleep</span><br />
Monthly periods deplete iron levels in the body which is why you can feel so tired during your period.  Make sure you’re getting plenty of sleep and eating plenty of iron rich foods like red meat and leafy greens.<br />
<br />
 <span style="font-style: italic; color: #999999;">Top Tip: Try Swisse Ultiboost Iron supplement - a convenient way to get the iron your body needs.</span><br />
<br />
 <span style="font-weight: bold;">4. Eat</span><br />
Salty foods, caffeine and alcohol all lead to increased fluid retention and can make PMS symptoms worse. While that seems really unfair, try to eat more fruit, vegetables and foods high in protein and calcium. Trust us, your body will thank you for it.<br />
<br />
 <span style="font-weight: bold;">5. Magnesium</span><br />
Magnesium is a super-star of PMS prevention. Taking high does of magnesium before your period arrives can give you relief from symptoms like headaches, fluid retention, dizziness, and sugar cravings.<br />
<br />
 <span style="font-style: italic; color: #999999;">Top Tip: Try Swisse Ultiboost Magnesium – great for cramping, and general health and wellness.</span><br />
<br />', N'pms.jpg', N'PMS.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (11, N'Top 5 Tips For a Better Night''s Sleep', N'<p class="MsoNormal"><b>These days we’rebusier than ever. But as we try to pack more and more into each day have we lostthe art of getting good sleep?<o:p /></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">If you have trouble falling asleep, staying asleep or wakingup in the mornings, give these natural tips a try. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>1. Download An App.<o:p /></b></p>
<p class="MsoNormal">These clever apps work with your natural sleep cycle to helpyou get to sleep and wake up refreshed and ready for the day. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Sleep Cycle for iOS (US$1.29) or Sleepbot for Android (free)<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">These two apps work by analysing your unique sleep patternsand gently waking you in the lightest phase of your sleep cycle. You’ll wakemore naturally feeling rested and not like you’ve been wrestled out of a deepsleep. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Up by Jawbone iOS (Free)<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The Up app monitors not only your sleep but your activitylevels during the day. The in-depth sleep graphs let you see how you’reprogressing and the Power Nap and Smart Alarm features help you get to sleepquickly and wake at your optimal time.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>2. Find Your Rhythm<o:p /></b></p>
<p class="MsoNormal">All living things have an internal biological clock calledthe circadian rhythm. These 24-hour cycles regulate things like sleep, hormonesand body temperature. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The main impact on your circadian rhythm is light. When it’sdark, melatonin levels in the blood rise preparing you for sleep, so as bedtimeapproaches try to keep lights low and avoid the bright light from iPads,televisions and computer screens. In the mornings getting into bring light assoon as possible can help you wake up.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>3. Keep Your Cool.<o:p /></b></p><span style="font-size:12.0pt;font-family:cambria,serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:ms mincho;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:times new roman;mso-bidi-theme-font:minor-bidi;mso-ansi-language:en-us;mso-fareast-language:en-us;mso-bidi-language:ar-sa">Studies have found the ideal bedroom temperaturefor sleep is around 18.5 degrees. Being too hot or too cold can have a dramaticaffect how long you sleep&nbsp;</span><span style="font-size: 10pt;">and the quality of your REM sleep - the stage in which youdream.</span>
<p class="MsoNormal"><o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">So make sure your bedroom is cool, be wary of memory foampillows, which can make you overheat, and wear warm socks as cold feet can keepyou awake.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>4. Prioritise Sleep.<o:p /></b></p>
<p class="MsoNormal">Sleep plays a vital role in your health and general wellbeing. During sleep your brain is forming new pathways and your muscles andtissues are restoring. Sleep also helps you maintain a healthy immune systemand regulates the hormones the make you feel hungry.&nbsp; <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Adults should aim for between 7-8 hours sleep per night. Anyless can have serious consequences for mental and physical health so make it apriority to give yourself the gift of restorative sleep every single night. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><b>5. Make Your Bedrooma ‘Sleeproom’.<o:p /></b></p>
<p class="MsoNormal">Your bedroom should be an oasis of calm and comfort. Thismeans banishing the TV’s, the work desk, the clutter and the chaos to create aspace that’s inviting and conducive to sleep. <o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Think of your bedroom as a cave. If it’s cool, dark andquiet then you’re giving yourself the best chance at enjoying a restful andrejuvenating sleep every night.<o:p /></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Ready for bed? Enhance these tips &amp; techniques with <u>SwisseUltiboost Sleep</u>, a combination of magnesium and herbs, including valerianto assist in encouraging naturally restful sleep.<b><o:p /></b></p>', N'sleep-blog.jpg', N'BetterNightSleep.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (12, N'What Makes a Superfood ‘Super’?', N'<img src="images/superfood.jpg"><br /><br />
We’ve all heard the buzzword “superfoods” used to reference everything from green tea to chocolate, however the term is more accurately applied to naturally occurring foods that are unusually dense in nutrients for their calorie count.<br />
<br />
 Some common superfoods include lean meats like turkey, leafy greens like spinach, and foods that “punch above their weight” in terms of nutrition, such as blueberries, salmon, fruits and nuts. <br />
<br />
 Superfoods are naturally rich in certain nutrients like:<br />
<br />
 • <span style="font-weight: bold;">Antioxidants,</span> thought to prevent cancer<br />
• <span style="font-weight: bold;">Healthy fats,</span> for heart health<br />
• <span style="font-weight: bold;">Fibre,</span> thought to prevent digestive problems and diabetes<br />
• <span style="font-weight: bold;">Phytochemicals,</span> plant chemicals that possess numerous health benefits. <br />
<br />
 <span style="font-weight: bold;">Superfoods can support:</span><br />
<br />
 • Heart and digestive health<br />
• Metabolism<br />
• Detoxification<br />
• Bowel function <br />
• And reduced inflammation.<br />
<br />
 While it would be great if we could eat these nutrients on a daily basis, time and budget constraints, as well as seasonal availability, mean it’s not always realistic. In Singapore the proliferation of cheap and tasty hawker food can make it even more challenging to eat healthily!<br />
<br />
  Thankfully the Swisse range contains many of the nutrients found in superfoods in convenient multivitamin and supplement form. If you’re not getting the nutrition you need from your diet you may want to consider choosing a product from our range of comprehensive formulas.<br />
<br />
  Check out the full multivitamin range here and discover the Swisse difference for yourself.<br />
<br />', N'superfood.jpg', N'SuperFood.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (13, N'Formulated for You – Vitamin Needs by Gender and Life Stage', N'<p class="MsoNormal" style="margin-left:37.15pt"><b>The ImportanceOf Multivitamins For Men And Women. <o:p /></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:37.15pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">As we move through the different stagesof life our nutritional needs also change. During adolescence, pregnancy,menopause and old age, extra demands are placed on our bodies and we requirecertain nutrients to stay at optimal health. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><b><span lang="EN-AU" style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Are multivitamins necessary forhealthy individuals?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span lang="EN-AU" style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Even with the best diet it’s not always possible to meet the nutritionalrequirements our bodies need on a daily basis. Multivitamins help supportgeneral health and wellbeing and may help our bodies get everything they needto perform at their peak. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><b><span lang="EN-AU" style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Why does Swisse make differentmultivitamins for men and women? <o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman">Throughout life, men and women willhave nutritional needs that differ from one another. For instance, womenrequire additional support during pregnancy to ensure the health of themselvesand their baby, so it’s a good idea to choose multivitamins specificallyformulated to target those specific needs. &nbsp;<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><b>What about multivitamins for other life stages? <o:p /></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman">Swisse has a multivitamin speciallyformulated for every life stage. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman">Children experiencing growth spurts andchanges to their bodies can benefit from <u>Swisse Children’s Ultivite</u>,specially formulated for their busy active lives and nutritional needs.&nbsp; <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman">The <u>Swiss </u></span><u><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Men’s Ultivite</span></u><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au"> and <u>Swisse Women’s Ultivite</u> each contain around 50 premiumquality vitamins, minerals, antioxidants and herbs to help support men’s andwomen’s nutritional needs and assist with energy production, stamina and ahealthy nervous system.<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman">As we progress into old age our bodieswill experience a different set of demands and it’s important to take care ofjoints as well as mental health. The <u>Swisse range of 50+ Ultivites </u>containsa comprehensive variety of premium quality vitamins, minerals, antioxidants andherbs to help men and women over the age of 50 meet their nutritional needs andmaintain general wellbeing.</span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:37.15pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">What makes Swisse multivitamins different?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span lang="EN-AU" style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">The comprehensive formulas of Swisse Multivitamins contain up to 50 essentialingredients - sometimes twice as many as our next leading competitor - and beforethey hit the shelves have gone through rigorous testing, more than 300 qualitycontrol checks, and have been scientifically validated so you know you’regetting a trusted multivitamin with maximum efficacy. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size:10.0pt;mso-fareast-font-family:times new roman;mso-bidi-font-family:times new roman">Whatever stage of life you’re in, fromchildhood to old age, there’s a Swisse product specially tailored to meet yourspecific needs and keep you in peak physical and mental health. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:.5in"><span style="font-size: 10pt;">Check out the full range of </span><u style="font-size: 10pt;">SwisseMultivitamins here</u><span style="font-size: 10pt;">.&nbsp;<o:p /></span></p>', N'', N'Vitamins.aspx', 0)
INSERT [dbo].[tbl_Blog] ([id], [title], [blogContent], [blogImage], [blogLink], [deleted]) VALUES (14, N'Why do we put herbal extracts in Swisse Multivitamins?', N'<p class="MsoNormal" style="margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Many people ask us why we use natural herbal extracts in ourmultivitamins when many of our competitors do not. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">The reasons are simple. <o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">At Swisse we take a holisticapproach to health. Our formulas combine the very best available ingredients totarget specific health concerns and we often turn to natural herbs for theirefficacy in treating a wide range of issues as well as enhancing general wellbeing.&nbsp; <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Herbs have been used in traditionalChinese medicine for thousands of years, and Swisse understands the benefit ofincorporating the unique properties of plants into our formulas, especiallywhen they can be used in conjunction with the latest scientific methods andtechniques. <o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">What are some of the herbs used in Swisse formulas?<o:p /></span></b></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Swisse uses a huge range ofdifferent natural herb extracts including:<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">Boswellia</span></b><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au"> – This herb has a long history oftherapeutic uses and is known for its strong anti-inflammatory and analgesicproperties.</span><span style="font-size:10.0pt;mso-bidi-font-family:times new roman"><o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><b>St. Mary’s Thistle</b><span style="font-size:10.0pt;mso-bidi-font-family:times new roman">–Traditionally used as a liver tonic to help support and protect liver health,and assist with the regeneration of liver cells.<o:p /></span></p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;margin-left:42.55pt"><span lang="EN-AU" style="font-size:10.0pt;mso-bidi-font-family:times new roman;mso-ansi-language:en-au">You can find extracts of real herbsin much of the Swisse range, and definitely in all of our gender-targetedmultivitamins. Check out the range and learn how they can help support yourhealth and wellbeing during every stage of life. You can find the full <u>Swissemultivitmains range here</u>.<o:p /></span></p>', N'', N'HerbalExtract.aspx', 0)
SET IDENTITY_INSERT [dbo].[tbl_Blog] OFF
SET IDENTITY_INSERT [dbo].[tbl_Blog_HealthAndHappiness] ON 

INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (1, 8, 1)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (2, 8, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (3, 2, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (4, 2, 6)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (5, 10, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (6, 9, 1)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (7, 9, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (8, 1, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (9, 13, 1)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (10, 13, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (11, 13, 7)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (12, 3, 1)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (13, 3, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (14, 4, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (15, 5, 5)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (16, 5, 7)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (17, 7, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (18, 7, 7)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (19, 6, 7)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (20, 11, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (21, 12, 1)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (22, 12, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (23, 14, 2)
INSERT [dbo].[tbl_Blog_HealthAndHappiness] ([id], [blogid], [healthandhappinessblogid]) VALUES (24, 14, 7)
SET IDENTITY_INSERT [dbo].[tbl_Blog_HealthAndHappiness] OFF
SET IDENTITY_INSERT [dbo].[tbl_Blog_RelatedBlogs] ON 

INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (1, 8, 2)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (2, 8, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (3, 8, 12)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (4, 2, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (5, 2, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (6, 2, 9)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (7, 10, 11)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (8, 10, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (9, 10, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (10, 10, 6)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (11, 1, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (12, 1, 11)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (13, 1, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (14, 1, 12)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (15, 13, 9)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (16, 13, 14)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (17, 13, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (18, 13, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (19, 3, 9)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (20, 3, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (21, 3, 13)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (22, 4, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (23, 4, 2)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (24, 4, 11)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (25, 4, 14)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (26, 7, 12)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (27, 7, 11)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (28, 7, 14)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (29, 7, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (30, 6, 11)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (31, 6, 10)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (32, 6, 1)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (33, 6, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (34, 11, 1)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (35, 11, 10)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (36, 11, 2)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (37, 11, 4)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (38, 12, 14)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (39, 12, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (40, 12, 1)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (41, 12, 9)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (42, 14, 13)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (43, 14, 9)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (44, 14, 8)
INSERT [dbo].[tbl_Blog_RelatedBlogs] ([id], [blogid], [relatedblogid]) VALUES (45, 14, 4)
SET IDENTITY_INSERT [dbo].[tbl_Blog_RelatedBlogs] OFF
SET IDENTITY_INSERT [dbo].[tbl_Blog_RelatedProducts] ON 

INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (1, 8, 19)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (2, 8, 20)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (3, 8, 21)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (4, 2, 19)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (5, 2, 20)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (6, 2, 21)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (7, 10, 34)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (8, 10, 35)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (9, 10, 44)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (10, 9, 9)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (11, 1, 24)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (12, 1, 28)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (13, 1, 37)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (14, 13, 9)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (15, 3, 9)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (16, 4, 19)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (17, 4, 20)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (18, 4, 38)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (19, 5, 32)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (20, 5, 41)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (21, 7, 30)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (22, 7, 32)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (23, 6, 26)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (24, 6, 37)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (25, 11, 23)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (26, 11, 24)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (27, 11, 28)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (28, 12, 19)
INSERT [dbo].[tbl_Blog_RelatedProducts] ([id], [blogid], [productid]) VALUES (29, 14, 9)
SET IDENTITY_INSERT [dbo].[tbl_Blog_RelatedProducts] OFF
SET IDENTITY_INSERT [dbo].[tbl_Blog_Themes] ON 

INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (1, 8, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (2, 2, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (3, 10, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (4, 9, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (5, 1, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (6, 13, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (7, 3, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (8, 7, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (9, 6, 3)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (10, 11, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (11, 11, 4)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (12, 12, 2)
INSERT [dbo].[tbl_Blog_Themes] ([id], [blogid], [themeid]) VALUES (13, 14, 2)
SET IDENTITY_INSERT [dbo].[tbl_Blog_Themes] OFF
SET IDENTITY_INSERT [dbo].[tbl_BlogFormat] ON 

INSERT [dbo].[tbl_BlogFormat] ([blogFormatID], [blogFormat], [blogFormatDesc], [blogFormatStatus], [deleted]) VALUES (1, N'Articles', NULL, 1, 0)
INSERT [dbo].[tbl_BlogFormat] ([blogFormatID], [blogFormat], [blogFormatDesc], [blogFormatStatus], [deleted]) VALUES (2, N'Themes', NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[tbl_BlogFormat] OFF
SET IDENTITY_INSERT [dbo].[tbl_BlogTheme] ON 

INSERT [dbo].[tbl_BlogTheme] ([blogThemeID], [blogTheme], [blogThemeDesc], [blogThemeStatus], [deleted]) VALUES (1, N'Weight Loss', NULL, 1, 0)
INSERT [dbo].[tbl_BlogTheme] ([blogThemeID], [blogTheme], [blogThemeDesc], [blogThemeStatus], [deleted]) VALUES (2, N'Balanced Diet & Nutrition', NULL, 1, 0)
INSERT [dbo].[tbl_BlogTheme] ([blogThemeID], [blogTheme], [blogThemeDesc], [blogThemeStatus], [deleted]) VALUES (3, N'Beauty Tips', NULL, 1, 0)
INSERT [dbo].[tbl_BlogTheme] ([blogThemeID], [blogTheme], [blogThemeDesc], [blogThemeStatus], [deleted]) VALUES (4, N'Party Season', NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[tbl_BlogTheme] OFF
SET IDENTITY_INSERT [dbo].[tbl_CMSUser] ON 

INSERT [dbo].[tbl_CMSUser] ([id], [username], [password]) VALUES (1, N'mina', N'37+tSYehEhxJHaLq1bHtnA==')
SET IDENTITY_INSERT [dbo].[tbl_CMSUser] OFF
SET IDENTITY_INSERT [dbo].[tbl_Country] ON 

INSERT [dbo].[tbl_Country] ([countryID], [countryName], [countryLanguage]) VALUES (1, N'Singapore', N'English')
SET IDENTITY_INSERT [dbo].[tbl_Country] OFF
SET IDENTITY_INSERT [dbo].[tbl_FAQs] ON 

INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (1, N'FAQ', N'FAQ1.aspx', NULL, N'faq1.jpg', N'Map2', N'14,140,121,168', NULL)
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (2, N'FAQ2', N'FAQ2.aspx', NULL, N'faq2.jpg', N'Map3', N'15,141,120,169', NULL)
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (3, N'FAQ3', N'FAQ3.aspx', NULL, N'faq3.jpg', N'Map4', N'14,141,122,168', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (4, N'FAQ4', N'FAQ4.aspx', NULL, N'faq4.jpg', N'Map5', N'15,141,121,167', NULL)
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (5, N'FAQ5', N'FAQ5.aspx', NULL, N'faq5.jpg', N'Map6', N'14,143,123,168', NULL)
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (6, N'FAQ6', N'FAQ6.aspx', NULL, N'faq6.jpg', N'Map7', N'16,142,123,168', N'  <div class="clearfloat"></div>')
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (7, N'FAQ7', N'FAQ7.aspx', NULL, N'faq7.jpg', N'Map8', N'14,143,123,169', NULL)
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (8, N'FAQ8', N'FAQ8.aspx', NULL, N'faq8.jpg', N'', N'', NULL)
INSERT [dbo].[tbl_FAQs] ([id], [title], [link], [content], [image], [usemap], [coords], [divclass]) VALUES (9, N'FAQ9', N'FAQ9.aspx', NULL, N'faq9.jpg', N'', N'', NULL)
SET IDENTITY_INSERT [dbo].[tbl_FAQs] OFF
SET IDENTITY_INSERT [dbo].[tbl_Gender] ON 

INSERT [dbo].[tbl_Gender] ([id], [gender]) VALUES (1, N'Male')
INSERT [dbo].[tbl_Gender] ([id], [gender]) VALUES (2, N'Female')
SET IDENTITY_INSERT [dbo].[tbl_Gender] OFF
SET IDENTITY_INSERT [dbo].[tbl_HealthAndHappinessBlog] ON 

INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (1, N'Healthy Eating', N'HealthyEating.aspx', NULL)
INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (2, N'Healthy Living', N'HealthyLiving.aspx', NULL)
INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (3, N'Ambassador News', N'AmbassadorNews.aspx', NULL)
INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (4, N'Events', N'Events.aspx', NULL)
INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (5, N'Research News', N'ResearchNews.aspx', NULL)
INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (6, N'Expert Opinion', N'ExpertOpinion.aspx', NULL)
INSERT [dbo].[tbl_HealthAndHappinessBlog] ([id], [title], [link], [content]) VALUES (7, N'Product Update', N'ProductUpdate.aspx', NULL)
SET IDENTITY_INSERT [dbo].[tbl_HealthAndHappinessBlog] OFF
SET IDENTITY_INSERT [dbo].[tbl_Ingredients] ON 

INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (1, N'Calcium', N'<p class="MsoNormal">Calcium is needed for the maintenance of normal bones and teeth. Calcium is an important structural component of bone and teeth. Adequate calcium intake throughout childhood and adolescence is needed to achieve maximum peak bone mass in young adulthood which is an important determinant of bone mineral status in later life. The growth, development and maintenance of bone and teeth is related to the quantity of dietary calcium consumed and recommended intakes of calcium to meet requirements for growth, development and maintenance of bone at all ages have been established by various authorities.&nbsp;</p>
<p class="MsoNormal">Functions – Calcium contributes to:</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal muscle function and neurotransmission.</span> Normal muscle function (including the heart) and neurotransmission require adequate calcium concentrations within the cells and in the extracellular fluid.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal blood clotting.</span> Calcium is necessary to stabilize or allow maximal activity for a number of blood clotting enzymes.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">The normal function of digestive enzymes.</span> Several digestive enzymes (e.g. lipase) require a chemical association between calcium and enzyme protein(s) for full activity to occur.</p>
<p><br />
</p>', N'', N'Calcium.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (2, N'Chromium', N'<p class="MsoNormal">Functions – Chromuin contributes to:&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">The maintenance of normal blood glucose levels</span> - Hyperglycaemia is a common feature of chromium depletion in humans, which is reversed by the administration of chromium.</p>', N'', N'Chromium.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (3, N'Copper', N'<p class="MsoNormal">Function – Copper contributes to:&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">The normal function of the immune system</span> – A Copper-related enzyme (cytochrome c oxidase) is needed for energy production of immune cells. Moderate and even marginal copper deficiency affects some activities from different immune cells adversely.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">Maintenance of normal connective tissues</span> - Copper supports an enzyme (lysyl oxidase) that is critical to the formation and function of connective tissue throughout the body.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal energy-yielding metabolism</span> - The copper supports the cellular energy production.</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal functioning of the nervous system</span> - Copper is required for the formation and maintenance of myelin (which is essential for the proper functioning of the nervous system)</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal skin and hair pigmentation</span> - The role of copper in the pigmentation of skin, hair, and eyes is related to the requirement of the cuproenzyme for melanin synthesis.</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal iron transport in the body</span> - Copper is required for iron transport to the bone marrow for red blood cell formation. During severe copper deficiency, iron transport within the body is adversely affected, and iron tends to accumulate in many tissues.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">The protection of cells from oxidative stress</span> - Copper functions as a component of a number of enzymes (IoM, 2001). One of these enzymes provides a defense against oxidative damage from superoxide radicals.</p>
<p class="MsoNormal"><br />
<br />
</p>
<p class="MsoNormal"><br />
</p>', N'', N'Copper.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (4, N'Iodine', N'<p class="MsoNormal">Function – Iodone contributes to:&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal cognitive function and normal functioning of the nervous system</span> - A cause and effect relationship has been established between the dietary intake of iodine and contribution to normal cognitive and neurological function.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">To the normal growth of children</span> - Iodine is essential dietary for the synthesis of the thyroid hormones.. Evidence provided by reports from authoritative bodies and reviews shows that there is good consensus on the role of iodine in growth and development.&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal energy-yielding metabolism</span> - A cause and effect relationship has been established between the dietary intake of iodine and normal energy-yielding metabolism.</p>
<p class="MsoNormal"><br />
</p>', N'', N'Iodine.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (5, N'Iron', N'<p class="MsoNormal">Function – Iron contributes to:&nbsp;</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal formation of red blood cells and haemoglobin and to normal oxygen transport in the body.</span> It is well established that inadequate dietary iron intake in humans leads to hypochromic and microcytic anaemia.</p>
<p class="MsoNormal"><span style="font-weight: bold;">Normal cognitive function</span> - It is well established that inadequate dietary iron intake in humans leads to reduced oxygen transport, which could have an impact on cognitive function. The cognitive deficiency symptoms observed with iron-deficient anaemia include deficits in attention, perceptual motor speed, memory and verbal fluency. Cognitive function includes different functions such as memory, attention (concentration), learning, intelligence and problem solving.</p>
<p class="MsoNormal"><br />
<br />
</p>', N'', N'Iron.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (6, N'Magnesium', N'Function – Magnesium contributes to:<br />
<br />
<span style="font-weight: bold;">Muscle function</span> – Magnesium plays a role in calcium homeostasis helping to activate utilization of calcium stored in bones. Sufficient plasma levels of &nbsp;calcium are required for muscle contraction.<br />
<br />
<span style="font-weight: bold;">Overall energy production and reduction of tiredness and fatigue</span> – magnesium acts as a co-factor of enzymes that generate energy through ATP. ATP - Adenosine triphosphate - is the energy source for the processes that occurs inside cells.<br />
<br />
<span style="font-weight: bold;">Development and maintenance of bone and teeth:</span> magnesium is a key component of bone. 50 to 60 % of magnesium is located in the bone<br />
<br />
<span style="font-weight: bold;">Muscle function</span> – Magnesium plays a role in calcium homeostasis helping to activate utilization of calcium stored in bones. Sufficient plasma levels of &nbsp;calcium are required for muscle contraction.<br />
<br />
<span style="font-weight: bold;">Regulation of potassium and sodium levels/electrolyte balance</span> - Magnesium acts on the mechanism which maintains the electrical potential of nerve tissue and cell membranes, i.e.the concentration of sodium and potassium ions in the cells.<br />
<br />
<span style="font-weight: bold;">Has a role in the process of cell division:</span> Magnesium is able to form complexes with nucleic acids which are responsible for cell division.<br />
<br />
<br />
<br />', N'', N'Magnesium.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (7, N'Manganese', N'Function – Manganese contributes to:  <br />
<br />
<span style="font-weight: bold;">The normal formation of connective tissue</span> – Collagen is a protein and one of the major constituents of connective tissue. Manganese is a cofactor of many enzymes involved in amino acids metabolism, the protein’s building blocks <br />
<br />
<span style="font-weight: bold;">The maintenance of normal bones</span> – It is thougth that Manganese is part of the developmental process and the structure of the fragile ear bones and joint cartilage <br />
<br />
<span style="font-weight: bold;">Normal energy-yielding metabolism</span>  - Manganese is a cofactor of many enzymes involved in glucose metabolism (building block for energy production). <br />
<br />
<span style="font-weight: bold;">The protection of cells from oxidative stress</span> -  Manganese is a component of antioxidant enzyme manganese superoxide dismutase (MnSOD)', N'', N'Manganese.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (8, N'Selenium', N'Function – Selenium contributes to: <br />
 <br />
<span style="font-weight: bold;">The normal function of the immune system</span> - Selenium appears to play a role in cell-mediated immunity.. <br />
<br />
<span style="font-weight: bold;">Normal spermatogenesis</span> – There is a protein selenium-dependant in sperm (selenoprotein) that has a structural and an enzymatic role. It is responsible for both the maintenance of motility and the structural integrity of the tail of the sperm. Both human and other mammals exhibit reduced sperm motility and increased sperm rupture under conditions of low selenium supply. <br />
<br />
<span style="font-weight: bold;">The maintenance of normal hair and normal nails</span> - Deficiency in selenium has been shown to result in various disorders including impairment of hair. Clinical manifestations such as white nail beds, pseudoalbinism, alopecia and thin hair have been observed in particular in patients receiving total parenteral nutrition lacking selenium. Administration of selenium was able to restore these deficiency symptoms. <br />
<br />
<span style="font-weight: bold;">The normal thyroid function</span> - The known biological functions of selenium include regulation of thyroid hormone action.<br />
<br />', N'', N'Selenium.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (9, N'Vitamin B1', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Thiamin <br />
Thiamine <br />
Antiberiberi factor <br />
Aneurine <br />
Antineuritic factor <br />
<br />
<span style="font-weight: bold;">What It is</span> <br />
Vitamin B1 is one of eight water-soluble B vitamins. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin B1 acts as a coenzyme for energy production and therefore is important for muscles activity. It also contributes to normal functioning of the nervous system. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
A good source of vitamin B1 is brewer’s yeast. Other sources include meat (especially pork and ham products), tuna, whole grain cereals, nuts, potatoes, seeds and pulses.<br />
<br />', N'', N'VitaminB1.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (10, N'Vitamin B2', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Riboflavin <br />
Riboflavine <br />
Lactoflavin<br />
  Ovoflavin <br />
Vitamin G <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
 Vitamin B2 is one of eight water-soluble B vitamins. <br />
<br />
<span style="font-weight: bold;">Role in the human body </span><br />
Vitamin B2 is converted in the body to coenzymes responsible for many enzymatic reactions involved in energy production, red blood cell formation, antibody production and maintenance of vision, skin, hair and nails.  It is also essential for the conversion of vitamin B6 and folic acid into their active forms within the body. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
The most important and common dietary sources of vitamin B2 are milk and milk products, lean meat, eggs and green leafy vegetables.', N'', N'VitaminB2.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (11, N'Vitamin B3', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Nicotinamide <br />
Niacin  Nicotinic acid <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
Vitamin B3 is one of eight water-soluble B vitamins. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin B3 contributes to proper nerve conduction and function, muscle action, digestive health, maintenance of skin and tongue integrity. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
Meat, liver, poultry, beans, cereal grains, enriched and whole-grain breads, fish are all good sources of vitamin B3.', N'', N'VitaminB3.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (12, N'Vitamin B5', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Pantothenic acid <br />
Pantothenate <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
Vitamin B5 is one of eight water-soluble B vitamins. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin B5 is involved in energy production, fatty acid metabolism, maintenance and repair of cells and tissues, synthesis of hormones, drug metabolism and alcohol detoxification. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
Vitamin B5 is found in many foods including meat, liver, kidney, poultry, fish, shellfish, mushrooms, peanuts, yeast, eggs, milk and vegetables.<br />
<br />', N'', N'VitaminB5.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (13, N'Vitamin B6', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Pyridoxine <br />
Peyridoxal  <br />
Pyridoxamine <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
Vitamin B6 is one of eight water-soluble B vitamins. <br />
<br />
<span style="font-weight: bold;">Role in the human body </span><br />
Vitamin B6 serves as a coenzyme of approximately 100 enzymes that are involved in essential chemical reactions in the body that contribute to normal functioning of the immune system, nervous system and psychological function, red blood cell formation and energy production. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
Excellent sources of vitamin B6 are brewer’s yeast, chicken and the liver of beef, pork and veal. Good sources include fish (salmon, tuna, sardines, halibut, herring), nuts (walnuts, peanuts), bread, courgettes, bananas, corn and whole grain cereals.<br />
<br />', N'', N'VitaminB6.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (14, N'Vitamin B7', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Biotin <br />
Vitamin H <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
Vitamin B7 is one of eight water-soluble B vitamins. We obtain vitamin B7 or biotin through diet and through gut bacteria that produce it in the large intestine. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin B7 contributes to the maintenance of healthy hair, skin and nails. It also acts as a coenzyme for energy metabolism. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
The richest sources of vitamin B7 are brewer’s yeast, liver and kidney. Egg yolk, soybeans, nuts and cereals are also good sources.<br />
<br />', N'', N'VitaminB7.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (15, N'Vitamin B9', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Folic acid <br />
Folate <br />
Folacin<br />
 Vitamin M <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
Vitamin B9 is one of eight water-soluble B vitamins. Folate is the naturally occurring form of vitamin B9 and folic acid is a synthetic folate compound used in vitamin supplements. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin B9 is has many functions within the body including energy production, blood cell formation and normal psychological function. Vitamin B9 can help prevent neural tube defects in the developing foetus. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
The richest sources of vitamin B9 are liver, dark green leafy vegetables, beans, peanuts, wheat germ and yeast. Other sources are egg yolk, milk and dairy products, beets, orange juice and whole wheat bread.<br />
<br />', N'', N'VitaminB9.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (16, N'Vitamin B12', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Cobalamin <br />
 Antipernicicious-anaemia factor <br />
Castle´s extrinsic factor <br />
 Animal protein factor <br />
<br />
 <span style="font-weight: bold;">What It Is Vitamin</span> <br />
B12 is one of eight water-soluble B vitamins. Vitamin B12 is the largest and most complex of all the vitamins.  <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin B12 is necessary for the formation of blood cells, nerve sheaths and various proteins. It is therefore essential for growth, normal function of the blood, nervous and immune systems. It is also involved in energy production from fat and carbohydrate metabolism.  <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
Vitamin B12 is produced exclusively by bacteria in the digestive tract of animals. Therefore, animal protein products are the main source of vitamin B12, in particular organ meats such as liver and kidney. Other good sources are fish, shellfish, eggs and dairy products.<br />
<br />', N'', N'VitaminB12.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (17, N'Vitamin C', N'<span style="font-weight: bold;">Alternatives Name</span> <br />
 Ascorbic acid <br />
Antiscorbutic vitamin <br />
Ascorbate <br />
Hexuronic acid  <br />
<br />
<span style="font-weight: bold;">What It Is </span><br />
Vitamin C is a water-soluble vitamin that has numerous functions within the human body. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin C is an antioxidant that neutralizes free radicals and helps protect cells from oxidative stress. It also supports collagen formation, wound healing, immune function, iron absorption, maintenance of blood vessels and teeth, gum and eye health. <br />
<br />
<span style="font-weight: bold;">Where it’s found</span> <br />
Citrus fruits are a particularly rich source of vitamin C. It is also found any many other fruits and vegetables including: Blackcurrants, strawberries, guava, mango and kiwi, blackcurrants, peppers, green vegetables (e.g. broccoli, Brussels sprouts). Regular intake of potatoes, cabbage, spinach and tomatoes can also provide Vitamin C.The adverse toxic effects from excessive intake of preformed vitamin A, or retinol include enlargement of liver and spleen, intracranial pressure, dry &amp; itchy skin, headaches, fatigue, hair loss, blurred vision, and loss of appetite. Excess beta-carotene (provitamin A) intake has not been shown to produce toxic adverse effects but may cause yellowish skin discolouration.<br />
<br />', N'', N'VitaminC.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (18, N'Vitamin D', N'<span style="font-weight: bold;">Alternative Names </span><br />
Vitamin D3 (cholecalciferol) or Calciferol <br />
Vitamin D2 (ergocalciferol) <br />
<br />
<span style="font-weight: bold;">What It Is</span> <br />
Vitamin D is a fat-soluble vitamin that comes in two forms. Vitamin D3 comes from animal sources and is also synthesized in the skin from exposure to sunlight. Vitamin D2 comes from plants such as yeasts and fungi. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin D has the important roles of regulating calcium and phosphate levels in the body, bone mineralization, healthy bones and teeth, as well as the normal function of muscles and the immune system. In children it is critical for normal skeletal development. Where it’s found The richest natural sources of vitamin D are fish liver oils and saltwater fish such as sardines, herring, salmon and mackerel. Eggs, meat, milk and butter also contain small amounts.<br />
<br />', N'', N'VitaminD.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (19, N'Vitamin E', N'<span style="font-weight: bold;">Alternative Names </span> <br />
Tocopherol <br />
<br />
<span style="font-weight: bold;">What It Is</span><br />
Vitamin E is a fat soluble vitamin. <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
The major biological function of vitamin E is that of an antioxidant that neutralizes free radicals and protects cells from oxidative stress. <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
Vegetable oils (olive, soya beans, palm, corn, safflower, sunflower, etc.), nuts, whole grains and wheat germ are the most important sources of vitamin E.<br />
<br />', N'', N'VitaminE.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (20, N'Vitamin K', N'<span style="font-weight: bold;">Alternative Names</span> <br />
Phylloquinone <br />
Menaquinone <br />
<br />
 <span style="font-weight: bold;">What It Is</span> <br />
Fat-soluble vitamin found in plant food sources (Phylloquinone ) or made back bacteria within the intestinal tract (Menaquinone). <br />
<br />
<span style="font-weight: bold;">Role in the human body</span> <br />
Vitamin K is essential for the synthesis of the biologically active forms of a range of proteins called vitamin K dependent proteins. Such proteins play important roles in bone health and blood clotting.  <br />
<br />
<span style="font-weight: bold;">Where It Is Found</span> <br />
The best sources of vitamin K include green leafy vegetables such as spinach, broccoli, cabbage, Brussels sprouts and lettuce. Other sources are vegetable oils (soy, canola, olive), oats, potatoes, tomatoes, asparagus, butter, margarine, milk, carrots, corn and most fruits.<br />
<br />', N'', N'VitaminK.aspx', 0)
INSERT [dbo].[tbl_Ingredients] ([id], [title], [ingContent], [ingImage], [ingLink], [deleted]) VALUES (21, N'Zinc', N'Function – Zinc contributes to: <br />
<br />
<span style="font-weight: bold;">the normal function of the immune system</span> - Zinc deficiency is associated with a decline in most aspects of immune function. Zinc deficiency renders people more susceptible to infections, while zinc supplementation in humans has shown benefit in immune responses to bacterial and viral infections (IoM, 2001).<br />
<br />
<span style="font-weight: bold;">the maintenance of normal hair</span> - Clinical manifestations of marginal to severe human zinc deficiency include various epithelial lesions. After the onset of dermatitis, the hair may become hypopigmented and acquire a reddish hue. Patchy loss of hair is a feature of zinc deficiency.<br />
<br />
<span style="font-weight: bold;">the maintenance of normal nails</span> - Clinical manifestations of marginal to severe human zinc deficiency include various epithelial lesions. Nail dystrophy has been reported as a symptom of zinc deficiency (King and Cousins, 2006).<br />
<br />
<span style="font-weight: bold;">the maintenance of normal skin</span> - One of the clinical manifestations of severe zinc deficiency in humans is akrodermatitis, characterised by erythematous, vesiculobullous, and pustular rashes primarily around the body orifices and at the extremities.<br />
<br />
<span style="font-weight: bold;">normal fertility and reproduction</span> - Zinc plays a role in reproduction in males and females. Spermatogenesis is a zinc dependent process and seminal fluid is particularly rich in zinc.<br />
<br />
<span style="font-weight: bold;">the maintenance of normal testosterone levels in the blood</span> - An important class of “zinc finger” transcription factors is the steroid/thyroid receptor superfamily, which is responsible for mediating the biological response to a wide range of hormonal and metabolic signals (Freake, 2006).<br />
<br />
<span style="font-weight: bold;">the maintenance of normal vision</span> - Zinc regulates the metabolic conversion of retinol to retinaldeyde (retinal) through the zinc-dependent enzyme retinol dehydrogenase. The conversion of retinol to retinal is a critical step in the visual cycle in the retina of the eye (Christian and West, 1998).<br />
<br />
<span style="font-weight: bold;">normal metabolism of vitamin A</span> - Zinc participates in the absorption, mobilization, transport and metabolism of micronutrients, including vitamin A, through its involvement in protein synthesis and cellular enzyme functions (IoM, 2001).<br />
<br />
<span style="font-weight: bold;">the protection of cells from oxidative stress</span> - Zinc participates in the antioxidant defense system of the body. It can bind to thiol groups in proteins, making them less susceptible to oxidation. By displacing redox-reactive metals such as iron and copper from both proteins and lipids it can reduce the metal-induced formation of hydroxyl radicals and thus protect the macromolecules.<br />
<br />
<span style="font-weight: bold;">normal acid-base metabolism</span> - Zinc has essential structural, regulatory or catalytic roles in many enzymes. Carbonic anhydrase, is a zinc metalloenzyme, where zinc is a direct participant in the catalytic function.<br />
<br />
<span style="font-weight: bold;">normal carbohydrate metabolism/ protein synthesis</span> - Zinc is an essential component of a large number of enzymes that participate in the synthesis and degradation of carbohydrates, lipids, proteins and nucleic acids (Freake, 2006)<br />
<br />
<span style="font-weight: bold;">normal cognitive function</span> - In the central nervous system zinc has a role as a neurosecretory product or cofactor, and is highly concentrated in the synaptic vesicles of specific neurons.<br />
<br />
<span style="font-weight: bold;">normal macronutrient metabolism</span> - Its main physiological impact is in all steps of protein synthesis. Zinc activates DNA and RNA polymerases and is essential to histones regulation. It is also involved in several peptidic hormones stabilization (insulin, gustin, thymulin)”. <br />
<br />
<span style="font-weight: bold;">normal metabolism of fatty acids</span> - Zinc is necessary for the conversion of linoleic acid to gamma-linolenic acid and the mobilization of dihomogammalinolenic acid for the synthesis of series-1 prostaglandins (IoM, 2001).<br />
<br />
<span style="font-weight: bold;">the maintenance of normal bones</span> - Zinc is an essential cofactor for enzymes involved in synthesis of various bone matrix constituents and plays a particularly important role in the regulation of bone deposition and resorption. Zinc also plays a structural role in the bone matrix.<br />
<br />
Zinc has a role in the process of cell division and contributes to a normal DNA synthesis - Zinc has essential structural, regulatory or catalytic roles in many enzymes. Zinc plays a role in the stabilization of genetic material and is an essential component of some enzymes that participate in the synthesis of nucleic acids.<br />
<br />
Studies suggest that taking high levels of vit C and Zinc, even as symptoms appear have been shown to reduce severity of rhinorrea', N'', N'Zinc.aspx', 0)
SET IDENTITY_INSERT [dbo].[tbl_Ingredients] OFF
SET IDENTITY_INSERT [dbo].[tbl_Ingredients_RelatedProducts] ON 

INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (1, 1, 36)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (2, 1, 45)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (3, 3, 21)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (4, 3, 18)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (5, 4, 9)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (6, 5, 14)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (7, 5, 44)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (8, 6, 34)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (9, 6, 35)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (10, 9, 28)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (11, 10, 28)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (12, 12, 28)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (13, 13, 28)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (14, 14, 26)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (15, 16, 28)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (16, 17, 19)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (17, 17, 20)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (18, 17, 21)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (19, 18, 9)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (20, 18, 36)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (21, 19, 9)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (22, 21, 22)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (23, 21, 18)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (24, 2, 26)
INSERT [dbo].[tbl_Ingredients_RelatedProducts] ([id], [ingID], [productID]) VALUES (25, 2, 30)
SET IDENTITY_INSERT [dbo].[tbl_Ingredients_RelatedProducts] OFF
SET IDENTITY_INSERT [dbo].[tbl_IngredientsGlossary] ON 

INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (1, N'Calcium', N'Calcium.aspx', NULL, N'bdr-wd color-pnk', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (2, N'Chromium', N'Chromium.aspx', NULL, N'bdr-wd color-bl', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (3, N'Copper', N'Copper.aspx', NULL, N'bdr-wd color-rd', N'fltrt last', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (4, N'Iodine', N'Iodine.aspx', NULL, N'bdr-wd color-rd', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (5, N'Iron', N'Iron.aspx', NULL, N'bdr-wd color-pnk', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (6, N'Magnesium', N'Magnesium.aspx', NULL, N'bdr-wd color-or', N'fltrt last', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (7, N'Manganese', N'Manganese.aspx', NULL, N'bdr-wd color-gr', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (8, N'Selenium', N'Selenium.aspx', NULL, N'bdr-wd color-y', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (9, N'Vitamin B1', N'VitaminB1.aspx', NULL, N'bdr-wd color-bl', N'fltrt last', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (10, N'Vitamin B2', N'VitaminB2.aspx', NULL, N'bdr-wd color-bl', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (11, N'Vitamin B3', N'VitaminB3.aspx', NULL, N'bdr-wd color-rd', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (12, N'Vitamin B5', N'VitaminB5.aspx', NULL, N'bdr-wd color-gd', N'fltrt last', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (13, N'Vitamin B6', N'VitaminB6.aspx', NULL, N'bdr-wd color-gd', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (14, N'Vitamin B7', N'VitaminB7.aspx', NULL, N'bdr-wd color-lgry', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (15, N'Vitamin B9', N'VitaminB9.aspx', NULL, N'bdr-wd color-pnk', N'fltrt last', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (16, N'Vitamin B12', N'VitaminB12.aspx', NULL, N'bdr-wd color-pnk', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (17, N'Vitamin C', N'VitaminC.aspx', NULL, N'bdr-wd color-bl', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (18, N'Vitamin D', N'VitaminD.aspx', NULL, N'bdr-wd color-rd', N'fltrt last', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (19, N'Vitamin E', N'VitaminE.aspx', NULL, N'bdr-wd color-rd', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (20, N'Vitamin K', N'VitaminK.aspx', NULL, N'bdr-wd color-pnk', N'fltlft', NULL)
INSERT [dbo].[tbl_IngredientsGlossary] ([id], [title], [link], [content], [borderClass], [divLastClass], [divClass]) VALUES (21, N'Zinc', N'Zinc.aspx', NULL, N'bdr-wd color-or', N'fltrt last', N'<div class="clearfloat"></div>')
SET IDENTITY_INSERT [dbo].[tbl_IngredientsGlossary] OFF
SET IDENTITY_INSERT [dbo].[tbl_OffersAndPromos] ON 

INSERT [dbo].[tbl_OffersAndPromos] ([id], [title], [link], [content], [image], [usemap], [width], [height], [coords], [divclass]) VALUES (1, N'Offers and Promos', N'OffersAndPromos1.aspx', N'<h1>win 1 of 20 VIP Dinners for 2 at willin low''s restaurant wild rocket</h1>
<p class="txt20">Discover Chef Willin Low''s unique take on modern Singaporean cuisine at Wild Rocket, his flagship Mount Emily restaurant</p>
<div class="share-this-box fltlft">
<div class="share-box fltlft"><p class="fltlft mtop3">Share</p>
    <p class="fltlft"><a href="#"><img src="images/facebook-ico.jpg" width="24" height="24" alt="Facebook"></a><a href="#"><img src="images/twitter-ico.jpg" width="24" height="24" alt="Twitter"></a><a href="#"><img src="images/gplus-ico.jpg" width="24" height="24" alt="Google +"></a></p>
    <div class="clearfloat"></div>
    </div>
<div class="emailthis fltrt">
<p class="fltlft"><img src="images/email.jpg" width="24" height="24" alt="Email This"></p>
<p class="fltlft">email this</p>
<div class="clearfloat"></div>
</div>
<div class="clearfloat"></div>
</div>
<div class="clearfloat"></div>
<img src="images/win.jpg" width="600" height="330" alt="Win">
<p class="mtop20">PBR Portland Odd Future, Bushwick freegan literally synth skateboard bicycle rights normcore actually twee biodiesel butcher. Biodiesel mlkshk cornhole, Etsy chillwave yr Echo Park pop-up Vice Schlitz beard post-ironic 90''s retro. Cray occupy listicle chia mlkshk. Occupy you probably haven''t heard of them whatever hoodie. Gentrify chia ugh tofu, typewriter umami Odd Future taxidermy ethical selfies PBR VHS YOLO tattooed Bushwick. Literally cred Pitchfork, Wes Anderson four dollar toast photo booth Intelligentsia. Thundercats +1 fashion axe plaid, taxidermy meditation iPhone 8-bit authentic fap.</p>
 <div class="btn color-gr"><a href="#">Enter Now</a></div>', N'Offers- Promos-banner.jpg', N'Map', N'960', N'500', N'529,401,653,441', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_OffersAndPromos] ([id], [title], [link], [content], [image], [usemap], [width], [height], [coords], [divclass]) VALUES (2, N'A Makeover', N'OffersAndPromos2.aspx', NULL, N'a-makeover.jpg', N'Map2', N'640', N'400', N'350,333,473,374', NULL)
INSERT [dbo].[tbl_OffersAndPromos] ([id], [title], [link], [content], [image], [usemap], [width], [height], [coords], [divclass]) VALUES (3, N'Color Run', N'OffersAndPromos3.aspx', NULL, N'color-run.jpg', N'Map3', N'320', N'400', N'32,334,157,375', N'<div class="clearfloat"></div>')
INSERT [dbo].[tbl_OffersAndPromos] ([id], [title], [link], [content], [image], [usemap], [width], [height], [coords], [divclass]) VALUES (4, N'Immunity', N'OffersAndPromos4.aspx', NULL, N'immunity.jpg', N'Map4', NULL, NULL, N'23,344,141,374', NULL)
INSERT [dbo].[tbl_OffersAndPromos] ([id], [title], [link], [content], [image], [usemap], [width], [height], [coords], [divclass]) VALUES (5, N'Hair Skin and Nails', N'OffersAndPromos5.aspx', NULL, N'hair-skin-nails.jpg', N'Map5', NULL, NULL, N'24,345,141,376', NULL)
INSERT [dbo].[tbl_OffersAndPromos] ([id], [title], [link], [content], [image], [usemap], [width], [height], [coords], [divclass]) VALUES (6, N'Joints', N'OffersAndPromos6.aspx', NULL, N'joints.jpg', N'Map6', NULL, NULL, N'23,345,141,374', N'<div class="clearfloat"></div>')
SET IDENTITY_INSERT [dbo].[tbl_OffersAndPromos] OFF
SET IDENTITY_INSERT [dbo].[tbl_OurStory] ON 

INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (1, N'About Swisse', N'AboutSwisse.aspx', N'<p class="MsoNormal">Hello Singapore!</p>
<p class="MsoNormal">Allow us to introduce ourselves – we’re Swisse, Australia’s #1 Multivitamin Brand.&nbsp;</p>
<p class="MsoNormal">For over 50 years we’ve helped people live healthier and be happier, and today millions of people trust in our range of unique, scientifically validated formulas and our proven results.&nbsp;</p>
<p class="MsoNormal">When you take Swisse multivitamins and supplements you know you’re getting some of the purest and most potent ingredients available. Our men’s and women’s multivitamins alone contain around 50 ingredients, that’s over 20 more than Singapore’s leading Multivitamin brand. With Swisse you can feel confident knowing our products are made with world-class manufacturing techniques and rigorous testing.</p>
<p class="MsoNormal">Read more about our commitment to research and development in nutraceuticals and complementary medicine in Our Research.</p>
<p class="MsoNormal">At Swisse, we love to support sports stars, athletes, role models and people in the spotlight who embody our vision of health and happiness. Swisse has been the choice of many champion athletes and international superstars like Nicole Kidman, the Australian Olympic Team and tennis champion Lleyton Hewitt. We are proud of our associations with these ambassadors and hope they inspire you as they inspire us.</p>
<p class="MsoNormal">Find out more in Our Ambassadors section.</p>
<p class="MsoNormal">Join us to live healthy, be happy with Swisse!</p>')
INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (2, N'Our History', N'OurHistory.aspx', N'<p class="MsoNormal"><span lang="EN-US">Inspired by the development of naturalmedicines during a trip to&nbsp; Switzerlandin the 1960’s, Swisse founder Kevin Ring returned to Australia to develop hisown range of herbal and vitamin supplements. Soon afterwards he introduced thefirst product - pollen tablets - and Swisse was born!</span></p>
<p class="MsoNormal"><span lang="EN-US"><br />
<br />
<img src="images/01.jpg" /></span></p>
<p class="MsoNormal"><span lang="EN-US">By 1991 we were thriving. The launch ofSwisse Women’s Ultivite set the benchmark as the ‘Rolls Royce’ ofmultivitamins, and the release of Swisse Men’s Ultivites four years laterfirmly cemented our position as market leaders in Australia.</span></p>
<p class="MsoNormal"><span lang="EN-US">Our unique company culture is a large partof what makes Swisse a success. Our current CEO, Radek Sali, leads the companywith a passionate and progressive approach and his extraordinary businessacumen has seen him become one of the most celebrated CEOs in Australia.</span></p><img src="images/02.jpg" />
<p class="MsoNormal"><i><span lang="EN-US" style="mso-bidi-font-family:arial;color:#111b22">“From the moment I joinedSwisse, I had a vision to better educate people about how to look afterthemselves – on how to be healthier and happier. I’m incredibly proud to saythat, with the help of a very talented and committed team behind me, we haven’twavered in our pursuit of making this a reality.”<o:p /></span></i></p>
<p class="MsoNormal"><span lang="EN-US" style="mso-bidi-font-family:arial;color:#111b22">- Swisse Australia CEO Radek Sali</span></p>
<img src="images/03.jpg" />
<p class="MsoNormal"><span lang="EN-US">Today we continue to grow and expand, butwe never lose sight of the founding principles established by Kevin Ring over50 years ago - to help people live healthier and happier lives.</span></p>
<p class="MsoNormal"><span lang="EN-US">We’re delighted to be helping Singaporeanson their journey to be healthy and live happy!</span></p>
<p class="MsoNormal"><span lang="EN-US"><br />
</span></p>')
INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (3, N'Our Philosophy', N'OurPhilosophy.aspx', N'<img src="images/04.jpg" />

<div><span style="font-size: 13.3333330154419px;">Health, happiness and wellbeing. These three things are at the core of everything we do here at Swisse.&nbsp;</span></div>
<div><span style="font-size: 13.3333330154419px;"><br />
</span></div>
<div><span style="font-size: 13.3333330154419px;">We believe in encouraging people to make their health an everyday priority, rather than waiting until it suffers to take action. We believe that conventional approaches to health can always be improved upon, and we’re advocates for the powers of positive thinking, strong relationships, regular exercise and wholesome food. Most importantly, we believe in our philosophy of Celebrating Life Every Day. This means taking time out, every day, to appreciate all the great things about life. &nbsp;</span></div>
<div><span style="font-size: 13.3333330154419px;"><br />
</span></div>
<div><span style="font-size: 13.3333330154419px;">As part of our contribution to spreading good health and wellbeing in the community, we started The Celebrate Life Every Day foundation. The aim of this foundation is to support the programs already working hard to promote health in our communities. It’s our way of giving back some of the support we’ve been shown over the last almost 50 years.</span></div>
<div><span style="font-size: 13.3333330154419px;"><br />
</span></div>
<div><span style="font-size: 13.3333330154419px;">Our inaugural event, The Swisse Color Run, has been touted as the ‘happiest 5km on the planet’, and grows in participants every year allowing us to raise valuable funds for our charity partners. The Swisse Color Run is the perfect expression of our Celebrate Life Every Day philosophy!</span></div>
<div><span style="font-size: 13.3333330154419px;"><br />
</span></div>
<div><span style="font-size: 13.3333330154419px;">If you’d like to know more about the valuable work Swisse does check out our Corporate Social Responsibility section.</span></div>
<div><br />
</div>')
INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (4, N'Swisse Ambassadors', N'SwisseAmbassadors.aspx', N'<div class="col_721 fltrt mtop20">  <img src="images/meet-willin.jpg" alt="Meet Willin Low" width="720" height="400" usemap="#Map" />  <map name="Map">    <area shape="rect" coords="429,330,580,371" href="ViewA.aspx?id=WillinLow.aspx" />  </map>  <img src="images/rebeccalim.jpg" alt="Rebecca Lim" width="720" height="400" usemap="#Map7" />  <map name="Map7">    <area shape="rect" coords="413,330,565,370" href="ViewA.aspx?id=RebeccaLim.aspx" />  </map>  <img src="images/wallables.jpg" alt="Wallables" width="240" height="400" usemap="#Map2" class="fltlft" />  <map name="Map2">    <area shape="rect" coords="25,361,130,386" href="ViewA.aspx?id=Wallabies.aspx" />  </map>  <img src="images/lleyton.jpg" alt="Lleyton Hewitt" width="240" height="400" usemap="#Map3" class="fltlft" />  <map name="Map3">    <area shape="rect" coords="24,360,130,387" href="ViewA.aspx?id=Lleyton.aspx" />  </map>  <img src="images/ricky.jpg" alt="Ricky Ponting" width="240" height="400" usemap="#Map4" class="fltlft" />  <map name="Map4">    <area shape="rect" coords="24,361,133,388" href="ViewA.aspx?id=Ricky.aspx" />  </map>
<div class="clearfloat"></div>  </div>
<div class="clearfloat"></div>')
INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (5, N'Corporate Social Responsibility', N'CorporateSocialResponsibility.aspx', N'<div>At Swisse we believe that good health and wellbeing should extend beyond individuals, to encompass the world we live in and our wider community.</div>
<div><br />
</div>
<div>That’s why in Australia we offset carbon emissions and operate as a carbon neutral company, accredited by the Australian Government’s National Carbon Offset Standard (NCOS). As a leader in health and wellness it makes sense to us to take a leadership role when it comes to protecting the environment. &nbsp;We consider nature to be our number one partner and use natural ingredients in our products whenever possible, so we make it a priority to help ensure the long-term sustainability of our environment.&nbsp;</div>
<div><br />
</div>
<div>We’re also always looking for fun, new, and usually colourful ways to return the support our community has showed us for nearly 50 years.&nbsp;</div>
<div><br />
</div>
<img src="images/06.jpg" />
<div>We financially contribute to several major charities and not-for-profit organisations, and in 2012 founded The Swisse Celebrate Life Foundation. The foundation was created to assist established charities already working hard in our communities to promote health and wellbeing, and we feel pretty good knowing our funds are helping their great ideas and initiatives become a reality.</div>
<div><br />
</div>
<img src="images/05.jpg" />
<div>Swisse is also the proud naming sponsor of ‘The Swisse Color Run’ events in Australia. Dubbed ‘the happiest 5k on the planet’ these events have successfully raising around $1 million for charities such as the Australian Paralympic Committee, Make a Wish Foundation and Vision Australia, to name a few.</div>')
INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (6, N'Celebrate Life Everyday', N'', NULL)
INSERT [dbo].[tbl_OurStory] ([id], [title], [link], [content]) VALUES (7, N'Contact Us', N'ContactUs.aspx', NULL)
SET IDENTITY_INSERT [dbo].[tbl_OurStory] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product] ON 

INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (1, 1, N'Swisse Men''s Ultivite Multivitamin', N'<p class="MsoNormal"><font color="#1b2935">Swisse Men’s Ultivite is a comprehensive multivitamin with 27 more ingredients than the leading multivitamin in Singapore*. It contains over 50 (or 52) vitamins, minerals, antioxidants and herbs tailored for men to help maintain energy levels, mental performance, provide support during stress and assist with stamina and vitality.</font></p>', N'PGT897-SG_Mens-Ultivite-60TABS_LABEL_2014.v9.png', N'55.00', N'89.00', NULL, N'60', N'120', N'4987176011145', N'4987176011329', N'Tablets', N'www.swisse.com.sg/products/swisse-mens-ultivite', N'Swisse Men’s Ultivite is a unique multivitamin to help men maintain energy levels, provide support during stress and assist with stamina and vitality.', NULL, 1, 0, N'')
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (2, 12, N'Swisse Men''s Ultivite Multivitamin', N'Swisse Men’s Ultivite is a comprehensive multivitamin with 27 more ingredients than the leading multivitamin in Singapore. It contains over 50 vitamins, minerals, antioxidants and herbs tailored for men to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Mens-Ultivite-60TABS_LABEL_2014.v9.png', N'55.00', N'89.00', NULL, N'60', N'120', N'4987176011145', N'4987176011329', N'Tablets', N'www.swisse.com.sg/products/swisse-mens-ultivite', N'Swisse Men’s Ultivite is a unique multivitamin to help men maintain energy levels, provide support during stress and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (3, 1, N'Swisse Men''s Ultivite Multivitamin Capsules', N'Swisse Men’s Ultivite (Capsules) is a comprehensive multivitamin with 27 more ingredients than the leading multivitamin in Singapore. It contains over 50 vitamins, minerals, antioxidants and herbs tailored for men to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Mens-Ultivite-60CAPS_LABEL_2014.v9.png', N'44.00', N'71.00', NULL, N'60', N'120', N'4987176011718', N'4987176011756', N'Capsules', N'www.swisse.com.sg/products/swisse-mens-ultivite-capsules', N'Swisse Men’s Ultivite (Caps) is a unique multivitamin to help men maintain energy levels, provide support during stress and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (4, 12, N'Swisse Men''s Ultivite Multivitamin Capsules', N'Swisse Men’s Ultivite (Capsules) is a comprehensive multivitamin with 27 more ingredients than the leading multivitamin in Singapore. It contains over 50 vitamins, minerals, antioxidants and herbs tailored for men to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Mens-Ultivite-60CAPS_LABEL_2014.v9.png', N'44.00', N'71.00', NULL, N'60', N'120', N'4987176011718', N'4987176011756', N'Capsules', N'www.swisse.com.sg/products/swisse-mens-ultivite-capsules', N'Swisse Men’s Ultivite (Caps) is a unique multivitamin to help men maintain energy levels, provide support during stress and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (5, 1, N'Swisse Women''s Ultivite Multivitamin', N'Swisse Women’s Ultivite is a comprehensive multivitamin with 24 more ingredients than the leading multivitamin in Singapore. It contains 49 vitamins, minerals, antioxidants and herbs tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Womens-Ultivite-60TABS_LABEL_2014MU.png', N'55.00', N'89.00', NULL, N'60', N'120', N'4987176011121', N'4987176011343', N'Tablets', N'www.swisse.com.sg/products/swisse-womens-ultivite', N'Swisse Women’s Ultivite is a multivitamin tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (6, 11, N'Swisse Women''s Ultivite Multivitamin', N'Swisse Women’s Ultivite is a comprehensive multivitamin with 24 more ingredients than the leading multivitamin in Singapore. It contains 49 vitamins, minerals, antioxidants and herbs tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Womens-Ultivite-60TABS_LABEL_2014MU.png', N'55.00', N'89.00', NULL, N'60', N'120', N'4987176011121', N'4987176011343', N'Tablets', N'www.swisse.com.sg/products/swisse-womens-ultivite', N'Swisse Women’s Ultivite is a multivitamin tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (7, 1, N'Swisse Women''s Ultivite Multivitamin Capsules', N'Swisse Women’s Ultivite (Capsules) is a comprehensive multivitamin with 24 more ingredients than the leading multivitamin in Singapore. It contains 49 vitamins, minerals, antioxidants and herbs tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Womens-Ultivite-60CAPS_LABEL_2014.v6MU.png', N'44.00', N'71.00', NULL, N'60', N'120', N'4987176011695', N'4987176011732', N'Capsules', N'www.swisse.com.sg/products/swisse-womens-ultivite-capsules', N'Swisse Women’s Ultivite (Caps) is a multivitamin tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (8, 11, N'Swisse Women''s Ultivite Multivitamin Capsules', N'Swisse Women’s Ultivite (Capsules) is a comprehensive multivitamin with 24 more ingredients than the leading multivitamin in Singapore. It contains 49 vitamins, minerals, antioxidants and herbs tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', N'PGT897-SG_Womens-Ultivite-60CAPS_LABEL_2014.v6MU.png', N'44.00', N'71.00', NULL, N'60', N'120', N'4987176011695', N'4987176011732', N'Capsules', N'www.swisse.com.sg/products/swisse-womens-ultivite-capsules', N'Swisse Women’s Ultivite (Caps) is a multivitamin tailored for women to help maintain energy levels, mental performance and assist with stamina and vitality.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (9, 1, N'Swisse Children''s Ultivite', N'Swisse Children’s Ultivite is a great tasting children’s multivitamin, containing 21 vitamins, minerals, antioxidants and herbs to support children’s health and development.', N'PGT897-SG_Childrens-Ultivite-60TABS_LABEL_2014.v7MU.png', N'26.00', N'42.00', NULL, N'60', N'120', N'4987176011244', N'4987176011367', N'Tablets', N'www.swisse.com.sg/products/swisse-childrens-ultivite', N'Swisse Children’s Ultivite is a children’s multivitamin, containing 21 vitamins, minerals, antioxidants and herbs to support child health and development.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (10, 1, N'Swisse Men''s Ultivite 50+', N'Swisse Men’s Ultivite 50+ is a comprehensive multivitamin with 13 more ingredients than the leading multivitamin in Singapore*. Containing 41 vitamins, minerals, antioxidants and herbs tailored for men 50 years and over it helps maintain energy levels, stamina, mental performance and support a healthy immune system.', N'PGT897-SG_Mens-Ultivite-50-60TABS_LABEL_2014.v2.png', N'52.00', N'69.00', NULL, N'60', N'90', N'4987176011183', N'4987176011404', N'Tablets', N'www.swisse.com.sg/products/swisse-ultivite-50-mens', N'Swisse Men’s Ultivite 50+ is a unique formulation of 41 vitamins, antioxidants and herbs tailored to provide nutritional support for men 50 years and over.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (11, 12, N'Swisse Men''s Ultivite 50+', N'Swisse Men’s Ultivite 50+ is a comprehensive multivitamin with 13 more ingredients than the leading multivitamin in Singapore*. Containing 41 vitamins, minerals, antioxidants and herbs tailored for men 50 years and over it helps maintain energy levels, stamina, mental performance and support a healthy immune system.', N'PGT897-SG_Mens-Ultivite-50-60TABS_LABEL_2014.v2.png', N'52.00', N'69.00', NULL, N'60', N'90', N'4987176011183', N'4987176011404', N'Tablets', N'www.swisse.com.sg/products/swisse-ultivite-50-mens', N'Swisse Men’s Ultivite 50+ is a unique formulation of 41 vitamins, antioxidants and herbs tailored to provide nutritional support for men 50 years and over.', NULL, 1, 0, N'')
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (12, 1, N'Swisse Men''s Ultivite 50+ Capsules', N'Swisse Men’s Ultivite 50+ (Capsules)  is a comprehensive multivitamin with 13 more ingredients than the leading multivitamin in Singapore. Containing 41 vitamins, minerals, antioxidants and herbs tailored for men 50 years and over it helps maintain energy levels, stamina, mental performance and support a healthy immune system.', N'PGT897-SG_Mens-Ultivite-50-60CAPS_LABEL_2014.v4.png', N'79.00', N'', NULL, N'60', N'', N'4987176011794', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultivite-50-mens-capsule', N'Swisse Men’s Ultivite 50+ is a unique formulation of 41 vitamins, antioxidants and herbs tailored to provide nutritional support for men 50 years and over.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (13, 12, N'Swisse Men''s Ultivite 50+ Capsules', N'<p class="MsoNormal"><span lang="EN-US">Swisse Men’s Ultivite&nbsp; 50+ has been formulated based on scientificevidence to include 41 vitamins, minerals, antioxidants and herbal extracts that provide nutritionalsupport for a healthy active lifestyle for men 50 years and over.</span></p>
<p class="MsoNormal"><span lang="EN-US">Designed to support nutritional needs andmaintain general wellbeing Swisse Men’s Ultivite 50+ active ingredients include– but are not limited to:</span></p>
<ul>
<li>
<p class="MsoNormal">B complex vitamins to assist with energy production and stamina</p></li></ul>
<ul>
<li>
<p class="MsoNormal">Vitamin E a powerful antioxidant</p></li></ul>
<ul>
<li>
<p class="MsoNormal">Calcium helps support bone heath</p></li></ul>
<p class="MsoNormal"></p>
<ul>
<li>
<p class="MsoNormal">Zinc to support immune function</p></li></ul>', N'PGT897-SG_Mens-Ultivite-50-60CAPS_LABEL_2014.v4.png', N'79.00', N'', NULL, N'60', N'', N'4987176011794', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultivite-50-mens-capsule', N'Swisse Men’s Ultivite 50+ is a unique formulation of 41 vitamins, antioxidants and herbs tailored to provide nutritional support for men 50 years and over.', NULL, 1, 0, N'')
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (14, 1, N'Swisse Women''s Ultivite 50+', N'Swisse Women’s Ultivite 50+ is a comprehensive multivitamin with 15 more ingredients than the leading multivitamin in Singapore. Containing 43 vitamins, minerals, antioxidants and herbs tailored for women 50 years and over it helps maintain energy levels, stamina, mental performance and supports bone and immune system.', N'PGT897-SG_Womens-Ultivite-50-60TABS_LABEL_2014.v5.png', N'52.00', N'69.00', NULL, N'60', N'90', N'4987176011169', N'4987176011381', N'Tablets', N'www.swisse.com.sg/products/swisse-ultivite-50-womens', N'Swisse Women’s Ultivite 50+ is a unique formulation of 43 vitamins, antioxidants and herbs tailored to provide nutritional support for women aged 50+.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (15, 11, N'Swisse Women''s Ultivite 50+', N'Swisse Women’s Ultivite 50+ is a comprehensive multivitamin with 15 more ingredients than the leading multivitamin in Singapore. Containing 43 vitamins, minerals, antioxidants and herbs tailored for women 50 years and over it helps maintain energy levels, stamina, mental performance and supports bone and immune system.', N'PGT897-SG_Womens-Ultivite-50-60TABS_LABEL_2014.v5.png', N'52.00', N'69.00', NULL, N'60', N'90', N'4987176011169', N'4987176011381', N'Tablets', N'www.swisse.com.sg/products/swisse-ultivite-50-womens', N'Swisse Women’s Ultivite 50+ is a unique formulation of 43 vitamins, antioxidants and herbs tailored to provide nutritional support for women aged 50+.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (16, 1, N'Swisse Women''s Ultivite 50+ Capsules', N'Swisse Women’s Ultivite 50+ (Capsules) is a comprehensive multivitamin with 15 more ingredients than the leading multivitamin in Singapore. Containing 43 vitamins, minerals, antioxidants and herbs tailored for women 50 years and over it helps maintain energy levels, stamina, mental performance and supports bone and immune system health.', N'PGT897-SG_Womens-Ultivite-50-60CAPS_LABEL_2014.v6.png', N'79.00', N'', NULL, N'60', N'', N'4987176011770', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultivite-50-womens-capsule', N'Swisse Women’s Ultivite 50+ is a unique formulation of 43 vitamins, antioxidants and herbs tailored to provide nutritional support for women aged 50+.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (17, 11, N'Swisse Women''s Ultivite 50+ Capsules', N'Swisse Women’s Ultivite 50+ (Capsules) is a comprehensive multivitamin with 15 more ingredients than the leading multivitamin in Singapore. Containing 43 vitamins, minerals, antioxidants and herbs tailored for women 50 years and over it helps maintain energy levels, stamina, mental performance and supports bone and immune system health.', N'PGT897-SG_Womens-Ultivite-50-60CAPS_LABEL_2014.v6.png', N'79.00', N'', NULL, N'60', N'', N'4987176011770', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultivite-50-womens-capsule', N'Swisse Women’s Ultivite 50+ is a unique formulation of 43 vitamins, antioxidants and herbs tailored to provide nutritional support for women aged 50+.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (18, 2, N'Swisse Ultiboost Zinc', N'Swisse Ultiboost Zinc is a comprehensive formula to support immune function, healthy skin and assist reproductive health. Zinc is essential for the maintenance of good health and is one of the most abundant essential trace minerals in the body.', N'PGT896-SG_ZINC-60TABS_LABEL_2014.v10MU.png', N'24.00', N'', NULL, N'60', N'', N'4987176010902', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-zinc', N'Swisse Ultiboost Zinc is a comprehensive formula to support immune function, healthy skin and assist reproductive health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (19, 2, N'Swisse Ultiboost High Strength C', N'Swisse Ultiboost High Strength Vitamin C contains a non-acidic form of vitamin C to support immune function, maintain skin health and provide antioxidant support.', N'PGT896-SG_HIGH-STRENGTH-C-60TABS_LABEL_2014.v7MU.png', N'29.00', N'', NULL, N'60', N'', N'4987176011596', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-high-strength-c', N'Swisse Ultiboost High Strength Vitamin C is a formula designed to support immune function, maintain skin health and provide antioxidant support.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (20, 2, N'Swisse Ultiboost High Strength Vitamin C Effervescent', N'Swisse Ultiboost High Strength Vitamin C Effervescent is a great tasting effervescent formula containing vitamin C to support immune function, maintain skin health and provide antioxidant support.', N'PGT1193-SG-Ultiboost-High-Strength-Vitamin-C-20TAB_TUBE_-V1-2014-MOCKUP_V2.png', N'40.00', N'', NULL, N'60', N'', N'4987176011480', N'', N'Effervescent Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-high-strength-c-eff', N'Swisse Ultiboost High Strength Vitamin C effervescent is a great tasting antioxidant formula designed to support immune function and maintain skin health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (21, 2, N'Swisse Ultiboost Immune', N'Swisse Ultiboost Immune is a premium formula containing vitamins, minerals and herbs to help maintain healthy immune function and helps reduce tiredness and fatigue.', N'PGT896-SG_IMMUNE-60TABS_LABEL_2014.v8MU.png', N'39.00', N'', NULL, N'60', N'', N'4987176011046', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-immune', N'Swisse Ultiboost Immune is a premium formula designed to help maintain healthy immune function and helps reduce tiredness and fatigue.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (22, 6, N'Swisse Ultiboost Immune', N'Swisse Ultiboost Immune is a premium formula containing vitamins, minerals and herbs to help maintain healthy immune function and helps reduce tiredness and fatigue.', N'PGT896-SG_IMMUNE-60TABS_LABEL_2014.v8MU.png', N'39.00', N'', NULL, N'60', N'', N'4987176011046', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-immune', N'Swisse Ultiboost Immune is a premium formula designed to help maintain healthy immune function and helps reduce tiredness and fatigue.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (23, 4, N'Swisse Ultiboost Mood', N'Swisse Ultiboost Mood is a comprehensive formula with vitamins and amino acids to helps relieve restlessness, nervousness and support healthy mood balance.', N'PGT896-SG_MOOD-50TABS_LABEL_2014.MU.png', N'42.00', N'', NULL, N'50', N'', N'4987176011022', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-mood', N'Swisse Ultiboost Mood is a comprehensive formula to help relieve restlessness, nervousness and support healthy mood balance.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (24, 4, N'Swisse Ultiboost Sleep', N'Swisse Ultiboost Sleep has been formulated with magnesium and herbs, including valerian that helps to relieve nervous tension and assist natural, restful sleep.', N'PGT896-SG_SLEEP-60TABS_LABEL_2014.v6MU.png', N'42.00', N'', NULL, N'60', N'', N'4987176011008', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-sleep', N'Swisse Ultiboost Sleep has been formulated with magnesium and valerian that helps to relieve nervous tension and assist natural, restful sleep.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (25, 4, N'Swisse Ultiboost Children''s Smart Fish Oil', N'Swisse Ultiboost Children’s Fish Oil supports brain and eye health with Omega 3 fatty acids EPA and DHA, and is a convenient way to increase your child''s dietary intake of Omega-3.', N'PGT896-SG_CHILDRENS-SMART-FISH-OIL-90CAPS_LABEL_2014.v7MU.png', N'29.00', N'', NULL, N'90', N'', N'4987176011527', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-childrens-smart-fish-oil', N'Swisse Ultiboost Children’s Smart Fish Oil is a premium quality formula containing omega-3 fatty acids to help support healthy brain and eye development.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (26, 5, N'Swisse Ultiboost Hair Skin Nails', N'Swisse Ultiboost Hair Skin Nails is a premium beauty formula containing a special combination of vitamins and minerals that work from within to support hair, skin and nail health.', N'PGT896-SG_HAIR-SKIN-NAILS-60TABS_LABEL_2014.v8.png', N'49.00', N'', NULL, N'60', N'', N'4987176010988', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-hair-skin-nails', N'Swisse Ultiboost Hair Skin Nails is a beauty formula containing a combination of ingredients that work from within to support hair, skin and nail health.', NULL, 1, 0, N'')
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (27, 11, N'Swisse Ultiboost Hair Skin Nails', N'Swisse Ultiboost Hair Skin Nails is a premium beauty formula containing a special combination of vitamins and minerals that work from within to support hair, skin and nail health.', N'PGT896-SG_HAIR-SKIN-NAILS-60TABS_LABEL_2014.v8.png', N'49.00', N'', NULL, N'60', N'', N'4987176010988', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-hair-skin-nails', N'Swisse Ultiboost Hair Skin Nails is a beauty formula containing a combination of ingredients that work from within to support hair, skin and nail health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (28, 6, N'Swisse Ultivite + Energy Effervescent', N'Swisse Ultivite + Energy effervescent is a unique great tasting effervescent formula containing vitamins, minerals and a key antioxidant to support energy production, stamina, mental performance and provide support during stress.', N'PGT1193-SG-Ultivite--Energy-20TAB_TUBE_-V1-2014-MOCKUPV3.png', N'53.00', N'', NULL, N'60', N'', N'4987176011503', N'', N'Effervescent Tablets', N'www.swisse.com.sg/products/swisse-ultivite-energy-eff', N'Swisse Ultivite + Energy effervescent is a comprehensive multivitamin formulated to support energy production and provide support during stress.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (29, 1, N'Swisse Ultivite + Energy Effervescent', N'Swisse Ultivite + Energy effervescent is a unique great tasting effervescent formula containing vitamins, minerals and a key antioxidant to support energy production, stamina, mental performance and provide support during stress.', N'PGT1193-SG-Ultivite--Energy-20TAB_TUBE_-V1-2014-MOCKUPV3.png', N'53.00', N'', NULL, N'60', N'', N'4987176011503', N'', N'Effervescent Tablets', N'www.swisse.com.sg/products/swisse-ultivite-energy-eff', N'Swisse Ultivite + Energy effervescent is a comprehensive multivitamin formulated to support energy production and provide support during stress.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (30, 7, N'Swisse Ulitboost Glucosamine + Manganese + Chondroitin', N'Swisse Ultiboost Glucosamine + Manganese + Chondriotin is a premium multi-nutrient formula to support cartilage function, healthy connective tissue and joint mobility.', N'PGT896-SG_GLUCOSAMINECHONDROITIN-90TABS_LABEL_2014.v4MU.png', N'79.00', N'', NULL, N'90', N'', N'4987176011657', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-glucosamine-manganese-chondroitin', N'Swisse Ultiboost Glucosamine + Manganese + Chondriotin is a premium formula to support cartilage function, healthy connective tissue and joint mobility.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (31, 7, N'Swisse Ultiboost Glucosamine+', N'Swisse Ultiboost Glucosamine+ is a premium formula with glucosamine sulphate and ginger to support joint flexibility and help maintain cartilage health.', N'PGT896-SG_GLUCOSAMINE-90TABS_LABEL_2014.v4MU.png', N'79.00', N'', NULL, N'90', N'', N'4987176011572', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-glucosamine', N'Swisse Ultiboost Glucosamine+ is a premium formula with glucosamine sulphate and ginger to support joint flexibility and help maintain cartilage health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (32, 7, N'Swisse Ultiboost Joints', N'Swisse Ultiboost Joints’ special formula contains ingredients to help to support joint mobility, help maintain cartilage health and provide antioxidant support.', N'PGT896-SG_JOINTS-90CAPS_LABEL_2014.v6MU.png', N'79.00', N'', NULL, N'90', N'', N'4987176011619', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-joints', N'Swisse Ultiboost Joints’ supplement formula helps to support joint mobility, help maintain cartilage health and provide antioxidant support.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (33, 7, N'Swisse Ultiboost Vitamin D', N'Swisse Ultiboost Vitamin D is a high strength premium formula vitamin D3 to help support bone health and assist calcium absorption in a convenient dose.', N'PGT896-SG_VITAMIN-D-60CAPS_LABEL_2014.v5MU2.png', N'20.00', N'75.00', NULL, N'60', N'250', N'4987176011268', N'4987176011428', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-vitamin-d', N'Swisse Ultiboost Vitamin D is a high strength premium formula vitamin D3 to help support bone health and assist calcium absorption in a convenient dose.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (34, 7, N'Swisse Ultiboost Magnesium', N'Swisse Ultiboost Magnesium is formulated with a bioavailable form of magnesium citrate to help support healthy muscle function, bone health and support a healthy nervous system.', N'PGT896-SG_MAGNESIUM-60TABS_LABEL_2014.v10MU.png', N'26.00', N'', NULL, N'60', N'', N'4987176011558', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-magnesium', N'Swisse Ultiboost Magnesium is formulated to help support healthy muscle function, bone health and support a healthy nervous system.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (35, 7, N'Swisse Ultiboost Magnesium Effervescent', N'Swisse Ultiboost Magnesium Effervescent is a great tasting lemon flavored effervescent formula to help reduce muscle cramps and spasms and support a healthy nervous system.', N'PGT1193-SG-Ultiboost-Magnesium-20TAB_TUBE_-V1-2014-MOCKUP_V3.png', N'39.00', N'', NULL, N'60', N'', N'4987176011466', N'', N'Effervescent Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-magnesium-eff', N'Swisse Ultiboost Magnesium Effervescent is a lemon flavored formula to help reduce muscle cramps and spasms and support a healthy nervous system.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (36, 7, N'Swisse Ultiboost Calcium + Vitamin D', N'Swisse Ultiboost Calcium + Vitamin D is a multi-nutrient formula to help support healthy bones and teeth and provide a source of calcium.', N'PGT896-SG_CALCIUM--VITAMIN-D-90TABS_LABEL_2014.v8MU.png', N'36.00', N'45.00', NULL, N'90', N'150', N'4987176011633', N'4987176010889', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-calcium-vitamin-d', N'Swisse Ultiboost Calcium + Vitamin D is a multi-nutrient formula to help support healthy bones and teeth and provide a source of calcium.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (37, 8, N'Swisse Ultiboost Liver Detox', N'Swisse Ultiboost Liver Detox has been formulated based on traditional evidence to provide herbs beneficial to liver health in a convenient dose.', N'PGT896-SG_LIVER-DETOX-60TABS_LABEL_2014.v5MU.png', N'52.00', N'', NULL, N'60', N'', N'4987176010964', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-liver-detox', N'Swisse Ultiboost Liver Detox has been formulated based on traditional evidence to provide herbs beneficial to liver health in a convenient dose.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (38, 9, N'Swisse Ultiboost Odourless Wild Fish Oil', N'Swisse Ultiboost Odourless Wild Fish Oil contains 50% more omega-3 fatty acids EPA and DHA than a standard 1000mg fish oil capsule to support joint, heart, brain, eye and nervous system health.', N'PGT896-SG_ODOURLESS-HIGH-STRENGTH-WILD-FISH-OIL-200TABS_LABEL_2014.v6MU[1].png', N'55.00', N'89.00', NULL, N'200', N'400', N'4987176011855', N'4987176011442', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-odourless-wild-fish-oil', N'Swisse Ultiboost Odourless Wild Fish Oil contains 50% more omega-3 fatty acids than a standard capsule to support joint, heart, brain and eye health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (39, 9, N'Swisse Super Quad Strength Fish Oil', N'Swisse Ultiboost Quad Strength Fish Oil is a premium, high potency fish oil concentrate that is a source of omega-3 fatty acids to support joint, heart, brain and eye health as well as supporting a healthy nervous system and mood balance.  No other product in Singapore contains higher levels of Omega-3.', N'PGT896-SG_4-x-STRENGTH-WILD-FISH-OIL-CONCENTRATE-60CAPS_LABEL_2014.v7MU[1].png', N'55.00', N'', NULL, N'60', N'', N'4987176011893', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-4x-strength-fish-oil', N'Swisse Ultiboost Quad Strength Fish Oil is a high potency fish oil that supports joint, heart, brain and eye health, a healthy nervous system and mood balance.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (40, 9, N'Swisse Ultiboost High Strength (1000mg) Krill Oil', N'Swisse Ultiboost Triple Strength Wild Krill Oil is a premium quality formula containing Neptune Krill Oil™ (NKO®), a source of omega-3 and naturally occurring antioxidant, astaxanthin.', N'PGT896-SG_TRIPLE-STRENGTH-WILD-KRILL-OIL-30CAPS_LABEL_2014.v9MU[1].png', N'83', N'', NULL, N'30', N'', N'4987176011831', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-high-strength-krill-oil', N'Swisse Ultiboost Triple Strength Wild Krill Oil is a premium formula containing Neptune Krill Oil™, a source of omega-3 and naturally occurring antioxidant.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (41, 7, N'Swisse Ultiboost High Strength (1000mg) Krill Oil', N'Swisse Ultiboost Triple Strength Wild Krill Oil is a premium quality formula containing Neptune Krill Oil™ (NKO®), a source of omega-3 and naturally occurring antioxidant, astaxanthin.', N'PGT896-SG_TRIPLE-STRENGTH-WILD-KRILL-OIL-30CAPS_LABEL_2014.v9MU[1].png', N'83', N'', NULL, N'30', N'', N'4987176011831', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-high-strength-krill-oil', N'Swisse Ultiboost Triple Strength Wild Krill Oil is a premium formula containing Neptune Krill Oil™, a source of omega-3 and naturally occurring antioxidant.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (42, 10, N'Swisse Ultiboost CoQ10', N'Swisse Ultiboost Co-enzyme Q10 is a premium one-a-day formula to support healthy antioxidant activity, cellular energy production and to help maintain cardiovascular health.', N'PGT896-SG_CO-ENZYME-Q10-150mg-30CAPS_LABEL_2014.v8MU.png', N'73.00', N'', NULL, N'30', N'', N'4987176011282', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-coq10', N'Swisse Ultiboost Co-enzyme Q10 is a premium quality one-a-day formula supporting healthy antioxidant activity and maintaining cardiovascular health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (43, 10, N'Swisse Ultiboost Cholesterol Protect', N'Swisse Ultiboost Cholesterol Protect is a premium formula to support heart health, assist cellular energy production and help maintain normal cholesterol levels in healthy people.', N'PGT896-SG_CHOLESTEROL-PROTECT-30CAPS_LABEL_2014.v11MU.png', N'49.00', N'', NULL, N'30', N'', N'4987176010926', N'', N'Capsules', N'www.swisse.com.sg/products/swisse-ultiboost-cholesterol-protect', N'Swisse Ultiboost Cholesterol Protect is a premium formula to support heart health and help maintain normal cholesterol levels in healthy people.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (44, 11, N'Swisse Ultiboost Iron', N'Swisse Ultiboost Iron is a comprehensive formula to help maintain healthy blood and assist in the management of dietary iron insufficiency that can lead to tiredness and fatigue.', N'PGT896-SG_IRON-30TABS_LABEL_2014.v6MU.png', N'15.00', N'', NULL, N'30', N'', N'4987176011305', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-iron', N'Swisse Ultiboost Iron is a comprehensive formula to help maintain healthy blood and assist with dietary iron insufficiency that can lead to tiredness and fatigue.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (45, 11, N'Swisse Ultiboost Menopause Balance', N'Swisse Ultiboost Menopause Balance is a comprehensive formula with nutrients and herbs beneficial for menopausal symptoms and support bone health.', N'PGT896-SG_MENOPAUSE-BALANCE-60TABS_LABEL_2014.v6MU.png', N'45.00', N'', NULL, N'60', N'', N'4987176011060', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-menopause-balance', N'Swisse Ultiboost Menopause Balance is a comprehensive formula with nutrients and herbs beneficial for menopausal symptoms and support bone health.', NULL, 1, 0, NULL)
INSERT [dbo].[tbl_Product] ([productID], [prodcatID], [productName], [productDesc], [productImage], [productPrice], [productPrice2], [productManufacturer], [productSize], [productSize2], [productGTIN], [productGTIN2], [productForm], [productCanonicalURL], [productMetaDescription], [productCount], [productStatus], [deleted], [productShortDesc]) VALUES (46, 12, N'Swisse Ultiboost Prostate', N'Swisse Ultiboost Prostate is a comprehensive formula containing vitamins, minerals and herbs to support urinary function, reproductive and prostate health.', N'PGT896-SG_PROSTATE-50TABS_LABEL_2014.v4MU.png', N'45.00', N'', NULL, N'50', N'', N'4987176010940', N'', N'Tablets', N'www.swisse.com.sg/products/swisse-ultiboost-prostate', N'Swisse Ultiboost Prostate is a comprehensive formula containing vitamins, minerals and herbs to support urinary function, reproductive and prostate health.', NULL, 1, 0, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Product] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product_Age] ON 

INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (1, 26, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (2, 26, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (3, 26, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (4, 26, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (5, 25, 1)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (6, 23, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (7, 23, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (8, 23, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (9, 23, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (10, 24, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (11, 24, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (12, 24, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (13, 24, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (14, 43, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (15, 43, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (16, 43, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (17, 43, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (18, 42, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (19, 42, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (20, 42, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (21, 42, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (22, 37, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (23, 37, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (24, 37, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (25, 37, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (26, 22, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (27, 22, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (28, 22, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (29, 22, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (30, 28, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (31, 28, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (32, 28, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (33, 28, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (34, 19, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (35, 19, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (36, 19, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (37, 19, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (38, 20, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (39, 20, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (40, 20, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (41, 20, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (42, 21, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (43, 21, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (44, 21, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (45, 21, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (46, 18, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (47, 18, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (48, 18, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (49, 18, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (50, 30, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (51, 30, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (52, 30, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (53, 30, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (54, 36, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (55, 36, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (56, 36, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (57, 36, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (58, 31, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (59, 31, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (60, 31, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (61, 31, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (62, 41, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (63, 41, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (64, 41, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (65, 41, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (66, 32, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (67, 32, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (68, 32, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (69, 32, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (70, 34, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (71, 34, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (72, 34, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (73, 34, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (74, 35, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (75, 35, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (76, 35, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (77, 35, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (78, 33, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (79, 33, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (80, 33, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (81, 33, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (82, 11, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (83, 13, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (84, 2, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (85, 2, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (86, 2, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (87, 4, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (88, 4, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (89, 4, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (90, 46, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (91, 46, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (92, 9, 1)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (93, 10, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (94, 12, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (95, 1, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (96, 1, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (97, 1, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (98, 3, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (99, 3, 4)
GO
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (100, 3, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (101, 29, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (102, 29, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (103, 29, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (104, 29, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (105, 14, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (106, 16, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (107, 5, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (108, 5, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (109, 5, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (110, 7, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (111, 7, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (112, 7, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (113, 39, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (114, 39, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (115, 39, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (116, 39, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (117, 40, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (118, 40, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (119, 40, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (120, 40, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (121, 38, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (122, 38, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (123, 38, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (124, 38, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (125, 27, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (126, 27, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (127, 27, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (128, 27, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (129, 44, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (130, 44, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (131, 44, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (132, 44, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (133, 45, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (134, 45, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (135, 15, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (136, 17, 6)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (137, 6, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (138, 6, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (139, 6, 5)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (140, 8, 3)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (141, 8, 4)
INSERT [dbo].[tbl_Product_Age] ([id], [productID], [ageID]) VALUES (142, 8, 5)
SET IDENTITY_INSERT [dbo].[tbl_Product_Age] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product_Gender] ON 

INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (1, 26, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (2, 25, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (3, 25, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (4, 23, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (5, 23, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (6, 24, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (7, 24, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (8, 43, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (9, 43, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (10, 42, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (11, 42, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (12, 37, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (13, 37, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (14, 22, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (15, 22, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (16, 28, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (17, 28, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (18, 19, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (19, 19, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (20, 20, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (21, 20, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (22, 21, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (23, 21, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (24, 18, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (25, 18, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (26, 30, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (27, 30, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (28, 36, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (29, 36, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (30, 31, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (31, 31, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (32, 41, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (33, 41, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (34, 32, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (35, 32, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (36, 34, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (37, 34, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (38, 35, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (39, 35, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (40, 33, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (41, 33, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (42, 11, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (43, 13, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (44, 2, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (45, 4, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (46, 46, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (47, 9, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (48, 9, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (49, 10, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (50, 12, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (51, 1, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (52, 3, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (53, 29, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (54, 29, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (55, 14, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (56, 16, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (57, 5, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (58, 7, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (59, 39, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (60, 39, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (61, 40, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (62, 40, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (63, 38, 1)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (64, 38, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (65, 27, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (66, 44, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (67, 45, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (68, 15, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (69, 17, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (70, 6, 2)
INSERT [dbo].[tbl_Product_Gender] ([id], [productID], [genderID]) VALUES (71, 8, 2)
SET IDENTITY_INSERT [dbo].[tbl_Product_Gender] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product_ProductConcern] ON 

INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (1, 26, 5)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (2, 23, 4)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (3, 23, 13)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (4, 24, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (5, 43, 9)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (6, 43, 6)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (7, 42, 9)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (8, 42, 6)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (9, 37, 10)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (10, 22, 12)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (11, 22, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (12, 28, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (13, 28, 4)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (14, 19, 12)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (15, 20, 12)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (16, 21, 12)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (17, 21, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (18, 18, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (19, 30, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (20, 36, 8)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (21, 31, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (22, 41, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (23, 41, 6)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (24, 41, 9)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (25, 32, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (26, 34, 4)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (27, 34, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (28, 35, 4)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (29, 35, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (30, 33, 8)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (31, 11, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (32, 13, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (33, 2, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (34, 4, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (35, 9, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (36, 10, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (37, 12, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (38, 1, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (39, 3, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (40, 29, 4)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (41, 29, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (42, 14, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (43, 16, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (44, 5, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (45, 7, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (46, 39, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (47, 39, 6)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (48, 39, 9)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (49, 40, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (50, 40, 6)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (51, 40, 9)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (52, 38, 6)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (53, 38, 2)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (54, 38, 9)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (55, 27, 5)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (56, 44, 11)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (57, 44, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (58, 45, 8)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (59, 15, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (60, 17, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (61, 6, 3)
INSERT [dbo].[tbl_Product_ProductConcern] ([id], [productID], [prodconcernID]) VALUES (62, 8, 3)
SET IDENTITY_INSERT [dbo].[tbl_Product_ProductConcern] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product_ProductIngredient] ON 

INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (1, 26, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (2, 26, 9)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (3, 25, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (4, 23, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (5, 23, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (6, 23, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (7, 24, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (8, 43, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (9, 43, 15)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (10, 37, 12)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (11, 22, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (12, 22, 9)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (13, 22, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (14, 28, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (15, 28, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (16, 28, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (17, 28, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (18, 28, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (19, 19, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (20, 20, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (21, 21, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (22, 21, 9)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (23, 21, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (24, 18, 9)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (25, 18, 13)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (26, 18, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (27, 30, 8)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (28, 36, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (29, 36, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (30, 31, 8)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (31, 41, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (32, 41, 15)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (33, 32, 8)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (34, 32, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (35, 32, 15)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (36, 34, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (37, 35, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (38, 33, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (39, 11, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (40, 11, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (41, 11, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (42, 11, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (43, 11, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (44, 11, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (45, 13, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (46, 13, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (47, 13, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (48, 13, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (49, 13, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (50, 13, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (51, 2, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (52, 2, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (53, 4, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (54, 4, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (55, 46, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (56, 46, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (57, 46, 9)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (58, 9, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (59, 9, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (60, 9, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (61, 9, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (62, 9, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (63, 10, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (64, 10, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (65, 10, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (66, 10, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (67, 10, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (68, 10, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (69, 12, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (70, 12, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (71, 12, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (72, 12, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (73, 12, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (74, 12, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (75, 1, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (76, 1, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (77, 3, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (78, 3, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (79, 29, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (80, 29, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (81, 29, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (82, 29, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (83, 29, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (84, 14, 13)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (85, 14, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (86, 14, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (87, 14, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (88, 14, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (89, 14, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (90, 14, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (91, 16, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (92, 16, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (93, 16, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (94, 16, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (95, 16, 1)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (96, 16, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (97, 5, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (98, 5, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (99, 7, 4)
GO
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (100, 7, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (101, 39, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (102, 40, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (103, 40, 15)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (104, 38, 2)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (105, 27, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (106, 27, 9)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (107, 44, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (108, 44, 13)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (109, 44, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (110, 44, 11)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (111, 45, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (112, 45, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (113, 15, 13)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (114, 15, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (115, 15, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (116, 15, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (117, 15, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (118, 15, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (119, 15, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (120, 17, 13)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (121, 17, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (122, 17, 5)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (123, 17, 6)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (124, 17, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (125, 17, 14)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (126, 17, 10)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (127, 6, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (128, 6, 16)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (129, 8, 4)
INSERT [dbo].[tbl_Product_ProductIngredient] ([id], [productID], [productingredientID]) VALUES (130, 8, 16)
SET IDENTITY_INSERT [dbo].[tbl_Product_ProductIngredient] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product_RelatedProducts] ON 

INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (1, 26, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (2, 26, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (3, 26, 24)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (4, 26, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (5, 25, 9)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (6, 25, 38)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (7, 25, 39)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (8, 25, 41)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (9, 23, 24)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (10, 23, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (11, 23, 28)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (12, 23, 37)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (13, 24, 28)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (14, 24, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (15, 24, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (16, 24, 37)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (17, 43, 42)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (18, 43, 38)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (19, 43, 39)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (20, 43, 41)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (21, 42, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (22, 42, 38)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (23, 42, 39)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (24, 42, 41)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (25, 37, 24)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (26, 37, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (27, 37, 28)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (28, 37, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (29, 22, 20)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (30, 22, 19)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (31, 22, 18)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (32, 22, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (33, 28, 24)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (34, 28, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (35, 28, 1)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (37, 19, 21)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (38, 19, 20)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (39, 19, 18)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (40, 19, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (41, 20, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (42, 20, 19)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (43, 20, 18)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (44, 20, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (45, 21, 20)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (46, 21, 19)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (47, 21, 18)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (48, 21, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (49, 18, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (50, 18, 20)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (51, 18, 19)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (52, 18, 23)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (53, 30, 31)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (54, 30, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (55, 30, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (56, 30, 34)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (57, 36, 34)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (58, 36, 35)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (59, 36, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (60, 36, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (61, 31, 30)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (62, 31, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (63, 31, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (64, 31, 34)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (65, 41, 38)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (66, 41, 39)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (67, 41, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (68, 41, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (69, 32, 31)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (70, 32, 30)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (71, 32, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (72, 32, 34)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (73, 34, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (74, 34, 35)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (75, 34, 36)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (76, 34, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (77, 35, 34)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (78, 35, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (79, 35, 36)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (80, 35, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (81, 33, 34)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (82, 33, 35)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (83, 33, 36)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (84, 33, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (85, 11, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (86, 11, 46)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (87, 11, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (88, 11, 12)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (89, 13, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (90, 13, 46)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (91, 13, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (92, 13, 10)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (93, 2, 3)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (94, 2, 10)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (95, 2, 21)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (96, 2, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (97, 4, 1)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (98, 4, 17)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (99, 4, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (100, 4, 32)
GO
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (101, 46, 10)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (102, 46, 13)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (103, 46, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (104, 46, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (105, 9, 25)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (106, 9, 1)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (107, 9, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (108, 9, 19)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (109, 10, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (110, 10, 46)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (111, 10, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (112, 10, 12)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (113, 12, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (114, 12, 46)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (115, 12, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (116, 12, 10)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (117, 1, 3)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (118, 1, 10)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (119, 1, 21)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (120, 1, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (121, 3, 1)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (122, 3, 12)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (123, 3, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (124, 3, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (125, 29, 24)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (126, 29, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (127, 29, 1)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (128, 29, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (129, 14, 45)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (130, 14, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (131, 14, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (132, 14, 17)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (133, 16, 45)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (134, 16, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (135, 16, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (136, 16, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (137, 5, 8)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (138, 5, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (139, 5, 26)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (140, 5, 22)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (141, 7, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (142, 7, 17)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (143, 7, 26)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (144, 7, 21)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (145, 39, 38)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (146, 39, 41)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (147, 39, 25)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (148, 39, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (149, 40, 38)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (150, 40, 39)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (151, 40, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (152, 40, 43)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (153, 38, 39)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (154, 38, 40)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (155, 38, 25)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (156, 38, 32)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (157, 27, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (158, 27, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (159, 27, 24)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (160, 27, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (161, 44, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (162, 44, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (163, 44, 26)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (164, 44, 45)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (165, 45, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (166, 45, 17)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (167, 45, 26)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (168, 45, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (169, 15, 45)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (170, 15, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (171, 15, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (172, 15, 17)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (173, 17, 45)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (174, 17, 44)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (175, 17, 33)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (176, 17, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (177, 6, 8)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (178, 6, 15)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (179, 6, 26)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (180, 6, 21)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (181, 8, 6)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (182, 8, 17)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (183, 8, 26)
INSERT [dbo].[tbl_Product_RelatedProducts] ([id], [productID], [relatedproductID]) VALUES (184, 8, 21)
SET IDENTITY_INSERT [dbo].[tbl_Product_RelatedProducts] OFF
SET IDENTITY_INSERT [dbo].[tbl_ProductCategory] ON 

INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (1, N'Multivitamins', N'Multivitamins', N'multivitamins.jpg', 1, 0, 1)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (2, N'Immunity', N'Immunity', N'immunity-nav.jpg', 1, 0, 2)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (4, N'Brain & Vision', N'Brain & Vision', N'bv.jpg', 1, 0, 3)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (5, N'Beauty & Skin', N'Beauty & Skin', N'', 1, 0, 4)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (6, N'Energy', N'Energy', N'', 1, 0, 5)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (7, N'Joints, Muscles & Bones', N'Joints, Muscles & Bones', N'gmb.jpg', 1, 0, 6)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (8, N'Digestion & Liver', N'Digestion & Liver', N'', 1, 0, 7)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (9, N'Omegas', N'Omegas', N'omega.jpg', 1, 0, 8)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (10, N'Cardiovascular', N'Cardiovascular', N'', 1, 0, 9)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (11, N'Women''s Health', N'Women''s Health', N'', 1, 0, 10)
INSERT [dbo].[tbl_ProductCategory] ([prodcatID], [prodcatName], [prodcatDesc], [prodcatImage], [prodcatStatus], [deleted], [rank]) VALUES (12, N'Men''s Health', N'Men''s Health', N'', 1, 0, 11)
SET IDENTITY_INSERT [dbo].[tbl_ProductCategory] OFF
SET IDENTITY_INSERT [dbo].[tbl_ProductConcern] ON 

INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (1, N'Diet & Weight Management', N'Diet & Weight Management', 1, 0, 1)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (2, N'Joint Pain & Arthritis', N'Joint Pain & Arthritis', 1, 0, 2)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (3, N'Tiredness & Fatigue', N'Tiredness & Fatigue', 1, 0, 3)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (4, N'Anxiety & Stress', N'Anxiety & Stress', 1, 0, 4)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (5, N'Hair', N'Hair', 1, 0, 5)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (6, N'Cholesterol', N'Cholesterol', 1, 0, 6)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (7, N'Sleep', N'Sleep', 1, 0, 7)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (8, N'Menopause', N'Menopause', 1, 0, 8)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (9, N'Heart Health', N'Heart Health', 1, 0, 9)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (10, N'Detox', N'Detox', 1, 0, 10)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (11, N'Iron Deficiency', N'Iron Deficiency', 1, 0, 11)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (12, N'Cold Symptoms', N'Cold Symptoms', 1, 0, 12)
INSERT [dbo].[tbl_ProductConcern] ([prodconcernID], [prodconcernName], [prodconcernDesc], [prodconcernStatus], [deleted], [rank]) VALUES (13, N'Mood', N'Mood', 1, 0, 13)
SET IDENTITY_INSERT [dbo].[tbl_ProductConcern] OFF
SET IDENTITY_INSERT [dbo].[tbl_ProductDetail] ON 

INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (1, 1, N'Swisse Men’s Ultivite is the only multivitamin formulated based on scientific evidence to include over 50 vitamins, minerals, antioxidants and herbal extracts to provide nutritional support for an active and stressful lifestyle.  <br />
<br />
Designed to support men’s nutritional needs and maintain general wellbeing, Swisse Men’s Ultivite assists with energy, stamina, mental performance and helps maintain vitality.<br />
<br />
Its active ingredients include – but are not limited to:<br />
<br />
<ul>
  <li>B complex vitamins to assist with energy production, stamina and vitality </li>
  <li>Vitamin B5 for mental performance and provide support during stress </li>
  <li>Vitamin E a powerful antioxidant </li>
  <li>Vitamin C Helps support immune function </li>
  <li>Zinc to help maintain healthy hair, skin and nails</li>
</ul>

<span style="color: #000000; font-size: 13.3333330154419px;">.</span>', N'<p class="MsoNormal"><span style="font-weight: bold;">EXTRACTS EQUIVALENT TO DRY:&nbsp;</span></p>
<p class="MsoNormal">Celery (Apium graveolens) seed 20 mg</p>
<p class="MsoNormal">Astragalus (Astragalus membranaceus) root 50 mg</p>
<p class="MsoNormal">Buchu (Agathosma betulina) leaf 10 mg</p>
<p class="MsoNormal">Gotu kola (Centella asiatica) herb 50 mg</p>
<p class="MsoNormal">Hawthorn (Crataegus monogyna) fruit 100 mg</p>
<p class="MsoNormal">Horsetail (Equisetum arvense) herb 30 mg</p>
<p class="MsoNormal">Fennel (Foeniculum vulgare) fruit 15 mg</p>
<p class="MsoNormal">Sarsaparilla (Smilax officinalis) root 50 mg</p>
<p class="MsoNormal">Damiana (Turnera diffusa) leaf 120 mg</p>
<p class="MsoNormal">Parsley (Petroselinum crispum) herb 10 mg</p>
<p class="MsoNormal">Ginger (Zingiber officinale) rhizome 5 mg&nbsp;</p>
<p class="MsoNormal"><br />
</p>
<p class="MsoNormal"><span style="font-weight: bold;">EXTRACTS EQUIVALENT TO FRESH:&nbsp;</span></p>
<p class="MsoNormal">Globe artichoke (Cynara scolymus) leaf 50 mg</p>
<p class="MsoNormal">Oats (Avena sativa) herb 500 mg&nbsp;</p>
<p class="MsoNormal"><br />
</p>
<p class="MsoNormal"><span style="font-weight: bold;">STANDARDISED EXTRACTS EQUIVALENT TO:</span></p>
<p class="MsoNormal">Bilberry (Vaccinium myrtillus) fresh fruit 25 mg (equiv. anthocyanosides 81 mcg)</p>
<p class="MsoNormal">Grape seed (Vitis Vinifera) dry seed 1 g (equiv. procyanidins 7.9 mg)</p>
<p class="MsoNormal">St. Mary’s thistle (Silybum marianum) dry fruit 50 mg (equiv. flavanolignans calculated as silybin 572 mcg)</p>
<p class="MsoNormal">Korean ginseng (Panax ginseng) dry root 50 mg (equiv. ginsenosides calculated as ginsenosides Rg1 1 mg)</p>
<p class="MsoNormal">Ginkgo (Ginkgo biloba) dry leaf 100 mg (equiv. ginkgolides &amp; bilobalide 120 mcg, ginkgo flavonglycosides 480 mcg)</p>
<p class="MsoNormal">Saw palmetto (Serenoa repens) seed dry 200 mg (equiv. fatty acids 18 mg)</p>
<p class="MsoNormal">Green tea (Camellia sinensis) dry leaf 20 mg (equiv. epigallocatechin-3-0-gallate 1 mg)</p>
<p class="MsoNormal">Tomato (Lycopersicon esculentum) fresh fruit 700 mg (equiv. lycopene 120 mcg)</p>
<p class="MsoNormal"><br />
</p>
<p class="MsoNormal">Also contains: Cellulose Microcrystalline, Crospovidone, Povidone, Silica Colloidal Anhydrous, Magnesium Stearate, Hypromellose, Macrogol 400, Titanium Dioxide, Chlorophyllin Copper Complex, Peppermint Oil</p>', N'One tablet daily, during or immediately after a meal, or as directed by a healthcare professional. <br />
<br />
Store below 25°C. Do not use if cap seal is broken. Keep out of the reach of children. <br />
<br />
<span style="color: #ff0000;">For all dietary supplements, consult your healthcare professional to determine if this supplement is right for you or if symptoms persist.  Vitamin supplements should not replace a balanced diet.   Accidental overdose of iron-containing products can be fatal in children under six years old.  <span style="font-weight: bold;">Contains Lactose, Phenylalanine, Gluten, Soy, Sulfites, and small amounts of Caffeine.</span></span>')
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (2, 2, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (3, 3, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (4, 4, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (5, 5, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (6, 6, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (7, 7, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (8, 8, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (9, 9, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (10, 10, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (11, 11, N'test', N'test', N'test')
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (12, 12, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (13, 13, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (14, 14, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (15, 15, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (16, 16, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (17, 17, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (18, 18, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (19, 19, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (20, 20, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (21, 21, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (22, 22, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (23, 23, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (24, 24, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (25, 25, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (26, 26, N'test', N'test', N'test')
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (27, 27, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (28, 28, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (29, 29, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (30, 30, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (31, 31, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (32, 32, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (33, 33, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (34, 34, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (35, 35, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (36, 36, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (37, 37, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (38, 38, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (39, 39, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (40, 40, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (41, 41, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (42, 42, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (43, 43, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (44, 44, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (45, 45, NULL, NULL, NULL)
INSERT [dbo].[tbl_ProductDetail] ([id], [productid], [productBenefit], [productIngredient], [productDirection]) VALUES (46, 46, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_ProductDetail] OFF
SET IDENTITY_INSERT [dbo].[tbl_ProductIngredient] ON 

INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (1, N'Magnesium', N'Magnesium', 1, 0, 1)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (2, N'Omega 3', N'Omega 3', 1, 0, 2)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (4, N'Vitamin B12', N'Vitamin B12', 1, 0, 3)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (5, N'Vitamin C', N'Vitamin C', 1, 0, 4)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (6, N'Vitamin D', N'Vitamin D', 1, 0, 5)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (7, N'Ginseng', N'Ginseng', 1, 0, 6)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (8, N'Glucosamine', N'Glucosamine', 1, 0, 7)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (9, N'Zinc', N'Zinc', 1, 0, 8)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (10, N'Folic Acid', N'Folic Acid', 1, 0, 9)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (11, N'Iron', N'Iron', 1, 0, 10)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (12, N'St Mary''s Thistle', N'St Mary''s Thistle', 1, 0, 11)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (13, N'Vitamin A', N'Vitamin A', 1, 0, 12)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (14, N'Calcium', N'Calcium', 1, 0, 13)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (15, N'Krill Oil', N'Krill Oil', 1, 0, 14)
INSERT [dbo].[tbl_ProductIngredient] ([prodingredientID], [prodingredientName], [prodingredientDesc], [prodingredientStatus], [deleted], [rank]) VALUES (16, N'Vitamin B6', N'Vitamin B6', 1, 0, 15)
SET IDENTITY_INSERT [dbo].[tbl_ProductIngredient] OFF
SET IDENTITY_INSERT [dbo].[tbl_ShopProducts] ON 

INSERT [dbo].[tbl_ShopProducts] ([id], [title], [link]) VALUES (1, N'Product Categories', N'ProductCategories.aspx')
INSERT [dbo].[tbl_ShopProducts] ([id], [title], [link]) VALUES (2, N'Product Concerns', N'ProductConcerns.aspx')
INSERT [dbo].[tbl_ShopProducts] ([id], [title], [link]) VALUES (3, N'Product Ingredients', N'ProductIngredients.aspx')
INSERT [dbo].[tbl_ShopProducts] ([id], [title], [link]) VALUES (4, N'Products', N'Products.aspx')
SET IDENTITY_INSERT [dbo].[tbl_ShopProducts] OFF
SET IDENTITY_INSERT [dbo].[tbl_SwisseAmbassadors] ON 

INSERT [dbo].[tbl_SwisseAmbassadors] ([id], [title], [ambassadorContent], [ambassadorImage], [ambassadorLink], [deleted]) VALUES (1, N'Willin Low', N'<img src="images/Will.jpg" />
<br />
<br />
 <span style="font-weight: bold;">ABOUT</span><br />
The father of Mod Sin (modern Singaporean) cuisine, Willin is one of Singapore’s most prolific chefs. He has been named as one of the world''s top 100 emerging culinary stars by fellow contemporary chefs in &quot;Coco: 10 World-Leading Masters Choose 100 Contemporary Chefs“. <br />
<br />
 In 2009, The New York Times featured Chef Willin as one of three chefs in Singapore who were &quot;reinventing the city’s traditional food culture“. He was also featured in The Huffington Post in 2010. Willin has appeared on The Martha Stewart Show, Top Chef and Mediacorp Channel 8’s food variety shows and starred in his very own show A Party Affair on Asian Food Channel. The lawyer turned restaurateur/chef founded Wild Rocket Group which comprises of Mod Sin restaurant Wild Rocket and Relish.<br />
<br />
 Willin is also deeply passionate about philanthropy. Among the charities he has helped over the years are the Children’s Cancer Foundation, Club Rainbow, Dover Park Hospice and the Autism Resource Centre. His current adopted charity is the Morning Star Foundation, which cares for orphaned and abandoned babies in China and Uganda who have severe hart disease and raises money for their medical and surgical treatment. Besides donating cash, the Wild Rocket Group also carries out acts of goodwill such as cooking meals during festivals for old folk and youths at risk, and holding etiquette and tasting sessions with youths from underprivileged families.<br />
<br />
<br />
  <span style="font-weight: bold;">Q&amp;A</span><br />
 <span style="font-weight: bold;">1. How would you describe healthy living?</span><br />
I am of the belief that it is all about the right balance. Healthy living to me is where one is mentally, physically and spiritually healthy. Once you have all three, you will be happy.<br />
<br />
 <span style="font-weight: bold;">2. What makes you tired or fatigued?</span><br />
One of my greatest pet peeves is the sort of folks who tend to complain non-stop yet without providing you with a solution or seem to be interested in finding some form of resolution. In both my personal life and professional career as a chef, it is inevitable that I will meet and deal with such sorts of characters and it personally tires me out. In my humble view, you are either part of the problem or part of the solution.<br />
<br />
 <span style="font-weight: bold;">3. What are the important aspects of your family life?</span><br />
My mother is the constant beacon of love in our family. From an early age, she has instilled in us a strong sense of compassion. She is the center that binds everything together, she is my rock.<br />
<br />
 <span style="font-weight: bold;">4. What’s one thing that people don’t know which helps give you an edge?</span><br />
That I can be a little crazy sometimes! On a serious note, I feel being unexpected and spontaneous at times (aka just be a little crazy once in a while), can make it hard for others to anticipate your next move, which could give you an edge.<br />
<br />
 <span style="font-weight: bold;">5. What’s your secret to performing under pressure?</span><br />
I keep reminding myself to focus on the big picture and not to lose sight of that.<br />
<br />
 <span style="font-weight: bold;">6. What kind of person do you aspire to be?</span><br />
I aspire to be someone who can make a positive difference in the lives of others.<br />
<br />
 <span style="font-weight: bold;">7. If you could be anyone or do anything else, what would you do?</span><br />
I would love to be Spiderman (obviously a huge super hero fan here!) because with great power comes great responsibility. I would want to save this world from the evil-doers out there.<br />
<br />', N'Will.jpg', N'WillinLow.aspx', 0)
INSERT [dbo].[tbl_SwisseAmbassadors] ([id], [title], [ambassadorContent], [ambassadorImage], [ambassadorLink], [deleted]) VALUES (2, N'Rebecca Lim', N'<img src="images/rebecca.jpg" /><br /><br />

<span style="font-weight: bold;">ABOUT</span><br />
Rebecca is a fast-rising star in the Singapore entertainment industry, having acted in English and Mandarin dramas across a diverse range of roles. Rebecca first rose to fame as a top 5 contestant in Miss Singapore Universe 2005 at the tender age of 18, and has been in the spotlight since. <br />
<br />
 Rebecca has demonstrated her versatility and maturity as an actress, and her notable appearances include her portrayal of a trainee lawyer in 2010’s The Pupil, a psychiatrist in 2013’s UnRiddle 2, and a free spirit in 2014’s The Dream Makers. Amongst Rebecca’s numerous acting accolades include Best Supporting Actress at the Star Awards 2014, Best Actress in a Leading Role at the Asian Television Awards 2010 and Actress of the Year at the ELLE Awards 2010. She currently stars in ____________<br />
<br />
 Outside of her busy filming schedule, Rebecca enjoys yoga and spending time with close friends and family.<br />
<br />
 Rebecca holds an Accountancy degree from Singapore Management University.<br />
<br />
<span style="font-weight: bold;">Q&amp;A</span><br />
<br />
 <span style="font-weight: bold;">1. How would you describe healthy living?</span> <br />
A balanced lifestyle. I think a lot of people mix fitness with health. Fitness comes from exercising but I think health is a combination of many things. Mental health, state of mind, the food you consume, the vitamins and supplements you take, your general well-being. <br />
<br />
 <span style="font-weight: bold;">2. How does ‘personal energy’ impact your day?</span><br />
I believe in believing in oneself and living right and that your personal energy can impact the way you live and love very significantly. If you think you can''t, it''s half the battle lost already. <br />
<br />
 <span style="font-weight: bold;">3. What does ‘beauty’ mean to you?</span><br />
Beauty is being comfortable in your own skin. I''ve learnt to acknowledge that everyone is different and created differently, but lovingly. Also, external beauty, though important, doesn''t last forever. At the end of the day, real beauty lies within. <br />
<br />
 <span style="font-weight: bold;">4. What are the important aspects of your family life?</span><br />
Family is my number one. I doubt I''ll be able to survive without family. Due to my hectic schedule, I hardly have time for family, so now, any available time spent with them is precious and important. It doesn''t matter what we do. It could just be a simple home cooked meal together and I will be very happy. <br />
<br />
 <span style="font-weight: bold;">5. What’s your secret to performing under pressure?</span><br />
Having as much rest as I can, taking my vitamins and supplements to keep my mind and body healthy and strong, and I try to transform all the negative energy of stress and anxiety to positivity. And tell myself that all will be well. <br />
<br />
 <span style="font-weight: bold;">6. What kind of person do you aspire to be?</span><br />
I aspire to be like my Dad. He really is a hero and an inspiration. He has had many setbacks in life and yes, he does have days where he complains (he is after all human). But he has never once given up. In fact, he has come out of each challenge stronger and even inspired many along the way to press on when the going gets tough. <br />
<br />
 <span style="font-weight: bold;">7. Why is success important to you?</span><br />
I guess no one would like for their efforts and hard work to go to waste. So to know that whatever that I''m working on is in the right direction also gives me motivation to work harder. Also, I know that there are many people around me who have believed in me and helped me get to where I am today. To expect little in my life would be letting them down. To have low expectations of what I can achieve will not only let myself down, but those who have believe in me down as well.<br />
<br />
<br />
<br />', N'rebeccalim.jpg', N'RebeccaLim.aspx', 0)
INSERT [dbo].[tbl_SwisseAmbassadors] ([id], [title], [ambassadorContent], [ambassadorImage], [ambassadorLink], [deleted]) VALUES (3, N'The Wallabies', N'<img src="images/Wallabies.jpg" /><br />
<br />
The Wallabies is the nickname for the Australian national rugby union team. Two times World Cup champions, they are currently ranked fourth in the International Rugby Board World Rankings.<br />
<br />
 The Wallabies have been proud Swisse ambassadors for a number of years and enjoy products across the range. <br />
<br />
 Bernard Foley fullback for the Wallabies says the Swisse contribution to the team can’t be understated. <br />
<br />
 <span style="color: #999999; font-style: italic;">“Going into a game and playing for your country, you can''t afford to lack energy. Preparing and making sure you get the most out of every training session is so important. The Swisse range helps with energy production, and is a great way to add extra fuel to a high intensity workout”.</span><br />
<br />
 Swisse is proud to be the choice of the Wallabies and the continuing support we provide to this great team of athletes.<br />
<br />', N'wallables.jpg', N'Wallabies.aspx', 0)
INSERT [dbo].[tbl_SwisseAmbassadors] ([id], [title], [ambassadorContent], [ambassadorImage], [ambassadorLink], [deleted]) VALUES (4, N'Lleyton Hewitt', N'<img src="images/LleytonHewitt.jpg" /><br />
<br />
Truly one of the greats of Australian tennis, Lleyton Hewitt is a two-time Grand Slam winner, US Open and Wimbledon champion, and at the age of 20 became the youngest male ever to be ranked World No. 1 in singles. <br />
<br />
 Lleyton is a natural fit with Swisse for his sportsmanship, his spirit and his ongoing contribution to the success of Australian sport around the world.<br />
<br />
 <span style="font-style: italic; color: #999999;">“For me it’s fantastic to be an ambassador for Swisse and to have a partnership with such a great brand. Like me they’re about having a healthy lifestyle and that’s something that really fits well with me being an athlete, and obviously I enjoy a lot of their products”</span><br />
<br />
 Lleyton relies on many Swisse products for energy and recovery as they provide him with the power and support he needs before and after matches during his demanding tennis schedule.<br />
<br />
 Lleyton takes Swisse Men’s Ultivite for stamina on and off the court. Watch the video<br />
<br />
 As someone who gives 100% every time he steps onto the court, Lleyton has proved himself year after year as one of the games’ grittiest competitors and Swisse is proud to include Lleyton among our ambassadors.<br />
<br />
 Lleyton uses Swisse Ultiboost Joints for flexibility and recovery. Watch the video<br />
<br />', N'lleyton.jpg', N'Lleyton.aspx', 0)
INSERT [dbo].[tbl_SwisseAmbassadors] ([id], [title], [ambassadorContent], [ambassadorImage], [ambassadorLink], [deleted]) VALUES (5, N'Ricky Ponting', N'<img src="images/RickyPonting.jpg" /><br /><br />
In the world of Australian cricket Ricky Ponting is a living legend. As captain of the Australian side he led his team to a record 34 wins in a row in One Day Internationals, and personally boasts more records than any other player in Australian History.<br />
<br />
 Ricky was the first sportsman to become a Swisse ambassador and has enjoyed nearly 10 years with the brand - a partnership he describes as ‘remarkable’.<br />
<br />
 <span style="color: #999999; font-style: italic;">“I’ve had a lot of fun along the way, it’s been a great journey and some of the people I’ve met have been absolutely terrific. I’ve been privileged to be a part of the brand from a very small brand to one of the leading brands of vitamins around the world”</span> <br />
<br />
 Ricky credits Men’s Ultivites for keeping him feeling good and getting him through long tough days and long, tough tours. “They’ve kept me healthy and allowed me to play international sport for as long as I have.” <br />
<br />', N'images/ricky.jpg', N'Ricky.aspx', 0)
SET IDENTITY_INSERT [dbo].[tbl_SwisseAmbassadors] OFF
SET IDENTITY_INSERT [dbo].[tbl_SwisseAmbassadors_RelatedProducts] ON 

INSERT [dbo].[tbl_SwisseAmbassadors_RelatedProducts] ([id], [ambassadorID], [productID]) VALUES (1, 3, 30)
INSERT [dbo].[tbl_SwisseAmbassadors_RelatedProducts] ([id], [ambassadorID], [productID]) VALUES (2, 3, 31)
INSERT [dbo].[tbl_SwisseAmbassadors_RelatedProducts] ([id], [ambassadorID], [productID]) VALUES (3, 3, 32)
SET IDENTITY_INSERT [dbo].[tbl_SwisseAmbassadors_RelatedProducts] OFF
SET IDENTITY_INSERT [dbo].[tbl_TheScienceOfSwisse] ON 

INSERT [dbo].[tbl_TheScienceOfSwisse] ([id], [title], [link], [content]) VALUES (2, N'Our Research', N'OurResearch.aspx', N'<img src="images/image research.jpg" /><br />
<br />
<p>At Swisse, our focus has always been on creating and manufacturing scientifically validated vitamin and supplement products to help people gain optimal health, so we’re proud to be a leader in our category for investment in up-to-date and independently verified scientific research.</p>
<p>Swisse is dedicated to the area of product innovation and development. Every step in our process is carefully considered. We source our ingredients from some of the world’s best suppliers, and all of our manufacturers comply with the Guide to Good Manufacturing Practice for Medicinal Products (GMP), ensuring the highest safety and quality standards are met. All materials used for manufacturing Swisse products are also subject to a thorough and rigorous testing regime.&nbsp;<span style="line-height: 1.6em; font-size: 0.8em;">the ingredients for our products which reflect the highest bio-availability. It’s all part of the reason Swisse continues to set the benchmark for premium quality health products.&nbsp;</span></p>
<p>Read about how <u>Swisse sustainably sources Antarctic Krill for its Krill Oil products here.</u></p>
<p><br />
</p>
<p><b>Research Development and Partnerships</b></p>
<p> </p>
<p>Swisse is committed to the development of research in vitamins and supplements, nutraceuticals and complementary medicine.</p>
<p><br />
</p>
<p>We regularly engage our esteemed Scientific Advisory Panel - a team of academics, scientists and leading clinicians – to help formulate new products and educate the business on emerging research, population-specific health concerns and areas where complementary medicine may assist.</p>
<p>Read more about the <u>Swisse Scientific Advisory Panel.</u></p>
<p>Our numerous research partnerships include collaborations with the foremost academic research universities, world-leading researchers, and Ph.D. and Post-Doctoral projects – all supporting researcher-led inquiry.</p>
<p><b style="line-height: 1.6em; font-size: 0.8em;">Innovations in Complementary Medicine</b></p>
<p>Swisse supports complementary medicine innovation through the bi-annual Health Symposium, a complementary medicine compendium for the dissemination of complementary medicine research. Our proactive approach to research aims to create a distinct industry standard for complementary medicine.</p>
<p>Swisse is committed to significantly contributing to the evidence base of complementary medicine and nutraceutical research. To this effect, we’ll continue our research into key lifestyle factors and the positive role complementary medicines can play in areas such as cognitive health, cardiovascular diseases, musculoskeletal disorders and mental and digestive health, to shed valuable light on this booming area of the global market.&nbsp;</p>
<p><span style="line-height: 1.6em; font-size: 0.8em;">Read more about </span><u style="line-height: 1.6em; font-size: 0.8em;">Swisse’s extensive clinical trials</u><span style="line-height: 1.6em; font-size: 0.8em;">.&nbsp;</span></p>')
INSERT [dbo].[tbl_TheScienceOfSwisse] ([id], [title], [link], [content]) VALUES (3, N'Our Scientific Partnership', N'OurScientificPartnership.aspx', N'Swisse is at the forefront of investing in science and research for the complementary medicines industry. Our numerous research partnerships include collaborations with renowned academic research universities, world-leading researchers and Ph.D. and Post-Doctoral projects – all supporting researcher-led inquiry. As well as helping to extend the body of evidence for the nutriceuticals industry, our partners independently review all of our health benefit claims and complete full assessments to ensure our products efficacy.<br />
<br />
 Our formal partnerships include: <br />
<br />
 <span style="font-weight: bold;">National Institute of Integrative Medicine</span><br />
<br />
<img src="images/niim.jpg" /><br />
 The Australian National Institute of Integrative Medicine (NIIM) is dedicated to providing evidence-based conventional and complementary approaches to the prevention and treatment of disease. NIIM also engage in many research projects and conduct a series of educational seminars. Swisse supports NIIM across all of its activities, and is the marquee sponsor of NIIM’s free lecture series, which aims to increase the understanding of integrative medicine to those involved with healthcare delivery and the public. <br />
<br />
 <span style="font-weight: bold;">Swinburne University</span><br />
<br />
<img src="images/swinburne.jpg" /><br /><br />
 Swinburne is ranked among the top 400 universities in the world by the Academic Ranking of World Universities, and the Times Higher Education World University Rankings. The Centre for Human Psychopharmacology has over 30 researchers and is the largest group in the world researching the effects of natural products, supplements and other nutritional interventions. They conduct government and industry funded trials as well as investigator-initiated studies. <br />
<br />
 Swinburne University conducted what is believed to be the first study to examine the subjective experience of taking a multivitamin during a clinical trial using Swisse Ultivite multivitamins. <br />
<br />
 Their aim is to understand fundamental aspects of neurocognitive change and to use the methodologies of applied human psychopharmacology in order to benefit behavioural and brain processes and to translate this knowledge for the public good. The Centre also aims to educate the public on the efficacy and safety of natural medicines and illicit drugs on the human brain.<br />
<br />
 <span style="font-weight: bold;">La Trobe University</span><br />
<br />
<img src="images/latrobe.jpg" /><br />
<br />
La Trobe University is ranked among the top 100 universities worldwide under 50 years old and their Faculty of Health Sciences offers substantive nutrition and research capability. The faculty has 4 schools (Allied Health, Public Health and Human Biosciences, Nursing and Midwifery and La Trobe Rural Health School) and 28 department and research centres across 8 campuses. This includes a new and dedicated clinical trial centre. In the second wave of work this centre will progress to testing new combinant formulae and the research of new claims from current products.<br />
<br />
 Our research and new product development is also supported by the Swisse Scientific Advisory Panel, read more about the Panel here.<br />
<br />')
INSERT [dbo].[tbl_TheScienceOfSwisse] ([id], [title], [link], [content]) VALUES (4, N'Clinical Trials', N'ClinicalTrials.aspx', N'<img src="images/image clinical trial.jpg" /><br />
<br />
<p>Here at Swisse, one of our defining principles is the development of scientifically validated formulas that target specific health outcomes.</p>
<p>A key part of our strategy to raise awareness of the proven nature of our products is to conduct our own self-funded clinical trials. We’re currently in the process of putting 80% of our product portfolio into clinical trials, and we regularly publish our research results.</p>
<p><span style="font-weight: bold;">Why does Swisse go to these efforts?&nbsp;</span></p>
<p>Firstly, we want to ensure our product formulations are the very best they can be. Secondly, our commitment to research helps us stand out from the crowd. And finally, we do it to promote the advancement of the evidence base for complementary health.</p>
<p>And, as all Swisse customers know, it helps ensure that when you take Swisse products you’re getting the results you seek. Check out the trial details for our Men’s and Women’s multivitamins below.</p>
<p><span style="font-weight: bold; font-size: 12pt;">SWISSE MEN’S ULTIVITE F1 CLINICAL TRIAL</span></p>
<p><span style="font-weight: bold;">Study title: The effect of micronutrient and herbal supplementation on cognition, mood, anxiety and stress in adults at risk of cognitive decline.</span></p>
<p><span style="font-weight: bold;">Study centre:</span> Brain Sciences Institute, Swinburne University of Technology</p>
<p><span style="font-weight: bold;">Products:</span> Swisse Men’s Ultivite Formula 1</p>
<p><span style="font-weight: bold;">Commencement date:</span> May 2005</p>
<p><span style="font-weight: bold;">Current status:</span> Completed</p>
<p><span style="font-weight: bold;">Study overview:</span> This double blind placebo controlled randomised study investigated the effects of 8 weeks supplementation with Men’s Ultivite Formula 1 on cognitive and mood measures in 50 men aged 50-69. <span style="font-weight: bold;">Compared to placebo there were significant improvements in general day-to-day wellbeing, increased alertness and an improvement in the overall score according to the DASS scale.</span>&nbsp;</p>
<p><span style="font-weight: bold;">These findings have been published:</span></p>
<p>Harris, E., Kirk, J., Rowsell, R., Vitetta, L., Sali, A., Scholey, A., Pipingas, A., 2011. The effect of multivitamin supplementation on mood and stress in healthy older men. Human Psychopharmacology, 26(8): 560-567.</p>
<p><span style="font-weight: bold;">Abstract available at</span>: <a href="http://www.ncbi.nlm.nih.gov/pubmed/22095836" target="_self">http://www.ncbi.nlm.nih.gov/pubmed/22095836</a></p>
<p><br />
</p>
<p><span style="font-weight: bold;">SWISSE WOMEN’S ULTIVITE F1 CLINICAL TRIAL</span></p>
<p><span style="font-weight: bold;">Study title:</span> Randomized clinical trial investigating effects of a multi-nutrient combination on mood, stress, sleep and cognition.</p>
<p><span style="font-weight: bold;">Study centre:</span> Centre for Human Psychopharmacology, Swinburne</p>
<p><span style="font-weight: bold;">Products:</span> Swisse Men’s Ultivite Formula 1 and Swisse Women’s Ultivite Formula 1</p>
<p><span style="font-weight: bold;">Commencement date:</span> March 2011</p>
<p><span style="font-weight: bold;">Status:</span> Data collection for this study is complete. Data analysis is underway.</p>
<p><span style="font-weight: bold;">Clinical Trial Registry code:</span> ACTRN12611000092998</p>
<p>This is a placebo-controlled, double-blind, randomised, parallel groups study that sought to investigate the effects of taking the Men’s or Women’s Ultivite daily for 16-weeks on mood, stress, sleep and cognition in healthy men and women aged 20 to 50 years. Participants completed questionnaires relating to their feelings of general well being, stress and anxiety, sleep quality and feelings of fatigue, mood and any physical complaints. Sensitive computerised tasks were used to assess executive function, memory, speed of processing and multitasking ability and the effect that cognitive stressors have on mood, fatigue and ability to concentrate. The study also examined potential biological mechanisms through which Ultivite use may be beneficial including blood measures, cardiovascular health and brain function.</p>
<p>Preliminary analyses (non-peer reviewed) indicate that both Men’s and Women’s Ultivite F1 formulations lower homocysteine.</p>')
INSERT [dbo].[tbl_TheScienceOfSwisse] ([id], [title], [link], [content]) VALUES (5, N'Ingredients Glossary', N'IngredientsGlossary.aspx', NULL)
INSERT [dbo].[tbl_TheScienceOfSwisse] ([id], [title], [link], [content]) VALUES (6, N'FAQs', N'FAQs.aspx', NULL)
INSERT [dbo].[tbl_TheScienceOfSwisse] ([id], [title], [link], [content]) VALUES (7, N'Swisse Scientific Advisory Panel', N'SwisseScientificAdvisoryPanel.aspx', N'<span style="font-weight: bold;">Swisse closely collaborates with a panel of advisors to remain at the forefront of the latest developments in vitamins and supplements, nutraceuticals and complementary medicine.</span> <br />
<br />
 The Swisse Scientific Advisory Panel consists of world-class academics, scientists and clinicians. They inform Swisse on specific health affairs as well as emerging and evolving science, new ingredients, and opportunities to formulate new products providing key insights that help shape multivitamin and supplement formula development.<br />
<br />
 Swisse is committed to certifying the efficacy of our scientific formulations and is in the process of putting 80% of our product portfolio into clinical trials. <br />
<br />
 If you’d like to learn more about our extensive research into vitmains, complementary medicine and nutraceuticals, please visit Our Research.<br />
<br />
 <span style="font-weight: bold;">Professor Avni Sali. MBBS, PhD, FRACS, FACS, FACNEM</span><br />
Australian surgeon, researcher, author, and media personality Professor Avni is primarily known for advocating an &quot;integrative&quot; approach to medicine combining evidence-based conventional treatments with complementary therapies. <br />
<br />
 <span style="font-weight: bold;">Assoc Prof. Andrew Pipingas.</span> <br />
Andrew Pipingas is a clinical researcher involved with investigating the effects of nutraceuticals of cognitive function and health. <br />
<br />
 <span style="font-weight: bold;">Dr. Karin Ried. PhD, MSc, GDPH, Cert Integrative Medicine</span> <br />
Dr Karin Ried is Research Director at the National Institute of Integrative Medicine. Dr Ried has over 15 years experience in medical and public health research, as well as research interest in complementary and integrative medicine with a focus on nutritional health. Karin is considered to be a world authority on research related to garlic and chocolate.<br />
<br />
  <span style="font-weight: bold;">Mr. Gerald Quigley.</span> <br />
Gerald Quigley is a practicing Community Pharmacist, as well as an Accredited Herbalist. These joint qualifications give Gerald a unique overview of health from a holistic perspective, meaning he doesn’t only look at the symptomatic treatment, which leaves a gap in the recovery phase. Gerald is also a well-known media personality and health commentator. <br />
<br />
 <span style="font-weight: bold;">Dr. Valerie Malka. MD, FRACS, MIPH, MA</span> <br />
Dr. Valerie Malka is a Trauma and General Surgeon and was the Director of Trauma Services for Westmead Hospital and Sydney West Area Health Services for over a decade. Dr Malka has worked extensively in patient safety and the maintenance of ethics in healthcare and worked with the International Committee of the Red Cross and the International Rescue Committee.<br />
<br />
<br />
<br />')
SET IDENTITY_INSERT [dbo].[tbl_TheScienceOfSwisse] OFF
SET IDENTITY_INSERT [dbo].[tbl_User] ON 

INSERT [dbo].[tbl_User] ([userID], [userName], [userEmail], [userPassword], [userFN], [userMN], [userLN], [userBirthMonth], [userBirthYear], [userPostCode], [userStatus], [firstLogged], [checkAgreed]) VALUES (1, N'mina', N'mina@mina.com', N'37+tSYehEhxJHaLq1bHtnA==', N'Mina', NULL, N'Mimi', N'June', N'1991', N'1632', 1, 1, 1)
INSERT [dbo].[tbl_User] ([userID], [userName], [userEmail], [userPassword], [userFN], [userMN], [userLN], [userBirthMonth], [userBirthYear], [userPostCode], [userStatus], [firstLogged], [checkAgreed]) VALUES (2, N'test', N'ayungin@gmail.com', N'ADX4VeuwJ0BAoXSOXntYdJQJoRd61V+N/TNePnJfu38=', N'test', NULL, N'test', N'April', N'1998', N'', 1, 0, 0)
SET IDENTITY_INSERT [dbo].[tbl_User] OFF
SET IDENTITY_INSERT [dbo].[tbl_User_Country] ON 

INSERT [dbo].[tbl_User_Country] ([id], [userid], [countryid]) VALUES (1, 2, 0)
SET IDENTITY_INSERT [dbo].[tbl_User_Country] OFF
ALTER TABLE [dbo].[tbl_AboutSwisse_RelatedProducts] ADD  CONSTRAINT [DF_tbl_AboutSwisse_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_Banner] ADD  CONSTRAINT [DF_tbl_Banner_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[tbl_Banner] ADD  CONSTRAINT [DF_tbl_Banner_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_Blog] ADD  CONSTRAINT [DF_tbl_Blog_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_Blog_Formats] ADD  CONSTRAINT [DF_tbl_Blog_Formats_blogid]  DEFAULT ((0)) FOR [blogid]
GO
ALTER TABLE [dbo].[tbl_Blog_Formats] ADD  CONSTRAINT [DF_tbl_Blog_Formats_formatid]  DEFAULT ((0)) FOR [formatid]
GO
ALTER TABLE [dbo].[tbl_Blog_HealthAndHappiness] ADD  CONSTRAINT [DF_tbl_Blog_HealthAndHappiness_blogid]  DEFAULT ((0)) FOR [blogid]
GO
ALTER TABLE [dbo].[tbl_Blog_HealthAndHappiness] ADD  CONSTRAINT [DF_tbl_Blog_HealthAndHappiness_healthandhappinessblogid]  DEFAULT ((0)) FOR [healthandhappinessblogid]
GO
ALTER TABLE [dbo].[tbl_Blog_RelatedBlogs] ADD  CONSTRAINT [DF_tbl_Blog_RelatedBlogs_blogid]  DEFAULT ((0)) FOR [blogid]
GO
ALTER TABLE [dbo].[tbl_Blog_RelatedBlogs] ADD  CONSTRAINT [DF_tbl_Blog_RelatedBlogs_relatedblogid]  DEFAULT ((0)) FOR [relatedblogid]
GO
ALTER TABLE [dbo].[tbl_Blog_RelatedProducts] ADD  CONSTRAINT [DF_tbl_Blog_RelatedProducts_blogid]  DEFAULT ((0)) FOR [blogid]
GO
ALTER TABLE [dbo].[tbl_Blog_RelatedProducts] ADD  CONSTRAINT [DF_tbl_Blog_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_Blog_Themes] ADD  CONSTRAINT [DF_tbl_Blog_Themes_blogid]  DEFAULT ((0)) FOR [blogid]
GO
ALTER TABLE [dbo].[tbl_Blog_Themes] ADD  CONSTRAINT [DF_tbl_Blog_Themes_themeid]  DEFAULT ((0)) FOR [themeid]
GO
ALTER TABLE [dbo].[tbl_BlogFormat] ADD  CONSTRAINT [DF_Table_1_blogThemeStatus]  DEFAULT ((0)) FOR [blogFormatStatus]
GO
ALTER TABLE [dbo].[tbl_BlogFormat] ADD  CONSTRAINT [DF_tbl_BlogFormat_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_BlogTheme] ADD  CONSTRAINT [DF_Table_1_prodcatStatus_2]  DEFAULT ((0)) FOR [blogThemeStatus]
GO
ALTER TABLE [dbo].[tbl_BlogTheme] ADD  CONSTRAINT [DF_tbl_BlogTheme_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_ClinicalTrials_RelatedProducts] ADD  CONSTRAINT [DF_tbl_ClinicalTrials_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_ContactUs] ADD  CONSTRAINT [DF_tbl_ContactUs_checkAgreed]  DEFAULT ((0)) FOR [checkAgreed]
GO
ALTER TABLE [dbo].[tbl_CorporateSocial_RelatedProducts] ADD  CONSTRAINT [DF_tbl_CorporateSocial_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_Ingredients] ADD  CONSTRAINT [DF_tbl_Ingredients_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_Ingredients_RelatedIngredients] ADD  CONSTRAINT [DF_Table_1_blogid]  DEFAULT ((0)) FOR [ingid]
GO
ALTER TABLE [dbo].[tbl_Ingredients_RelatedIngredients] ADD  CONSTRAINT [DF_Table_1_relatedblogid]  DEFAULT ((0)) FOR [relatedingid]
GO
ALTER TABLE [dbo].[tbl_Ingredients_RelatedProducts] ADD  CONSTRAINT [DF_tbl_Ingredients_RelatedProducts_ingID]  DEFAULT ((0)) FOR [ingID]
GO
ALTER TABLE [dbo].[tbl_Ingredients_RelatedProducts] ADD  CONSTRAINT [DF_tbl_Ingredients_RelatedProducts_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_OurHistory_RelatedProducts] ADD  CONSTRAINT [DF_tbl_OurHistory_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_OurPhilosophy_RelatedProducts] ADD  CONSTRAINT [DF_tbl_OurPhilosophy_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_OurResearch_RelatedProducts] ADD  CONSTRAINT [DF_tbl_OurResearch_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_OurScientificPartnership_RelatedProducts] ADD  CONSTRAINT [DF_tbl_OurScientificPartnership_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_Product] ADD  CONSTRAINT [DF_tbl_Product_prodcatID]  DEFAULT ((0)) FOR [prodcatID]
GO
ALTER TABLE [dbo].[tbl_Product] ADD  CONSTRAINT [DF_tbl_Product_productStatus]  DEFAULT ((0)) FOR [productStatus]
GO
ALTER TABLE [dbo].[tbl_Product] ADD  CONSTRAINT [DF_tbl_Product_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_Product_Age] ADD  CONSTRAINT [DF_tbl_Product_Age_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_Age] ADD  CONSTRAINT [DF_tbl_Product_Age_ageID]  DEFAULT ((0)) FOR [ageID]
GO
ALTER TABLE [dbo].[tbl_Product_Gender] ADD  CONSTRAINT [DF_tbl_Product_Gender_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_Gender] ADD  CONSTRAINT [DF_tbl_Product_Gender_genderID]  DEFAULT ((0)) FOR [genderID]
GO
ALTER TABLE [dbo].[tbl_Product_Keywords] ADD  CONSTRAINT [DF_tbl_ProductKeywords_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_MetaOpenGraphTitle] ADD  CONSTRAINT [DF_tbl_Product_MetaOpenGraphTitle_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_Product_ProductConcern] ADD  CONSTRAINT [DF_tbl_Product_ProductConcern_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_ProductConcern] ADD  CONSTRAINT [DF_tbl_Product_ProductConcern_prodconcernID]  DEFAULT ((0)) FOR [prodconcernID]
GO
ALTER TABLE [dbo].[tbl_Product_ProductIngredient] ADD  CONSTRAINT [DF_tbl_Product_ProductIngredient_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_ProductIngredient] ADD  CONSTRAINT [DF_tbl_Product_ProductIngredient_productingredientID]  DEFAULT ((0)) FOR [productingredientID]
GO
ALTER TABLE [dbo].[tbl_Product_RelatedProducts] ADD  CONSTRAINT [DF_tbl_Product_RelatedProducts_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_RelatedProducts] ADD  CONSTRAINT [DF_Table_1_productingredientID]  DEFAULT ((0)) FOR [relatedproductID]
GO
ALTER TABLE [dbo].[tbl_Product_Retailers] ADD  CONSTRAINT [DF_tbl_ProductRetailer_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_Product_TitleTag] ADD  CONSTRAINT [DF_tbl_Product_TitleTag_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_ProductCategory] ADD  CONSTRAINT [DF_tbl_ProductCategory_prodcatStatus]  DEFAULT ((0)) FOR [prodcatStatus]
GO
ALTER TABLE [dbo].[tbl_ProductCategory] ADD  CONSTRAINT [DF_tbl_ProductCategory_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_ProductCategory] ADD  CONSTRAINT [DF_tbl_ProductCategory_rank]  DEFAULT ((0)) FOR [rank]
GO
ALTER TABLE [dbo].[tbl_ProductConcern] ADD  CONSTRAINT [DF_Table_1_prodcatStatus]  DEFAULT ((0)) FOR [prodconcernStatus]
GO
ALTER TABLE [dbo].[tbl_ProductConcern] ADD  CONSTRAINT [DF_tbl_ProductConcern_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_ProductConcern] ADD  CONSTRAINT [DF_tbl_ProductConcern_rank]  DEFAULT ((0)) FOR [rank]
GO
ALTER TABLE [dbo].[tbl_ProductDetail] ADD  CONSTRAINT [DF_tbl_ProductDetail_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_ProductIngredient] ADD  CONSTRAINT [DF_Table_1_prodcatStatus_1]  DEFAULT ((0)) FOR [prodingredientStatus]
GO
ALTER TABLE [dbo].[tbl_ProductIngredient] ADD  CONSTRAINT [DF_tbl_ProductIngredient_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_ProductIngredient] ADD  CONSTRAINT [DF_tbl_ProductIngredient_rank]  DEFAULT ((0)) FOR [rank]
GO
ALTER TABLE [dbo].[tbl_ProductRecentlyViewed] ADD  CONSTRAINT [DF_tbl_ProductRecentlyViewed_userid]  DEFAULT ((0)) FOR [userid]
GO
ALTER TABLE [dbo].[tbl_ProductRecentlyViewed] ADD  CONSTRAINT [DF_tbl_ProductRecentlyViewed_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_SwisseAdvisory_RelatedProducts] ADD  CONSTRAINT [DF_tbl_SwisseAdvisory_RelatedProducts_productid]  DEFAULT ((0)) FOR [productid]
GO
ALTER TABLE [dbo].[tbl_SwisseAmbassadors] ADD  CONSTRAINT [DF_tbl_SwisseAmbassadors_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_SwisseAmbassadors_RelatedLinks] ADD  CONSTRAINT [DF_tbl_SwisseAmbassadors_RelatedLinks_ambassadorID]  DEFAULT ((0)) FOR [ambassadorID]
GO
ALTER TABLE [dbo].[tbl_SwisseAmbassadors_RelatedLinks] ADD  CONSTRAINT [DF_tbl_SwisseAmbassadors_RelatedLinks_relatedLinkID]  DEFAULT ((0)) FOR [relatedLinkID]
GO
ALTER TABLE [dbo].[tbl_SwisseAmbassadors_RelatedProducts] ADD  CONSTRAINT [DF_tbl_SwisseAmbassadors_RelatedProducts_ambassadorID]  DEFAULT ((0)) FOR [ambassadorID]
GO
ALTER TABLE [dbo].[tbl_SwisseAmbassadors_RelatedProducts] ADD  CONSTRAINT [DF_tbl_SwisseAmbassadors_RelatedProducts_productID]  DEFAULT ((0)) FOR [productID]
GO
ALTER TABLE [dbo].[tbl_User] ADD  CONSTRAINT [DF_tbl_Users_userStatus]  DEFAULT ((0)) FOR [userStatus]
GO
ALTER TABLE [dbo].[tbl_User] ADD  CONSTRAINT [DF_tbl_User_firstLogged]  DEFAULT ((0)) FOR [firstLogged]
GO
ALTER TABLE [dbo].[tbl_User] ADD  CONSTRAINT [DF_tbl_User_checkAgreed]  DEFAULT ((0)) FOR [checkAgreed]
GO
ALTER TABLE [dbo].[tbl_User_Country] ADD  CONSTRAINT [DF_tbl_UserCountry_userid]  DEFAULT ((0)) FOR [userid]
GO
ALTER TABLE [dbo].[tbl_User_Country] ADD  CONSTRAINT [DF_tbl_UserCountry_countryid]  DEFAULT ((0)) FOR [countryid]
GO
ALTER TABLE [dbo].[tbl_UserLogs] ADD  CONSTRAINT [DF_tbl_UserLogs_userID]  DEFAULT ((0)) FOR [userID]
GO
