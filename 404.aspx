﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="_404" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

<title>Page Not Found</title>
<meta property="og:title" content="Page Not Found" />
<meta name="keywords" content="multivitamins, multivite, multivitamin tablets, women's health, men's health, vitamins, supplements, integrative health, natural medicine, traditional chinese medicine, vitamin supplements" />
<meta name="description" content="At Swisse we develop scientifically validated vitamins and supplements specially formulated to help fill nutritional gaps. Australia’s #1 Multivitamin Brand." />
<link rel="canonical" href="http://www.swisse.com.sg/404.aspx" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="errorPage">
        <div class="container">
            <h1>PAGE NOT FOUND</h1>
            <h2>404</h2>
            <p>We’re sorry, the page you have requested cannot be found.</p>
        </div>
    </div>
    <div id="errorContent">
        <div class="container">
            <p>Please use the header navigation to find the section you’re looking for, or try returning to the <a href="/index.aspx">HOMEPAGE</a> to start again.</p>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerContent" Runat="Server">
</asp:Content>

