﻿<%@ Page Title="About Swisse" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AboutSwisse.aspx.cs" Inherits="HeaderNavigationFlyout_AboutSwisse" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>About Swisse | Swisse Vitamins</title>
<meta property="og:title" content="About Swisse Vitamins" />
<meta name="keywords" content="multivitmains, vitamins, Australian companies, about Swisse, about Swisse Australia, vitamins and supplements, health companies, health brands, health and wellbeing" />
<meta name="description" content="Read all about Australia’s Number 1 Multivitamin brand, Swisse, and the journey we’ve been on to help people live happier and healthier lives." />
<link rel="canonical" href="http://www.swisse.com.sg/our-story/about-swisse" />
<link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/our-story">Our Story</a>
                <span class="mlr">></span>
                About Swisse
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>
<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">About Swisse</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litAboutSwisseRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>
<li><a href="/our-story/about-swisse">About Swisse</a></li>
<li><a href="/our-story/our-philosophy">Our Philosophy</a></li>
<li><a href="/our-story/swisse-ambassadors">Swisse Ambassadors</a></li>
<li><a href="/the-science-of-swisse/our-research">Our Research</a></li>
</ul>
</div>
</div>
</div>
  <a href="/products">
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" class="mtop20">
  </a>
  </div>
<div class="clearfloat"></div>
</div>
</section>
</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>

</asp:Content>