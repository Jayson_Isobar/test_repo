﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Net;
using Swisse.ExtInterface;
public partial class Account_Register : BaseForm
{
    String gmailSMTP = ConfigurationManager.AppSettings["gmailSMTP"];
    String gmailSMTPpass = ConfigurationManager.AppSettings["gmailSMTPpass"];
    String siteUrl = ConfigurationManager.AppSettings["siteUrl"];
    String lnkFromEmailReg = ConfigurationManager.AppSettings["lnkFromEmailReg"];

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie myCookie = new HttpCookie("underage");
        myCookie = Request.Cookies["underage"];
        if (myCookie != null)
        {
            Response.Redirect("Rejection.aspx");
        }

        //if (Session["age"] != null)
        //{
        //    Response.Redirect("Rejection.aspx");
        //}
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }


    protected void btnRegister_Click(object sender, EventArgs e)
    {
        UserCls GU = new UserCls();
        string Email = txtEmail.Text;
        GU = UserCls.CheckEmail(Email);

        if (GU != null)
        {
            lblDuplicateEmail.Visible = true;

        }
        else
        {
            lblDuplicateEmail.Visible = false;

            if (txtPassword.Text.Length < 8 || txtPassword.Text.Length > 15)
            {
                lblErrPass.Visible = true;
                lblErrReq.Visible = true;
            }
            else
            {

                int age = (Int32.Parse(DateTime.Today.ToString("yyyy")) - Int32.Parse(ddlyear.SelectedItem.Text));

                if (age < 13)
                {
                    HttpCookie myCookie = new HttpCookie("underage");
                    
                    // Set the cookie value.
                    myCookie.Value = "underage";
                    // Set the cookie expiration date.
                    myCookie.Expires = DateTime.Now.AddMinutes(30);

                    Response.Cookies.Add(myCookie);

                    Session["age"] = "underage";
                    Response.Redirect("Rejection.aspx");
                }
                else {
                    UserCls uAdd = new UserCls();
                    uAdd.userEmail = txtEmail.Text.Trim();
                    uAdd.userfName = txtfName.Text.Trim();
                    uAdd.userlName = txtlName.Text.Trim();
                    //uAdd.userName = txtuName.Text.Trim();
                    uAdd.userPassword = Encrypt(txtPassword.Text);
                    uAdd.userStatus = 1;
                    uAdd.userBirthMonth = ddlMonth.SelectedItem.Text.Trim();
                    uAdd.userBirthYear = ddlyear.SelectedItem.Text.Trim();
                    uAdd.userPostCode = txtPostCode.Text.Trim();
                    if (cbAgreed.Checked == true)
                    {
                        uAdd.checkAgreed = 1;
                    }
                    else
                    {
                        uAdd.checkAgreed = 0;
                    }
                    uAdd.AddUser();

                    lblErrReq.Visible = false;
                    lblErrPass.Visible = false;

                    //SendEmail();
                    Send2API();

                    Session["regSuccess"] = "Registration Success";
                    Response.Redirect("Login.aspx");
                }
            }
        }

    }

    public void SendEmail()
    {
        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        client.Timeout = 10000;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(gmailSMTP, gmailSMTPpass);

        MailMessage mm = new MailMessage(gmailSMTP, txtEmail.Text, "USER CONFIRMATION", "Please click this link to confirm:" + siteUrl + lnkFromEmailReg + "?email=" + txtEmail.Text);
        mm.BodyEncoding = UTF8Encoding.UTF8;
        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

        client.Send(mm);
    }

    //public void Send2API() {
    //    const string uri = "https://rtm.dls.dream.epsilon.com/rtmsd";

    //    var request = (HttpWebRequest)
    //    WebRequest.Create(uri); request.KeepAlive = false;
    //    request.ProtocolVersion = HttpVersion.Version10;
    //    request.Method = "POST";
    //    request.Credentials = new NetworkCredential("Swisse_SG_RTM_appUser", "b578835bd9ae072013f7a9c8139cc7ef92326ac03d8d1ebf50ebda512f4ddec0");

    //    string postData = "<?xml version='1.0' encoding='utf-8'?>" +
    //                    "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'" +
    //                     "xmlns:SOAP-ENC='http://schemas.xmlsoap.org/soap/encoding/'" + 
    //                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'" + 
    //                     "xmlns:xsd='http://www.w3.org/2001/XMLSchema'" + 
    //                     "xmlns:rtmwsdl='DREAMRTMReq.wsdl'" + 
    //                     "xmlns:rtmreq='DREAMRTMWebletReq.xsd'" + 
    //                     "xmlns:rtmresp='DREAMRTMResp.xsd'>" +
    //                    "<SOAP-ENV:Body>" +
    //                    "<rtmreq:RTMTriggerRequest xmlns:rtmreq='DREAMRTMWebletReq.xsd'>" +
    //                    "<Header>" +
    //                    "<Partner>P&G_APAC</Partner>" +
    //                    "<Client>Swisse_SG</Client>" +
    //                    "<Timestamp>'"+ DateTime.Today +"'</Timestamp>" +
    //                    "<Username>user</Username>" +
    //                    "<Password>b578835bd9ae072013f7a9c8139cc7ef92326ac03d8d1ebf50ebda512f4ddec0</Password>" +
    //                    "</Header>" +
    //                    "<RTMTriggerTemplate>" +
    //                    "<TriggerEmailTarget>" +
    //                    "<Folder>RTM_Emails</Folder>" +
    //                    "<Template>Welcome_Email</Template>" +
    //                    "<ToEmailAddress>" +
    //                    "<EventEmailAddress>" +
    //                    "<EmailAddress>" + txtEmail.Text.Trim() + "</EmailAddress>" +
    //                    "<FromEmailAddress>swissesg@sg.com</FromEmailAddress>" +
    //                    "<FromDisplayName>SWISSE</FromDisplayName>" +
    //                    "<ReplyToEmailAddress>swissesg@sg.com</ReplyToEmailAddress>" +
    //                    "<ReplyToDisplayName>SWISSE SG</ReplyToDisplayName>" +
    //                    "<EventVariables>" +
    //                    "<Variable>" +
    //                    "<Name>FIRST_NAME</Name>" +
    //                    "<Value>" + txtfName.Text.Trim() +"</Value>" +
    //                    "</Variable>" +
    //                    "<Variable>" +
    //                    "<Name>MessageToResetPwd</Name>" +
    //                    "<Value>http://www.olay.com.au/au/Pages/ResetPassword.aspx?id=ktP2ilhxgVTDAGijZwWxKexIA7rSw3XXXLK7Zz%2fKQJ4G2N8oV75r%2f4jHCA73bTZv</Value>" +
    //                    "</Variable>" +
    //                    "</EventVariables>" +
    //                    "</EventEmailAddress>" +
    //                    "</ToEmailAddress>" +
    //                    "</TriggerEmailTarget>" +
    //                    "</RTMTriggerTemplate>" +
    //                    "</rtmreq:RTMTriggerRequest>" +
    //                    "</SOAP-ENV:Body>" +
    //                    "</SOAP-ENV:Envelope>";

    //    var postBytes = Encoding.ASCII.GetBytes(postData);

    //    request.ContentType = "application/x-www-form-urlencoded";
    //    request.ContentLength = postBytes.Length;
    //    var requestStream = request.GetRequestStream();

    //    requestStream.Write(postBytes, 0, postBytes.Length);
    //    requestStream.Close();

    //    var response = (HttpWebResponse)request.GetResponse();
    //    Console.WriteLine(new StreamReader(response.GetResponseStream()).ReadToEnd());
    //    Console.WriteLine(response.StatusCode);
    //}

    public void Send2API()
    {
        IMailSender senderRTM = new EpsilonSender("Swisse_SG", "Swisse_SG_RTM_appUse", "p@$andBx1");
        senderRTM.SendRegistrationEmail(txtEmail.Text.Trim(), txtfName.Text.Trim());
    }
}
