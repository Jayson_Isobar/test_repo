﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class ItemRouteHandler : IRouteHandler
{

    /// <summary>
    /// Product route handler
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns></returns>
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {

        var pageUrl = requestContext.RouteData.Values["PageUrl"] as string;
        if (pageUrl != null && pageUrl.EndsWith("/"))
        {
            pageUrl = pageUrl.Substring(0, pageUrl.Length - 1);
        }
        if (string.IsNullOrEmpty(pageUrl) || Path.HasExtension(pageUrl))
            return null;
        else
        {

            var item = XmlHelper.GetItem(pageUrl);
            // Get information about this product

            //get all the product list
            if (item == null)
                return UrlHelper.GetNotFoundHttpHandler();
            else
            {
                // Store the Product object in the Items collection
                HttpContext.Current.Items["Item"] = item;
                switch (item.Type)
                { 
                    case "1":// category : format products/{category name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ProductCategory.aspx", typeof(Page)) as Page;
                    case "2": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ProductDetails.aspx", typeof(Page)) as Page;
                    case "4": //the science of swisse  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/" + item.Id, typeof(Page)) as Page;
                    case "5": //the science of swisse  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ViewIng.aspx", typeof(Page)) as Page;
                    case "6": //the science of swisse  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/" + item.Id, typeof(Page)) as Page;


                    case "7": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/BlogCat.aspx", typeof(Page)) as Page;
                    case "8": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ViewBlog.aspx", typeof(Page)) as Page;
            

                    case "9": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ProductConcern.aspx", typeof(Page)) as Page;

                    case "10": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ViewA.aspx", typeof(Page)) as Page;

                    case "11": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ProductConcern.aspx", typeof(Page)) as Page;

                    case "12": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ProductIngredient.aspx", typeof(Page)) as Page;

                    case "13": //product details  : format products/{product name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/" + item.Id, typeof(Page)) as Page;

                  
                    default:
                        return UrlHelper.GetNotFoundHttpHandler();
                }
            }
        }


    }


}