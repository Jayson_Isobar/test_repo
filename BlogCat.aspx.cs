using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;


public partial class BlogCat : BaseForm
{

    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    string id = "";
    protected Item Item
    {
        get
        {
            return HttpContext.Current.Items["Item"] as Item;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadHealthAndHappinessBlog();
            LoadData();
        }
    }
    public void LoadHealthAndHappinessBlog()
    {
        HealthAndHappinessBlogCls hhb = new HealthAndHappinessBlogCls();
        hhb = HealthAndHappinessBlogCls.GetHealthAndHappinessBlogCls();
        string healthandhappinessblog = hhb.healthandhappinessblog;

        litContent.Text = healthandhappinessblog;
    }

    public void LoadData()
    {
        //id = Request.QueryString["cat"].ToString();
        id = Item.Id;

        HealthAndHappinessBlogCls pc3 = new HealthAndHappinessBlogCls();
        pc3 = HealthAndHappinessBlogCls.GetTotalBlogByCategory(id);
        string cat3 = pc3.totalBlog;

        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }

            string Theme = "";
            if (txtBlogTheme.Text == "")
            {
                Theme = null;
            }
            else
            {
                Theme = txtBlogTheme.Text;
            }
            string Format = "";
            if (txtBlogFormat.Text == "")
            {
                Format = null;
            }
            else
            {
                Format = txtBlogFormat.Text;
            }

            HealthAndHappinessBlogCls b = new HealthAndHappinessBlogCls();
            b = HealthAndHappinessBlogCls.GetBlogFilterCat(id, Theme, Format);
            if(b != null){
                string blog = b.healthandhappinessblog;
				if(blog  != null){
					blog = blog.Replace("<img src='", "<img src='" + base.CDNdomain);
				}			
                litBlog.Text = blog;
            }
    }



    protected void cbBlogTheme_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields();
    }

    public void LoadCBDataFields()
    {


        if (cbBlogTheme.SelectedIndex != -1)
        {
            txtBlogTheme.Text = GetCheckBoxListSelections();
            LoadData();
        }
        else
        {
            txtBlogTheme.Text = null;
            LoadData();
        }


    }

    private string GetCheckBoxListSelections()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbBlogTheme.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbBlogFormat_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields2();
    }

    public void LoadCBDataFields2()
    {


        if (cbBlogFormat.SelectedIndex != -1)
        {
            txtBlogFormat.Text = GetCheckBoxListSelections2();
            LoadData();
        }
        else
        {
            txtBlogFormat.Text = null;
            LoadData();
        }


    }

    private string GetCheckBoxListSelections2()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbBlogFormat.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }
}

























      
