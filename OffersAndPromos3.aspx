﻿<%@ Page Title="Offers And Promos" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OffersAndPromos3.aspx.cs" Inherits="HeaderNavigationFlyout_OffersAndPromos3" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  <section class="blog-content">
<section class="breadcrumbs2">
<div class="container">
<ul>
<li>Home &rsaquo;</li>
<li>Swisse Lifestyle Blog</li>
<div class="fltrt">
<p class="txt-up"><a onclick="window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print">
</a></p>
</div>
</ul>
</div>
</section>
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">offers & promos</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>

</div>
<div class="col_32 fltrt">
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map">
  <map name="Map">
    <area shape="rect" coords="32,296,161,336" href="#">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>

</asp:Content>
