CREATE TABLE [dbo].[tbl_Blog_Recommended](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[blogid] [int] NULL,
	[datetime] [timestamp] NULL,
 CONSTRAINT [PK_tbl_Blog_Recommended] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO