﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class GlobalRouteHandler : IRouteHandler
{
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        var page = requestContext.RouteData.Values["SinglePage"] as string;
        if (page != null && page.EndsWith("/"))
        {
            page = page.Substring(0, page.Length - 1);
        }
        if (!string.IsNullOrEmpty(page))
        {
            switch (page)
            {
                case "products": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/shopproducts.aspx", typeof (Page)) as Page;
                case "the-science-of-swisse": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/TheScienceOfSwisse.aspx", typeof(Page)) as Page;
                case "our-story": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/OurStory.aspx", typeof(Page)) as Page;
                case "health-happiness-blog": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/HealthAndHappinessBlog.aspx", typeof(Page)) as Page;
                case "offers-and-promos": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/OffersAndPromos.aspx", typeof(Page)) as Page;
                case "citibank": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/edm.aspx", typeof(Page)) as Page;
                case "store-locator": // category : format products/{category name}
                    return BuildManager.CreateInstanceFromVirtualPath("~/StoreLocator.aspx", typeof(Page)) as Page;
                default:
                    return BuildManager.CreateInstanceFromVirtualPath("~/" + page + ".aspx", typeof(Page)) as Page;
            }
        }
        return BuildManager.CreateInstanceFromVirtualPath("~/index.aspx", typeof(Page)) as Page;
    }
}