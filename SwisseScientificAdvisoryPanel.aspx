﻿<%@ Page Title="Swisse Scientific Advisory Panel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SwisseScientificAdvisoryPanel.aspx.cs" Inherits="HeaderNavigationFlyout_SwisseScientificAdvisoryPanel" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>Swisse Scientific Advisory Panel | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Vitamins - Scientific Advisory Panel" />
<meta name="keywords" content="vitamins, multivitamins, scientific research, integrative health, clinical trials, vitamin research, health research, research and development, neutraceuticals, vitamin science" />
<meta name="description" content="Swisse closely collaborates with a panel of scientific advisors to remain at the forefront of the latest developments in vitamins and supplements." />
<link rel="canonical" href="http://www.swisse.com.sg/the-science-of-swisse/scientific-advisory-panel" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/the-science-of-swisse">The Science of Swisse</a>
                <span class="mlr">></span>
                Swisse Scientific Advisory Panel
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>
<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>
<li><a href="/the-science-of-swisse/our-research">Our Research</a></li>
<li><a href="/the-science-of-swisse/our-research">Our Scientific Partnerships</a></li>
<li><a href="/the-science-of-swisse/clinical-trials">Clinical Trials</a></li>
<li><a href="/the-science-of-swisse/ingredients-glossary">Ingredients Glossary</a></li>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>


</asp:Content>
