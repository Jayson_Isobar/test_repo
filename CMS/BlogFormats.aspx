﻿<%@ Page Title="Blog Formats" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="BlogFormats.aspx.cs" Inherits="BlogFormats" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
   
     <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvBlog" Runat="server" 
            emptydatatext="No records found."
            OnRowEditing="gvBlog_RowEditing"
            OnPageIndexChanging="gvBlog_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
           >
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
             
               <asp:TemplateField HeaderText="Blog Format" SortExpression="blogFormat">
                    <ItemTemplate>
                        <asp:Label ID="lblblogTheme" runat="server" Text='<%# Bind("blogFormat") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>
        <p></p>
        <p></p>
    <asp:Label ID="Label1" runat="server" Text="Format"></asp:Label>
    <p></p>
    <asp:DropDownList ID="ddlTheme" runat="server">
    </asp:DropDownList>
    <p></p>
    <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
</asp:Content>
