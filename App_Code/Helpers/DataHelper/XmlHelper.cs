﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for XxmlHelper
/// </summary>
public class XmlHelper
{

    public static List<Item> ItemList { get; set; }
    /// <summary>
    /// Load Xml data
    /// </summary>
    /// <returns></returns>
    public static List<Item> GetItemList()
    {
        const string key = "item_List";
        var cacheItemList = CacheHelper.Get<List<Item>>(key);
        if (cacheItemList != null) return cacheItemList;

        var path = HttpContext.Current.Server.MapPath("/data/swisse.xml");

        var newItems = new List<Item>();
        if (path == null) return newItems;
        using (var reader = new StreamReader(path))
        {
            var serializer = new XmlSerializer(typeof (Items));
            var items = (Items) serializer.Deserialize(reader);
            if (items.ItemList != null)
            {
                newItems = items.ItemList;
                ItemList = items.ItemList;
            }
            CacheHelper.Add(items.ItemList, key);
        }
        return newItems;
    }
    /// <summary>
    /// get item by url
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>

    public static Item GetItem(string url)
    {
        if (ItemList == null)
        {
            ItemList = GetItemList();
        }
        var item = ItemList.FirstOrDefault(x => x.Url.Equals(url, StringComparison.OrdinalIgnoreCase));
        if(item != null)
        {
            var currentUrl = HttpContext.Current.Request.Url;
            var canonicalUrl = currentUrl.Scheme + "://" + currentUrl.Host + currentUrl.AbsolutePath;
            item.CanonicalUrl = canonicalUrl;
        }
        return item;
    }
    /// <summary>
    /// Ge Parge url by Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static string GetPageUrl(string id)
    {
        if (ItemList == null)
        {
            ItemList = GetItemList();
        }
        var item = ItemList.FirstOrDefault(x => x.Id.Equals(id));
        return item != null ? item.Url : string.Empty;
    }

    public static string GetPageUrl(string id, PageType.Type pagetype)
    {
        if (ItemList == null)
        {
            ItemList = GetItemList();
        }
        var pageType = (int) pagetype;
        var item = ItemList.FirstOrDefault(x => x.Id.Split(',').Contains(id) && x.Type.Equals(pageType.ToString(), StringComparison.OrdinalIgnoreCase));
        return item != null ? item.Url : string.Empty;
    }

  

}