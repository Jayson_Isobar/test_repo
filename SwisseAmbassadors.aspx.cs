﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_SwisseAmbassadors : BaseForm
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetOurStoryContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@link", "SwisseAmbassadors.aspx");
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();

        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                litContent.Text = myRow[0].ToString();
            }
            var imageUrl = string.IsNullOrEmpty(base.CDNdomain) ? "/" : base.CDNdomain;
            litContent.Text = litContent.Text.Replace("<img src=\"", "<img src=\"" + imageUrl);
        }

        con.Close();
    }
}