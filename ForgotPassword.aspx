﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs color-blck">
<div class="container">
<p>Home &rsaquo; Forgot Password</p>
</div>
</section>

<section class="container">
  
    <div class="col_60 fltlft">

   
    <h2>Recover your password</h2>
    <div id="create-account-wrap" class="mtop20">
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
     <label class="mtop20">Email</label>
    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>    
    <div class="clearfloat"></div>     
    </div>
      <div class="clearfloat"></div>
    </div>
     <asp:Button ID="btnSend" runat="server" class="btn color-rd" Text="Recover Password"></asp:Button>
    
    <div class="clearfloat"></div>

    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>

</asp:Content>
