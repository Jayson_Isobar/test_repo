﻿
<%@ Page Title="Login CMS" Language="C#" MasterPageFile="~/CMS/CMSLogin.master" AutoEventWireup="true"
    CodeFile="LoginCMS.aspx.cs" Inherits="LoginCMS" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   <div class="form-vertical login-form">
          <h3 class="form-title">Login to your account</h3>
          <asp:ValidationSummary ID="vsLogin" runat="server" CssClass="alert alert-error" ValidationGroup="vg" />
          <div class="control-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                 <asp:Label ID="lblError" runat="server" Text="Invalid Username/ Password" Visible="False" ForeColor="Red"></asp:Label>
                   <p></p>
                 <asp:Label ID="lblUserID" runat="server" Visible="False"></asp:Label>
                 <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="controls">
              <div class="input-icon left">
                <i class="icon-user"></i>
                 <asp:TextBox ID="UserName" placeholder="Username" runat="server" CssClass="m-wrap placeholder-no-fix"></asp:TextBox>
                 <asp:RequiredFieldValidator  ID="rvUN" runat="server" ErrorMessage="UserName is required!" ControlToValidate="UserName" ValidationGroup="vg" Display="None"></asp:RequiredFieldValidator>    
              </div>
            </div>
          </div>
          <div class="control-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="controls">
              <div class="input-icon left">
                    <i class="icon-lock"></i>
                    <asp:TextBox ID="Password" runat="server"  placeholder="Password" CssClass="m-wrap placeholder-no-fix" TextMode="Password" MaxLength="8"></asp:TextBox>
                    <asp:RequiredFieldValidator  ID="rvPass" runat="server" ErrorMessage="Password is required!" ControlToValidate="Password" ValidationGroup="vg" Display="None"></asp:RequiredFieldValidator>
              </div>
            </div>
          </div>
          <div class="form-actions">
              <asp:CheckBox ID="chkRememberMe" runat="server" />Remember Me
              <p></p>
              <asp:Button ID="LoginButton" CssClass="btn blue pull-right" runat="server" CommandName="Login" Text="Log In" ValidationGroup="vg" OnClick ="LoginButton_Click"/>
          </div>
    </div>
</asp:Content>
