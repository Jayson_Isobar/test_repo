﻿<%@ Page Title="View Blog" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewBlog.aspx.cs" Inherits="HeaderNavigationFlyout_ViewBlog" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>Swisse Health & Happiness Blog | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Multivitamins Health & Happiness Blog" />
<meta name="keywords" content="vitamins, multivitamins, nutrition, recipes, yoga, exercise, weight management, lifestyle hacks, natural health, integrative health, vitamin supplements" />
<meta name="description" content="Check out the Swisse Health & Happiness blog for the latest research news, lifestyle articles, recipes and tips from Australia’s No.1 Multivitamin Brand*." />

<link rel="canonical" href="http://www.swisse.com.sg/health-happiness-blog" />
<link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/health-happiness-blog">Health & Happiness Blog</a>
                <span class="mlr">></span>
                <asp:Literal ID="litBreadCrumbs" runat="server"></asp:Literal>
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>

<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">health & Happiness blog</h4>
<p></p>

<asp:Button ID="btnRecommend" runat="server" Text="Recommend This" 
        onclick="btnRecommend_Click" CssClass="btn"></asp:Button>
  <p></p>
<asp:Literal ID="litCtrRecommend" runat="server"></asp:Literal>   
<p></p>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litBlogRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related blogs</h4>
<div class="list">
<ul>
<asp:Literal ID="litRelatedBlog" runat="server"></asp:Literal>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>

</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>

</asp:Content>