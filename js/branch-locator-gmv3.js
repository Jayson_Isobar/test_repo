/* global */
var togbl = 0;
var radius = 1.0; // nearby search (kilometers)
 
// Search Action
$("#bl-search-btn").click(function(e) {
	e.preventDefault();  
	if( !$(this).hasClass("disabled") ) {
		clearOverlays();
		$(".bl-selector").html("");
		if( $("#bl-area").val() == 4){ 
		   if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(
				   getNearbyBranches,
					function() {
						// failed to get a GPS location before timeout!
						alert("failed to get a GPS location before timeout!");
					}, {
						enableHighAccuracy: false,
						timeout: 10000,
						maximumAge: 10000
					});
			} else {
				// no support for geolocation
				alert("no support for geolocation");
			}						
		}
		else {
			initBranchList(1);
		}
		$(".bl-selector").show();
		$(this).addClass('active');
	}
}) 

$("#branch-locator .bl-tab").click(function() {
	if(togbl == 0) {
	$( "#branch-locator .bl-selector-container" ).animate({
		"marginLeft": '-=300px'
	  }, 100, function() {
			$("#branch-locator .bl-selector").css({'overflow':'hidden', 'opacity':'0', 'visibility':'hidden'});
			$("#branch-locator .bl-tab").addClass("blselectionshow");
			$("#branch-locator .bl-tab").removeClass("blselectionhide");
			togbl = 1;
	 });
	}	
	else {
	$( "#branch-locator .bl-selector-container" ).animate({
		"marginLeft": '+=300px'
	  }, 100, function() {
			$("#branch-locator .bl-selector").css({'overflow-y':'scroll', 'overflow-x':'auto', 'opacity':'1', 'visibility':'visible'});
			$("#branch-locator .bl-selector-container").css('left','');
			$("#branch-locator .bl-tab").addClass("blselectionhide");
			$("#branch-locator .bl-tab").removeClass("blselectionshow");
			togbl = 0;
	 });
	}			
}) 

// Selects map view
$("#branch-locator .bl-map-tab").click(function() {
	$(this).addClass("selectedtab");
	$("#branch-locator .bl-seach-tab").removeClass("selectedtab");
	$("#map").css({"position":"inherit" , "left":"0px","display":"block"  })
	$(".bl-search").hide();
	$(".showmap").hide();	
	//$("html, body").animate({ scrollTop: $('#branch-locator').offset().top }, 500);
}) 
	
// Selects search view
$("#branch-locator .bl-seach-tab").click(function() {
	$(this).addClass("selectedtab");
	$("#branch-locator .bl-map-tab").removeClass("selectedtab");
	$("#map").css({"position":"absolute" , "left":"-99999px","display":"block"  })
	$(".bl-search").show();
	$(".showmap").show();
})   
 
// Shows the branch information 
$( "#showesinfo" ).click(function(e) {
  e.preventDefault();  
  $( "#es-image-link" ).trigger( "click" );
});
	
// Disable submit button if both checkboxes are unchecked
$( "#chk-branch, #chk-easy-spot" ).click(function(e) {
	if($( "#chk-branch" ).prop( "checked" ) == false && $( "#chk-easy-spot" ).prop( "checked" ) == false) {
		$("#bl-search-btn").addClass("disabled");
	}
	else {
		$("#bl-search-btn").removeClass("disabled");
	}
});
  
function initBranchList(sbutton) {
  
	var geteasySpot = 0;
	var getbranches = 0;
	var currentArea = $("#bl-area").val();	
	
	var dte = new Date();
	var ns = dte.getTime();
	
	// Load json data from json
    $.ajax({
		   async: false,
            url: "/branch-json.php?v="+ns,
            type: "get",
            dataType: "json"
        }).success(function(data) {	
		
		window.HK  = data["香港"];
		window.KL  = data["九龍"];
		window.NT = data["新界"];

	}).fail(function() {
		alert('Unable to load the json file containing the branch locations.');
	});	
	
	if(sbutton != 1) {	
		if( qs["area"] ) {
			currentArea = qs["area"] ;
			$(".bl-selector").show();
			document.getElementById("o"+currentArea).selected="true";
		}	
	}
			
	if(!sbutton) {
		if(qs["checked"] == "Easy Spot" || qs["checked"] == "Easy_Spot") {
			$( "#chk-easy-spot" ).prop( "checked", true );
			$( "#chk-branch" ).prop( "checked", false );
			$(".bl-selector").show();
		}
		
		if(qs["checked"] == "Full Service" || qs["checked"] == "Full_Service") {
			$( "#chk-easy-spot" ).prop( "checked", false );
			$( "#chk-branch" ).prop( "checked", true );
			$(".bl-selector").show();
		}	
	}
		
	if($( "#chk-easy-spot:checked" ).val() == 1) {
		geteasySpot = 1;
	}

	if($( "#chk-branch:checked" ).val() == 1) {
		getbranches = 1;
	}
	
    // HK
    if(!currentArea || currentArea == 1) {
		getBranchesByArea("hkSelector", geteasySpot, getbranches)  
		if ($( window ).width() >=  768) {
			moveCenter(22.29, 113.96) 
			zoomMap(12, 0.18);
		}
	}

	// Kowloon
	else if(currentArea == 2) {
		getBranchesByArea("kwSelector", geteasySpot, getbranches)  
		if ($( window ).width() >=  768) {
			moveCenter(22.35, 113.96) 
			zoomMap(12, 0.17);
		}
	}
	
	// New Territories
	else if(currentArea == 3) {
		getBranchesByArea("ntSelector", geteasySpot, getbranches)  
		if ($( window ).width() >=  768) {
			moveCenter(22.43, 113.89) 
			zoomMap(11, 0.10);
		}
	}
}

function getBranchesByArea(areaID, geteasySpot, getbranches)  {
			
	var currentArea = $("#bl-area").val();	
	if(currentArea == 1) { myArea = window.HK; }
	else if(currentArea == 2) { myArea = window.KL; }
	else if(currentArea == 3) { myArea = window.NT; }
	
	$(".bl-selector").html('<div id = "'+areaID+'"><dl class="accordion-branchlist"></dl></div>');
	
	for(branch in myArea) {
		var classBranchtext = "";
		if(myArea[branch]['shop'].length >= 15){
			classBranchtext = "bl-branch-text long";
		}else{
			classBranchtext="bl-branch-text";
		}
		renderBranchSelection(branch, myArea[branch]['shop'], classBranchtext, geteasySpot, getbranches);
	}			
}
  
function getNearbyBranches(position) {

	var currLat = position.coords.latitude;
	var currLng = position.coords.longitude;
	
	//currLat = 22.203; // test latitude
	//currLng = 113.958; // test longitude
	
	var geteasySpot = 0;
	var getbranches = 0;
		
	if($( "#chk-easy-spot:checked" ).val() == 1) {
		geteasySpot = 1;
	}

	if($( "#chk-branch:checked" ).val() == 1) {
		getbranches = 1;
	}
	
	$(".bl-selector").html('<div id = "nbSelector"><dl class="accordion-branchlist"></dl></div>');
	for(currArea=1; currArea <= 3; currArea++) {			
		if(currArea == 1) { myArea = window.HK; }
		else if(currArea == 2) { myArea = window.KL; }
		else if(currArea == 3) { myArea = window.NT; }
		
		for(branch in myArea) {
		
			var lat = myArea[branch]['lat'];
			var lng = myArea[branch]['lng'];
			var distance = calcDistance(currLat, currLng, lat, lng);
			
			var classBranchtext = "";
			if(myArea[branch]['shop'].length >= 15){
				classBranchtext = "bl-branch-text long";
			}else{
				classBranchtext="bl-branch-text";
			}
			
			$("#hbranches").append('<div id="'+ branch +'" data-area="'+ currArea +'" data-distance="'+  distance  +'" data-lat="'+ lat +'"  data-lng="'+ lng +'" data-branchtext="'+ myArea[branch]['shop'] +'"></div>');
			if(parseFloat(distance) <= parseFloat(radius)) {
				renderBranchSelection(branch, myArea[branch]['shop'], classBranchtext, geteasySpot, getbranches);
			}
		}	
	}	
	
	var myTvar3=setInterval(function(){
	if(!$( ".bl-selector" ).has( "dd" ).length) {
	     var xdistance = new Array();
		 var i = 0;
		$( "#hbranches div" ).each(function() {
		  var xid = $(this).attr("id");
		  xdistance[i] = parseFloat($(this).attr("data-distance"));
		  i++;
		})
		
	    var s = bubbleSort(xdistance)
		var nearestBranch;
		var branchtext;
	
		$( "#hbranches div" ).each(function() {
		  
			if($(this).attr("data-distance") == xdistance[0]) {
				nearestBranch = $(this).attr("id") 
				branchtext = $(this).attr("data-branchtext");
			}
		})	
		
		var classBranchtext;
		if(branchtext >= 15){
		classBranchtext = "bl-branch-text long";
		}else{
		classBranchtext="bl-branch-text"
		}	
		renderBranchSelection(nearestBranch, branchtext, classBranchtext, geteasySpot, getbranches);
	}
	clearInterval(myTvar3);},3000);
}

function renderBranchSelection(branch, branchtext, classBranchtext, geteasySpot, getbranches) {
	if( geteasySpot == 0 && getbranches == 0  && branch !="") {
		if(branch.substr(0,2) == "es") {
			$(".bl-selector .accordion-branchlist").append('<dd><a href="javascript:void(0);"onclick="showTheShop(\''+branch+'\')"><div class="bl-toggle-icon collapse">+</div><div class="'+classBranchtext+'">'+branchtext+'</div><div class="bl-easy-spot-icon"></div></a><div id="blpanel'+branch+'" class="content"></div></dd>');
			setBranchMarker(branch);
		}
		else {
			$(".bl-selector .accordion-branchlist").append('<dd><a href="javascript:void(0);"  onclick="showTheShop(\''+branch+'\')"><div class="bl-toggle-icon collapse">+</div><div class="'+classBranchtext+'">'+branchtext+'</div></a><div id="blpanel'+branch+'" class="content"></div></dd>');
			setBranchMarker(branch);
		}
	}
	else {
		if(branch.substr(0,2) == "es" && geteasySpot == 1 && branch !="" ){
			$(".bl-selector .accordion-branchlist").append('<dd><a href="javascript:void(0);"onclick="showTheShop(\''+branch+'\')"><div class="bl-toggle-icon collapse">+</div><div class="'+classBranchtext+'">'+branchtext+'</div><div class="bl-easy-spot-icon"></div></a><div id="blpanel'+branch+'" class="content"></div></dd>');
			setBranchMarker(branch);
		}
		
		if(branch.substr(0,2) != "es" && getbranches == 1 && branch !="" ){
			$(".bl-selector .accordion-branchlist").append('<dd><a href="javascript:void(0);"  onclick="showTheShop(\''+branch+'\')"><div class="bl-toggle-icon collapse">+</div><div class="'+classBranchtext+'">'+branchtext+'</div></a><div id="blpanel'+branch+'" class="content"></div></dd>');
			setBranchMarker(branch);
		}		
	}										
}

function setBranchMarker(currBranch) {
	
	var currentArea = $("#bl-area").val();	
	if(currentArea == 1) { myArea = window.HK; }
	else if(currentArea == 2) { myArea = window.KL; }
	else if(currentArea == 3) { myArea = window.NT; }

	for(branch in myArea) {
		if(branch == currBranch) {
			var lat = myArea[branch]['lat'];
			var lng = myArea[branch]['lng'];
			var zoom = 17;
			var name = myArea[branch]['shop'];
			var addr = myArea[branch]['address'];
			addr = addr.replace("{", "<");
			addr = addr.replace("}", ">");
			addr = addr.replace("{u}", "<u>");
			addr = addr.replace("{/u}", "</u>");
			var phone = myArea[branch]['phone'];
			var fax = myArea[branch]['fax'];
			var workingHours = myArea[branch]['workingHours'];;
			workingHours = workingHours.replace("{", "<");
			workingHours = workingHours.replace("}", ">");
			var type = myArea[branch]['type'];
			updateLocation(branch, name, lat, lng, zoom, addr, phone, fax, workingHours, type);
		}	
	}		
}

function showTheShop(currBranch) {

	$( ".infoBox" ).hide();

	if ($("#blpanel"+currBranch).css('display') == 'block' ) {
		  $("#blpanel"+currBranch).slideUp("fast", function() {
			
				$(this).siblings().find(".bl-toggle-icon").html("-");			
				$(this).siblings().find(".bl-toggle-icon").stop().addClass("collapse").removeClass("expand");
				$(this).siblings().find(".bl-branch-text").stop().parent().removeClass('currentbranch');
				$(this).parent().stop().removeClass('currentbranch');
		  
			});	
	}
	else {
		if (currBranch != "") {
		} else {    
			document.getElementById('branchInfo').innerHTML = '';
		}
	
		var currentArea = $("#bl-area").val();	
		
		if(currentArea == 1) { myArea = window.HK; }
		else if(currentArea == 2) { myArea = window.KL; }
		else if(currentArea == 3) { myArea = window.NT; }
		
		for(branch in myArea) {
		
			if(branch == currBranch) {
			
				var lat = myArea[branch]['lat'];
				var lng = myArea[branch]['lng'];
				var zoom = 17;
				zoomMoveToLocation(lat, lng, zoom);
			}
		}		
	
		//clickThroughStore(currBranch);
	
	  // hide show branch selection
	    $("#branch-locator .content").slideUp("fast", function() {
			$(this).siblings().find(".bl-toggle-icon").html("+");
			$(this).siblings().find(".bl-toggle-icon").stop().addClass("collapse").removeClass("expand");
			$(this).siblings().find(".bl-branch-text").stop().parent().removeClass('currentbranch');
			$(this).parent().stop().removeClass('currentbranch');
	    });
		
	    $("#blpanel"+currBranch).slideDown("fast", function() {
			$(this).siblings().find(".bl-toggle-icon").html("-");
			$(this).siblings().find(".bl-toggle-icon").stop().addClass("expand").removeClass("collapse");
			$(this).siblings().find(".bl-branch-text").stop().parent().addClass('currentbranch');
			$(this).parent().stop().addClass('currentbranch');
			
			if(detectBrowser() == "iPhone" || detectBrowser() == "Android") {
				if($("#map").css("position") == "absolute" ) {
					$("html, body").stop().animate({ scrollTop: ($(this).position().top)+300 }, 500);				
				}
			}
		});

		var bindex = $("#blpanel"+currBranch).parent().index()
		
		if(   bindex <= 3) {
			$(".bl-selector").animate({
				scrollTop:   0
			}, 800);
		}
		
		if(   bindex >3 && bindex <= 5) {
			$(".bl-selector").animate({
				scrollTop:   200
			}, 800);
		}	
		
		if(   bindex >5 && bindex <= 10) {
			$(".bl-selector").animate({
				scrollTop:   250
			}, 800);
		}	
		
		if(   bindex >=11) {
			$(".bl-selector").animate({
				scrollTop:   $(".bl-selector").height()
			}, 800);
		}
	}
}

function revealMap() {

	//$("#map").show();
	
	$("#map").css({"position":"inherit" , "left":"0px","display":"block"  })
	
	$(".bl-search").hide();
	$(".showmap").hide();
	$('.bl-seach-tab').removeClass('selectedtab');
	$('.bl-map-tab').addClass('selectedtab');
	
	
	if(detectBrowser() == "iPhone" || detectBrowser() == "Android") {
		$("html, body").animate({ scrollTop: $('#branch-locator').offset().top }, 600);
	}	
	else {
		$("html, body").animate({ scrollTop: 0 }, 600);
	}
}
  
function openPage(){
	/*Sitemap easy spot*/
	$( "#es-image-link" ).trigger( "click" );
}

function showShopInfoByClickingMarker(branch) {
	if(  $( "#blpanel"+branch).css("display") == "none"   ) {
		showTheShop(branch);
	}
}

function clickThroughStore(storeName) {
	dataLayer.push({
	'event': 'view store details',
	'store name': storeName
	});
}
