﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.IO;

using System.Text;
using System.Security.Cryptography; 

public partial class LoginCMS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
            {
                UserName.Text = Request.Cookies["UserName"].Value;
                Password.Attributes["value"] = Request.Cookies["Password"].Value;
            }
        }
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
        SqlConnection con1 = new SqlConnection();
        con1.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string sql1 = "select a.id, a.username from tbl_CMSUser a WHERE a.username=@username AND a.password=@pass";
        SqlCommand cmd1 = new SqlCommand(sql1, con1);
        cmd1.Parameters.AddWithValue("username", UserName.Text);
        cmd1.Parameters.AddWithValue("pass", Encrypt(Password.Text));

        con1.Open();

        SqlDataReader dr = cmd1.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            lblUserID.Text = dt.Rows[0]["id"].ToString();
        }

        Session["CMSuid"] = lblUserID.Text;

        if (Session["CMSuid"] == null || Session["CMSuid"] == "")
        {
            lblError.Visible = true;
        }
        else
        {
            Response.Redirect("CMSHome.aspx");
        }

        con1.Close();

        if (chkRememberMe.Checked)
        {
            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
        }
        else
        {
            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

        }
        Response.Cookies["UserName"].Value = UserName.Text.Trim();
        Response.Cookies["Password"].Value = Password.Text.Trim();
    }
}
