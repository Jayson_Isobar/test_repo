﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductModel
/// </summary>
public class ProductModel : CategoryModel
{
    public string ProductId { get; set; }
 
    public string ProductName { get; set; }
  
}