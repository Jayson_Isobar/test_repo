﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ProductRelatedAges : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
                LoadData();
                LoadDDLProdAge();
        }
    }

    public void LoadDDLProdAge()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetAgeList";
        SqlCommand cmd1 = new SqlCommand(spName, con);
        cmd1.CommandType = CommandType.StoredProcedure;
        con.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
        DataTable dtPC = new DataTable();
        adapter.Fill(dtPC);

        ddlProdAge.DataSource = dtPC;

        ddlProdAge.DataTextField = "agerange";
        ddlProdAge.DataValueField = "id";
        ddlProdAge.DataBind();

        con.Close(); 
    }
 
    protected void btnBackView_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdRelatedAgeList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.Parameters.AddWithValue("@productID", Request.QueryString["cid"]);
                command.CommandType = CommandType.StoredProcedure;
       
                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdAge.DataSource = ds;
        gvProdAge.DataBind();
        con.Close();

    }


    protected void gvProdAgeRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProductRelatedAgeByID";
            cmd.Parameters.AddWithValue("@id", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
      
    }

    protected void gvProdAge_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProdAge.EditIndex = e.NewEditIndex;
        LoadData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductRelatedAges.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProdAge.Visible = false;
        pnlViewProdAge.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProdRelatedAge";
        cmd.Parameters.AddWithValue("@productID", Request.QueryString["cid"].ToString());
        cmd.Parameters.AddWithValue("@prodageID", ddlProdAge.SelectedValue);
        

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("Products.aspx?cid="+ Request.QueryString["cid"].ToString());
    }

    protected void gvProdAge_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProdAge.PageIndex = e.NewPageIndex;
        LoadData();
    }
    public void PendingRecordsGridview_RowDeleting(Object sender, GridViewDeleteEventArgs e)
    {
        Response.Redirect("ProductRelatedAges.aspx?cid=" + Request.QueryString["cid"].ToString());
    } 
}
