﻿<%@ Page Title="Our History" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OurHistory.aspx.cs" Inherits="HeaderNavigationFlyout_OurHistory" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<title>Our History | Swisse Vitamins</title>
<meta property="og:title" content="Our History - Swisse Vitamins" />
<meta name="keywords" content="multivitmains, vitamins, Australian companies, Swisse history, Swisse Australia history, company history, vitamins and supplements, health companies, health brands, health and wellbeing" />
<meta name="description" content="Inspired by the development of natural medicines during a 1960’s trip to Switzerland, Swisse founder Kevin Ring developed the first Swisse product – pollen tablets." />
<link rel="canonical" href="http://www.swisse.com.sg/our-story/our-history" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/our-story">Our Story</a>
                <span class="mlr">></span>
                Our History
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>
<section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">Our History</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litAboutSwisseRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>

<li><a href="/our-story/about-swisse">About Swisse</a></li>
<li><a href="/our-story/our-philosophy">Our Philosophy</a></li>
<li><a href="/our-story/swisse-ambassadors">Swisse Ambassadors</a></li>
<li><a href="/the-science-of-swisse/our-research">Our Research</a></li>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>
</asp:Content>
