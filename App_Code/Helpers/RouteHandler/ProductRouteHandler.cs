﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class ProductRouteHandler : IRouteHandler
{

    /// <summary>
    /// Product route handler
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns></returns>
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {

        var productName = requestContext.RouteData.Values["ProductName"] as string;
        var categoryName = requestContext.RouteData.Values["CategoryName"] as string;
        if (string.IsNullOrEmpty(productName))
            return UrlHelper.GetNotFoundHttpHandler();
        else
        {
            var productService = new ProductService();
            var item =productService.GetProduct(productName, categoryName);
            // Get information about this product
            
            //get all the product list
            if (item == null)
                return UrlHelper.GetNotFoundHttpHandler();
            else
            {
                // Store the Product object in the Items collection
                HttpContext.Current.Items["Product"] = item;

            return BuildManager.CreateInstanceFromVirtualPath("~/ProductDetails.aspx", typeof(Page)) as Page;
           }
        }
   

    }


}