﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StoreLocator.aspx.cs" Inherits="StoreLocator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<link href="<%=CDNdomain %>css/store.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<section class="breadcrumbs">
    <div class="container">
        <p>
            <a href="/index.aspx">Home</a>
            <span class="mlr">></span>
            Store Locator
        </p>
    </div>
</section>   
<section id="storelocator" class="container">
    <h2>Store Locator</h2>
    <div id="stores" class="col_14 fltlft color-dgray">
        <div class="scroll">
            <div id="searchStore">
                <h4>enter Zip Code or Address</h4>
                <div class="formfields">
                    <div class="formfield searchTxt">
                        <asp:textbox ID="tbSearchStore" class="searchbox" runat="server" />
                    </div>
                    <div class="formfield searchBtn">
                        <asp:button ID="btnSearch" class="btn color-rd" text="Use Current Location" runat="server" />
                    </div>
                    <p>Legal copy about use of location infomration lorem ipsum dolorsit lore m ipsum dolor lorem ipsum dolor lorem</p>
                    <h4><span id="totalStores"></span> Results</h4>

                </div>
            </div>

            <div id="storelist">
            </div>
        </div>

    </div>
    <div id="map" class="col_72 fltlft">
        <div id="map-canvas">
        </div>
    </div>
    <div class="clearfloat">
    </div>

</section> 


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerContent" Runat="Server">

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAQF6YVBotMbP2SIUgzLnJixpKhRrc4Zk"></script>
<script type="text/javascript" src="<%=CDNdomain %>js/infobox.js"></script>
<script type="text/javascript" src="<%=CDNdomain %>js/googleMap-gmv3.js"></script>
<script type="text/javascript" src="<%=CDNdomain %>js/store.js"></script>

</asp:Content>

