﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Ingredient : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
        Session["ingID"] = Request.QueryString["id"].ToString();
    }
    public void LoadData()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetIngredient";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                txtTitle.Text = myRow[1].ToString();
                txtContent.Content = myRow[2].ToString();
                txtImage.Text = myRow[3].ToString();
                txtLink.Text = myRow[4].ToString();
            }
        }

        con.Close();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_UpdateIngredient";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
        cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
        cmd.Parameters.AddWithValue("@Content", txtContent.Content);
        cmd.Parameters.AddWithValue("@Image", txtImage.Text);
        cmd.Parameters.AddWithValue("@Link", txtLink.Text);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ViewIngredients.aspx"); 
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewIngredients.aspx");
    }
  
    protected void btnRelatedProducts_Click(object sender, EventArgs e)
    {
        Response.Redirect("IngredientsRelatedProducts.aspx?id=" + Session["ingID"]);
    }
    protected void btnRelatedBlogs_Click(object sender, EventArgs e)
    {
        Response.Redirect("IngredientsRelatedBlogs.aspx?id=" + Session["ingID"]);
    }
}
