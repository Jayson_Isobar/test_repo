// Reviews

// API request configuration

//Production
var api_server = "http://api.bazaarvoice.com/data";
var api_key = "35i153n3vsdlz536o00uz6idb";


//Staging
//var api_server = "http://stg.api.bazaarvoice.com/data";
//var api_key = "dee7cv698wrjhtxga2akf6r7";


// Refer to http://developer.bazaarvoice.com to find the latest version
var api_version = "5.4";
var num_items = 100;
var res_per_page = 10;
var popup = false;
var form = $( "#form1" );
var callBackUrl;
var pager;
var reviewmode = false;
var bvSort = "";
var bvStart = "";

$(document).ready(function () {
    // Allow for API results to be cached
    $.ajaxSetup({ cache: true });


    // Load Submit review default
    setActiveWindow(0, 0, 0);

    // Load Ratings on Grid
    loadProductsReview($(".product-rate"));

    // Load Reviews on Product
    loadProductReview();

    // Load Reviews on selected size changed
    $('#selOption').change(function () {
        $("#productID, #productIDtop, #productIDwrite").html("");
        loadProductReview();
        loadProductsReview($(".product-rate#productIDtop, .product-rate#productID, .product-rate#productIDwrite"));
    });

    // Sort rating from high-low
    $("#sortByDesc").click(function () {
        bvSort = "desc";
        loadProductReview();
    });

    // Sort rating 3 starts above
    $("#sortByStars").click(function () {
        bvStart = "gte3";
        loadProductReview();
    });

    // Show all ratins
    $("#showAll").click(function () {
        loadProductReview();
    });


    function loadProductsReview(target) {
        $("#reviews").fadeOut("slow", function () {
            $("#reviews").html("");
        });
        setTimeout(function () {
            target.each(function () {
                productID = $(this).attr("data-sku")
                $(this).bvOverallRatingWidget({ bvproductID: productID });
            });
            $("#reviews").fadeIn("slow");
        }, 500);
    }

    function loadProductReview() {
        $("#reviews").fadeOut("slow", function () {
            $("#reviews").html("");
        });
        setTimeout(function () {
            productID = $("#productID").attr("data-sku");
            $("#reviews").bvDisplayRatingReviewWidget({ bvproductID: productID });
            $("#reviews").fadeIn("slow");
        }, 500);
    }

    function setActiveWindow(write, confirm, close) {
        $("#review-write").css("display", (write == 0 ? "none" : "block"));
        $("#review-confirm").css("display", (confirm == 0 ? "none" : "block"));
        $("#review-close").css("display", (close == 0 ? "none" : "block"));
    }

    function setFormDefault() {
        $("#popup").fadeOut();
        $("#submitForm").fadeOut();

        $("#usernickname").val("");
        $("#useremail").val("");
        $("#userlocation").val("");
        $("#contextdatavalue_Age").selectedIndex == 0;
        $("#rating").attr("value", "");
        $("#title").val("");
        $("#reviewtext").val("");
        $("input[name=isrecommended][0]").checked = true;

        $("#_nickname").text("");
        $("#_email").text("");
        $("#_location").text("");
        $("#_age").text("");
        $("#_rating").width("0%");
        $("#_date").text("");
        $("#_title").text("");
        $("#_review").text("");
        $("#_recommended").text("");

        $("input.rating_selector").attr('value', '');
        $(".br-widget a").removeClass('star-on');
    }

    // Initialize Rating Widget
    $(".rating_selector").bvRatingSelectorWidget();

    // Validate the form
    var validator = form.validate();

    // Extend validation - Alphanumeric
    $.validator.addMethod("loginRegex", function (value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Nickname must contain only letters, numbers, or dashes.");

    // Nickname validation rules
    $("#usernickname").change(function () {
        $(this).rules("add", {
            loginRegex: true,
            required: true,
            minlength: 5,
            maxlength: 25,
            messages: {
                required: "Please enter your nick name.",
                minlength: "Please enter minimum of 4 characters",
                loginRegex: "Nickname is invalid format"
            }
        });
    });


    $(".showReviews").click(function () {
        $("html, body").delay(500).animate({ scrollTop: $("#ratings").offset().top }, 500);
    });

    $(".showForm").click(function () {
        // Set the submission form action
        form.attr('action', api_server + '/submitreview.json');
        $("input[name='passkey']").attr('value', api_key);

        $("#prodImg").attr("src", $(".product-detail-img img").attr("src"))
        $("#prodName, #prodTitle").text($(".product-detail-wrapper-details h1").text());
        $("#prodDesc").text($(".product-detail-wrapper-details span p").text());

        // Set productID
        $("input[name='productid']").val($("#productID").attr("data-sku"));

        $("#popup").fadeIn();
        $("#submitForm").fadeIn();
        setActiveWindow(1, 0, 0);

        $("html, body").delay(500).animate({ scrollTop: 0 }, 500);
    });

    $("#btnPreview").click(function (evt) {
        evt.preventDefault();
        if (form.valid()) {
            setActiveWindow(0, 1, 0);

            $("#_nickname").text($("#usernickname").val());
            $("#_email").text($("#useremail").val());
            $("#_location").text($("#userlocation").val());
            $("#_age").text($("#contextdatavalue_Age :selected").text());
            $("#_rating").width(($("#rating").val() * 20) + "%");
            $("#_title").text($("#title").val());
            $("#_review").text($("#reviewtext").val());

            var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();

            $("#_date").text(curr_date + " " + m_names[curr_month] + " " + curr_year);

            var isrecommended = "";
            if ($("input[name=isrecommended]:checked").next().text() == "Yes") {
                $("#_recommended").text("Yes, I recommend this product.");
            } else {
                $("#_recommended").text("No, I don't recommend this product.");
            }
        }

        $("html, body").delay(500).animate({ scrollTop: 0 }, 500);
    });

    $("#btnEdit").click(function (evt) {
        evt.preventDefault();
        setActiveWindow(1, 0, 0);

        $("html, body").delay(500).animate({ scrollTop: 0 }, 500);
    });

    // Form Submission
    $("#btnSubmit").click(function () {
        if (form.valid()) {
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize()
            }).done(function (result) {
                // Optionally alert the user of success here...
                setActiveWindow(0, 0, 1);
                $("html, body").delay(500).animate({ scrollTop: 0 }, 500);
                console.log(result);
            }).fail(function () {
                // Optionally alert the user of an error here...
            });
        }
    });


    // Cancel submit review
    $("#btnCancel, #btnClose, #btnCloseForm").click(function () {
        setActiveWindow(0, 0, 0);
        setFormDefault();

        validator.resetForm();
        form[0].reset();
    });

    $("#useremail").change(function () {
        $("input[name='HostedAuthentication_AuthenticationEmail']").val($(this).val());
    });

});

// Truncate a string on word boundaries rather than character boundries. 
// Dustin Mihalik
(function(){
	this.truncate = function truncate(str, length) {
	  var trunc = str;
	  if (trunc && trunc.length > length) {
		  /* Truncate the content of the P, then go back to the end of the previous word. */
		  trunc = trunc.substring(0, length);
		  trunc = trunc.replace(/\w+$/, '');
		  return trunc + "...";
	  } else {
		  return trunc;
	  }
	};
  
	// Round Off
	this.roundoff = function roundoff(num, places) {
	    var multiplier = Math.pow(10, places);
		var num = Math.round(num*multiplier)/multiplier;
		if( parseInt(num) === num) {
			num = num + '.0';
		}
		return num;
	};  
	
	// Date Format
	this.formatDate = function formatDate(str) {
		var fdate = str;
		var myarr = fdate.split("T");
		return myarr[0].replace(/-/g, "/");
	}; 

	// Date Format
	this.formatDateReverse = function formatDateReverse(str) {
		var fdate = str;
		var myarr = fdate.split("T");
		var ndarr = myarr[0].split("-");
		return ndatestr = ndarr[2] + "/" +  ndarr[1]  + "/" + ndarr[0];
	};

	this.stripHtmlTags =  function stripHtmlTags(str) {
		var strippedStr = "";
	    if(str) {
			strippedStr = str.replace(/(<([^>]+)>)/ig,"");
		}
		return strippedStr;
	}	
})();

// Pagination
(function($) {
    $.fn.quickPagination = function(options) {
        var defaults = {
            pageSize: 10,
            currentPage: 1,
            holder: null,
            pagerLocation: "after"
        };
        var options = $.extend(defaults, options);
        return this.each(function() {
            var selector = $(this);
            var pageCounter = 1;
            selector.wrap("<div class='simplePagerContainer'></div>");
            selector.parents(".simplePagerContainer").find("ul.simplePagerNav").remove();
            selector.children().each(function(i) { 
                if (i < pageCounter * options.pageSize && i >= (pageCounter - 1) * options.pageSize) {
                    $(this).addClass("simplePagerPage" + pageCounter);
                } else {
                    $(this).addClass("simplePagerPage" + (pageCounter + 1));
                    pageCounter++;
                }
				
				
            });
            selector.children().hide();
            selector.children(".simplePagerPage" + options.currentPage).show();
            if (pageCounter <= 1) {
                return;
            }
            var pageNav = "<ul class='simplePagerNav'>";
			
			// Added Prev and next links - Jun Armando 10/29/2014 - begin
			if(options.currentPage == 1) {
				pageNav += "<li class='simplePageNav previousPage'><a rel='1' href='#'><</a></li>";
			}
			// Added Prev and next links - Jun Armando 10/29/2014 - end
			
			var extraClass = "";
			var nitem = options.pageSize;
            for (i = 1; i <= pageCounter; i++) {
			
				if (i == 1) { 
					extraClass = " firstPage"; 
					nitem = options.pageSize;
				}
				else if (i == pageCounter) {
					extraClass = " lastPage"; 
					nitem = options.pageSize;
				}
				else {
					extraClass = "";
					nitem = options.pageSize;
				}
				
				if ( i ==  pageCounter-1) {
					nitem = selector.children(".simplePagerPage" + pageCounter).length;
				}
			
                if (i == options.currentPage) {
                    pageNav += "<li class='currentPage simplePageNav" + i + extraClass +"'><a rel='" + i + "' href='#' data-nitem='"+nitem+"'>" + i + "</a></li>";
                } else {
                    pageNav += "<li class='simplePageNav" + i + extraClass + "'><a rel='" + i + "' href='#' data-nitem='"+nitem+"'>" + i + "</a></li>";
                }
				
            }
			
			var nextPage = options.currentPage + 1;
			
			// Added Prev and next links - Jun Armando 10/29/2014 - begin
			if(pageCounter > 1) {
				pageNav += "<li class='simplePageNav nextPage'><a rel='" + nextPage  + "' href='#'>></a></li>";
			}
			// Added Prev and next links - Jun Armando 10/29/2014 - end
			
            pageNav += "</ul>";
            if (!options.holder) {
                switch (options.pagerLocation) {
                    case "before":
                        selector.before(pageNav);
                        break;
                    case "both":
                        selector.before(pageNav);
                        selector.after(pageNav);
                        break;
                    default:
                        selector.after(pageNav);
                }
            } else {
                $(options.holder).append(pageNav);
            }
			
			//center the pager
			var nleftpos = $("body").width()/2 - $(".simplePagerNav").width()/2 - 33;
			$(".simplePagerNav").css({'left':nleftpos + 'px'});
			
			
            selector.parent().find(".simplePagerNav a").bind( "click", function(){
                var clickedLink = $(this).attr("rel");
				
                options.currentPage = clickedLink;
                if (options.holder) {
                    $(this).parent("li").parent("ul").parent(options.holder).find("li.currentPage").removeClass("currentPage");
                    $(this).parent("li").parent("ul").parent(options.holder).find("a[rel='" + clickedLink + "']").parent("li").addClass("currentPage");
                } else {
                    $(this).parent("li").parent("ul").parent(".simplePagerContainer").find("li.currentPage").removeClass("currentPage");
                    $(this).parent("li").parent("ul").parent(".simplePagerContainer").find("a[rel='" + clickedLink + "']").parent("li").addClass("currentPage");
					
					
                }				
				
				// Added Prev and next links - Jun Armando 10/29/2014 - begin
				if(clickedLink > 1 ){
					$(".simplePageNav.previousPage a").attr("rel", clickedLink -1 );
				}
				if(clickedLink < pageCounter){
					$(".simplePageNav.nextPage a").attr("rel", eval(clickedLink - (-1)) );
				}
				$(".simplePageNav.previousPage"). removeClass("currentPage");
				$(".simplePageNav.nextPage"). removeClass("currentPage");
				// Added Prev and next links - Jun Armando 10/29/2014 - end
				
				if ( $(".firstPage").hasClass("currentPage") ) {
					parent.$(".sidePagerNav.prevNav").hide();
					parent.$(".sidePagerNav.nextNav").show();
				}
				if ( $(".lastPage").hasClass("currentPage") ) {
					parent.$(".sidePagerNav.nextNav").hide();
					parent.$(".sidePagerNav.prevNav").show();
				}
				if ( !$(".firstPage").hasClass("currentPage")  &&  !$(".lastPage").hasClass("currentPage") ) {
					parent.$(".sidePagerNav.prevNav").show();
					parent.$(".sidePagerNav.nextNav").show();
					parent.$(".sidePagerNav.nextNav .nextpgnum").text('次の'+res_per_page+'件');
				}
				
				if ( $(".currentPage"). next().hasClass("lastPage") ) {
				
					var n = $(".currentPage a").attr("data-nitem");
				
					parent.$(".sidePagerNav.nextNav .nextpgnum").text('次の'+n+'件');
				}	
				else {
					parent.$(".sidePagerNav.nextNav .nextpgnum").text('次の'+res_per_page+'件');
				}
								
                selector.children().hide();
                selector.find(".simplePagerPage" + clickedLink).show();
                return false;
            });
        });
		
    }
})(jQuery);


// Bazaarvoice widgets
(function($) {
	// Overall Rating
    $.fn.bvOverallRatingWidget = function(options) {
        var defaults = {
			//bvTemplate:"",
			bvproductID:""
        };
        var options = $.extend(defaults, options);
		var bvParam = "passkey=" + api_key + "&apiversion=" + api_version + "&filter=ProductId:" + options.bvproductID + "&stats=NativeReviews" + "&Limit=" + num_items;
        return this.each(function() {
            var selector = $(this);
			var jqxhr = $.getJSON( api_server + "/statistics.json?callback=?", bvParam,
			function(json){
				// Render the template into a variable

                var output = '';
                var Review    

				if(json.TotalResults != 0) {
					//var output = _.template(document.getElementById(options.bvTemplate).innerHTML, json);
                    $.each(json.Results, function() {
                        Review = this;
                        var rating = roundoff(Review.ProductStatistics.NativeReviewStatistics.AverageOverallRating, 0);
                        output += '    <span class="rating">';
                        output += '        <span class="ratingHighlight" style="width:'+100*(rating/5)+'%"></span>';
                        output += '    </span>';
                        output += '    <span class="rating_text">'+ rating + ' / 5</span>';
                    }); 
				} else {
					//var output = _.template(document.getElementById(options.bvTemplate + '_empty').innerHTML, json);
                    output += '    <span class="rating">';
                    output += '        <span class="ratingHighlight"></span>';
                    output += '    </span>';
                    output += '    <span class="rating_text">(0)</span>';
				}

                $(selector).append(output);

			});				
        });	
	}
	
	// Ratings and Reviews
    $.fn.bvDisplayRatingReviewWidget = function(options) {
        var defaults = {bvproductID:""};

        var options = $.extend(defaults, options);

        if(bvSort == "desc"){
            var bvParam = "passkey=" + api_key + "&apiversion=" + api_version + "&filter=DisplayLocale:en_SG&filter=ProductId:" + options.bvproductID + "&Sort=Rating:" + bvSort + "&Limit=" + num_items;
            bvSort = "";
        }else if(bvStart == "gte3"){
            var bvParam = "passkey=" + api_key + "&apiversion=" + api_version + "&filter=DisplayLocale:en_SG&filter=ProductId:" + options.bvproductID + "&filter=Rating:gte:3&Limit=" + num_items;
            bvStart = "";
        }else{
            var bvParam = "passkey=" + api_key + "&apiversion=" + api_version + "&filter=DisplayLocale:en_SG&filter=ProductId:" + options.bvproductID + "&Limit=" + num_items;
            bvSort = "";
            bvStart = "";
        };

        return this.each(function() {
            var selector = $(this);
                var jqxhr = $.getJSON( api_server + "/reviews.json?callback=?", bvParam,
                function(json){
                // Render the template into a variable
                //var output = _.template(document.getElementById(options.bvTemplate).innerHTML, json);
                var output = '';
                var Review    
                
                output += '<ul class="pagination">';
                
                $.each(json.Results, function() {
                    Review = this;
                                
                    var ReviewAuthor = Review.AuthorId;
                                
                    output += '<li class="review">';
           
                    var authorName = "Anonymous"; 
                    if(ReviewAuthor && ReviewAuthor.UserNickname) {
                        authorName = ReviewAuthor.UserNickname;
                    }

                    var IsRecommended = "";
                    if(Review.IsRecommended == true) {
                        IsRecommended = "Yes, I recommend this product";
                    }else if (Review.IsRecommended == false){
                        IsRecommended = "No, I don't recommend this product";
                    }else if (Review.IsRecommended == null){
                        IsRecommended = ""
                    }



                    var gender = "";
                    if(Review.ContextDataValues.Gender){
                        gender = Review.ContextDataValues.Gender.ValueLabe;
                    };

                    output += '<div class="reviewer-wrap">';
                    output += '    <div class="col_21 fltlft"><h4>'+ Review.UserNickname +'</h4>';
                    output += '    <p class="txt-up">'+ Review.ContextDataValues.Age.ValueLabel +', '+ gender +'</p></div>';
                    output += '    <div class="col_64 fltlft">';
                    output += '    <div class="rate">';
                    output += '        <div class="fltlft">';
                    output += '            <span class="rating">';
                    output += '                <span class="ratingHighlight" style="width:'+100*(Review.Rating/5)+'%"></span>';
                    output += '            </span>';
                    output += '            <strong>'+formatDate(Review.SubmissionTime)+'</strong>';
                    output += '        </div>';
                    output += '        <div class="clearfloat"></div>';
                    output += '    </div>';
                    output += '    <p class="txt-up title"><strong>'+truncate(Review.Title, 200)+'</strong></p>';
                    output += '    <p class="desc">'+truncate(Review.ReviewText, 200)+'</p>';
                    output += '    <p class="reco">' + IsRecommended + '</p>';
                    output += '    <blockquote>';
                    output += '        <p>Was this review helpful to you <a href="#" class="txt-bl">Yes</a> | <a href="#" class="txt-bl">No</a></p>';
                    output += '    </blockquote>';
                    output += '    </div>';
                    output += '    <div class="clearfloat"></div>';
                    output += '</div>';  
                          
                    output += '</li>';
                    }); 
                                                                
                output += '</ul>';

                $(selector).append(output);
                                                                
                $("ul.pagination").quickPagination({pagerLocation:"bottom",pageSize:res_per_page});
                                                                
                });                                                           
        });	}
	
	
	// Rating Selector Widget
    $.fn.bvRatingSelectorWidget = function(options) { 
        var defaults = {bvproductID:""};
        var options = $.extend(defaults, options);
		var selector = $(this); 
		// Create Rating Widget
		var widget = "<div class='br-widget'>";
		widget += "<a href='javascript:void(0);' data-rating-value='1' data-rating-text='Poor' class='star-off'><span></span></a>";
		widget += "<a href='javascript:void(0);' data-rating-value='2' data-rating-text='Fair' class='star-off'><span></span></a>";
		widget += "<a href='javascript:void(0);' data-rating-value='3' data-rating-text='Average' class='star-off'><span></span></a>";
		widget += "<a href='javascript:void(0);' data-rating-value='4' data-rating-text='Good' class='star-off'><span></span></a>";
		widget += "<a href='javascript:void(0);' data-rating-value='5' data-rating-text='Excellent' class='star-off'><span></span></a>";
		widget += "</div>";
		//selector.after(widget);
		selector.html(widget);
	    
		selector.parent().find('a').mouseover(function() {
			var currSelected = $(this);
			var rating = currSelected.attr('data-rating-value');	
			currSelected.parent().find('a').each(function() {
				if( $(this).attr('data-rating-value') <=  rating ) {
					$(this).removeClass( 'star-off' );
					$(this).addClass( 'star-on' );
				}
				else{
					$(this).removeClass(  'star-on' );
					$(this).addClass(  'star-off' );
				}
			});
		});
		
		selector.parent().find('a').click(function() { 
			var rating = $(this).attr('data-rating-value');
			var currSelected = $(this);
			currSelected.parents(".formfield").find('input').attr('value', rating);
            currSelected.parents(".overall").find('label.error').hide();
			currSelected.parent().find('a').each(function() {
				if( $(this).attr('data-rating-value') <=  rating ) {
					$(this).removeClass( 'star-off' );
					$(this).addClass( 'star-on' );
				}
				else{
					$(this).removeClass(  'star-on' );
					$(this).addClass(  'star-off' );
				}
			});
		});
		
		selector.parent().mouseout(function() {
			var currSelected = $(this);
			var rating = currSelected.find('input').attr('value');
			if(!rating) {
				currSelected.find('a').removeClass( 'star-on' );
				currSelected.find('a').addClass( 'star-off' );
			}
			else {
				currSelected.find('a').each(function() {
					if( $(this).attr('data-rating-value') <=  rating ) {
						$(this).removeClass( 'star-off' );
						$(this).addClass( 'star-on' );
					}
					else{
						$(this).removeClass( 'star-on' );
						$(this).addClass( 'star-off' );
					}
				});
			}
		});			
	}	
})(jQuery);

(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

$(document).ready(function () {
    $('#tabs a').each(function () {
        $(this).on('click', function () {
            if ($(this).attr('href') == '#retailers') {
                $('.bin').each(function () {
                    if ($(this).attr('href').search('443993171') > 0) {
                        $(this).parent().css('display', 'none');
                    }
                });
            }
        });
    });
});
