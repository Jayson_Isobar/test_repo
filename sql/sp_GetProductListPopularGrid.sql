CREATE PROCEDURE [dbo].[sp_GetProductListPopularGrid]
   @pcatid int,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT 
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN,
			(SELECT COUNT (b.productid) FROM tbl_ProductRecentlyViewed b WHERE b.productid = a.productid) as countPopular
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 AND a.prodcatID = @prodcatID';


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause + ' ORDER BY countPopular DESC ';
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID;
     
	

    
END
