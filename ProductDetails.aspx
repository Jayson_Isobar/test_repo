<%@ Page Title="Product Details" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProductDetails.aspx.cs" Inherits="HeaderNavigationFlyout_ProductDetails" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

    <title><%= Item.MetaTitle %></title>
    <meta property="og:title" content="<%= Item.MetaOpenGraphTitle %>"  />
    <meta name="description" content="<%= Item.MetaDescription %>" />
    <meta name="keywords" content="<%= Item.MetaKeywords %>" />
    <link rel="canonical" href="<%= Item.CanonicalUrl %>" />

    <link href="<%=CDNdomain %>css/featherlight.css" rel="stylesheet" type="text/css">
    <link href="<%=CDNdomain %>css/tabulous.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/featherlight.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/tabulous.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/common.js"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('#tabs').tabulous({
                effect: 'scale'
            });
        });

        //onchange select
        $(document).ready(function () {
            $("#productIDtop, #productIDwrite").attr("data-sku", $('#selOption option:selected').data('sku'));

            BIN.GetBIN($('#selOption option:selected').data('sku'), "#bin_tbl")

            $('#selOption').change(function () {
                var currentSKU = $('#selOption option:selected').attr("data-sku");
                var currentPrice = $('#selOption option:selected').attr("data-price");

                BIN.GetBIN(currentSKU, "#bin_tbl");

                $('#txtID').val($(this).val());
                $('#MainContent_lblDescPrice').text(currentPrice);
                $('#MainContent_litProdSKURatingHeader').text(currentSKU);
                $("#productID, #productIDtop, #productIDwrite").attr("data-sku", currentSKU);
                $("input[name='productid']").val(currentSKU);
            });


            $('.btn h4 a').click(function (e) {
                $("#tabs_container div").addClass("hidescale make_transist").removeClass("showscale");
                $("#tabs ul li a").removeClass("tabulous_active");
                $("#tabs_container #retailers").addClass("showscale").removeClass("hidescale make_transist");
                $("#tabs ul li a[href$='#retailers']").addClass("tabulous_active");
                $('html, body').animate({
                    scrollTop: $("#botholder").offset().top
                }, 1000);
                e.preventDefault();
            });

            //pop-up cancel button
            $('.window, #MainContent_btnCancel').click(function (e) {
                e.preventDefault();
                $('.featherlight, .window').hide();
            });
        })
    </script>

    <link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="product-detail-wrapper">
        <div class="breadcrumbs">
            <div class="container">
                <p class="fltlft">
                    <a href="/index.aspx">Home</a>
                    <span class="mlr">></span>
                    <a href="/products">Our Products</a>
                    <span class="mlr">></span>
                    Product Details
                </p>
                <div class="share-box fltrt" runat="server" visible="false">
                    <p>
                        <span class="mtop3">Share</span>
                        <asp:Literal ID="litSNS" runat="server" />
                    </p>
                    <div class="clearfloat">
                    </div>
                </div>
                <div class="clearfloat">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="product-detail-img">
                <asp:Literal ID="litProdImage" runat="server"></asp:Literal>
            </div>
            <div class="product-detail-wrapper-details">
                <asp:Literal ID="litProductDetail" runat="server"></asp:Literal>
                <div class="product-rate mtop20" id="productIDtop">
                </div>

                <p class="txt-up mtop10">
                    <strong><a href="javascript:void(0);" class="txt-bl showReviews">Read All <span class="data-numreview"></span>Reviews</a> | 
				<a class="showForm txt-bl" href="javascript:void(0);">Write a review</a></strong>
                </p>

                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                <h2 class="mtop20 txt-up">SGD
                <asp:Label ID="lblDescPrice" runat="server" class="data-price"></asp:Label>
                    <span class="txt12">msrp</span>
                </h2>

                <div>
                    <label class="txtsize">size</label>
                    <asp:Literal ID="litProdSizesSKU" runat="server"></asp:Literal>

                    <!--<asp:DropDownList ID="sizeOption" runat="server" CssClass="selOption" AutoPostBack="true">
                </asp:DropDownList>-->

                    <div class="clearfloat">
                    </div>
                    <div class="btn color-gry mtop20">
                        <h4><a href="#">buy now</a></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfloat">
        </div>
    </div>

    <div class="container" id="botholder">
        <div class="col_63 fltlft">
            <div id="tabs" class="trigger">
                <ul>
                    <li><a href="#product-benefits" class="tabulous_active">Product Benefits</a></li>
                    <li><a href="#ingredients" title="">Ingredients</a></li>
                    <li><a href="#directions" title="">Directions</a></li>
                    <li><a href="#retailers" title="">Buy Now</a></li>
                </ul>
                <div id="tabs_container">
                    <div id="product-benefits">
                        <asp:Literal ID="litProductBenefit" runat="server"></asp:Literal>
                        <h4>
                            <span class="txt-rd">Available sizes:</span>
                            <asp:Literal ID="litProductSizes" runat="server"></asp:Literal>
                        </h4>
                    </div>
                    <asp:Literal ID="litIngDir" runat="server"></asp:Literal>

                    <div id="retailers">
                        <p>
                            Purchase now from an online retailer:
                        </p>
                        <span id="bin_tbl" class="table">
                            <!-- bin -->
                            <!-- bin -->
                        </span>

                        <span class="f-store mtop20" style="display: none !important;">
                            <p class="mtop20">
                                Or enter your post code to find a store near you
                            </p>
                            <input name="find" type="text" class="fltlft"><span class="btn color-gry fltlft">
                                <h4><a href="SwisseProductFinder.aspx">Find Store</a></h4>
                            </span>
                            <div class="clearfloat">
                            </div>
                        </span>
                    </div>
                </div>
                <!--End tabs container-->
            </div>
            <!--End tabs-->
        </div>
        <div class="col_321 fltrt">
            <div class="alsolike-wrap">
                <h4 class="txt-c">you might also like</h4>
                <asp:Literal ID="litProductRelatedProducts" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="clearfloat">
        </div>
        <div id="ratings" class="rating-reviews-wrap">
            <div class="rating-reviews-header">
                <div class="col_32 fltlft">
                    <h3>rating & reviews</h3>
                </div>
                <div class="fltrt">
                    <asp:Literal ID="litProdSKURatingHeader" runat="server"></asp:Literal>
                </div>
                <div class="clearfloat">
                </div>
                <p class="txt12">
                    This area allows consumers like you to express their own opinions and comments. Swisse does not represent or warrant the accuracy of any statements or product claims made here, nore endorses any opinions expressed by the reviewer.
                </p>
            </div>
            <div class="sort-review-wrap">
                <div class="col_60 fltlft">
                    <ul>
                        <li>Sort</li>
                        <li><a href="javascript:void(0);" id="sortByDesc">Rating - High to low</a></li>
                        <li><a href="javascript:void(0);" id="showAll">Show</a></li>
                        <li><a href="javascript:void(0);" id="sortByStars">3 Stars and Above</a></li>
                    </ul>
                </div>
                <div class="col_18 fltrt">
                    <h4 class="txt-rt mtop5"><a class="showForm txt-bl" href="javascript:void(0);">write a review</a></h4>
                    <div>
                    </div>
                </div>
                <div class="clearfloat">
                </div>
            </div>
            <div id="reviews"></div>
        </div>
    </div>

    <!-- write a review popo-up -->
    <div id="popup">
    </div>
    <div id="submitForm">
        <input type="hidden" name="apiversion" value="5.4" />
        <input type="hidden" name="passkey" />
        <input type="hidden" name="action" value="submit" />
        <input type="hidden" name="HostedAuthentication_CallbackUrl" value="http://swisse.com.sg" />
        <input type="hidden" name="HostedAuthentication_AuthenticationEmail" value />
        <input type="hidden" name="productid" />
        <input type="hidden" name="locale" value="en_SG" />

        <div id="review-write">
            <h2>write a review</h2>
            <a href="javascript:void(0);" id="btnClose">
                <img src="<%=this.CDNdomain %>images/x-icon2.png" alt="Close" /></a>
            <div class="product-list-view">
                <div class="product-list-view-img fltlft">
                    <img id="prodImg" src="<%=this.CDNdomain %>images/ultivite men.jpg" alt="Ultivite" class="centerImage" height="210" />
                </div>
                <div class="product-list-view-desc fltlft col_63">
                    <h3 id="prodName">Ultivite Men</h3>
                    <p id="prodDesc">Swisse Women’s Ultivite is the only targeted multivitamin to include over 50 premium quality vitamins, minerals, antioxidants and herbal extracts to help maintain a woman’s nutritional health.antioxidants and herbal extracts to help maintain a woman’s nutritional health.                        </p>
                    <div class="product-rate mtop20" id="productIDwrite"></div>
                    <div class="clearfloat">
                    </div>
                    <div class="product-price">
                        <p>$39.00 <span class="txt12">MSRP</span></p>
                    </div>
                    <%--<div class="product-compare">
                            <input name="compare" type="checkbox" value="" class="fltlft" />
                            <p class="fltlft">Compare</p>
                            <div class="clearfloat">
                            </div>
                        </div>--%>
                    <div class="clearfloat"></div>
                </div>
                <div class="clearfloat">
                </div>
            </div>

            <div class="write-review mtop10">
                <p class="remarks">* Mandatory Fields</p>

                <div class="formfield overall">
                    <label>overall rating</label>
                    <div class="rating_selector"></div>
                    <input type="text" id="rating" name="rating" title="Overall rating reqired" value="" required class="hiddenfield" />
                </div>
                <!-- END: overall -->
                <div class="clearfloat"></div>

                <div class="formfield recommended">
                    <label>*Would you recommend this product?</label>
                    <input type="radio" name="isrecommended" value="true" checked="checked" /><span>Yes</span>
                    <input type="radio" name="isrecommended" value="false" /><span>No</span>
                </div>
                <!-- END: recommended -->
                <div class="clearfloat"></div>

                <div class="formfield title">
                    <label>* Review summary</label>
                    <input id="title" name="title" title="You must write a minimum of 4 character and a maximum of 50 characters for this field." type="text" required="" minlength="4" maxlength="50" aria-required="true" class="error" aria-invalid="true" />
                    <span class="caption">Example: This product is great.</span>
                </div>
                <!-- END: title -->
                <div class="clearfloat"></div>

                <div class="formfield reviewtext">
                    <label>* your review</label>
                    <textarea cols="20" id="reviewtext" name="reviewtext" title="Please enter the review with a minimum of 50 characters." rows="2" required="" minlength="50" maxlength="300"></textarea>
                    <span class="caption">You must write at least 50 characters for this field</span>
                </div>
                <!-- END: reviewtext -->
                <div class="clearfloat"></div>
            </div>

            <div class="write-review mtop10">
                <div class="formfield nickname">
                    <label>*Enter nickname:</label>
                    <input id="usernickname" name="usernickname" type="text" title="Please enter your nick name." required="" minlength="4" aria-required="true" class="error" aria-invalid="true" />
                    <span class="caption">Example:Ryanm31. For Privacy reasons, do not use your fullname or email.</span>
                </div>
                <!-- END: nickname -->
                <div class="clearfloat"></div>

                <div class="formfield location">
                    <label>*your location</label>
                    <input id="userlocation" name="userlocation" type="text" title="Please enter your location." required="" minlength="4" aria-required="true" class="error" aria-invalid="true" />
                    <span class="caption">Example: New York, NY</span>
                </div>
                <!-- END: location -->
                <div class="clearfloat"></div>

                <div class="formfield age">
                    <label>*your age</label>
                    <select name="contextdatavalue_Age" id="contextdatavalue_Age" type="text" title="Please select your age." required="" aria-required="true" class="error" aria-invalid="true" />
                    <option value="" selected="selected">Select</option>
                    <option value="17orUnder">Under 18</option>
                    <option value="18to24">19-29</option>
                    <option value="25to34">30-39</option>
                    <option value="35to44">40-49</option>
                    <option value="45to54">50+</option>
                    </select>
                </div>
                <!-- END: age -->
                <div class="clearfloat"></div>

                <div class="formfield email">
                    <label>*your email</label>
                    <input id="useremail" name="useremail" type="email" title="Enter a valid email address" required="" aria-required="true" class="error" aria-invalid="true" />
                    <span class="caption">We will ONLY use your email to notify you in regards to your submission.</span>
                </div>
                <!-- END: email -->
                <div class="clearfloat"></div>

                <div class="formfield terms">
                    <label></label>
                    <input id="agreedtotermsandconditions" title="You must agree with the Terms &amp; Conditions" type="checkbox" name="agreedtotermsandconditions" value="True" required="" />
                    <span>* By selecting this box and the submit button, I agree to the conditions stated in the <a onclick="window.open(this.href,null,'left=50,top=50,width=500,height=500,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" href="/TermsAndConditions.aspx" class="txt-bl">Terms and Conditions.</a></span>
                    <label id="agreedtotermsandconditions-error" class="error" for="agreedtotermsandconditions"></label>
                </div>
                <!-- END: terms -->
                <div class="clearfloat"></div>
            </div>

            <div class="formbuttons">
                <button class="fltlft" id="btnPreview" type="button" name="confirmreview">preview</button>
                <span>
                    <a onclick="window.open(this.href,null,'left=50,top=50,width=500,height=500,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" href="/TermsAndConditions.aspx">Terms and Conditions</a><strong>|</strong>
                    <a onclick="window.open(this.href,null,'left=50,top=50,width=500,height=500,toolbar=1,location=0,resizable=1,scrollbars=1'); return false;" href="/ReviewGuidelines.aspx">Review Guidelines</a>
                </span>
                <button class="fltrt" id="btnCancel" type="button">cancel</button>

            </div>
            <!-- END: terms -->
        </div>
        <!-- END: review-write -->

        <div id="review-confirm">
            <div class="write-review mtop10">
                <h2>Confirm Your Review</h2>
                <div class="reviewer-wrap">
                    <div class="col_21 fltlft">
                        <h4 id="_nickname"></h4>
                        <p id="_email"></p>
                        <p id="_age"></p>
                        <p id="_location"></p>
                    </div>
                    <div class="col_60 fltlft">
                        <div class="rate">
                            <div class="fltlft">
                                <span class="rating">
                                    <span class="ratingHighlight" id="_rating"></span>
                                </span>
                                <strong id="_date"></strong>
                            </div>
                            <div class="clearfloat"></div>
                        </div>
                        <p class="txt-up"><strong id="_title"></strong></p>
                        <p id="_review"></p>
                        <p id="_recommended"></p>
                    </div>
                    <div class="clearfloat"></div>
                </div>
            </div>
            <div class="formbuttons">
                <button class="fltlft" id="btnSubmit" type="button">Submit</button>
                <button class="fltrt" id="btnEdit" type="button">Edit</button>
            </div>
        </div>
        <!-- END: review-confirm -->

        <div id="review-close">
            <div class="write-review mtop10">
                <h2>Thank You</h2>
                <p>Thanks for writing a review about <strong id="prodTitle"></strong></p>
            </div>
            <div class="formbuttons">
                <button class="centerImage" id="btnCloseForm" type="button">Close</button>
            </div>
        </div>
        <!-- END: review-close -->
    </div>

    <!-- Pop-up END -->
</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>

    <!-- SNS -->
    <script type="text/javascript">
        function share(url) {
            var urlEncoded = encodeURI(url);
            window.open(urlEncoded, "Share", "status = 1, height = 600, width = 700, resizable = 0")
        }
    </script>

</asp:Content>
