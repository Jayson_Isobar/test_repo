﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Filter
/// </summary>
public class ProductFilter
{
    public string Type { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }

	public ProductFilter()
	{
		
	}
}