﻿<%@ Page Title="Ingredients Glossary" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="IngredientsGlossary.aspx.cs" Inherits="HeaderNavigationFlyout_IngredientsGlossary" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Ingredients Glossary | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Vitamins Ingredients Glossary" />
<meta name="keywords" content="vitamins, multvitamins, minerals, antioxidants, herbs for health, vitamin supplements, vitamin ingredients, vitamin sources, sources of vitamins," />
<meta name="description" content="Swisse sources premium ingredients from around the globe, developing scientifically validated formulas from a combination of vitamins, antioxidants and herbs." />
<link rel="canonical" href="http://www.swisse.com.sg/the-science-of-swisse/ingredients-glossary" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="breadcrumbs">
    <div class="container">
        <p>
            <a href="/index.aspx">Home</a>
            <span class="mlr">></span>
            Ingredients Glossary
        </p>
    </div>
</div>
<section class="container">
<h2>the science of swisse</h2>
<div class="col_23 fltlft color-dgray mtop20">
<div class="sidebar-menu list">
<h3>The science of Swisse</h3>
<ul>
<li><a href="/the-science-of-swisse/our-research">our research</a></li>
<li><a href="/the-science-of-swisse/our-scientific-partnerships">our scientific partnerships</a></li>
<li><a href="/the-science-of-swisse/clinical-trials">clinical trials</a></li>
<li><a class="current">ingredients glossary</a></li>
<%--<li><a href="FAQs.aspx">faqs</a></li>--%>
<li><a href="SwisseScientificAdvisoryPanel.aspx">swisse scientific advisory panel</a></li>
</ul>
</div>
</div>
<div class="col_722 fltrt mtop20">
  <img src="<%=this.CDNdomain %>images/ingredients.jpg" alt="Ingredients">
<div class="clearfloat"></div>
<asp:Literal ID="litContent" runat="server"></asp:Literal>

</div>
<div class="clearfloat"></div>
</section>

</asp:Content>
