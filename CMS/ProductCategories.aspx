﻿<%@ Page Title="Product Categories" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductCategories.aspx.cs" Inherits="ProductCategories" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT CATEGORY" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Product Categories List" 
        onclick="btnBackView_Click" Visible="False" />
        <p></p>
         <asp:Button ID="btnViewDeleted" runat="server" Text="View Deleted Product Categories List" 
        onclick="btnViewDeleted_Click" />
        <p></p>
    <asp:Panel ID="pnlGVProdCat" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdCat" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdCatRowCommand"
            OnRowEditing="gvProdCat_RowEditing"
            OnPageIndexChanging="gvProdCat_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                        <asp:LinkButton Text="Update Details" ID="lnkUpdate" runat="server" CommandName="Edit"
                        CommandArgument='<%# Eval("prodcatid") %>'/>    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("prodcatid") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Category Name" SortExpression="prodcatName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodcatName" runat="server" Text='<%# Bind("prodcatName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Product Category Description" SortExpression="prodcatDesc">
                    <ItemTemplate>
                        <asp:Label ID="lblprodcatDesc" runat="server" Text='<%# Bind("prodcatDesc") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Status" SortExpression="status">
                    <ItemTemplate>
                        <asp:Label ID="lblprodcatStatus" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdCat" runat="server" Visible="False">
             <asp:Label ID="Label1" runat="server" Text="Product Category Name"></asp:Label>
             <asp:TextBox ID="txtProdCatName" runat="server"></asp:TextBox>
             <p></p>
             <asp:Label ID="Label2" runat="server" Text="Product Category Description"></asp:Label>
             <asp:TextBox ID="txtProdCatDesc" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label3" runat="server" Text="Active ?"></asp:Label>
             <asp:CheckBox ID="cbStatus" runat="server" Checked="True" />
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnUpdate" runat="server" Text="UPDATE" onclick="btnUpdate_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
