﻿<%@ Page Title="View Swisse Ambassadors" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewA.aspx.cs" Inherits="HeaderNavigationFlyout_ViewA" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="<%=CDNdomain %>css/reviews.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <section class="breadcrumbs2">
        <div class="container">
            <p class="fltlft">
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                <a href="/our-story">Our Story</a>
                <span class="mlr">></span>
                <a href="/our-story/swisse-ambassadors">Swisse Ambassadors</a>
                <span class="mlr">></span>
                <asp:Literal ID="litBreadCrumbs" runat="server"></asp:Literal>
            </p>
            <h4 class="fltrt">
                <a href="javascript:window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print"></a>
            </h4>
        </div>
    </section>

  <section class="blog-content">
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">Swisse Ambassadors</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3><asp:Label ID="lblRP" Visible="False" runat="server" Text="Related Products"></asp:Label></h3>
	<asp:Literal ID="litBlogRelatedProd" runat="server"></asp:Literal>
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related ambassadors</h4>
<div class="list">
<ul>
<asp:Literal ID="litRelatedIng" runat="server"></asp:Literal>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="0,0,320,413" href="/products">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>

</asp:Content>

<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script src="http://jqueryvalidation.org/files/lib/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/underscore-min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript" src="<%=CDNdomain %>js/reviews.js"></script>

</asp:Content>