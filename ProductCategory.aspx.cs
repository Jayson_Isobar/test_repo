﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;


public partial class ProductCategory : BaseForm
{

    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    string id = "";
    string Ing = "";
    string Con = "";
    string Age = "";
    string Gender = "";
    ArrayList viewingFilters = new ArrayList();

    protected Item Item
    {
        get
        {
            return HttpContext.Current.Items["Item"] as Item;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        id = Item.Id;
        //id = Request.QueryString["id"].ToString();

        if (!IsPostBack)
        {
            LoadProdCat();
            LoadProdRecentView();
            //LoadData();
            LoadSortPopular();
        }
    }


    public void LoadData()
    {
        id = Request.QueryString["id"].ToString();
       
        ShopProductsCls pc3 = new ShopProductsCls();
        pc3 = ShopProductsCls.GetTotalProdByCategory(id);
        string cat3 = pc3.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }

            ShopProductsCls pc = new ShopProductsCls();
            string Ing = "";
            if (txtIngredients.Text == "")
            {
                Ing = null;
            }
            else
            {
                Ing = txtIngredients.Text;
            }
            string Con = "";
            if (txtConcerns.Text == "")
            {
                Con = null;
            }
            else
            {
                Con = txtConcerns.Text;
            }
            //string Age = "";
            //if (txtAges.Text == "")
            //{
            //    Age = null;
            //}
            //else
            //{
            //    Age = txtAges.Text;
            //}
            //string Gender = "";
            //if (txtGenders.Text == "")
            //{
            //    Gender = null;
            //}
            //else
            //{
            //    Gender = txtGenders.Text;
            //}
          
            string Cat = id;
            pc = ShopProductsCls.GetProdCatFilter2(Cat, Ing, Con, Age, Gender);
            string cat = pc.prodcatName;
            cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
            litProduct.Text = cat;
            litPop.Text = pc.productPop;
          
            ShopProductsCls pc2 = new ShopProductsCls();
            pc2 = ShopProductsCls.GetProdCatByCategory(id);
            string cat2 = pc2.prodcatName;
            litProdCatName.Text = cat2;
        
    }

    public void LoadProdCat()
    {
        ShopProductsCls pc = new ShopProductsCls();
        pc = ShopProductsCls.GetProdCategories();
        string cat = pc.prodcatName;
        litProdCat.Text = cat;
    }

  
    public void LoadProdRecentView() 
    {
        ShopProductsCls prod = new ShopProductsCls();
        prod = ShopProductsCls.GetProductsRecentView();
        if(prod != null){
        string product = prod.product;
        product = product.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProdRecentView.Text = product;
        }
    }

    protected void cbIng_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields();
        LoadSortPopular();
    }

    public void LoadCBDataFields()
    {


        if (cbIng.SelectedIndex != -1)
        {
            txtIngredients.Text = GetCheckBoxListSelections();
            //LoadData();
        }
        else
        {
            txtIngredients.Text = null;
            //LoadData();
        }


    }

    private string GetCheckBoxListSelections()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbIng.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbConcern_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields2();
        LoadSortPopular();
    }

    public void LoadCBDataFields2()
    {


        if (cbConcern.SelectedIndex != -1)
        {
            txtConcerns.Text = GetCheckBoxListSelections2();
            //LoadData();
        }
        else
        {
            txtConcerns.Text = null;
            //LoadData();
        }


    }

    private string GetCheckBoxListSelections2()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbConcern.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbAge_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields3();
        LoadSortPopular();
    }

    public void LoadCBDataFields3()
    {


        if (cbAge.SelectedIndex != -1)
        {
            txtAges.Text = GetCheckBoxListSelections3();
            //LoadData();
        }
        else
        {
            txtAges.Text = null;
            //LoadData();
        }


    }

    private string GetCheckBoxListSelections3()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbAge.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbGender_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields4();
        LoadSortPopular();
    }

    public void LoadCBDataFields4()
    {


        if (cbGender.SelectedIndex != -1)
        {
            txtGenders.Text = GetCheckBoxListSelections4();
            //LoadData();
        }
        else
        {
            txtGenders.Text = null;
            //LoadData();
        }


    }

    private string GetCheckBoxListSelections4()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbGender.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        if (cblSelections.Contains("1"))
        {
            if (!cblSelections.Contains("2"))
            {
                cblSelections.Add("2");
            }

            if (!cblSelections.Contains("3"))
            {
                cblSelections.Add("3");
            }
        }
        if (cblSelections.Contains("2") || cblSelections.Contains("3") && (!cblSelections.Contains("1")))
        {
            cblSelections.Add("1");
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }
    public void LoadConditionIngConAgeGen()
    {

        if (txtIngredients.Text == "")
        {
            Ing = null;
        }
        else
        {
            Ing = txtIngredients.Text;
        }

        if (txtConcerns.Text == "")
        {
            Con = null;
        }
        else
        {
            Con = txtConcerns.Text;
        }

        if (txtAges.Text == "")
        {
            Age = null;
        }
        else
        {
            Age = txtAges.Text;
        }

        if (txtGenders.Text == "")
        {
            Gender = null;
        }
        else
        {
            Gender = txtGenders.Text;
        }
    }
   

    public void LoadSortPopular()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();



        pc = ShopProductsCls.GetProdCatSortPopularGridByCat(id, Ing, Con, Age, Gender);
        string cat = pc.prodcatName;

        //string curCategory = ShopProductsCls.GetCategoryName(Int32.Parse(id));

        //if (curCategory != "")
        //{
        //    ProductFilter pf = new ProductFilter();
        //    pf.Type = "cat";
        //    pf.Name = curCategory;

        //    viewingFilters.Insert(0, pf);
        //}


        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;
        string cat3 = pc.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }

        SetViewingFilters();
    }

    public void LoadSortAlphabetically()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatSortAlphabeticallyGrid(id, Ing, Con);
        string cat = pc.prodcatName;
        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;
        string cat3 = pc.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }
    }

    public void LoadSortPriceASC()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatSortPriceASCGrid(id,Ing, Con);
        string cat = pc.prodcatName;
        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;
        string cat3 = pc.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }
    }

    protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSort.SelectedItem.Text == "Most Popular")
        {
            LoadSortPopular();
        }
        else if (ddlSort.SelectedItem.Text == "Alphabetically")
        {
            LoadSortAlphabetically();
        }
        else if (ddlSort.SelectedItem.Text == "Price")
        {
            LoadSortPriceASC();
        }
    }

    private void SetViewingFilters()
    {
        ProductFilter productFilter;

        foreach (ListItem item in cbIng.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "ing";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }

        }

        foreach (ListItem item in cbConcern.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "concern";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }
        }

        foreach (ListItem item in cbAge.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "age";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }

        }


        foreach (ListItem item in cbGender.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "gender";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }

        }

        if (viewingFilters.Count > 0)
        {
            filterRepeater.DataSource = viewingFilters;
            filterRepeater.DataBind();
            viewingFilterPanel.Visible = true;
        } else {
            viewingFilterPanel.Visible = false;
        }
    }

    public void FilterRemove_Command(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "FilterRemove")
        {
            string[] args = e.CommandArgument.ToString().Split(new char[] { ',' });
            int index = Int32.Parse(args[0]);
            string value = args[1];
            string type = args[2];

            ListItem selectedFilter = null;

            switch (type)
            {
                case "ing":
                    selectedFilter = cbIng.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields();
                    break;
                case "concern":
                    selectedFilter = cbConcern.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields2();
                    break;
                case "age":
                    selectedFilter = cbAge.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields3();
                    break;
                case "gender":
                    selectedFilter = cbGender.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields4();
                    break;
            }
        }
        else if (e.CommandName == "FilterRemoveAll")
        {
            toggleCheckBox(cbIng);
            toggleCheckBox(cbConcern);
            toggleCheckBox(cbAge);
            toggleCheckBox(cbGender);
            LoadCBDataFields();
            LoadCBDataFields2();
            LoadCBDataFields3();
            LoadCBDataFields4();
        }

        LoadSortPopular();
    }

    private void toggleCheckBox(CheckBoxList item, bool check = false)
    {
        foreach (ListItem i in item.Items)
        {
            i.Selected = check;
        }
    }
}

























      
