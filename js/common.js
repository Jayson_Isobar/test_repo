﻿var BIN = {
    GetBIN: function (sku, placeHolder) {
        var str_html;
        $.ajax({
            url: "/BINctrl.aspx",
            type: "post",
            data: "sku=" + sku,
            success: function (ret) {
                $(placeHolder).html(ret);
            },
            error: function () {
                $(placeHolder).html("NO AVAILABLE STOCK");
            }
        });
        return str_html;
    }
};