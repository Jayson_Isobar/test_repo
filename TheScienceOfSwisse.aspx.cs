﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HeaderNavigationFlyout_TheScienceOfSwisse : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadTheScienceOfSwisse();
    }
    public void LoadTheScienceOfSwisse()
    {
        TheScienceOfSwisseCls tsos = new TheScienceOfSwisseCls();
        tsos = TheScienceOfSwisseCls.GetTheScienceOfSwisseCls();
        string science = tsos.thescienceofswisse;

        litContent.Text = science;
    }
}