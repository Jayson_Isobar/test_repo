
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRelatedFAQProducts]    Script Date: 2/20/2015 4:24:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 20, 2015>
-- Description:	<All Related FAQ Products>
-- exec sp_GetRelatedFAQProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRelatedFAQProducts]
@faqid int
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT a.id, b.productName
    FROM tbl_FAQ_RelatedProducts a
	LEFT JOIN tbl_Product b
	ON a.productid = b.productid
    WHERE a.faqid = @faqid
    ORDER BY productName asc
END
