﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;


public partial class ProductIngredient : BaseForm
{

    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    string id = "";
    string Ing = "";
    string Con = "";
    string Age = "";
    string Gender = "";
    string IngCat = "";


    protected Item Item
    {
        get
        {
            return HttpContext.Current.Items["Item"] as Item;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //IngCat = Request.QueryString["ing"].ToString();
        IngCat = Item.Id;
        if (!IsPostBack)
        {
            LoadProdCat();
            LoadProdRecentView();
            //LoadData();
            LoadSortPopular();
        }
    }


    public void LoadData()
    {

        ShopProductsCls pc3 = new ShopProductsCls();
        pc3 = ShopProductsCls.GetTotalProdByCategory(id);
        string cat3 = pc3.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }

        ShopProductsCls pc = new ShopProductsCls();
        string Ing = "";
        if (txtIngredients.Text == "")
        {
            Ing = null;
        }
        else
        {
            Ing = txtIngredients.Text;
        }
        string Con = "";
        if (txtConcerns.Text == "")
        {
            Con = null;
        }
        else
        {
            Con = txtConcerns.Text;
        }
        string Age = "";
        if (txtAges.Text == "")
        {
            Age = null;
        }
        else
        {
            Age = txtAges.Text;
        }
        string Gender = "";
        if (txtGenders.Text == "")
        {
            Gender = null;
        }
        else
        {
            Gender = txtGenders.Text;
        }

        string Cat = id;
        pc = ShopProductsCls.GetProdCatFilter2(Cat, Ing, Con, Age, Gender);
        string cat = pc.prodcatName;
        litProduct.Text = cat;
        litPop.Text = pc.productPop;

        ShopProductsCls pc2 = new ShopProductsCls();
        pc2 = ShopProductsCls.GetProdCatByCategory(id);
        string cat2 = pc2.prodcatName;
        litProdCatName.Text = cat2;

    }

    public void LoadProdCat()
    {
        ShopProductsCls pc = new ShopProductsCls();
        pc = ShopProductsCls.GetProdCategories();
        string cat = pc.prodcatName;

        litProdCat.Text = cat;
    }


    public void LoadProdRecentView()
    {
        ShopProductsCls prod = new ShopProductsCls();
        prod = ShopProductsCls.GetProductsRecentView();
        if (prod != null)
        {
            string product = prod.product.Replace("<img src='", "<img src='" + base.CDNdomain);
            litProdRecentView.Text = product;
        }
    }

    protected void cbIng_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields();
    }

    public void LoadCBDataFields()
    {


        if (cbIng.SelectedIndex != -1)
        {
            txtIngredients.Text = GetCheckBoxListSelections();
           // LoadData();
            LoadSortPopular();
        }
        else
        {
            txtIngredients.Text = null;
            //LoadData();
            LoadSortPopular();
        }


    }

    private string GetCheckBoxListSelections()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbIng.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbConcern_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields2();
    }

    public void LoadCBDataFields2()
    {


        if (cbConcern.SelectedIndex != -1)
        {
            txtConcerns.Text = GetCheckBoxListSelections2();
           // LoadData();
            LoadSortPopular();
        }
        else
        {
            txtConcerns.Text = null;
           // LoadData();
            LoadSortPopular();
        }


    }

    private string GetCheckBoxListSelections2()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbConcern.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbAge_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields3();
    }

    public void LoadCBDataFields3()
    {


        if (cbAge.SelectedIndex != -1)
        {
            txtAges.Text = GetCheckBoxListSelections3();
           // LoadData();
            LoadSortPopular();
        }
        else
        {
            txtAges.Text = null;
           // LoadData();
            LoadSortPopular();
        }


    }

    private string GetCheckBoxListSelections3()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbAge.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbGender_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields4();
    }

    public void LoadCBDataFields4()
    {


        if (cbGender.SelectedIndex != -1)
        {
            txtGenders.Text = GetCheckBoxListSelections4();
           // LoadData();
            LoadSortPopular();
        }
        else
        {
            txtGenders.Text = null;
           // LoadData();
            LoadSortPopular();
        }


    }

    private string GetCheckBoxListSelections4()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbGender.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }
    public void LoadConditionIngConAgeGen()
    {

        if (txtIngredients.Text == "")
        {
            Ing = null;
        }
        else
        {
            Ing = txtIngredients.Text;
        }

        if (txtConcerns.Text == "")
        {
            Con = null;
        }
        else
        {
            Con = txtConcerns.Text;
        }

        if (txtAges.Text == "")
        {
            Age = null;
        }
        else
        {
            Age = txtAges.Text;
        }

        if (txtGenders.Text == "")
        {
            Gender = null;
        }
        else
        {
            Gender = txtGenders.Text;
        }
    }


    public void LoadSortPopular()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatSortPopularGridByCon(id, IngCat, Con);
        string cat = pc.prodcatName;
        litProduct.Text = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        string cat3 = pc.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }
    }

    public void LoadSortAlphabetically()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatSortAlphabeticallyGrid(id, IngCat, Con);
        string cat = pc.prodcatName;
        litProduct.Text = cat;
        string cat3 = pc.totalProduct;
        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }
    }

    public void LoadSortPriceASC()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatSortPriceASCGrid(id, IngCat, Con);

        string cat = pc.prodcatName;
        litProduct.Text = cat;

        string cat3 = pc.totalProduct;

        if (Int32.Parse(cat3) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        }
        else if (Int32.Parse(cat3) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        }
        else if (Int32.Parse(cat3) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }

    }

    protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSort.SelectedItem.Text == "Most Popular")
        {
            LoadSortPopular();
        }
        else if (ddlSort.SelectedItem.Text == "Alphabetically")
        {
            LoadSortAlphabetically();
        }
        else if (ddlSort.SelectedItem.Text == "Price")
        {
            LoadSortPriceASC();
        }
    }
}

























      
