﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class FAQs : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }
    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetFAQsList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvBlog.DataSource = ds;
        gvBlog.DataBind();
        con.Close();
    }

    protected void gvBlogRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteFAQByID";
            cmd.Parameters.AddWithValue("@id", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        else if (e.CommandName == "Edit")
        {
            var someId = e.CommandArgument;
            Response.Redirect("FAQEdit.aspx?id=" + someId);
        }

    }

    protected void gvBlog_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvBlog.EditIndex = e.NewEditIndex;
        LoadData();
    }

    protected void gvBlog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBlog.PageIndex = e.NewPageIndex;
        LoadData();
    }
    public void PendingRecordsGridview_RowDeleting(Object sender, GridViewDeleteEventArgs e)
    {
        Response.Redirect("FAQs.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("FAQ.aspx");
    }
}
