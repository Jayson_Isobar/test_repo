﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Blog : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
        Session["blogID"] = Request.QueryString["id"].ToString();
    }
    public void LoadData()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetBlog";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                txtTitle.Text = myRow[1].ToString();
                txtContent.Content = myRow[2].ToString();
                txtImage.Text = myRow[3].ToString();
                txtLink.Text = myRow[4].ToString();
            }
        }

        con.Close();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_UpdateBlog";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
        cmd.Parameters.AddWithValue("@blogTitle", txtTitle.Text);
        cmd.Parameters.AddWithValue("@blogContent", txtContent.Content);
        cmd.Parameters.AddWithValue("@blogImage", txtImage.Text);
        cmd.Parameters.AddWithValue("@blogLink", txtLink.Text);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ViewBlogs.aspx"); 
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewBlogs.aspx");
    }
    protected void btnRelatedThemes_Click(object sender, EventArgs e)
    {
        Response.Redirect("BlogThemes.aspx?id=" + Session["blogID"]);
    }
    protected void btnRelatedFormats_Click(object sender, EventArgs e)
    {
        Response.Redirect("BlogFormats.aspx?id=" + Session["blogID"]);
    }
    protected void btnRelatedHealthAndHappiness_Click(object sender, EventArgs e)
    {
        Response.Redirect("BlogHealthAndHappiness.aspx?id=" + Session["blogID"]);
    }
    protected void btnRelatedProducts_Click(object sender, EventArgs e)
    {
        Response.Redirect("BlogRelatedProducts.aspx?id=" + Session["blogID"]);
    }
    protected void btnRelatedBlogs_Click(object sender, EventArgs e)
    {
        Response.Redirect("BlogRelatedBlogs.aspx?id=" + Session["blogID"]);
    }
}
