﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ContactUs.aspx.cs" Inherits="HeaderNavigationFlyout_ContactUs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Contact Us / Swisse Vitamins</title>
<meta property="og:title" content="Contact Us - Swisse Vitamins" />
<meta name="keywords" content="multivitmains, vitamins, Australian companies, about Swisse, about Swisse Australia, vitamins and supplements, health companies, health brands, health and wellbeing" />
<meta name="description" content="Got a question? Find out how to get in touch with Swisse to learn more about our range of vitamins and supplements." />
<link rel="canonical" href="http://www.swisse.com.sg/our-story/contact-us" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr"> > </span>
                <a href="our-story">Our Story</a>
                <span class="mlr"> > </span>
                Contact Us
            </p>
        </div>
    </div>
<div class="container">
  <div class="col_60 fltlft">
    <h2>Contact Us</h2>
	
	<p>Thank you for your interest in Swisse, Australia's No.1 Multivitamin Brand.</p>
	
	<p>If you have feedback or questions please feel to contact us using one of the methods below:</p>
	
	<div class="create-account-wrap" class="mtop20">
		<h4>BY PHONE</h4>
		<p class="mtop10">Contact our customer service team on our hotline <u><b>8001206145</b></u></p>
    </div>
	
	<div class="create-account-wrap" class="mtop20">
		<h4>BY EMAIL</h4>
		<p class="mtop10">Submit an inquiry via email using our <a href="https://pgconsumercare.secure.force.com/ContactUs/SitesContactUsV2?sid=SwisseSG" target="_blank"><u><b>online form</b></u></a><img class="online-icon" src="<%=this.CDNdomain %>images/online-icon.png"></p>
    </div>
	
	<div class="create-account-wrap" class="mtop20">
		<h4>IMPORTED BY</h4>
		<p class="mtop10"><i>Procter & Gamble Singapore Pte. Ltd.<br />11 Buona Vista Drive, Level 21 The Metropolis Tower 2,<br />Singapore, 138589.</i></p>
    </div>
  </div>
    <div class="col_32 fltrt">
        <a href="Register.aspx">
            <img src="<%=this.CDNdomain %>images/cimg.png">
        </a>
    </div>
    <div class="clearfloat"></div>
</div>



</asp:Content>
