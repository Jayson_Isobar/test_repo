﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for OurStoryCls
/// </summary>
public class OurStoryCls
{
 
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;

    public string ourstory { get; set; }

    public static OurStoryCls GetOurStoryCls()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetOurStoryList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        OurStoryCls rc;
        rc = new OurStoryCls();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                rc.ourstory += "<li><a href='" + myRow[2].ToString() + "'>" + myRow[1].ToString() + "</a></li>";
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }


}


