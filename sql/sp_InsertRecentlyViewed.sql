
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertRecentlyViewed]    Script Date: 2/20/2015 3:20:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 31, 2015>
-- Description:	<Insert Recently Viewed>
-- exec sp_InsertRecentlyViewed
-- =============================================
ALTER PROCEDURE [dbo].[sp_InsertRecentlyViewed]
   
    @userid int,
    @productid int
AS
BEGIN
	
	SET NOCOUNT ON;


	IF EXISTS (SELECT 1 FROM tbl_ProductRecentlyViewed WHERE productid = @productid)  
	BEGIN
		UPDATE tbl_ProductRecentlyViewed 
		SET userid = @userid, productid = @productid 
		WHERE productid = @productid;
	END
	ELSE 
	BEGIN
		INSERT INTO tbl_ProductRecentlyViewed (userid, productid) 
		VALUES ( @userid,
	    @productid);
	END


END
