﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;


public partial class HeaderNavigationFlyout_ShopProducts : BaseForm
{

    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    string Ing = "";
    string Con = "";
    string Age = "";
    string Gender = "";

    ArrayList viewingFilters = new ArrayList();

   

    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!IsPostBack)
        {
            LoadProdCat();
            //LoadTotalItems();
            LoadProdRecentView();
            //LoadData();
            //LoadSortPopular();

            lblTotalItems.Visible = false;

            cbAge.DataBound += new EventHandler(cbAge_DataBound);
            cbConcern.DataBound += new EventHandler(cbConcern_DataBound);
            cbIng.DataBound += new EventHandler(cbIngredient_DataBound);
            cbGender.DataBound += new EventHandler(cbGender_DataBound);
            cbGender.DataBound += (s, ev) => LoadSortPopular();
        }
    }

   

    public void LoadProdCat()
    {
        ShopProductsCls pc = new ShopProductsCls();
        pc = ShopProductsCls.GetProdCategories();
        string cat = pc.prodcatName;

        litProdCat.Text = cat;
    }
    public void LoadConditionIngConAgeGen() {
        
        if (txtIngredients.Text == "")
        {
            Ing = null;
        }
        else
        {
            Ing = txtIngredients.Text;
        }
      
        if (txtConcerns.Text == "")
        {
            Con = null;
        }
        else
        {
            Con = txtConcerns.Text;
        }
       
        if (txtAges.Text == "")
        {
            Age = null;
        }
        else
        {
            Age = txtAges.Text;
        }
       
        if (txtGenders.Text == "")
        {
            Gender = null;
        }
        else
        {
            Gender = txtGenders.Text;
        }
    }

    public void LoadData()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatFilter(Ing, Con, Age, Gender);
        string cat = pc.prodcatName;
        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;
    }

    public void LoadSortPopular()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetProdCatSortPopular(Ing, Con, Age, Gender);
        string cat = pc.prodcatName;
        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;

        //string cat3 = pc.totalProduct;
        //if (Int32.Parse(cat3) > 1)
        //{
        //    lblTotalItems.Text = "SHOWING " + cat3 + " ITEMS";
        //}
        //else if (Int32.Parse(cat3) == 1)
        //{
        //    lblTotalItems.Text = "SHOWING " + cat3 + " ITEM";
        //}
        //else if (Int32.Parse(cat3) == 0)
        //{
        //    lblTotalItems.Text = "NO ITEM/S FOUND";
        //}

        SetViewingFilters();
    }

    public void LoadTotalItems()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen();
        pc = ShopProductsCls.GetTotalProd(Ing, Con, Age, Gender);
        string cat = pc.totalProduct;

        if (Int32.Parse(cat) > 1)
        {
            lblTotalItems.Text = "SHOWING " + cat + " ITEMS";
        }
        else if (Int32.Parse(cat) == 1)
        {
            lblTotalItems.Text = "SHOWING " + cat + " ITEM";
        }
        else if (Int32.Parse(cat) == 0)
        {
            lblTotalItems.Text = "NO ITEM/S FOUND";
        }

     
    }

    public void LoadProdRecentView()
    {
        ShopProductsCls prod = new ShopProductsCls();
        prod = ShopProductsCls.GetProductsRecentView();
        if (prod != null)
        {
            string product = prod.product;
            litProdRecentView.Text = product;
        }
    }

    protected void cbIng_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadData();
        //LoadTotalItems();
        Response.Redirect(AssembleUrl(INGREDIENT_KEY, cbIng));
    }

    public void LoadCBDataFields()
    {
        if (cbIng.SelectedIndex != -1)
        {
            txtIngredients.Text = GetCheckBoxListSelections();
            //LoadData();
            //LoadTotalItems();
        }
        else
        {
            txtIngredients.Text = null;
            //LoadData();
            //LoadTotalItems();
        }


    }

    private string GetCheckBoxListSelections()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbIng.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbConcern_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadCBDataFields2();
        //LoadSortPopular();


        Response.Redirect(AssembleUrl(CONCERN_KEY, cbConcern));
    }

    public void LoadCBDataFields2()
    {
        if (cbConcern.SelectedIndex != -1)
        {
            txtConcerns.Text = GetCheckBoxListSelections2();
            //LoadData();
            //LoadTotalItems();
        }
        else
        {
            txtConcerns.Text = null;
            //LoadData();
            //LoadTotalItems();
        }
    }

    private string GetCheckBoxListSelections2()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbConcern.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }

        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbAge_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadCBDataFields3();
        //LoadSortPopular();
        Response.Redirect(AssembleUrl(AGE_KEY, cbAge));
    }

    public void LoadCBDataFields3()
    {


        if (cbAge.SelectedIndex != -1)
        {
            txtAges.Text = GetCheckBoxListSelections3();
            //LoadData();
            //LoadTotalItems();
        }
        else
        {
            txtAges.Text = null;
            //LoadData();
            //LoadTotalItems();
        }


    }

    private string GetCheckBoxListSelections3()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbAge.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    protected void cbGender_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadCBDataFields4();
        //LoadSortPopular();
        Response.Redirect(AssembleUrl(GENDER_KEY, cbGender));
    }

    public void LoadCBDataFields4()
    {
        if (cbGender.SelectedIndex != -1)
        {
            txtGenders.Text = GetCheckBoxListSelections4();
            //LoadData();
            //LoadTotalItems();
        }
        else
        {
            txtGenders.Text = null;
            //LoadData();
            //LoadTotalItems();
        }
    }

    private string GetCheckBoxListSelections4()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();

        foreach (ListItem item in cbGender.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }
        }

        if (cblSelections.Contains("1"))
        {
            if (!cblSelections.Contains("2"))
            {
                cblSelections.Add("2");
            }

            if (!cblSelections.Contains("3"))
            {
                cblSelections.Add("3");
            }
        }
        if (cblSelections.Contains("2") || cblSelections.Contains("3") && (!cblSelections.Contains("1")))
        {
            cblSelections.Add("1");
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    } 

    public void LoadSortAlphabetically()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen(); 
        pc = ShopProductsCls.GetProdCatSortAlphabetically(Ing, Con);
        string cat = pc.prodcatName;
        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;
    }

    public void LoadSortPriceASC()
    {
        ShopProductsCls pc = new ShopProductsCls();
        LoadConditionIngConAgeGen(); 
        pc = ShopProductsCls.GetProdCatSortPriceASC(Ing, Con);
        string cat = pc.prodcatName;
        cat = cat.Replace("<img src='", "<img src='" + base.CDNdomain);
        litProduct.Text = cat;
    }

    protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSort.SelectedItem.Text == "Most Popular")
        {
            LoadSortPopular();
        }
        else if (ddlSort.SelectedItem.Text == "Alphabetically")
        {
            LoadSortAlphabetically();
        }
        else if (ddlSort.SelectedItem.Text == "Price")
        {
            LoadSortPriceASC();
        }
    }

    private void SetViewingFilters()
    {
        ProductFilter productFilter;

        foreach (ListItem item in cbIng.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "ing";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }

        }

        foreach (ListItem item in cbConcern.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "concern";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }
        }

        foreach (ListItem item in cbAge.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "age";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }

        }


        foreach (ListItem item in cbGender.Items)
        {
            productFilter = new ProductFilter();
            if (item.Selected)
            {
                productFilter.Value = item.Value;
                productFilter.Type = "gender";
                productFilter.Name = item.Text;
                viewingFilters.Add(productFilter);
            }

        }

        if (viewingFilters.Count > 0)
        {
            filterRepeater.DataSource = viewingFilters;
            filterRepeater.DataBind();
            viewingFilterPanel.Visible = true;
        }
        else
        {
            viewingFilterPanel.Visible = false;
        }
    }

    public void FilterRemove_Command(Object sender, CommandEventArgs e)
    {
        if (e.CommandName == "FilterRemove")
        {
            string[] args = e.CommandArgument.ToString().Split(new char[] { ',' });
            int index = Int32.Parse(args[0]);
            string value = args[1];
            string type = args[2];

            ListItem selectedFilter = null;

            switch (type)
            {
                case "ing":
                    selectedFilter = cbIng.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields();
                    break;
                case "concern":
                    selectedFilter = cbConcern.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields2();
                    break;
                case "age":
                    selectedFilter = cbAge.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields3();
                    break;
                case "gender":
                    selectedFilter = cbGender.Items.FindByValue(value);
                    selectedFilter.Selected = false;
                    LoadCBDataFields4();
                    break;
            }
        }
        else if (e.CommandName == "FilterRemoveAll")
        {
            toggleCheckBox(cbIng);
            toggleCheckBox(cbConcern);
            toggleCheckBox(cbAge);
            toggleCheckBox(cbGender);
            LoadCBDataFields();
            LoadCBDataFields2();
            LoadCBDataFields3();
            LoadCBDataFields4();
        }

        LoadSortPopular();
    }

    private void toggleCheckBox(CheckBoxList item, bool check = false)
    {
        foreach (ListItem i in item.Items)
        {
            i.Selected = check;
        }
    }



    #region newCodes

    public const string INGREDIENT_KEY = "ingredients";
    public const string CONCERN_KEY = "concerns";
    public const string AGE_KEY = "ages";
    public const string GENDER_KEY = "genders";

    enum FilterType
    {
        Ingredient,
        Concern,
        Age,
        Gender
    }

    private Dictionary<string, string> GetFilterList(FilterType filterType)
    {
        Dictionary<string, string> filters = new Dictionary<string, string>();
        switch (filterType)
        {
            case FilterType.Ingredient: foreach (ListItem c in cbIng.Items) filters.Add(c.Value, c.Text); break;
            case FilterType.Concern: foreach (ListItem c in cbConcern.Items) filters.Add(c.Value, c.Text); break; ;
            case FilterType.Age: foreach (ListItem c in cbAge.Items) filters.Add(c.Value, c.Text); break;
            case FilterType.Gender: foreach (ListItem c in cbGender.Items) filters.Add(c.Value, c.Text); break;
        }
        return filters;
    }

    private List<string> GetSelectedFilters(CheckBoxList cbxLst)
    {
        List<string> selectedIngredients = new List<string>();
        foreach (ListItem item in cbxLst.Items)
        {
            if (item.Selected)
            {
                selectedIngredients.Add(HttpUtility.UrlEncode(item.Text.Replace(" ", string.Empty)));
            }
        }
        return selectedIngredients;
    }

    private void CheckCheckboxList(CheckBoxList cbl, List<string> s)
    {
        foreach (ListItem item in cbl.Items)
        {
            if (s.Contains(item.Value))
            {
                item.Selected = true;
            }
        }
    }


    private string AssembleUrl(string filterKey, CheckBoxList cbl)
    {
        string ingredientFilter = string.Join(",", GetSelectedFilters(cbl));
        string currentUrl = Request.Url.AbsoluteUri;
        string qs = string.Empty;

        if (currentUrl.Split('?').Count() > 1)
        {
            qs = currentUrl.Split('?')[1];
        }

        NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(qs);
        queryString.Remove(filterKey);

        if (!string.IsNullOrEmpty(ingredientFilter))
        {
            queryString[filterKey] = ingredientFilter;
        }

        string url = string.Format("~{0}?{1}", Request.Url.AbsolutePath, HttpUtility.UrlDecode(queryString.ToString()));

        return url;
    }

    #region event handlers
    protected void cbIngredient_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString[INGREDIENT_KEY]))
        {
            ProcessControlBinding(FilterType.Ingredient, INGREDIENT_KEY, cbIng, txtIngredients);
        }
    }

    protected void cbConcern_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString[CONCERN_KEY]))
        {
            ProcessControlBinding(FilterType.Concern, CONCERN_KEY, cbConcern, txtConcerns);
        }
    }
    protected void cbAge_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString[AGE_KEY]))
        {
            ProcessControlBinding(FilterType.Age, AGE_KEY, cbAge, txtAges);
        }
    }
    protected void cbGender_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString[GENDER_KEY]))
        {
            ProcessControlBinding(FilterType.Gender, GENDER_KEY, cbGender, txtGenders);
        }
    }



    private void ProcessControlBinding(FilterType filterType, string filterKey, CheckBoxList cbl, TextBox txtFilterContainer)
    {
        if (!string.IsNullOrEmpty(Request.QueryString[filterKey]))
        {
            List<string> filters = null;
            filters = Request.QueryString[filterKey].Split(',').ToList();

            var ids = GetFilterList(filterType)
                .Where(c => filters.Contains(c.Value.Replace(" ", string.Empty), StringComparer.CurrentCultureIgnoreCase))
                .Select(c => c.Key).ToList();

            txtFilterContainer.Text = string.Join(",", ids);

            CheckCheckboxList(cbl, ids);
        }

    }
    #endregion

    #endregion
     
}

























      
