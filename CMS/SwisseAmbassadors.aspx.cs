﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class SwisseAmbassadors : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }
    public void LoadData()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetOurStoryContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@link", "SwisseAmbassadors.aspx");
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();

        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                txtContent.Content = myRow[0].ToString();
            }
        }

        con.Close();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_UpdateOurStoryContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@link", "SwisseAmbassadors.aspx");
        cmd.Parameters.AddWithValue("@content", txtContent.Content);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();

        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                txtContent.Content = myRow[0].ToString();
            }
        }

        con.Close();
        Response.Redirect("OurStory.aspx");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("OurStory.aspx");
    }
}
