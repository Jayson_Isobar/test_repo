
GO
/****** Object:  StoredProcedure [dbo].[sp_GetProdPerCategory]    Script Date: 2/20/2015 2:51:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <January 23, 2015>
-- Description:	<All Products>
-- exec sp_GetProdPerCategory[2]
-- =============================================
ALTER PROCEDURE [dbo].[sp_GetProdPerCategory]
  @pcatid nvarchar(500) = null,
   @prodIngID nvarchar(500) = null,
   @prodConcernID nvarchar(500) = null,
   @prodAgeID nvarchar(500) = null,
   @prodGenderID nvarchar(500) = null
AS
BEGIN
	
	SET NOCOUNT ON;
	 DECLARE
	 @BaseQuery nvarchar(max) = N'SELECT DISTINCT 
			a.productid, a.productName, a.productShortDesc, a.productImage, a.productPrice, a.productCount, 
			b.prodcatName, a.productGTIN, a.productSize, a.productForm, CONVERT(VARCHAR(MAX), a.productDesc),
			 a.productSize2
			FROM tbl_Product a
			INNER JOIN tbl_ProductCategory b
			ON a.prodcatid = b.prodcatid
			LEFT JOIN tbl_Product_ProductIngredient c
			ON a.productID = c.productID
			LEFT JOIN tbl_Product_ProductConcern d
			ON a.productID = d.productID
			LEFT JOIN tbl_Product_Age e
			ON a.productID = e.productID
			LEFT JOIN tbl_Product_Gender f
			ON a.productID = f.productID'
    , @ParamList nvarchar(max) = N'@prodcatID int,@prodingredientID nvarchar(500),@prodcnID nvarchar(500),@prodaID nvarchar(500),@prodgID nvarchar(500)'
    , @WhereClause nvarchar(max) = ' WHERE a.deleted = 0 AND a.productStatus = 1 ';

	IF @pcatid IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND a.prodcatID IN  ('+ Convert(VARCHAR(500), @pcatid)+') ';
    END


	IF @prodIngID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND c.productingredientid IN  ('+ Convert(VARCHAR(500), @prodIngID)+') ';
    END

	IF @prodConcernID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND d.prodconcernid IN  ('+ Convert(VARCHAR(500), @prodConcernID)+') ';
    END

	IF @prodAgeID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND e.ageid IN  ('+ Convert(VARCHAR(500), @prodAgeID)+') ';
    END

	IF @prodGenderID IS NOT NULL
    BEGIN
        SET @WhereClause = @WhereClause + ' AND f.genderid IN  ('+ Convert(VARCHAR(500), @prodGenderID)+') ';
    END
	 
	 SET @BaseQuery = @BaseQuery + @WhereClause;
    
    EXECUTE sp_executesql @BaseQuery, @ParamList, @prodcatID = @pcatid, @prodingredientId = @prodIngID, @prodcnID = @prodConcernID, @prodaID = @prodAgeID, @prodgID = @prodGenderID;
     
	
END
