﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RegisterRoutes(RouteTable.Routes);
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    void RegisterRoutes(RouteCollection routes)
    {

        routes.Ignore("images/{*pathInfo}");
        routes.Ignore("js/{*pathInfo}");
        routes.Ignore("fonts/{*pathInfo}");
        routes.Ignore("css/{*pathInfo}");
        routes.Ignore("{resource}.axd/{*pathInfo}");
    
        routes.Add(
                "Global",
                new Route("{SinglePage}", new GlobalRouteHandler())
            );
        
        routes.Add(
            "View Item",
            new Route("{Page}/{*PageUrl}", new ItemRouteHandler())
        );

        routes.Add(
           "Sub Page",
           new Route("{Page}/{PageUrl}/{SubPage}", new SubPageRouteHandler())
       );
      
    }
       
</script>
