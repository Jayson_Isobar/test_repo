﻿<%@ Page Title="The Music Run Swisse Promotion – Terms" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TMRterms.aspx.cs" Inherits="TMRterms" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>The Music Run Swisse Promotion – Terms</title>
<meta property="og:title" content="The Music Run Swisse Promotion – Terms" />
<meta name="keywords" content="The Music Run Swisse Promotion – Terms" />
<meta name="description" content="The Music Run Swisse Promotion – Terms" />
<link rel="canonical" href="" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="breadcrumbs color-blck">
    <div class="container">
    <p>Home &gt; The Music Run Swisse Promotion – Terms</p>
    </div>
</div>
<section class="container">
    <div class="termsContent">
        <h2>The Music Run Swisse Promotion – Terms</h2>
        <p>
            By providing us with your personal information, you consent to the collection, use and/or disclosure of your personal information for the purposes of conducting this promotion, including but not limited to, selecting, notifying and publicizing the details and/or personal information of winners pursuant to the terms and conditions. This information may be used for our future marketing and promotional activities. We reserve the right to appoint third parties to conduct this promotion or our future marketing and promotional activities. You agree that we may provide all necessary information to such third parties to facilitate such service.  We will not use or divulge your personal information other than for the purposes and to the parties set out above and/or where you have given us written permission to do so, unless we are otherwise required to do so by law. You hereby declare that all information provided by you is true, accurate and complete. Any inaccurate, incomplete or false information given or any omission of information required, may at our discretion, render your entry invalid and we may refuse to accept the entry. You are responsible for informing us if there is any change in any of the details that you have provided to us. You agree to indemnify and absolve us of any liability arising out of any use and/or disclosure by us of any inaccurate or incomplete information due to your failure to update us promptly of any changes to your personal information. Trust is a cornerstone of our corporate mission. P&G is committed to maintaining your trust by protecting personal information we collect about you. Further details of P&G’s privacy practices may be found at www.pg.com/privacy/english/privacy_notice.html.
        </p>
        <h2>TERMS & CONDITIONS:</h2>
        <ul>
            <li>This promotion is exclusive to Watsons and Guardian, and is open to everyone except the employees and families of Procter & Gamble (Singapore) Pte Ltd (“P&G”) and its associated advertising agencies</li>
            <li>Multiple and/or photocopied entries are allowed but each entry must be accompanied by an original receipt evidencing the purchase of participating products at any Watsons and Guardian store</li>
            <li>Proof of postage is not proof of receipt</li>
            <li>P&G and its associated advertising agencies shall not be responsible for lost, late or misdirected mail</li>
            <li>Incomplete and/or illegible entries will be disqualified</li>
            <li>Whatsapp a PHOTO of your receipt to 91765490, together with ‘SWISSETMR<space>NAME<space>NRIC<space>RECEIPT NUMBER’ to participate”</li>
            <li>Original receipt must be retained as proof of purchase</li>
            <li>Limited to 1 redemption per receipt</li>
            <li>Entries must reach by 27 March 2015</li>
            <li>Illegible, inaccurate or incomplete entries will be disqualified</li>
            <li>The organizer’s decision is final and no further correspondence will be entertained</li>
            <li>Prizes are not transferrable or exchangeable for cash</li>
            <li>P&G reserve the right to substitute prizes of similar value without prior notice</li>
        </ul>
    </div>
    
<div class="clearfloat"></div>
</section>

</asp:Content>
