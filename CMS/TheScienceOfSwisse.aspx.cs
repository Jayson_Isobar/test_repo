﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class TheScienceOfSwisse : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }
    public void LoadData()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetScienceOfSwisseList";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();

        DataTable dt = new DataTable();
        dt.Load(dr);

        string titleNR = "";

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                titleNR += "<p><a href='" + myRow[2].ToString() + "'><strong>" + myRow[1].ToString() + "</strong></a></p>";
            }
        }

        litTheScienceOfSwisse.Text = titleNR + "</br>";

        con.Close();
    }

    protected void btnIngredients_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewIngredients.aspx");
    }
}
