﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;
using System.Net;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BIN
/// </summary>
public class BIN
{
    public DataTable tbl_BIN = new DataTable();
    public bool hasError = false;
    public string errorMsg = null;
    public int rowCtr = 0;
    private string inStockMessage = "IN STOCK";
    private string noStockMessage = "NO STOCK";
    private string notAvailable = "NO STOCK AVAILABLE";

    public void ParseInfo(string sku){

        bool Haserror = false;
        string errorMsg = null;
            
        try
        {
            string RawURL = ConfigurationManager.AppSettings["binURL"].ToString();
            string url = String.Format(RawURL, sku);
            string xmlData;

            System.Diagnostics.Debug.WriteLine("url=" + url);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            WebResponse response = request.GetResponse();

            using (Stream strm = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(strm))
                {
                    xmlData = reader.ReadToEnd();
                }
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlData);

            XmlNodeList dealers = doc.SelectNodes("//ci_data//dealers//dealer");
            XmlNodeList dealers_items = doc.SelectNodes("//ci_data//dealer_items//dealer_item");

            int int_ctr = 0;
            foreach (XmlNode dealers_items_node in dealers_items)
            {
                String item_dealer_id = dealers_items_node.SelectSingleNode("dealer_id").InnerText;
                foreach (XmlNode dealers_node in dealers)
                {
                    if (item_dealer_id == dealers_node.Attributes["id"].Value)
                    {
                        string availability = dealers_items_node.SelectSingleNode("availability").InnerText;

                        tbl_BIN.Rows.Add(
                            dealers_node.SelectSingleNode("dealer_name").InnerText,
                            dealers_node.SelectSingleNode("dealer_logo").InnerText,
                            dealers_items_node.SelectSingleNode("buy_url").InnerText,
                            dealers_items_node.SelectSingleNode("image_url").InnerText,
                            dealers_items_node.SelectSingleNode("price").InnerText,
                            (dealers_items_node.SelectSingleNode("availability").InnerText.ToString().ToUpper() == "YES") ? inStockMessage : noStockMessage);
                        //for mapping image from local source
                        int_ctr++;
                        break;
                    }
                }
            }
            rowCtr = int_ctr;
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.ToString());
            Haserror = true;
            errorMsg = ex.ToString();
        }
        
    }

	public BIN()
	{
        tbl_BIN.Columns.Add("dealer_name", typeof(string));
        tbl_BIN.Columns.Add("dealer_logo", typeof(string));
        tbl_BIN.Columns.Add("buy_url", typeof(string));
        tbl_BIN.Columns.Add("image_url", typeof(string));
        tbl_BIN.Columns.Add("price", typeof(string));
        tbl_BIN.Columns.Add("availability", typeof(string));       
	}
}
