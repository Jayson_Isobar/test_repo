﻿<%@ Page Title="Events" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Events.aspx.cs" Inherits="HeaderNavigationFlyout_Events" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  <section class="blog-content">
<section class="breadcrumbs2">
<div class="container">
<ul>
<li>Home &rsaquo;</li>
<li>Swisse Lifestyle Blog</li>
<div class="fltrt">
<p class="txt-up"><a onclick="window.print();">Print This <img src="<%=this.CDNdomain %>images/print.jpg" width="24" height="24" alt="Print">
</a></p>
</div>
</ul>
</div>
</section>
<div class="container">
<div class="col_60 fltlft">
<div class="main-content">
<h4 class="txt-rd bdr-bottom inblock">health & Happiness blog</h4>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>
<div class="clearfloat"></div>
<div class="mtop70">
<h3>Related Products</h3>
	<div class="recentlyviewed-box">
    <img src="<%=this.CDNdomain %>images/Sleep.jpg" width="128" height="196" alt="Swisse Ultiboost Sleep" class="centerImage">
<div class="product-details-wrap-recent">
          <p class="product-name">Ultivite Men</p>
          <div class="product-rate">
          <ul class="rating threestar fltlft">
                <li class="one"><a href="#" title="1 Star">1</a></li>
                <li class="two"><a href="#" title="2 Stars">2</a></li>
                <li class="three"><a href="#" title="3 Stars">3</a></li>
                <li class="four"><a href="#" title="4 Stars">4</a></li>
                <li class="five"><a href="#" title="5 Stars">5</a></li>
            </ul>
            <p class="fltlft">(13)</p>
            <div class="clearfloat"></div>
          </div>
          <div class="clearfloat"></div>
          <div class="product-price"><p>$39.00 <span class="txt12">MSRP</span></p></div>
          <div class="clearfloat"></div>
          </div>    
    </div>
	<div class="recentlyviewed-box">
    <img src="<%=this.CDNdomain %>images/Anxiety.jpg" width="128" height="196" alt="Swisse Ultiboost Sleep" class="centerImage">
    <div class="product-details-wrap-recent">
          <p class="product-name">Ultivite Men</p>
          <div class="product-rate">
          <ul class="rating threestar fltlft">
                <li class="one"><a href="#" title="1 Star">1</a></li>
                <li class="two"><a href="#" title="2 Stars">2</a></li>
                <li class="three"><a href="#" title="3 Stars">3</a></li>
                <li class="four"><a href="#" title="4 Stars">4</a></li>
                <li class="five"><a href="#" title="5 Stars">5</a></li>
            </ul>
            <p class="fltlft">(13)</p>
            <div class="clearfloat"></div>
          </div>
          <div class="clearfloat"></div>
          <div class="product-price"><p>$39.00 <span class="txt12">MSRP</span></p></div>
          <div class="clearfloat"></div>
        </div>
    </div>
	<div class="recentlyviewed-box">
    <img src="<%=this.CDNdomain %>images/ultivite men.jpg" width="140" height="210" alt="Swisse Ultiboost Sleep" class="centerImage">
    <div class="product-details-wrap-recent">
          <p class="product-name">Ultivite Men</p>
          <div class="product-rate">
          <ul class="rating threestar fltlft">
                <li class="one"><a href="#" title="1 Star">1</a></li>
                <li class="two"><a href="#" title="2 Stars">2</a></li>
                <li class="three"><a href="#" title="3 Stars">3</a></li>
                <li class="four"><a href="#" title="4 Stars">4</a></li>
                <li class="five"><a href="#" title="5 Stars">5</a></li>
            </ul>
            <p class="fltlft">(13)</p>
            <div class="clearfloat"></div>
          </div>
          <div class="clearfloat"></div>
          <div class="product-price"><p>$39.00 <span class="txt12">MSRP</span></p></div>
          <div class="clearfloat"></div>
        </div>
    </div>  
    <div class="clearfloat"></div>              
</div>
</div>
<div class="col_32 fltrt">
<div class="color-dgray txt-w">
<div class="pd20">
<h4>related</h4>
<div class="list">
<ul>
<li><a href="#">Clinical Trials</a></li>
<li><a href="#">How we source out ingredients</a></li>
<li><a href="#">Swisse Scientific Advisory Panel</a></li>
<li><a href="#">Frequently Asked Questions</a></li>
</ul>
</div>
</div>
</div>
  <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Uniqu Formula" usemap="#Map" class="mtop20">
  <map name="Map">
    <area shape="rect" coords="32,297,162,338" href="#">
  </map>
  </div>
<div class="clearfloat"></div>
</div>
</section>

</asp:Content>
