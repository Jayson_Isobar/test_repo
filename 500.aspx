﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="500.aspx.cs" Inherits="_500" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

<title>Internal Server Error</title>
<meta property="og:title" content="Internal Server Error" />
<meta name="keywords" content="multivitamins, multivite, multivitamin tablets, women's health, men's health, vitamins, supplements, integrative health, natural medicine, traditional chinese medicine, vitamin supplements" />
<meta name="description" content="At Swisse we develop scientifically validated vitamins and supplements specially formulated to help fill nutritional gaps. Australia’s #1 Multivitamin Brand." />
<link rel="canonical" href="http://www.swisse.com.sg/500.aspx" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="errorPage">
        <div class="container">
            <h1>INTERNAL SERVER ERROR</h1>
            <h2>500</h2>
            <p>We’re sorry, something has gone wrong with that request.</p>
        </div>
    </div>
    <div id="errorContent">
        <div class="container">
            <p>Please try returning to the <a href="/index.aspx">HOMEPAGE</a> to start again: <a href="/index.aspx">www.swisse.com.sg</a></p>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footerContent" Runat="Server">
</asp:Content>

