﻿<%@ Page Title="Swisse Ambassadors" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SwisseAmbassadors.aspx.cs" Inherits="HeaderNavigationFlyout_SwisseAmbassadors" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Swisse Ambassadors | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Vitamins Ambassadors" />
<meta name="keywords" content="Willin Low, Rebecca Lim, Ricky Ponting, Lleyton Hewitt, Swisse ambassadors, swisse athletes, swisse role models," />
<meta name="description" content="At Swisse we love to support our Ambassadors – athletes, role models and people in the spotlight who embody our vision of health, happiness and success. " />
<link rel="canonical" href="http://www.swisse.com.sg/our-story/swisse-ambassadors" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
        <div class="breadcrumbs">
            <div class="container">
                <p>
                    <a href="/index.aspx">Home</a>
                    <span class="mlr">></span>
                    <a href="/our-story">Our Story</a>
                    <span class="mlr">></span>
                    Swisse Ambassadors
                </p>
            </div>
        </div>
<div class="container">
<h2>Swisse Ambassadors</h2>
<div class="col_39 fltlft color-dgray mtop20">
<div class="sidebar-menu list">
<h4>our story</h4>
<ul>
    
<li><a href="/our-story/about-swisse">About Swisse</a></li>
<li><a href="/our-story/our-history">our history</a></li>
<li><a href="/our-story/our-philosophy">Our Philosophy</a></li>
<li><a href="/our-story/swisse-ambassadors">Swisse Ambassadors</a></li>
<li><a href="/our-story/corporate-social-responsibility">Swisse Social Responsibility</a></li>

<%--<li><a href="CelebrateLifeEveryday.aspx">celebrate life everyday</a></li>--%>
<li><a href="/our-story/contact-us">contact us</a></li>
</ul>
</div>
</div>

    <asp:Literal ID="litContent" runat="server"></asp:Literal>
</div>

</asp:Content>
