﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_ViewA : BaseForm
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected Item Item
    {
        get
        {
            return HttpContext.Current.Items["Item"] as Item;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var link = Item.Id;
        //Request.QueryString["id"].ToString()
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetAmbassadorsContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@link", link);
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                litBreadCrumbs.Text = "<span>" + myRow[1].ToString() + "</span>";
                litContent.Text = "<h3><strong>" + myRow[1].ToString() + "</strong></h3><p></p>" + myRow[2].ToString().Replace("<img src=\"", "<img src=\"" + base.CDNdomain);


                string spName2 = "";
                spName2 = "sp_GetRelatedProductsAmbassadorList";
                SqlCommand cmd2 = new SqlCommand(spName2, con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@ambassadorID", myRow[0].ToString());
                SqlDataReader dr2 = cmd2.ExecuteReader();
                DataTable dt2 = new DataTable();
                dt2.Load(dr2);

                if (dt2.Rows.Count > 0)
                {
                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        lblRP.Visible = true;
                        litBlogRelatedProd.Text += "<div class='recentlyviewed-box'>" +
                                                        "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" +
                                                            "<img src='" + base.CDNdomain + "images/" + myRow2[2].ToString() + "' width='128' height='196' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                                                        "</a>" +
                                                        "<div class='product-details-wrap-recent'>" +
                                                            "<p class='product-name'>" +
                                                                "<a href='" + UrlHelper.FormatPageUrl(myRow2[0].ToString(), PageType.Type.Product) + "'>" + myRow2[1].ToString() + "</a>" +
                                                            "</p>" +
                                                            "<div class='product-rate' data-sku='" + myRow2[4].ToString() + "'></div>" +
                                                            "<div class='clearfloat'></div>" +
                                                            //"<div class='product-price'><p>SGD " + myRow2[3].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                                                            "<div class='clearfloat'></div>" +
                                                        "</div>" +
                                                    "</div>";

                    }
                }


                string spName3 = "";
                spName3 = "sp_GetRelatedAmbassadors";
                SqlCommand cmd3 = new SqlCommand(spName3, con);
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Parameters.AddWithValue("@ambassadorID", myRow[0].ToString());
                SqlDataReader dr3 = cmd3.ExecuteReader();
                DataTable dt3 = new DataTable();
                dt3.Load(dr3);

                if (dt3.Rows.Count > 0)
                {
                    foreach (DataRow myRow3 in dt3.Rows)
                    {
                        litRelatedIng.Text += "<li><a href='ViewA.aspx?id=" + myRow3[2].ToString() + "'>" + myRow3[1].ToString() + "</a></li>";

                    }
                }
            }

        }

        con.Close();
    }
}