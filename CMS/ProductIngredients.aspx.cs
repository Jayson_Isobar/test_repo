﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ProductIngredients : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["cid"] != null)
            {
                LoadViewpnlViewProdIngredient();
            }
            else
            {
                LoadData();
            }
        }
    }

    protected void btnViewDeleted_Click(object sender, EventArgs e)
    {
        gvProdIngredient.Columns[0].Visible = false;
        btnBackView.Visible = true;
        btnViewDeleted.Visible = false;
        LoadData2();
    }

    protected void btnBackView_Click(object sender, EventArgs e)
    {
        gvProdIngredient.Columns[0].Visible = true;
        btnBackView.Visible = false;
        btnViewDeleted.Visible = true;
        LoadData();
    }

    public void LoadData()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdIngredientList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdIngredient.DataSource = ds;
        gvProdIngredient.DataBind();
        con.Close();

    }

    public void LoadData2()
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);

        string spName = "";

        spName = "sp_GetProdIngredientDeletedList";

        using (con)
        {
            using (SqlCommand command = new SqlCommand(spName, con))
            {
                command.CommandType = CommandType.StoredProcedure;

                con.Open();

                da = new SqlDataAdapter(command);
                da.SelectCommand = command;

                da.Fill(ds);
            }
        }

        gvProdIngredient.DataSource = ds;
        gvProdIngredient.DataBind();
        con.Close();

    }

    protected void gvProdIngredientRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            var someId = e.CommandArgument;
            Response.Redirect("ProductIngredients.aspx?cid=" + someId.ToString(), false);
        }
        else if (e.CommandName == "Delete")
        {
            var someId = e.CommandArgument;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "sp_DeleteProdIngredientByID";
            cmd.Parameters.AddWithValue("@prodingredientID", someId);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Redirect("ProductIngredients.aspx");
        }
    }

    protected void gvProdIngredient_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvProdIngredient.EditIndex = e.NewEditIndex;
        LoadData();
    }

    public void LoadViewpnlViewProdIngredient()
    {
        pnlGVProdIngredient.Visible = false;
        pnlViewProdIngredient.Visible = true;
        btnUpdate.Visible = true;

        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetProdIngredientDetailsByID";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@prodingredientID", Request.QueryString["cid"].ToString());
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            txtProdIngredientName.Text = dt.Rows[0]["prodingredientName"].ToString().Trim();
            txtProdIngredientDesc.Text = dt.Rows[0]["prodingredientDesc"].ToString().Trim();
            if (dt.Rows[0]["prodingredientStatus"].ToString().Trim() == "1")
            {
                cbStatus.Checked = true;
            }
            else {
                cbStatus.Checked = false;
            }
        }
        con.Close();
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_UpdateProdIngredientByID";
        cmd.Parameters.AddWithValue("@prodingredientID", Request.QueryString["cid"]);
        cmd.Parameters.AddWithValue("@prodingredientName", txtProdIngredientName.Text.Trim());
        cmd.Parameters.AddWithValue("@prodingredientDesc", txtProdIngredientDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else{
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@prodingredientStatus", cbStat);

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ProductIngredients.aspx"); 
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProductIngredients.aspx"); 
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnlGVProdIngredient.Visible = false;
        pnlViewProdIngredient.Visible = true;
        btnSave.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "sp_InsertProdIngredient";
        cmd.Parameters.AddWithValue("@prodingredientName", txtProdIngredientName.Text.Trim());
        cmd.Parameters.AddWithValue("@prodingredientDesc", txtProdIngredientDesc.Text.Trim());

        string cbStat = "";
        if (cbStatus.Checked == true)
        {
            cbStat = "1";
        }
        else
        {
            cbStat = "0";
        }

        cmd.Parameters.AddWithValue("@prodingredientStatus", cbStat);

        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("ProductIngredients.aspx"); 
    }

    protected void gvProdIngredient_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProdIngredient.PageIndex = e.NewPageIndex;
        LoadData();
    }
}
