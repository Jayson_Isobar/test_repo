﻿<%@ Page Title="Product Ingredients" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductIngredients.aspx.cs" Inherits="ProductIngredients" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT INGREDIENT" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Product Ingredients List" 
        onclick="btnBackView_Click" Visible="False" />
        <p></p>
         <asp:Button ID="btnViewDeleted" runat="server" Text="View Deleted Product Ingredients List" 
        onclick="btnViewDeleted_Click" />
        <p></p>
    <asp:Panel ID="pnlGVProdIngredient" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdIngredient" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdIngredientRowCommand"
            OnRowEditing="gvProdIngredient_RowEditing"
            OnPageIndexChanging="gvProdIngredient_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                        <asp:LinkButton Text="Update Details" ID="lnkUpdate" runat="server" CommandName="Edit"
                        CommandArgument='<%# Eval("prodingredientid") %>'/>    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("prodingredientid") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Ingredient Name" SortExpression="prodingredientName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodingredientName" runat="server" Text='<%# Bind("prodingredientName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Product Ingredient Description" SortExpression="prodingredientDesc">
                    <ItemTemplate>
                        <asp:Label ID="lblprodingredientDesc" runat="server" Text='<%# Bind("prodingredientDesc") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Status" SortExpression="status">
                    <ItemTemplate>
                        <asp:Label ID="lblprodingredientStatus" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdIngredient" runat="server" Visible="False">
             <asp:Label ID="Label1" runat="server" Text="Product Ingredient Name"></asp:Label>
             <asp:TextBox ID="txtProdIngredientName" runat="server"></asp:TextBox>
             <p></p>
             <asp:Label ID="Label2" runat="server" Text="Product Ingredient Description"></asp:Label>
             <asp:TextBox ID="txtProdIngredientDesc" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label3" runat="server" Text="Active ?"></asp:Label>
             <asp:CheckBox ID="cbStatus" runat="server" Checked="True" />
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnUpdate" runat="server" Text="UPDATE" onclick="btnUpdate_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
