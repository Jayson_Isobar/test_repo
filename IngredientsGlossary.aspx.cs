﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_IngredientsGlossary : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadIngredientsGlossary();
    }
    public void LoadIngredientsGlossary()
    {
        IngredientsGlossaryCls ig = new IngredientsGlossaryCls();
        ig = IngredientsGlossaryCls.GetIngredientsGlossaryCls();
        string ingredientsglossary = ig.ingredientsglossary;

        litContent.Text = ingredientsglossary;
    }
}