﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for IngredientsGlossaryCls
/// </summary>
public class IngredientsGlossaryCls
{
 
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;

    public string ingredientsglossary { get; set; }

   

    public static IngredientsGlossaryCls GetIngredientsGlossaryCls()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetIngredientsGlossaryList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        IngredientsGlossaryCls rc;
        rc = new IngredientsGlossaryCls();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                rc.ingredientsglossary += "<a href='" + UrlHelper.FormatPageUrl(myRow[2].ToString(), PageType.Type.Ingredients) + "'>" +
                                            "<span class='ingredients-list-box " + myRow[4].ToString() + "'>" +
                                                "<div class='" + myRow[3].ToString() + "'></div>" +
                                                "<h3>" + myRow[1].ToString() + "</h3>" +
                                            "</span>" +
                                        "</a>" +
                                        myRow[5].ToString();
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }

  

}


