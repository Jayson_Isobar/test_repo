﻿<%@ Page Title="Reset Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ResetPass.aspx.cs" Inherits="ResetPass" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Reset Password | Swisse Vitamins</title>
<meta property="og:title" content="Reset Password | Swisse Vitamins" />
<meta name="keywords" content="vitamins, multivitamins, natural health, integrative health, vitamin supplements, subscribe, sign-up, membership, database, newsletters" />
<meta name="description" content="Reset Password to Swisse to get the latest research news, lifestyle articles, recipes and tips from Australia’s No.1 Multivitamin Brand*." />
<link rel="canonical" href="http://www.swisse.com.sg/reset-password" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                Reset Password
            </p>
        </div>
    </div>

<section class="container">
  
    <div class="col_60 fltlft">

    <h2>Recover your password</h2>
    <div id="create-account-wrap" class="mtop20">
    <div class="clearfloat"></div>
    <div class="col_32 fltlft">
        <label>Email</label>
        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
        <asp:RegularExpressionValidator ForeColor = "Red" ID="REVEmail" runat="server" ErrorMessage="Please enter only alphanumeric." 
          ControlToValidate="txtEmail" ValidationExpression="^[_.@a-zA-Z0-9]+$" ValidationGroup="LoginUserValidationGroup"></asp:RegularExpressionValidator>
        
        <asp:RequiredFieldValidator ForeColor = "Red" ID="UserNameRequired" runat="server" ControlToValidate="txtEmail"
                    CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="Email is required."
                    ValidationGroup="LoginUserValidationGroup">Email is required</asp:RequiredFieldValidator> 

    </div>
    <div class="clearfloat"></div>
    
    
    <div class="clearfloat"></div>
    </div>
    
    <div class="clearfloat"></div>

    <asp:Button ID="btnSave" runat="server" class="btn color-rd" Text="Save" ValidationGroup="LoginUserValidationGroup"
        onclick="btnSave_Click"></asp:Button>
    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
    <div id="review-close">
        <div class="write-review mtop10">
            <h2>Temporary password sent</h2>
            <p>Please change your password.</p>
        </div>
        <div class="formbuttons">
            <button class="centerImage" id="btnCloseForm" type="button">Close</button>
        </div>
    </div>
</section>

</asp:Content>
<asp:Content ID="footerContent" runat="server" ContentPlaceHolderID="footerContent">
    <script type="text/javascript">
        function ShowMessageNow() {
            $("#review-close").show();
        }
</script>
</asp:Content>
