﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Account_Register" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Subscribe to Swisse | Member Registration</title>
<meta property="og:title" content="Sign-up to hear more from Swisse Multivitamins" />
<meta name="keywords" content="vitamins, multivitamins, natural health, integrative health, vitamin supplements, subscribe, sign-up, membership, database, newsletters" />
<meta name="description" content="Sign-up to Swisse to get the latest research news, lifestyle articles, recipes and tips from Australia’s No.1 Multivitamin Brand*." />
<link rel="canonical" href="http://www.swisse.com.sg/register" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                Register
            </p>
        </div>
    </div>
<section class="container">
  <div class="col_60 fltlft">
      <div class="registerTopContent">
          <h2>create your swisse account</h2>
          <img src="/images/trial-pack.jpg" />
          <p>Enter your info below to register now and we'll send you a coupon valid in participating Watsons and Guardian Pharmacies which you can redeem for a <strong>Free 14 day Trial Pack</strong> from April 1</p>
          <br />
          <div>
              <img src="/images/logo-guardian.png" />
              <img src="/images/logo-watson.png" />
           </div>
          <br />
          <span>Only one free trial pack available per person, only available from April 1 and while stocks last.</span>
      </div>
    <h4 class="mtop15">Your basic information</h4>
    <p class="mtop10">This is what you'll use to sign into the site, recover password and personalize you experience.</p>
   
    <div id="create-account-wrap" class="mtop20 create-account-wrap">
    <%--<div class="col_32 fltlft">
    <label>First Name</label>
    <asp:TextBox ID="txtuName" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ForeColor = "Red" ID="REValphaOnly" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtuName" ValidationExpression="^[a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
      </div>--%>
    <div class="col_32 fltlft">
    <label>First Name</label>
   <asp:TextBox ID="txtfName" runat="server"></asp:TextBox>
   <asp:RegularExpressionValidator ForeColor = "Red" ID="REVFN" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtfName" ValidationExpression="^[a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
    
    </div>
    <div class="col_14 fltrt">
    <label>Last Name</label>
    <asp:TextBox ID="txtlName" runat="server"></asp:TextBox>  
    <asp:RegularExpressionValidator ForeColor = "Red" ID="REVLN" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtlName" ValidationExpression="^[a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
    
    </div>  
    <div class="clearfloat"></div>
    <div class="col_32 fltlft mtop20">
    <label>Email</label>
    <asp:TextBox ID="txtEmail" runat="server" CssClass="textEntry"></asp:TextBox>
    <asp:RegularExpressionValidator ForeColor = "Red" ID="REVEmail" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtEmail" ValidationExpression="^[_.@a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
    
    <p></p>
    <asp:Label ID="lblDuplicateEmail" Visible="false" runat="server" ForeColor="Red">Duplication of E-mail</asp:Label> 
    </div>
      
    <div class="col_32 fltlft mtop20">
    <label>Password</label>
    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>
    <asp:RegularExpressionValidator ForeColor = "Red" ID="REVPass" runat="server" ErrorMessage="Please enter atleast 1 digit and 1 alphabetic character, no special characters." ControlToValidate="txtPassword" ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$"></asp:RegularExpressionValidator>
    
    <p></p>
    <asp:Label ID="lblErrReq" Visible="false" runat="server" ForeColor="Red">Password Required</asp:Label>
    <p></p>
    <asp:Label ID="lblErrPass" Visible="false" runat="server" ForeColor="Red">Must be a Minimum of 8 characters and a Maximum of 15 characters</asp:Label>
    </div>
    <div class="col_14 fltrt mtop20">
    <label>Confirm Password</label>
     <asp:TextBox ID="txtConfirmPass" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>    
    </div>    
    <div class="clearfloat"></div>
    <label class="mtop20">Birthday</label>
   <asp:DropDownList ID="ddlMonth" runat="server" class="fltlft">
        <asp:ListItem Selected="True" Value="0">Month</asp:ListItem>
        <asp:ListItem>January</asp:ListItem>
        <asp:ListItem>February</asp:ListItem>
        <asp:ListItem>March</asp:ListItem>
        <asp:ListItem>April</asp:ListItem>
        <asp:ListItem>May</asp:ListItem>
        <asp:ListItem>June</asp:ListItem>
        <asp:ListItem>July</asp:ListItem>
        <asp:ListItem>August</asp:ListItem>
        <asp:ListItem>September</asp:ListItem>
        <asp:ListItem>October</asp:ListItem>
        <asp:ListItem>November</asp:ListItem>
        <asp:ListItem>December</asp:ListItem>
    </asp:DropDownList>
    <asp:DropDownList ID="ddlyear" runat="server" class="fltlft">
        <asp:ListItem Selected="True" Value="0">Year</asp:ListItem>
        <asp:ListItem>2015</asp:ListItem>
        <asp:ListItem>2014</asp:ListItem>
        <asp:ListItem>2013</asp:ListItem>
        <asp:ListItem>2012</asp:ListItem>
        <asp:ListItem>2011</asp:ListItem>
        <asp:ListItem>2010</asp:ListItem>
        <asp:ListItem>2009</asp:ListItem>
        <asp:ListItem>2008</asp:ListItem>
        <asp:ListItem>2007</asp:ListItem>
        <asp:ListItem>2006</asp:ListItem>
        <asp:ListItem>2005</asp:ListItem>
        <asp:ListItem>2004</asp:ListItem>
        <asp:ListItem>2003</asp:ListItem>
        <asp:ListItem>2002</asp:ListItem>
        <asp:ListItem>2001</asp:ListItem>
        <asp:ListItem>2000</asp:ListItem>
        <asp:ListItem>1999</asp:ListItem>
        <asp:ListItem>1998</asp:ListItem>
        <asp:ListItem>1997</asp:ListItem>
        <asp:ListItem>1996</asp:ListItem>
        <asp:ListItem>1995</asp:ListItem>
        <asp:ListItem>1994</asp:ListItem>
        <asp:ListItem>1993</asp:ListItem>
        <asp:ListItem>1992</asp:ListItem>
        <asp:ListItem>1991</asp:ListItem>
        <asp:ListItem>1990</asp:ListItem>
        <asp:ListItem>1989</asp:ListItem>
        <asp:ListItem>1988</asp:ListItem>
        <asp:ListItem>1987</asp:ListItem>
        <asp:ListItem>1986</asp:ListItem>
        <asp:ListItem>1985</asp:ListItem>
        <asp:ListItem>1984</asp:ListItem>
        <asp:ListItem>1983</asp:ListItem>
        <asp:ListItem>1982</asp:ListItem>
        <asp:ListItem>1981</asp:ListItem>
        <asp:ListItem>1980</asp:ListItem>
        <asp:ListItem>1979</asp:ListItem>
        <asp:ListItem>1978</asp:ListItem>
        <asp:ListItem>1977</asp:ListItem>
        <asp:ListItem>1976</asp:ListItem>
        <asp:ListItem>1975</asp:ListItem>
        <asp:ListItem>1974</asp:ListItem>
        <asp:ListItem>1973</asp:ListItem>
        <asp:ListItem>1972</asp:ListItem>
        <asp:ListItem>1971</asp:ListItem>
        <asp:ListItem>1970</asp:ListItem>
        <asp:ListItem>1969</asp:ListItem>
        <asp:ListItem>1968</asp:ListItem>
        <asp:ListItem>1967</asp:ListItem>
        <asp:ListItem>1966</asp:ListItem>
        <asp:ListItem>1965</asp:ListItem>
        <asp:ListItem>1964</asp:ListItem>
        <asp:ListItem>1963</asp:ListItem>
        <asp:ListItem>1962</asp:ListItem>
        <asp:ListItem>1961</asp:ListItem>
        <asp:ListItem>1960</asp:ListItem>
        <asp:ListItem>1959</asp:ListItem>
        <asp:ListItem>1958</asp:ListItem>
        <asp:ListItem>1957</asp:ListItem>
        <asp:ListItem>1956</asp:ListItem>
        <asp:ListItem>1955</asp:ListItem>
        <asp:ListItem>1954</asp:ListItem>
        <asp:ListItem>1953</asp:ListItem>
        <asp:ListItem>1952</asp:ListItem>
        <asp:ListItem>1951</asp:ListItem>
        <asp:ListItem>1950</asp:ListItem>
        <asp:ListItem>1949</asp:ListItem>
        <asp:ListItem>1948</asp:ListItem>
        <asp:ListItem>1947</asp:ListItem>
        <asp:ListItem>1946</asp:ListItem>
        <asp:ListItem>1945</asp:ListItem>
        <asp:ListItem>1944</asp:ListItem>
        <asp:ListItem>1943</asp:ListItem>
        <asp:ListItem>1942</asp:ListItem>
        <asp:ListItem>1941</asp:ListItem>
        <asp:ListItem>1940</asp:ListItem>
    </asp:DropDownList>
    <div class="clearfloat"></div>
    <label class="mtop20">Post Code</label>
    <asp:TextBox ID="txtPostCode" runat="server"></asp:TextBox>    
     <asp:RegularExpressionValidator ForeColor = "Red" ID="REVPostCode" runat="server" ErrorMessage="Please enter only numeric." ControlToValidate="txtPostCode" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
    
    <div class="clearfloat"></div>     
    <p class="mtop20"><asp:CheckBox ID="cbAgreed" runat="server"></asp:CheckBox>Yes! Yes sign me up tp receive occasional emails from Swisse as well as emails from other trusted P&G Brands which will help me:</p>
    <ul class="mtop15">
    <li>Save money with exclusive coupons</li>
    <li>Get inspired with useful tips & ideas</li>
    <li>Learn about free samples, new products, exciting promotions and other information from P&G and carefully-selected business partners</li>
    </ul>
    <p class="mtop20 txt12">Trust is a cornerstone of our corporate mission, and the success of our business depends on it. P&G is committed to maintaining your trust by protecting personal information we collect about yuo, our consumers.</p>   
    <p class="mtop20 txt12"><a href="http://www.pg.com/privacy/english/privacy_notice.shtml" target="_blank">Click here</a> for the full deatils of our Privacy Statement.</p>
    </div>
   <asp:Button ID="btnRegister" class="btn color-rd" runat="server" 
        Text="Create your account" onclick="btnRegister_Click"></asp:Button>
    <p class="mtop15">Already have an account? <a href="Login.aspx">Sign In</a></p>
    </div>
    <div class="col_32 fltrt">
    <img src="/images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="/images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="0,0,320,413" href="/products">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>
</asp:Content>
