﻿<%@ Page Title="Swisse Product Finder" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SwisseProductFinder.aspx.cs" Inherits="HeaderNavigationFlyout_SwisseProductFinder" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   <section class="breadcrumbs">
<div class="container">
<p>Home</p>
</div>
</section><section class="container">
<h2>Store Locator</h2>
<div class=" col_14 fltlft color-dgray mtop20">
<div class="store-address-wrap">
<div class="store-address">
<h4 class="txt-c">Enter zip code or Address</h4>
<form action="zip" method="get">
<input name="zipcode" type="text">
<div class="btn color-rd mtop20"><h4><a href="#">use current location</a></h4></div>
</form>
<p class="txt12 mtop15">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
<h4 class="mtop5">27 results</h4>
<dl class="num">

	<dt>How much wood would a wood chuck chuck if a wood chuck could chuck wood?</dt>
	<dd>1,000,000</dd>
	
	<dt>What is the air-speed velocity of an unladen swallow?</dt>
	<dd>What do you mean? An African or European swallow?</dd>
	
	<dt>Why did the chicken cross the road?</dt>
	<dd>To get to the other side</dd>
    
	<dt>Why did the chicken cross the road?</dt>
	<dd>To get to the other side</dd>

	<dt>Why did the chicken cross the road?</dt>
	<dd>To get to the other side</dd>
    
	<dt>Why did the chicken cross the road?</dt>
	<dd>To get to the other side</dd>            
	
</dl>
</div>
</div>
</div>
<div class="col_72 fltrt mtop20">
<div class="store-map">
  <img src="<%=this.CDNdomain %>images/storemap.jpg" alt="Store Map"> </div>
</div>
<div class="clearfloat"></div>
</section>


</asp:Content>
