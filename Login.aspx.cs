﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.IO;

public partial class Account_Login : System.Web.UI.Page
{
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
    UserCls user;
    String lnkRegisterURL = ConfigurationManager.AppSettings["lnkRegister"];
    String lnkForgotPassURL = ConfigurationManager.AppSettings["lnkForgotPass"];


    protected void Page_Load(object sender, EventArgs e)
    {
        lnkForgotPass.NavigateUrl = lnkForgotPassURL;
        lnkRegister.NavigateUrl = lnkRegisterURL;
        
        if (Session["regSuccess"] != null)
        {
            lblRegSuccess.Visible = true;
        }
        else {
            lblRegSuccess.Visible = false;
        }

        if (Session["UserName"] != null)
        {
            Response.Redirect("index.aspx");
        }

    }



    protected void btnLogin_Click(object sender, EventArgs e)
    {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();

            string spName2 = "";
            spName2 = "sp_GetFailedAttempt";
            SqlCommand cmd2 = new SqlCommand(spName2, con);
            cmd2.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd2.CommandType = CommandType.StoredProcedure;
            con.Open();
            SqlDataReader dr2 = cmd2.ExecuteReader();

            DataTable dt2 = new DataTable();
            dt2.Load(dr2);

            if (dt2.Rows.Count > 0)
            {
                foreach (DataRow myRow2 in dt2.Rows)
                {
                    int ctrFailed = 0;
                    ctrFailed = string.IsNullOrEmpty(myRow2[0].ToString()) ? 0: Int32.Parse(myRow2[0].ToString());

                    if (ctrFailed < 10)
                    {
                        UserCls GU = new UserCls();
                       
                        string Email = txtEmail.Text;
                        string Password = Encrypt(txtPassword.Text);
                        GU = UserCls.GetUserByEmailPass(Email, Password);

                        if (GU != null)
                        {
                            SqlCommand cmd;

                            // update first logged
                            if (! GU.firstLogged)
                            {
                                cmd = new SqlCommand("sp_UpdateUserFirstLogged", con);
                                cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.ExecuteNonQuery();
                            }


                            string spName = "";
                            spName = "sp_UpdateFailedAttemptBackZero";
                            cmd = new SqlCommand(spName, con);
                            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
                            cmd.CommandType = CommandType.StoredProcedure;
                            SqlDataReader dr = cmd.ExecuteReader();
                            DataTable dt = new DataTable();
                            dt.Load(dr);

                            
                            Session["uid"] = GU.userID;
                            Session["Email"] = GU.userEmail;
                            Session["Pass"] = GU.userPassword;
                            Session["UserName"] = GU.userName;
                            Session["CheckAgreed"] = GU.checkAgreed;
                            if (txtPassword.Text == "Swisse2015!")
                            {
                                Response.Redirect("ResetPassword.aspx");
                            }
                            else
                            {
                                Response.Redirect("index.aspx");
                            }

                        }else{
                            lblErr.Visible = true;
                            string spNameFA = "";
                            spNameFA = "sp_UpdateFailedAttempt";
                            SqlCommand cmdFA = new SqlCommand(spNameFA, con);
                            cmdFA.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
                            cmdFA.CommandType = CommandType.StoredProcedure;
                            SqlDataReader drFA = cmdFA.ExecuteReader();
                            DataTable dtFA = new DataTable();
                            dtFA.Load(drFA);
                        }
                    }else{
                        lblErr.Visible = true;
                        lblErr.Text = "User account locked!";
                    }
                }
            }
                 con.Close();

    }
    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }
   
}






