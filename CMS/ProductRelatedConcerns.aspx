﻿<%@ Page Title="Product Related Concerns" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductRelatedConcerns.aspx.cs" Inherits="ProductRelatedConcerns" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT CONCERN" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Products List" 
        onclick="btnBackView_Click" />
        <p></p>
        
    <asp:Panel ID="pnlGVProdConcern" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdConcern" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdConcernRowCommand"
            OnRowEditing="gvProdConcern_RowEditing"
            OnPageIndexChanging="gvProdConcern_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
            OnRowDeleting="PendingRecordsGridview_RowDeleting">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("id") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Concern Name" SortExpression="prodconcernName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodconcernName" runat="server" Text='<%# Bind("prodconcernName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

               
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdConcern" runat="server" Visible="False">
            
             <asp:Label ID="Label1" runat="server" Text="Product Concern Name"></asp:Label>
             <asp:DropDownList ID="ddlProdConcern" runat="server" AutoPostBack="True">
             </asp:DropDownList>
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
