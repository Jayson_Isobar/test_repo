﻿<%@ Page Title="Our Story" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OurStory.aspx.cs" Inherits="HeaderNavigationFlyout_OurStory" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Our Story | Swisse Vitamins</title>
<meta property="og:title" content="The Swisse Vitamins Story" />
<meta name="keywords" content="multivitmains, vitamins, Australian companies, about Swisse, about Swisse Australia, vitamins and supplements, health companies, health brands, health and wellbeing" />
<meta name="description" content="Founded in the 1960s, Swisse is about more than our scientifically formulated vitamins – we’re a health company helping people celebrate life every day!" />
<link rel="canonical" href="http://www.swisse.com.sg/our-story" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="breadcrumbs">
    <div class="container">
        <p>
            <a href="/index.aspx">Home</a>
            <span class="mlr">></span>
            Our Story
        </p>
    </div>
</div>
<section class="container">
<h2>our story</h2>
<div class="col_14 fltlft color-dgray mtop20">
<div class="sidebar-menu">
<h3>our story</h3>
<ul>
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</ul>
</div>
</div>
  <div class="col_72 fltrt mtop20">
  <a href="/our-story/about-swisse">
      <img src="<%=this.CDNdomain %>images/about-swisse.jpg" alt="About Swisse">
  </a>

  <a href="/our-story/our-philosophy">
      <img src="<%=this.CDNdomain %>images/our-philosophy.jpg" alt="Our Philosophy" class="fltlft" />
  </a>

  <a href="/our-story/our-history">
    <img src="<%=this.CDNdomain %>images/our-history.jpg" alt="Our History" class="fltrt" />
  </a>
  <div class="clearfloat"></div>

  <a href="/our-story/swisse-ambassadors">
    <img src="<%=this.CDNdomain %>images/swisse-ambassadors.jpg" alt="Swisse Ambassadors" class="fltlft" />
  </a>

  <a href="/our-story/corporate-social-responsibility">
    <img src="<%=this.CDNdomain %>images/csr.jpg" alt="Corporate Social Responsibility" class="fltlft" />
  </a>

  <a href="/our-story/contact-us">
      <img src="<%=this.CDNdomain %>images/contact-us.jpg" alt="Contact Us" class="fltrt" />
  </a>
  <div class="clearfloat"></div>
  </div>
  <div class="clearfloat"></div>
  </section>
</asp:Content>
