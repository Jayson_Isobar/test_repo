﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class SubPageRouteHandler : IRouteHandler
{

    /// <summary>
    /// Product route handler
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns></returns>
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {

        var pageUrl = requestContext.RouteData.Values["SubPage"] as string;

        if (string.IsNullOrEmpty(pageUrl) || Path.HasExtension(pageUrl))
            return null;
        else
        {

            var item = XmlHelper.GetItem(pageUrl);
            // Get information about this product

            //get all the product list
            if (item == null)
                return UrlHelper.GetNotFoundHttpHandler();
            else
            {
                // Store the Product object in the Items collection
                HttpContext.Current.Items["Item"] = item;
                switch (item.Type)
                { 
                    case "5":// category : format products/{category name}
                        return BuildManager.CreateInstanceFromVirtualPath("~/ViewIng.aspx", typeof(Page)) as Page;
                 
                    default:
                        return UrlHelper.GetNotFoundHttpHandler();
                }
            }
        }


    }


}