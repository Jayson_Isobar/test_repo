﻿<%@ Page Title="Offers and Promos" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OffersAndPromos.aspx.cs" Inherits="HeaderNavigationFlyout_OffersAndPromos" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<title>Swisse Offers & Promotions | Swisse Vitamins</title>
<meta property="og:title" content="Swisse Multivitamins Offers & Promotions" />
<meta name="keywords" content="vitamins, multivitamins, nutrition, natural health, integrative health, vitamin supplements, products, discounts, offers" />
<meta name="description" content="Check out the current Swisse Offers & Promotions for discounts, new products, samples and chances to WIN from Australia’s No.1 Multivitamin Brand*." />
<link rel="canonical" href="http://www.swisse.com.sg/offers-promos" />

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="breadcrumbs">
        <div class="container">
            <p>
                <a href="/index.aspx">Home</a>
                <span class="mlr">></span>
                Offers & Promos
            </p>
        </div>
    </div><section class="container">
<asp:Literal ID="litContent" runat="server"></asp:Literal>
</section>
    <script>
        $(document).ready(
            function () {
                outboundUrl = 'http://';
                $('section.container').find('a[href^="' + outboundUrl + '"]').attr('target', '_blank');
            }
        );
</script>
</asp:Content>