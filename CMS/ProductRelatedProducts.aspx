﻿<%@ Page Title="Product Related Products" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="ProductRelatedProducts.aspx.cs" Inherits="ProductRelatedProducts" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT RELATED PRODUCT" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Products List" 
        onclick="btnBackView_Click" />
        <p></p>
        
    <asp:Panel ID="pnlGVProdrelatedProd" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProdrelatedProd" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdrelatedProdRowCommand"
            OnRowEditing="gvProdrelatedProd_RowEditing"
            OnPageIndexChanging="gvProdrelatedProd_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
            OnRowDeleting="PendingRecordsGridview_RowDeleting">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("productid") %>'/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
               <asp:TemplateField HeaderText="Product Related Product" SortExpression="productname">
                    <ItemTemplate>
                        <asp:Label ID="lblprodrelatedProdName" runat="server" Text='<%# Bind("productname") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

               
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProdrelatedProd" runat="server" Visible="False">
            
             <asp:Label ID="Label1" runat="server" Text="Product Related Product"></asp:Label>
             <asp:DropDownList ID="ddlProdrelatedProd" runat="server" AutoPostBack="True">
             </asp:DropDownList>
              <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
