﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
public class PageType
{
    public enum Type { Category = 1, Product = 2, ScienceOfSwisse = 4, Ingredients = 5, OurStory = 6, BlogCategory = 7, BlogItem = 8, ProductIngredient = 9, Ambassador = 10, Concern = 11, Ingredient = 12, Promo= 13 };
}
