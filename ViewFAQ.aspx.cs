﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_ViewFAQ : BaseForm
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetFAQContents";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@link", Request.QueryString["id"].ToString());
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                litContent.Text = "<p></p><h3><strong>" + myRow[1].ToString() + "</strong></h3><p></p>" + myRow[2].ToString();


                string spName2 = "";
                spName2 = "sp_GetRelatedProductsFAQList";
                SqlCommand cmd2 = new SqlCommand(spName2, con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@faqid", myRow[0].ToString());
                SqlDataReader dr2 = cmd2.ExecuteReader();
                DataTable dt2 = new DataTable();
                dt2.Load(dr2);

                if (dt2.Rows.Count > 0)
                {
                    foreach (DataRow myRow2 in dt2.Rows)
                    {
                        lblRP.Visible = true;
                        litBlogRelatedProd.Text += "<div class='recentlyviewed-box'>" +
                        "<img src='images/" + myRow2[2].ToString() + "' width='128' height='196' alt='" + myRow2[1].ToString() + "' class='centerImage'>" +
                            " <div class='product-details-wrap-recent'>" +
                            "  <p class='product-name'>" + myRow2[1].ToString() + "</p>" +
                             " <div class='product-rate'>" +
                           "  <span class='rating'></span>" +
                               " <div class='clearfloat'></div>" +
                             " </div>" +
                              "<div class='clearfloat'></div>" +
                              "<div class='product-price'><p>SGD " + myRow2[3].ToString() + "<span class='txt12'> MSRP</span></p></div>" +
                              "<div class='clearfloat'></div>" +
                              "</div>" +
                        "</div>";

                    }
                }


                //string spName3 = "";
                //spName3 = "sp_GetRelatedBlogs";
                //SqlCommand cmd3 = new SqlCommand(spName3, con);
                //cmd3.CommandType = CommandType.StoredProcedure;
                //cmd3.Parameters.AddWithValue("@blogid", myRow[0].ToString());
                //SqlDataReader dr3 = cmd3.ExecuteReader();
                //DataTable dt3 = new DataTable();
                //dt3.Load(dr3);

                //if (dt3.Rows.Count > 0)
                //{
                //    foreach (DataRow myRow3 in dt3.Rows)
                //    {
                //        litRelatedBlog.Text += "<li><a href='ViewBlog.aspx?id=" + myRow3[2].ToString() + "'>" + myRow3[1].ToString() + "</a></li>";

                //    }
                //}
            }

        }

        con.Close();
    }
}