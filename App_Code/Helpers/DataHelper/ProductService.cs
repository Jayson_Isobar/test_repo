﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductService
/// </summary>
public class ProductService
{
	public ProductService()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Get cache or new product list
    /// </summary>
    /// <returns></returns>
    public List<ProductModel> GetProductList()
    {
        const string key = "product_List";
        var products = CacheHelper.Get<List<ProductModel>>(key);
        if (products != null) return products;
        var productList = ShopProductsCls.GetProductListWithCategory();
        CacheHelper.Add(productList, key);
        return productList;
    }


    /// <summary>
    /// Get cache or new product model
    /// </summary>
    /// <returns></returns>
    public ProductModel GetProduct(string productName, string categoryName)
    {
        var porductList = GetProductList();
        var productModel = new ProductModel();
        if (porductList != null)
        {
            productModel =
               porductList.FirstOrDefault(x => x.ProductName.Equals(productName, StringComparison.OrdinalIgnoreCase) && x.CategoryName.Equals(categoryName, StringComparison.OrdinalIgnoreCase));

        }
        return productModel;
    }


    //dm
    public ProductModel GetProduct(string productName)
    {
        var porductList = GetProductList();
        var productModel = new ProductModel();
        if (porductList != null)
        {
            productModel =
               porductList.FirstOrDefault(x => x.ProductName.Equals(productName, StringComparison.OrdinalIgnoreCase));

        }
        return productModel;
    }
}