﻿<%@ Page Title="Products" Language="C#" MasterPageFile="~/CMS/SiteCMS.master" AutoEventWireup="true"
    CodeFile="Products.aspx.cs" Inherits="Products" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:Button ID="btnAdd" runat="server" Text="ADD NEW PRODUCT" 
        onclick="btnAdd_Click" />
         <p></p>
         <asp:Button ID="btnBackView" runat="server" Text="View Product List" 
        onclick="btnBackView_Click" Visible="False" />
        <p></p>
         <asp:Button ID="btnViewDeleted" runat="server" Text="View Deleted Product List" 
        onclick="btnViewDeleted_Click" />
         <asp:Button ID="btnViewProdIngredients" runat="server" Text="View Related Ingredients" 
        onclick="btnViewProdIngredients_Click" Visible="false" />
          <asp:Button ID="btnViewProdConcerns" runat="server" Text="View Related Concerns" 
        onclick="btnViewProdConcerns_Click" Visible="false" />
        <asp:Button ID="btnViewProdAge" runat="server" Text="View Related Ages" 
        onclick="btnViewProdAge_Click" Visible="false" />
        <asp:Button ID="btnViewProdGender" runat="server" Text="View Related Genders" 
        onclick="btnViewProdGender_Click" Visible="false" />
        <asp:Button ID="btnViewProdProduct" runat="server" Text="View Related Products" 
        onclick="btnViewProdProduct_Click" Visible="false" />
         <asp:Button ID="btnViewProdTitleTags" runat="server" Text="View Related Title Tags" 
        onclick="btnViewProdTitleTags_Click" Visible="false" />
         <asp:Button ID="btnViewProdKeywords" runat="server" Text="View Related Keywords" 
        onclick="btnViewProdKeywords_Click" Visible="false" />
         <asp:Button ID="btnViewProdMetaOpen" runat="server" Text="View Related Meta Open Graph Title" 
        onclick="btnViewProdMetaOpen_Click" Visible="false" />
        <p></p>
    <asp:Panel ID="pnlGVProd" runat="server">
   
    <asp:GridView CssClass="table table-striped table-bordered table-hover"
            ID="gvProd" Runat="server" 
            emptydatatext="No records found."
            OnRowCommand="gvProdRowCommand"
            OnRowEditing="gvProd_RowEditing"
            OnPageIndexChanging="gvProd_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" PageSize="10">
            <FooterStyle BackColor="White"></FooterStyle>
            <PagerStyle HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>
               <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>                                                                    
                        <asp:LinkButton Text="Update Details" ID="lnkUpdate" runat="server" CommandName="Edit"
                        CommandArgument='<%# Eval("productid") %>'/>    
                         <asp:LinkButton Text="Delete" ID="lnkDelete" runat="server" CommandName="Delete"
                        CommandArgument='<%# Eval("productid") %>' Visible="False"/>    
	                </ItemTemplate>
               </asp:TemplateField>
             
              <asp:TemplateField HeaderText="Product Category" SortExpression="prodcatName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodCat" runat="server" Text='<%# Bind("prodcatName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Product Name" SortExpression="productName">
                    <ItemTemplate>
                        <asp:Label ID="lblprodName" runat="server" Text='<%# Bind("productName") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Product Description" SortExpression="productDesc">
                    <ItemTemplate>
                        <asp:Label ID="lblprodDesc" runat="server" Text='<%# Bind("productDesc") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Product Image" SortExpression="productImage">
                    <ItemTemplate>
                        <asp:Label ID="lblprodImage" runat="server" Text='<%# Bind("productImage") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="Status" SortExpression="status">
                    <ItemTemplate>
                        <asp:Label ID="lblprodStatus" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                    </ItemTemplate>
               </asp:TemplateField>
            </Columns>
            <SelectedRowStyle ForeColor="White" 
                Font-Bold="True" BackColor="#669999"></SelectedRowStyle>
            <RowStyle ForeColor="#000066"></RowStyle>
        </asp:GridView>

         </asp:Panel>

         <asp:Panel ID="pnlViewProd" runat="server" Visible="False">
          <asp:Label ID="Label4" runat="server" Text="Product Category"></asp:Label>
             <asp:DropDownList ID="ddlProdCat" runat="server" AutoPostBack="True">
             </asp:DropDownList>
             <p></p>
             <asp:Label ID="Label1" runat="server" Text="Product Name"></asp:Label>
             <asp:TextBox ID="txtProdName" runat="server"></asp:TextBox>
             <p></p>
             <asp:Label ID="Label2" runat="server" Text="Product Long Description"></asp:Label>
             
                <cc1:Editor ID="txtProdDesc" runat="server" />
             <p></p>
             <asp:Label ID="Label15" runat="server" Text="Product Short Description"></asp:Label>
             <asp:TextBox ID="txtProdShortDesc" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label5" runat="server" Text="Product Image Filename"></asp:Label>
             <asp:TextBox ID="txtProdImage" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label6" runat="server" Text="Size Variant 1"></asp:Label>
             <asp:TextBox ID="txtProdSize" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label7" runat="server" Text="Size Variant 2"></asp:Label>
             <asp:TextBox ID="txtProdSize2" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label8" runat="server" Text="Price Size Variant 1"></asp:Label>
             <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label9" runat="server" Text="Price Size Variant 2"></asp:Label>
             <asp:TextBox ID="txtPrice2" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label10" runat="server" Text="GTIN Size Variant 1"></asp:Label>
             <asp:TextBox ID="txtGTIN" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label11" runat="server" Text="GTIN Size Variant 2"></asp:Label>
             <asp:TextBox ID="txtGTIN2" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label12" runat="server" Text="Form"></asp:Label>
             <asp:TextBox ID="txtProdForm" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label13" runat="server" Text="Canonical URL"></asp:Label>
             <asp:TextBox ID="txtCanonicalURL" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label14" runat="server" Text="Meta Description"></asp:Label>
             <asp:TextBox ID="txtMetaDesc" runat="server"></asp:TextBox>
             <p></p>
              <asp:Label ID="Label3" runat="server" Text="Active ?"></asp:Label>
             <asp:CheckBox ID="cbStatus" runat="server" Checked="True" />
              <p></p>
               <asp:Label ID="Label16" runat="server" Text="Product Benefits"></asp:Label>
             
                <cc1:Editor ID="txtProdBenefits" runat="server" />
             <p></p>
             <asp:Label ID="Label17" runat="server" Text="Product Ingredients"></asp:Label>
             
                <cc1:Editor ID="txtProdIng" runat="server" />
             <p></p>
             <asp:Label ID="Label18" runat="server" Text="Product Directions"></asp:Label>
             
                <cc1:Editor ID="txtProdDir" runat="server" />
             <p></p>
               <asp:Button ID="btnSave" runat="server" Text="SAVE" onclick="btnSave_Click" Visible = "False" />
             <asp:Button ID="btnUpdate" runat="server" Text="UPDATE" onclick="btnUpdate_Click" Visible = "False" />
             <asp:Button ID="btnCancel" runat="server" Text="BACK" 
                 onclick="btnCancel_Click" />
          </asp:Panel>

          
</asp:Content>
