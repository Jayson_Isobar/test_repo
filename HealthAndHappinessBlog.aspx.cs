﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class HeaderNavigationFlyout_HealthAndHappinessBlog : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            LoadHealthAndHappinessBlog();
            LoadData();
        }
    }
    public void LoadData()
    {
        HealthAndHappinessBlogCls b = new HealthAndHappinessBlogCls();

        b = HealthAndHappinessBlogCls.GetBlogFilter(txtBlogTheme.Text, txtBlogFormat.Text);
        string blog = b.healthandhappinessblog;
        blog = blog.Replace("<img src='", "<img src='" + base.CDNdomain);
        litBlog.Text = blog;
    }
    public void LoadHealthAndHappinessBlog()
    {
        HealthAndHappinessBlogCls hhb = new HealthAndHappinessBlogCls();
        hhb = HealthAndHappinessBlogCls.GetHealthAndHappinessBlogCls();
        string healthandhappinessblog = hhb.healthandhappinessblog;

        litContent.Text = healthandhappinessblog;
    }
   
    protected void cbBlogTheme_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields();
        LoadHealthAndHappinessBlog();
        LoadData();
    }

    public void LoadCBDataFields()
    {
        if (cbBlogTheme.SelectedIndex != -1)
        {
            txtBlogTheme.Text = GetCheckBoxListSelections();
            lblSearchThemes.Visible = true;
            lblSearchThemes.Text = "<h4>Viewing</h4> " + GetCheckBoxListNameSelections();

            litViewing.Visible = true;
            litViewing.Text =   "<div class='color-blck redefine'>" +
                                    "<h4 id='viewing'>Viewing:  " + GetCheckBoxListNameSelections() + "<span class='theme clearAll'><a href='javascript:void(0);'>Clear All</a></span></h4>" +
                                "</div>";
        }
        else
        {
            txtBlogTheme.Text = null;
            lblSearchThemes.Visible = false;

            litViewing.Visible = false;
        }
    }

    private string GetCheckBoxListSelections()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbBlogTheme.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }

    private string GetCheckBoxListNameSelections()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbBlogTheme.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add("<span class='theme'>" + item.Text + "<a href='javascript:void(0);'><img src='/images/btnX.png' /></a></span>");
            }
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join("", cblItems);
    }

    protected void cbBlogFormat_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCBDataFields2();
        LoadHealthAndHappinessBlog();
        LoadData();
    }

    public void LoadCBDataFields2()
    {
        if (cbBlogFormat.SelectedIndex != -1)
        {
            txtBlogFormat.Text = GetCheckBoxListSelections2();
            lblSearchFormats.Visible = true;
            lblSearchFormats.Text = GetCheckBoxListNameSelections2();
        }
        else
        {
            lblSearchFormats.Visible = false;
            txtBlogFormat.Text = null;
        }
    }
    private string GetCheckBoxListNameSelections2()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbBlogFormat.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Text);
            }
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }
    private string GetCheckBoxListSelections2()
    {
        string[] cblItems;
        ArrayList cblSelections = new ArrayList();
        foreach (ListItem item in cbBlogFormat.Items)
        {
            if (item.Selected)
            {
                cblSelections.Add(item.Value);
            }
        }

        cblItems = (string[])cblSelections.ToArray(typeof(string));
        return string.Join(",", cblItems);
    }
}