﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;



/// <summary>
/// Summary description for FAQsCls
/// </summary>
public class FAQsCls
{
 
    SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());

    SqlHelper _helper;

    public string faqs { get; set; }

    public static FAQsCls GetFAQsCls()
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ToString());
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        cmd = new SqlCommand("sp_GetFAQsList", con);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);


        DataTable tbRow = ds.Tables[0];
        FAQsCls rc;
        rc = new FAQsCls();
        if (tbRow.Rows.Count > 0)
        {
            foreach (DataRow myRow in tbRow.Rows)
            {
                //rc.faqs += "<p></p><img src='images/" + myRow[3].ToString() + "' alt='" + myRow[1].ToString() + "' usemap='#" + myRow[4].ToString() + "' class='fltlft'>" +
                //          "<map name='" + myRow[4].ToString() + "'>" +
                //            "<area shape='rect' coords='" + myRow[5].ToString() + "' href='ViewFAQ.aspx?id=" + myRow[2].ToString() + "'>" +
                //          "</map>";
                rc.faqs += "<a name='" + myRow[4].ToString() + "' href='ViewFAQ.aspx?id=" + myRow[2].ToString() + "'>" +
                                "<img src='images/" + myRow[3].ToString() + "' alt='" + myRow[1].ToString() + "' usemap='#" + myRow[4].ToString() + "' class='fltlft'>" +
                            "</a>";
            }
            con.Close();
            return rc;
        }
        con.Close();
        return rc;
    }


}


