﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class EditProfile : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadProfile();
        }
    }
    public void LoadProfile()
    {

        UserCls GU = new UserCls();

        if (Session.Count <= 0)
        {
            Response.Redirect(ConfigurationManager.AppSettings["lnkLogin"]);
        }
        else
        {
            string Email = Session["Email"].ToString();
            GU = UserCls.GetUserProfile(Email);

            txtFN.Text = GU.userfName;
            txtLN.Text = GU.userlName;
            txtEmail.Text = Email;
            ddlMonth.SelectedValue = GU.userBirthMonth;
            ddlyear.SelectedValue = GU.userBirthYear;
            txtPostCode.Text = GU.userPostCode;

            if (GU.checkAgreed == 1)
            {
                cbSignUp.Checked = true;
            }
            else
            {
                cbSignUp.Checked = false;
            }
        }
        
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect(ConfigurationManager.AppSettings["lnkForgotPass"]);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Email = Session["Email"].ToString();
        UserCls uEdit = new UserCls();
        uEdit.userEmail = Email;
        uEdit.userfName = txtFN.Text;
        uEdit.userlName = txtLN.Text;
        uEdit.userBirthMonth = ddlMonth.SelectedValue;
        uEdit.userBirthYear = ddlyear.SelectedValue;
        uEdit.userPostCode = txtPostCode.Text;
        uEdit.optInStatus = 1;
        uEdit.checkAgreed = Int16.Parse(Session["CheckAgreed"].ToString());

        if (cbSignUp.Checked == Convert.ToBoolean(uEdit.checkAgreed)) // no update on opt-in 
        {
            uEdit.optInStatus = 0;
        }

        if(cbSignUp.Checked == true){
            uEdit.checkAgreed = 1;
            uEdit.userStatus = 1;
        }
        else{
            // uEdit.userStatus = 0;
            uEdit.checkAgreed = 0;
            uEdit.userStatus = 0;
        }

        if (uEdit.EditProfile(Email))
        {
            Session["CheckAgreed"] = uEdit.checkAgreed;
        }

        Response.Redirect("ViewProfile.aspx");
    }
}
