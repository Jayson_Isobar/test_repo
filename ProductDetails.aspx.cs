using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class HeaderNavigationFlyout_ProductDetails : BaseForm
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();

    string productID = "";

    String siteUrl = ConfigurationManager.AppSettings["siteUrl"];
    protected Item Item
    {
        get
        {
            return HttpContext.Current.Items["Item"] as Item;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Item != null)
        {
            var id = Item.Id.Split(',');
            productID = id[0];
            
        }
        else
        {
            productID = Request.QueryString["id"].ToString();
        
        }
      
     
        
        if (!IsPostBack)
         {
             LoadProdDesc();
             LoadProdDetail();
             LoadProdRetail();
             InsertToRecentView();
             LoadProdRelatedProds();
         }
        //litTermsReview.Text = "<h4><a href='#' class='txt-bl'>terms and conditions</a> | <a href='#' class='txt-bl'>review guidelines</a></h4>";
    }

    public void InsertToRecentView() {

        string uid = "0";
        if (Session["uid"] != null)
        {
            uid = Session["uid"].ToString();
        }

        ShopProductsCls uAdd = new ShopProductsCls();
        uAdd.userid = uid;
        uAdd.productID = productID;
       
        uAdd.InsertToRecentView();

      
    }

    public void LoadProdDesc()
    {
        ShopProductsCls desc = new ShopProductsCls();
        desc = ShopProductsCls.GetProductDescPrice(productID);
        if (desc != null)
        {
            string d = desc.product;
            lblDescription.Text = d;
            lblDescPrice.Text = desc.productID;
        }

        ShopProductsCls sku = new ShopProductsCls();
        sku = ShopProductsCls.GetProductSKU(productID);
        if (sku != null)
        {
            string gtin = sku.product;
            litProdSKURatingHeader.Text = gtin;
        }

        ShopProductsCls pcat = new ShopProductsCls();
        pcat = ShopProductsCls.GetProductDetail(productID);
        if (pcat != null)
        {
            string pc = pcat.product;
            litProductDetail.Text = pc;
        }

         ShopProductsCls prodImage = new ShopProductsCls();
        prodImage = ShopProductsCls.GetProductDetailImage(productID);
        if (prodImage != null)
        {
            string prodImg = prodImage.product.Replace("<img src='", "<img src='" + base.CDNdomain);
            litProdImage.Text = prodImg;
        }

        ShopProductsCls prodSizesSKU = new ShopProductsCls();
        prodSizesSKU = ShopProductsCls.GetProductSizesSKU(productID);
        if (prodSizesSKU != null)
        {
            string prodSSKU = prodSizesSKU.product;
            litProdSizesSKU.Text = prodSSKU;
        }
        ShopProductsCls pSNS = new ShopProductsCls();
        pSNS = ShopProductsCls.GetProductSNS(productID);
        if (pSNS != null)
        {
            string sns = pSNS.product;
            litSNS.Text = sns;
        }
    }

    public void LoadProdDetail()
    {
        ShopProductsCls benefit = new ShopProductsCls();
        benefit = ShopProductsCls.GetProductBenefit(productID);
        if (benefit != null)
        {
            string ben = benefit.product;
            litProductBenefit.Text = ben;
        }

        ShopProductsCls ingDir = new ShopProductsCls();
        ingDir = ShopProductsCls.GetProductIngredientDirection(productID);
        if (ingDir != null)
        {
            string indr = ingDir.product;
            litIngDir.Text = indr;
        }

        ShopProductsCls prodSizes = new ShopProductsCls();
        prodSizes = ShopProductsCls.GetProductSizes(productID);
        if (prodSizes != null)
        {
            string pSizes = prodSizes.product;
            litProductSizes.Text = pSizes;
        }
    }

    public void LoadProdRetail() 
    {
        ShopProductsCls pRetail = new ShopProductsCls();
        pRetail = ShopProductsCls.GetProductRetail(productID);
        if (pRetail != null)
        {
            string pr = pRetail.product;
            //litRetailer.Text = pr;
        }
    }

    public void LoadProdRelatedProds()
    {
        ShopProductsCls pRelatedProd = new ShopProductsCls();
        pRelatedProd = ShopProductsCls.GetProductRelatedProduct(productID);
        if (pRelatedProd != null)
        {
            string pr = pRelatedProd.product;
			if(pr  != null){
				pr = pr.Replace("<img src='", "<img src='" + base.CDNdomain);
			}
            litProductRelatedProducts.Text = pr;
        }
    }


    protected void btnPreview_Click(object sender, EventArgs e)
    {
        //btnSubmit.Visible = true;
        //btnBack.Visible = true;
        //btnPreview.Visible = false;
        //btnCancel.Visible = false;

        //txtReviewSummary.Enabled = false;
        //lblReviewSummary.Visible = false;
        //txtReview.Enabled = false;
        //lblReview.Visible = false;
        //txtNickName.Enabled = false;
        //lblNickName.Visible = false;
        //txtLocation.Enabled = false;
        //lblLocation.Enabled = false;
        //ddlage.Enabled = false;
        //txtEmail.Enabled = false;
        //lblEmail.Visible = false;
    }
}