
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertFAQProducts]    Script Date: 2/20/2015 4:26:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mina Escurel>
-- Create date: <February 20, 2015>
-- Description:	<Insert New FAQ Products>
-- exec sp_InsertFAQProducts
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertFAQProducts]
    @productid int,
	@faqid int
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO tbl_FAQ_RelatedProducts (productid,faqid)
    VALUES  (
        @productid,
	    @faqid
  )
END
