﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class FAQEdit : System.Web.UI.Page
{
    SqlDataAdapter da;
    SqlConnection con;
    DataSet ds = new DataSet();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
        Session["faqid"] = Request.QueryString["id"].ToString();
    }
    public void LoadData()
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_GetFAQsListByID";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow myRow in dt.Rows)
            {
                txtTitle.Text = myRow[1].ToString();
                txtContent.Content = myRow[7].ToString();
                txtImage.Text = myRow[3].ToString();
                txtLink.Text = myRow[2].ToString();
            }
        }

        con.Close();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection();
        con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
        string spName = "";
        spName = "sp_UpdateFAQ";
        SqlCommand cmd = new SqlCommand(spName, con);
        cmd.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
        cmd.Parameters.AddWithValue("@title", txtTitle.Text);
        cmd.Parameters.AddWithValue("@content", txtContent.Content);
        cmd.Parameters.AddWithValue("@image", txtImage.Text);
        cmd.Parameters.AddWithValue("@link", txtLink.Text);
        cmd.CommandType = CommandType.StoredProcedure;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("FAQs.aspx"); 
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("FAQs.aspx");
    }
 
    protected void btnRelatedProducts_Click(object sender, EventArgs e)
    {
        Response.Redirect("FAQRelatedProducts.aspx?id=" + Session["faqid"]);
    }
   
}
