﻿<%@ Page Title="Edit Profile" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EditProfile.aspx.cs" Inherits="EditProfile" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

 <section class="breadcrumbs color-blck">
<div class="container">
<p>Home</p>
</div>
</section>
<section class="container">
  <div class="col_60 fltlft">
  <asp:Button ID="btnReset" class="btn color-rd fltrt" runat="server" 
          Text="Reset Password" onclick="btnReset_Click"></asp:Button>
    <h2>edit your profile</h2>
    <h4 class="mtop15">Your basic information</h4>
    <div id="create-account-wrap" class="mtop20">
    <div class="col_32 fltlft">
    <label>First Name</label>
  <asp:TextBox ID="txtFN" runat="server"></asp:TextBox>
    </div>
    <div class="col_14 fltrt">
    <label>Last Name</label>
    <asp:TextBox ID="txtLN" runat="server"></asp:TextBox>  
    </div>
    <div class="clearfloat"></div>
    <div class="col_32 fltlft mtop20">
    <label>Email</label>
   <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox> 
    </div>    
    <div class="clearfloat"></div>
    <label class="mtop20">Birthday</label>
     <asp:DropDownList ID="ddlMonth" runat="server" class="fltlft">
        <asp:ListItem Selected="True" Value="0">Month</asp:ListItem>
        <asp:ListItem>January</asp:ListItem>
        <asp:ListItem>February</asp:ListItem>
        <asp:ListItem>March</asp:ListItem>
        <asp:ListItem>April</asp:ListItem>
        <asp:ListItem>May</asp:ListItem>
        <asp:ListItem>June</asp:ListItem>
        <asp:ListItem>July</asp:ListItem>
        <asp:ListItem>August</asp:ListItem>
        <asp:ListItem>September</asp:ListItem>
        <asp:ListItem>October</asp:ListItem>
        <asp:ListItem>November</asp:ListItem>
        <asp:ListItem>December</asp:ListItem>
    </asp:DropDownList>
    <asp:DropDownList ID="ddlyear" runat="server" class="fltlft">
        <asp:ListItem Selected="True" Value="0">Year</asp:ListItem>
        <asp:ListItem>2015</asp:ListItem>
        <asp:ListItem>2014</asp:ListItem>
        <asp:ListItem>2013</asp:ListItem>
        <asp:ListItem>2012</asp:ListItem>
        <asp:ListItem>2011</asp:ListItem>
        <asp:ListItem>2010</asp:ListItem>
        <asp:ListItem>2009</asp:ListItem>
        <asp:ListItem>2008</asp:ListItem>
        <asp:ListItem>2007</asp:ListItem>
        <asp:ListItem>2006</asp:ListItem>
        <asp:ListItem>2005</asp:ListItem>
        <asp:ListItem>2004</asp:ListItem>
        <asp:ListItem>2003</asp:ListItem>
        <asp:ListItem>2002</asp:ListItem>
        <asp:ListItem>2001</asp:ListItem>
        <asp:ListItem>2000</asp:ListItem>
        <asp:ListItem>1999</asp:ListItem>
        <asp:ListItem>1998</asp:ListItem>
        <asp:ListItem>1997</asp:ListItem>
        <asp:ListItem>1996</asp:ListItem>
        <asp:ListItem>1995</asp:ListItem>
        <asp:ListItem>1994</asp:ListItem>
        <asp:ListItem>1993</asp:ListItem>
        <asp:ListItem>1992</asp:ListItem>
        <asp:ListItem>1991</asp:ListItem>
        <asp:ListItem>1990</asp:ListItem>
        <asp:ListItem>1989</asp:ListItem>
        <asp:ListItem>1988</asp:ListItem>
        <asp:ListItem>1987</asp:ListItem>
        <asp:ListItem>1986</asp:ListItem>
        <asp:ListItem>1985</asp:ListItem>
        <asp:ListItem>1984</asp:ListItem>
        <asp:ListItem>1983</asp:ListItem>
        <asp:ListItem>1982</asp:ListItem>
        <asp:ListItem>1981</asp:ListItem>
        <asp:ListItem>1980</asp:ListItem>
        <asp:ListItem>1979</asp:ListItem>
        <asp:ListItem>1978</asp:ListItem>
        <asp:ListItem>1977</asp:ListItem>
        <asp:ListItem>1976</asp:ListItem>
        <asp:ListItem>1975</asp:ListItem>
        <asp:ListItem>1974</asp:ListItem>
        <asp:ListItem>1973</asp:ListItem>
        <asp:ListItem>1972</asp:ListItem>
        <asp:ListItem>1971</asp:ListItem>
        <asp:ListItem>1970</asp:ListItem>
        <asp:ListItem>1969</asp:ListItem>
        <asp:ListItem>1968</asp:ListItem>
        <asp:ListItem>1967</asp:ListItem>
        <asp:ListItem>1966</asp:ListItem>
        <asp:ListItem>1965</asp:ListItem>
        <asp:ListItem>1964</asp:ListItem>
        <asp:ListItem>1963</asp:ListItem>
        <asp:ListItem>1962</asp:ListItem>
        <asp:ListItem>1961</asp:ListItem>
        <asp:ListItem>1960</asp:ListItem>
        <asp:ListItem>1959</asp:ListItem>
        <asp:ListItem>1958</asp:ListItem>
        <asp:ListItem>1957</asp:ListItem>
        <asp:ListItem>1956</asp:ListItem>
        <asp:ListItem>1955</asp:ListItem>
        <asp:ListItem>1954</asp:ListItem>
        <asp:ListItem>1953</asp:ListItem>
        <asp:ListItem>1952</asp:ListItem>
        <asp:ListItem>1951</asp:ListItem>
        <asp:ListItem>1950</asp:ListItem>
        <asp:ListItem>1949</asp:ListItem>
        <asp:ListItem>1948</asp:ListItem>
        <asp:ListItem>1947</asp:ListItem>
        <asp:ListItem>1946</asp:ListItem>
        <asp:ListItem>1945</asp:ListItem>
        <asp:ListItem>1944</asp:ListItem>
        <asp:ListItem>1943</asp:ListItem>
        <asp:ListItem>1942</asp:ListItem>
        <asp:ListItem>1941</asp:ListItem>
        <asp:ListItem>1940</asp:ListItem>
    </asp:DropDownList>
    <div class="clearfloat"></div>
    <label class="mtop20">Post Code</label>
   <asp:TextBox ID="txtPostCode" runat="server"></asp:TextBox>
    <div class="clearfloat"></div>     
    <p class="mtop20"><asp:CheckBox ID="cbSignUp" runat="server"></asp:CheckBox>Yes! Yes sign me up to receive occasional emails from Swisse as well as emails from other trusted P&G Brands which will help me:</p>
    <ul class="mtop15">
    <li>Save money with exclusive coupons</li>
    <li>Get inspired with useful tips & ideas</li>
    <li>Learn about free samples, new products, exciting promotions and other information from P&G and carefully-selected business partners</li>
    </ul>
    </div>
    <asp:Button ID="btnSave" class="btn color-rd" runat="server" Text="Save" 
        onclick="btnSave_Click"></asp:Button>
    </div>
    <div class="col_32 fltrt">
    <img src="<%=this.CDNdomain %>images/3reasons.jpg" width="320" height="371" alt="3 Reasons why joining is a great idea">
    <img src="<%=this.CDNdomain %>images/unique-formula.jpg" alt="Our Unique formulas" width="320" height="371" usemap="#Map" class="mtop20">
    <map name="Map">
      <area shape="rect" coords="31,297,163,337" href="#">
    </map>
    </div>
    <div class="clearfloat"></div>
</section>
</asp:Content>
